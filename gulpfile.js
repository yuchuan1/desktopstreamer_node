/* jshint -W055 */
'use strict';
var fs = require('fs'),
  async = require('async'),
  gulp = require('gulp'),
  eslint = require('gulp-eslint'),
  debug = require('gulp-debug'),
  gutil = require('gulp-util'),
  compass = require('gulp-compass'),
  react = require('gulp-react'),
  moment = require('moment'),
  uglify = require('gulp-uglify'),
  //zip = require('gulp-zip'),
  zip = require('gulp-vinyl-zip').zip,
  unzip = require('gulp-unzip'),
  exec = require('child_process').exec,
  shell = require('shelljs'),
  pjson = require('./package.json'),
  rename = require('gulp-rename'),
  del = require('del'),
  svnRevision = '0',
  dsaVersion = pjson.version,
  //fileServerIp = '172.16.1.188',
  //fileServerUserName = 'eddie.chen',
  //fileServerPWD = 'essshare',
  ftp = require('vinyl-ftp'),
  nwjsVersion = '0.12.3';

var git = require('gulp-git');

function string_src(filename, string) {
  var src = require('stream').Readable({
    objectMode: true
  });
  src._read = function() {
    this.push(new gutil.File({
      cwd: '',
      base: '',
      path: filename,
      contents: new Buffer(string)
    }));
    this.push(null);
  };
  return src;
}

// svn revision function is broken with jenkins 1.49x on mac server
// getting svn revision from nodejs
console.log(pjson.version);
if (!process.env.BUILD_NUMBER) {
  svnRevision = 'dev';
} else if (process.platform === 'win32') {
  svnRevision = process.env.SVN_REVISION;
  console.log('win32 svn revision: ' + svnRevision);
  console.log('process.env: ', process.env);
} else if (process.platform === 'darwin') {
  require('svn-info')('/Users/Shared/Jenkins/Home/jobs/dsa2/workspace', 'HEAD', function(err, info) {
    if (err)
      throw err;
    console.log(info);
    svnRevision = info.revision;
    console.log('svn revision: ' + svnRevision);
  });
}

gulp.task('engineerRelease', function() {
  if (process.platform === 'win32') {
    if (!fs.existsSync('Z:\\EngineeringReleases\\NovoPro\\' + moment().format('YYYYMMDD'))) {
      console.log('Engineering Release Folder Not found, creating...');
      fs.mkdirSync('Z:\\EngineeringReleases\\NovoPro\\' + moment().format('YYYYMMDD'));
    }

    shell.cp('-R', 'build/release.txt', 'Z:\\EngineeringReleases\\NovoPro\\' + moment().format('YYYYMMDD') + '/');
    shell.cp('-R', 'build/' + process.platform + '/*.exe', 'Z:\\EngineeringReleases\\NovoPro\\' + moment().format('YYYYMMDD') + '/');
    /*
    exec('npm config get dsa.release', function (code, dsaRelease) {
      dsaRelease = dsaRelease.trim();
      if (dsaRelease === 'production') {
        gulp.src(['build/' + process.platform + "/*.exe"])
          .pipe(rename('DesktopStreamer_Setup_Win.exe'))
          .pipe(gulp.dest('Z:/EngineeringReleases/NovoPro/Jenkins/'), {
            overwrite: true
          });
      }
    });*/
    gulp.src(['build/usb*/**'])
      .pipe(zip('usb_' + svnRevision + '_' + process.env.BUILD_NUMBER + '.zip'))
      .pipe(gulp.dest('build/usb'), {
        overwrite: true
      })
      .on('finish', function() {
        shell.cp('-R', 'build/usb/*.zip', 'Z:/EngineeringReleases/NovoPro/' + moment().format('YYYYMMDD') + '/');
      })
      .on('error', function(err) {
        console.error(err);
      });
  }
});

gulp.task('compress', ['adjustForProduction'], function(cb) {
  exec('npm config get dsa.release', function(code, dsaRelease) {
    dsaRelease = dsaRelease.trim();
    if (dsaRelease === 'production' || dsaRelease === 'engineering') {
      console.log('compress svn revision: ' + svnRevision);
      var appPath = 'build/' + process.platform + '/hdd/app/',
        usbPath = 'build/usb/app/';
      gulp.src([
          //  '!' + appPath + '/js/i18next.min.js',
          //  appPath + '/js/*.js',
          '!' + appPath + '/node_modules/desktop_streamer_node/lib/*.min.js',
          appPath + '/node_modules/desktop_streamer_node/lib/*.js',
          appPath + '/node_modules/desktop_streamer_node/run/*.js',
          appPath + '/node_modules/desktop_streamer_node/test/*.js',
          appPath + '/node_modules/desktop_streamer_node/**/*.js',
          appPath + '/node_modules/desktop_streamer_node/youtube-validator/**/*.js',
          appPath + '/js/controllers.js',
          appPath + '/js/detect-element-resize.js',
          appPath + '/js/device_widget.js',
          appPath + '/js/device-manager.js',
          appPath + '/js/group-file.js',
          appPath + '/js/image-util.js',
          appPath + '/js/jquery.powertimer.js',
          appPath + '/js/main.js',
          appPath + '/js/novoconnect.js',
          appPath + '/js/qr.js',
          appPath + '/js/video.js',
          appPath + '/js/video_widget.js',
          appPath + '/js/voting.js',
          appPath + '/js/voting_widget.js',
          appPath + '/js/voting-editor.js',
          appPath + '/js/voting-file.js',

          //  '!' + usbPath + '/js/i18next.min.js',
          //  usbPath + '/js/*.js',
          '!' + usbPath + '/node_modules/desktop_streamer_node/lib/*.min.js',
          usbPath + '/node_modules/desktop_streamer_node/lib/*.js',
          usbPath + '/node_modules/desktop_streamer_node/run/*.js',
          usbPath + '/node_modules/desktop_streamer_node/test/*.js',
          usbPath + '/node_modules/desktop_streamer_node/**/*.js',
          usbPath + '/node_modules/desktop_streamer_node/youtube-validator/**/*.js',
          usbPath + '/js/controllers.js',
          usbPath + '/js/detect-element-resize.js',
          usbPath + '/js/device_widget.js',
          usbPath + '/js/device-manager.js',
          usbPath + '/js/group-file.js',
          usbPath + '/js/image-util.js',
          usbPath + '/js/jquery.powertimer.js',
          usbPath + '/js/main.js',
          usbPath + '/js/novoconnect.js',
          usbPath + '/js/qr.js',
          usbPath + '/js/video.js',
          usbPath + '/js/video_widget.js',
          usbPath + '/js/voting.js',
          usbPath + '/js/voting_widget.js',
          usbPath + '/js/voting-file.js',
        ], {
          base: './'
        })
        .pipe(uglify())
        .pipe(gulp.dest('./'))
        .on('finish', function() {
          cb();
        })
        .on('error', function(err, src) {
          console.error('gulp compress error:');
          console.error(err);
          if (src)
            console.error(JSON.stringify(src));
        });
    }
  });
});

gulp.task('zipUSB', function() {
  return gulp.src(['build/usb/**/*', 'build/usb/.*/**'])
    //  .pipe(debug())
  		.pipe(zip('usb_' + dsaVersion + '_' + svnRevision + '_' + process.env.BUILD_NUMBER + '.zip'))
      .pipe(gulp.dest('build/usb'), {
        overwrite: true
      })
      .on('finish', function() {
        console.error('USB Version zipped');
      })
      .on('error', function(err) {
        console.error('zipUSB');
        console.error(err);
      });


  //gulp.src(['build/usb/**', 'build/usb/.*/**'])
  /*
    .pipe(zip('usb_' + dsaVersion + '_' + svnRevision + '_' + process.env.BUILD_NUMBER + '.zip'))
    .pipe(gulp.dest('build/usb'), {
      overwrite: true
    })
    .on('finish', function() {
      //shell.cp('-R', 'build/usb/*.zip', 'Z:/EngineeringReleases/NovoPro/' + moment().format('YYYYMMDD') + '/');
      console.error('USB Version zipped');
    })
    .on('error', function(err) {
      console.error(err);
    });
    */
});

gulp.task('license', ['clean', 'jsLint'], function(cb) {
//  var appPath = 'build/' + process.platform + '/hdd/app/',
  var  usbPath = 'build/usb/';
  if (process.platform === 'win32') {
    async.series([
      function(done) {
        console.log('Copying license files for USB');
        gulp.src(['!license/License.ini', '!license/init', 'resources/autorun.inf']).pipe(gulp.dest(usbPath))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function() {
        console.log('Copying license files for HDD');
        exec('.\\license\\license.exe HDD', function(err/*, stdout, stderr*/) {
          if (err)
            console.log(err);
          return gulp.src('./license.lic', {
              base: './'
            }).pipe(gulp.dest(process.env.ProgramData + '/Novo/'))
            .on('finish', function() {
              cb();
            })
            .on('error', function(err) {
              console.error(err);
            });
        });
      }
    ]);
  } else if (process.platform === 'darwin') {
    /*
    appPath = 'build/' + process.platform + '/hdd/nwjs.app/Contents/Resources/app.nw/';
    usbPath = 'build/usb/';
    async.series([
      function (done) {
        console.log('Copying license files for USB');
        gulp.src(['license/License.ini', 'license/init']).pipe(gulp.dest(usbPath))
          .on('finish', function () {
            cb();
          })
          .on('error', function (err) {
            console.error(err);
          });
      }
      ,
      function (done) {
        console.log('Copying license files for HDD');
        shell.exec('chmod a+x license/license.app');
        exec('license/license.app HDD', function (err, stdout, stderr) {
          if (err)
            console.log(err);
          return gulp.src('./license.lic', {
              base: './'
            }).pipe(gulp.dest(appPath + 'nativeApp/'))
            .on('finish', function () {
              cb();
            })
            .on('error', function (err) {
              console.error(err);
            });
        });
         cb();
      }
    ]);*/
    cb();
  }
});

gulp.task('deploy', ['license'], function(cb) {
  var appPath = 'build/' + process.platform + '/hdd/app',
    usbPath = 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw';

  if (process.platform === 'win32') {
    async.series([
      function(done) {
        gulp.src(['release.txt']).pipe(gulp.dest('build'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        gulp.src(['app/**/*']).pipe(gulp.dest(appPath)).pipe(gulp.dest(usbPath))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        gulp.src(['resources/dsa.ini']).pipe(gulp.dest('build/' + process.platform + '/hdd'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      /*
            function (done) {
              gulp.src(['installer/images/SplashForm.bmp']).pipe(gulp.dest('build/' + process.platform + '/hdd/app')).pipe(gulp.dest('build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw'))
                .on('finish', function () {
                  done();
                })
                .on('error', function (err) {
                  console.error(err);
                });
            },*/
      function(done) {
        gulp.src(['resources/dsaUsb.ini'])
          .pipe(rename('dsa.ini'))
          .pipe(gulp.dest('build/usb'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        gulp.src(['resources/Novo Desktop Streamer.exe']).pipe(gulp.dest('build/' + process.platform + '/hdd'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        gulp.src(['resources/Novo Desktop Streamer.exe'])
          .pipe(rename('Launch Novo.exe')).pipe(gulp.dest('build/usb'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        shell.cp('-R', 'resources/nativeApp/darwin/DsaNativeService.app', './build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/nativeApp/');
        //shell.mv('build/usb/nwjs.app', 'build/usb/.app/DesktopStreamer.app');
        done();
      },
      function(done) {
        gulp.src(['resources/nwjs/' + nwjsVersion + '/osx32/nwjs.app/**/*'])
          .pipe(gulp.dest('build/usb/.app/DesktopStreamer.app'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        gulp.src(['resources/nwjs/' + nwjsVersion + '/win32/**/*']).pipe(gulp.dest('build/' + process.platform + '/hdd/nwjs')).pipe(gulp.dest('build/usb/.app/DesktopStreamer.app/Contents/Frameworks/nwjs'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        gulp.src(['resources/nativeApp/win32/**/*']).pipe(gulp.dest('build/' + process.platform + '/hdd/app/nativeApp')).pipe(gulp.dest('build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/nativeApp'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        if (typeof process.env.computername === 'string') {
          if (process.env.computername === 'TWTY2_ESS' || process.env.computername === 'TWTPEESS32') {
            var conn = new ftp({
              'host': '172.16.1.188',
              'user': 'eddie.chen',
              'pass': 'essshare'
            });
            conn.src(['/ESS/EngineeringReleases/NovoPro/0_release_temp/NovoDesktopStreamer.app.zip'])
              .pipe(unzip())
              .pipe(gulp.dest('build/temp/'))
              .on('finish', function() {
                done();
              });
          } else {
            done();
          }
        } else {
          done();
        }

      },
      function(done) {
        gulp.src(['build/temp/Novo Desktop Streamer.app/**'])
          .pipe(gulp.dest('build/usb/Launch Novo.app/'))
          .on('finish', function() {
            done();
          });
      },
      function() {
        del(['build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/nativeApp/license.exe', 'build/temp/**']).then(cb());
      }
    ]);
  } else if (process.platform === 'darwin') {
    appPath = 'build/' + process.platform + '/hdd/nwjs.app/Contents/Resources/app.nw';
    async.series([
      function(done) {
        gulp.src(['release.txt']).pipe(gulp.dest('build/' + process.platform + '/hdd'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        gulp.src(['resources/nwjs/' + nwjsVersion + '/osx32/**/*']).pipe(gulp.dest('build/' + process.platform + '/hdd'))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      function(done) {
        gulp.src(['app/**/*']).pipe(gulp.dest(appPath))
          .on('finish', function() {
            done();
          })
          .on('error', function(err) {
            console.error(err);
          });
      },
      /*
            function (done) {
              gulp.src('license/license.app').pipe(gulp.dest(appPath + '/nativeApp')).pipe(gulp.dest(usbPath + '/nativeApp'))
                .on('finish', function () {
                  shell.exec('chmod a+x ' + 'build/' + process.platform + '/hdd/nwjs.app/Contents/Resources/app.nw/nativeApp/license.app');
                  shell.exec('chmod a+x ' + 'build/' + process.platform + '/usb/nwjs.app/Contents/Resources/app.nw/nativeApp/license.app');
                  done();
                })
                .on('error', function (err) {
                  console.error(err);
                });
            },*/
      function() {
        if (process.platform === 'win32') {
          exec('chmod -R a+w ' + 'build/usb/nwjs.app/Contents/Resources/app.nw/', function(err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
          });
        }
        shell.cp('-R', 'resources/nativeApp/darwin/DsaNativeService.app', './build/darwin/hdd/nwjs.app/Contents/Resources/app.nw/nativeApp/');
        // shell.cp('-R', 'resources/nativeApp/darwin/DsaNativeService.app', './build/darwin/usb/nwjs.app/Contents/Resources/app.nw/nativeApp/');

        shell.mv('build/' + process.platform + '/hdd/nwjs.app', 'build/' + process.platform + '/hdd/DesktopStreamer.app');
        // shell.mv('build/' + process.platform + '/usb/nwjs.app', 'build/' + process.platform + '/usb/Launch Novo.app');
        cb();
      }
    ]);
  }
});

gulp.task('clean', ['hash'], function() {
  shell.rm('-rf', 'build');
  shell.rm('-f', [
    'license.lic',
    'app/*.log',
    'app/*.jpg',
    'app/*.png',
    'app/nativeApp/init',
    'app/nativeApp/license.lic',
    'app/nativeApp/License.ini'
  ]);
});

gulp.task('adjustForProduction', ['deploy'], function(cb) {
  if (process.platform === 'win32') {
    if (fs.existsSync('build/' + process.platform + '/hdd/nwjs/nw.exe')) {
      shell.mv('build/' + process.platform + '/hdd/nwjs/nw.exe', 'build/' + process.platform + '/hdd/nwjs/DesktopStreamer.exe');
    }
    if (fs.existsSync('build/usb/.app/DesktopStreamer.app/Contents/Frameworks/nwjs/nw.exe')) {
      shell.mv('build/usb/.app/DesktopStreamer.app/Contents/Frameworks/nwjs/nw.exe', 'build/usb/.app/DesktopStreamer.app/Contents/Frameworks/nwjs/DesktopStreamer.exe');
    }
    /*
        if (process.platform === 'win32') {
          console.log('remove build/' + process.platform + '/usb/app/nativeApp/license.exe');
          shell.rm('-rf', 'build/' + process.platform + '/usb/app/nativeApp/license.exe');
        } else if (process.platform === 'darwin') {
          console.log('remove build/' + process.platform + '/usb/nwjs.app/Contents/Resources/app.nw/nativeApp/license.app');
          shell.rm('-rf', 'build/' + process.platform + '/usb/nwjs.app/Contents/Resources/app.nw/nativeApp/license.app');
        }*/

    exec('npm config get dsa.release', function(code, dsaRelease) {
      dsaRelease = dsaRelease.trim();
      console.log('dsaRelease:' + dsaRelease);
      shell.sed('-i', 'exports.versionDevice = \'HDD\';', 'exports.versionDevice = "USB";', 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/node_modules/desktop_streamer_node/lib/settings.js');
      if (dsaRelease === 'production' || dsaRelease === 'engineering') {
        console.log('turn off toolbar');
        if (fs.existsSync('build/win32/hdd/app/package.json')) {
          shell.sed('-i', '"toolbar": true,', '"toolbar": false,', 'build/win32/hdd/app/package.json');
          shell.sed('-i', '"frame": true,', '"frame": false,', 'build/win32/hdd/app/package.json');
          shell.sed('-i', '"resizable": true,', '"resizable": false,', 'build/win32/hdd/app/package.json');
        }
        if (fs.existsSync('build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/package.json')) {
          shell.sed('-i', '"toolbar": true,', '"toolbar": false,', 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
          shell.sed('-i', '"frame": true,', '"frame": false,', 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
          shell.sed('-i', '"resizable": true,', '"resizable": false,', 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
        }

      } else {
        if (dsaRelease === 'development') {
          if (fs.existsSync('build/win32/hdd/app/package.json')) {
            shell.sed('-i', '"toolbar": false,', '"toolbar": true,', 'build/win32/hdd/app/package.json');
            shell.sed('-i', '"frame": false,', '"frame": true,', 'build/win32/hdd/app/package.json');
            shell.sed('-i', '"resizable": false,', '"resizable": true,', 'build/win32/hdd/app/package.json');
          }
          if (fs.existsSync('build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/package.json')) {
            shell.sed('-i', '"toolbar": false,', '"toolbar": true,', 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
            shell.sed('-i', '"frame": false,', '"frame": true,', 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
            shell.sed('-i', '"resizable": false,', '"resizable": true,', 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
          }
          shell.sed('-i', 'exports.logLevel = logLevel;', 'exports.logLevel = "debug";', 'build/win32/hdd/app/node_modules/desktop_streamer_node/lib/settings.js');
          shell.sed('-i', 'exports.logLevel = logLevel;', 'exports.logLevel = "debug";', 'build/win32/hdd/app/node_modules/dsa-native-service/settings.js');
          shell.sed('-i', 'exports.logLevel = logLevel;', 'exports.logLevel = "debug";', 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/node_modules/desktop_streamer_node/lib/settings.js');
          shell.sed('-i', 'exports.logLevel = logLevel;', 'exports.logLevel = "debug";', 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/node_modules/dsa-native-service/settings.js');

        }
      }
    });
    if (fs.existsSync('build/win32/hdd/app/views/main.html')) {
      shell.sed('-i', /version_number/g, pjson.version + '.' + svnRevision, 'build/win32/hdd/app/views/main.html');

      if (svnRevision !== 'dev')
        shell.sed('-i', /version_number/g, pjson.version + '.' + svnRevision, 'installer/dsa.nsi');
    }
    if (fs.existsSync('build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/views/main.html')) {
      shell.sed('-i', /version_number/g, pjson.version + '.' + svnRevision, 'build/usb/.app/DesktopStreamer.app/Contents/Resources/app.nw/views/main.html');

      if (svnRevision !== 'dev')
        shell.sed('-i', /version_number/g, pjson.version + '.' + svnRevision, 'installer/dsa.nsi');
    }
    cb();
  } else if (process.platform === 'darwin') {
    console.log('adjustForProduction svn revision: ' + svnRevision);
    shell.exec('chmod a+x ' + 'build/darwin/hdd/DesktopStreamer.app/Contents/MacOS/nwjs');
    shell.exec('chmod a+w ' + 'build/darwin/hdd/DesktopStreamer.app/');
    //shell.exec('chmod a+x ' + 'build/darwin/usb/Launch Novo.app/Contents/MacOS/nwjs');
    shell.rm('-rf', 'build/' + process.platform + '/hdd/dsa.ini');
    exec('npm config get dsa.release', function(code, dsaRelease) {
      dsaRelease = dsaRelease.trim();
      console.log('dsaRelease:' + dsaRelease);
      if (dsaRelease === 'production' || dsaRelease === 'engineering') {
        if (fs.existsSync('build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/package.json')) {
          shell.sed('-i', '"toolbar": true,', '"toolbar": false,', 'build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
          shell.sed('-i', '"frame": true,', '"frame": false,', 'build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
          shell.sed('-i', '"resizable": true,', '"resizable": false,', 'build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
        }
      } else if (dsaRelease === 'development') {
        if (fs.existsSync('build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/package.json')) {
          shell.sed('-i', '"toolbar": false,', '"toolbar": true,', 'build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
          shell.sed('-i', '"frame": false,', '"frame": true,', 'build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
          shell.sed('-i', '"resizable": false,', '"resizable": true,', 'build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/package.json');
        }
      }
    });

    if (fs.existsSync('build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/views/main.html')) {
      shell.sed('-i', /version_number/g, pjson.version + '.' + svnRevision, 'build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/views/main.html');
    }
    cb();
  }
});

gulp.task('buildWin32Installer', ['compress'], function() {
  if (process.platform === 'win32') {
    shell.exec('makensis ./installer/dsa.nsi');
  }
});

gulp.task('buildMacInstaller', function() {
  require('svn-info')('/Users/Shared/Jenkins/Home/jobs/dsa2/workspace', 'HEAD', function(err, info) {
    if (err)
      throw err;
    console.log(info);
    svnRevision = info.revision;
    console.log('svn revision: ' + svnRevision);
    if (process.platform === 'darwin') {
      shell.exec('./Mac_Installer/auto_build.sh "' + dsaVersion + '" "' + svnRevision + '" "server_build" "' + process.env.BUILD_NUMBER + '"');
    }
  });
});


gulp.task('codeSignWin32', function() {
  if (process.platform === 'win32') {
    if (svnRevision && process.env.BUILD_NUMBER) {
      //'build/' + process.platform + "/silent_setup.exe",
      fs.renameSync('build/' + process.platform + '/silent_setup.exe', 'build/' + process.platform + '/dsa2_silent_setup_' + dsaVersion + '_' + process.platform + '_' + svnRevision + '_' + process.env.BUILD_NUMBER + '.exe');
      fs.renameSync('build/' + process.platform + '/setup.exe', 'build/' + process.platform + '/dsa2_setup_' + dsaVersion + '_' + process.platform + '_' + svnRevision + '_' + process.env.BUILD_NUMBER + '.exe');
      //fs.renameSync('build/usb', "build/usb_" + svnRevision + "_" + process.env.BUILD_NUMBER);
    }
    exec('SignInstaller.bat', function(err, stdout, stderr) {
      console.log(stderr);
      console.log(stdout);
    });
  }
});

gulp.task('usbUpdate', function() {

  async.series([
    function(done) {
      if (process.platform === 'win32') {
        gulp.src(['build/usb/.app/**'])
          .pipe(gulp.dest('build/usb/~app'))
          .on('finish', function() {
            del.sync(['build/usb/.app']);
            done();
          });
      } else {
        done();
      }
    },
    function(done) {
      if (process.platform === 'win32') {
        if (svnRevision === 'dev') {
          gulp.src(['build/usb/*', 'resources/usbUpgrader/Upgrade_InstallFree.app.zip', 'resources/usbUpgrader/Upgrade_InstallFreeWin.zip', '!build/usb/init', '!build/usb/License.ini', '!build/usb/autorun.inf'], {
              dot: true
            })
            .pipe(zip('usb_dev_upgrade.zip'))
            .pipe(gulp.dest('build/usb_upgrade'), {
              overwrite: true
            })
            .on('finish', function() {
              done();
            })
            .on('error', function(err) {
              console.error(err);
            });
        } else {
          gulp.src(['build/usb*/**', 'resources/usbUpgrader/Upgrade_InstallFree.app.zip', 'resources/usbUpgrader/Upgrade_InstallFreeWin.zip', '!build/usb/init', '!build/usb/License.ini', '!build/usb/autorun.inf'], {
              dot: true
            })
            .pipe(zip('usb_' + svnRevision + '_' + process.env.BUILD_NUMBER + '_upgrade.zip'))
            .pipe(gulp.dest('build/usb_upgrade'), {
              overwrite: true
            })
            .on('finish', function() {
              done();
            })
            .on('error', function(err) {
              console.error(err);
            });
        }
      }
    },
    function() {
      gulp.src(['build/usb/~app/**'])
        .pipe(gulp.dest('build/usb/.app'))
        .on('finish', function() {
          del.sync(['build/usb/~app']);
        });
    }

  ]);
});

gulp.task('default', ['compress']); // build

/* below is compass scss*/
gulp.task('compass', function(cb) {
  gulp.src('./src/*.scss')
    .pipe(compass({
      config_file: './config.rb',
      css: './app/css',
      sass: './src'
    }))
    .pipe(gulp.dest('./temp'))
    .on('finish', function() {
      cb();
    })
    .on('error', function(err) {
      console.error(err);
    });
});

gulp.task('jsx', function() {
  return gulp.src('./src/voting-editor.jsx')
    .pipe(react())
    .pipe(gulp.dest('./app/js'));
});

gulp.task('watch', function() {
  gulp.watch('./src/*.scss', ['compass']);
  gulp.watch('./app/imgs/**/*', ['compass']);
  gulp.watch('./src/*.jsx', ['jsx']);
});

gulp.task('css', ['compass', 'jsx', 'watch']); // when writing css, jsx

gulp.task('syncSvn', function() {
  del(['C:/work/dsa_svn/*']).then(function(paths) {
    console.log('Deleted files/folders:\n', paths.join('\n'));
  });
  //gulp.src(['!node_modules', '!node_modules/**', '!/**/node_modules', '!/**/node_modules/**/*', '!build', '!license.lic', '!.git', './**/*'])
  //.pipe(gulp.dest('c:/work/testcopy'));
});

gulp.task('hash', function() {
  if (typeof process.env.computername === 'string') {
    if (process.env.computername !== 'TWTY2_ESS' && process.env.computername !== 'TWTPEESS32') {
      git.revParse({
        args: '--long HEAD'
      }, function(err, hash) {
        //if (err) ...
        return string_src('githash.txt', hash.replace('--long', ''))
          .pipe(gulp.dest('app/'));
      });
    }
  }
});

gulp.task('jsLint', function() {
  // ESLint ignores files with "node_modules" paths.
  // So, it's best to have gulp ignore the directory as well.
  // Also, Be sure to return the stream from the task;
  // Otherwise, the task may end before the stream has finished.
  return gulp.src(['desktop_streamer_node/lib/settings.js',
      '!node_modules/**'
    ])
    // eslint() attaches the lint output to the "eslint" property
    // of the file object so it can be used by other modules.
    .pipe(eslint())
    // eslint.format() outputs the lint results to the console.
    // Alternatively use eslint.formatEach() (see Docs).
    .pipe(eslint.format())
    .pipe(debug());
  // To have the process exit with an error code (1) on
  // lint error, return the stream and pipe to failAfterError last.
  //.pipe(eslint.failAfterError());
});
