'use strict';
var chai = require('chai'),
  //assert = chai.assert,
  expect = chai.expect,
  dsaUtil = require('../lib/dsaUtil.js');

describe('dsaUtil', function () {
  describe('utf8ToByteArray()', function () {
    it('should return a byteArray representing the UTF8 string', function () {
      var testString = 'this is a test!';
      var expected = [116, 104, 105, 115, 32, 105, 115, 32, 97, 32, 116, 101, 115, 116, 33];
      var actual = dsaUtil.utf8ToByteArray(testString);
      expect(actual).to.be.an('array');
      expect(actual).to.eql(expected);
    });
  });

  describe('utf8ToByteArray()', function () {
    it('should return a byteArray representing the UTF8 string in Chinese', function () {
      var testString = '中文測試!';
      var expected = [228, 184, 173, 230, 150, 135, 230, 184, 172, 232, 169, 166, 33];
      var actual = dsaUtil.utf8ToByteArray(testString);
      expect(actual).to.be.an('array');
      expect(actual).to.eql(expected);
    });
  });

  describe('byteArrayToUtf8()', function () {
    it('should return an UTF8 string', function () {
      var testArray = [116, 104, 105, 115, 32, 105, 115, 32, 97, 32, 116, 101, 115, 116, 33];
      var expected = 'this is a test!';
      var actual = dsaUtil.byteArrayToUtf8(testArray);
      expect(actual).to.be.a('string');
      expect(actual).to.eql(expected);
    });
  });

  describe('byteArrayToUtf8()', function () {
    it('should return an UTF8 string in Chinese', function () {
      var testArray = [228, 184, 173, 230, 150, 135, 230, 184, 172, 232, 169, 166, 33];
      var expected = '中文測試!';
      var actual = dsaUtil.byteArrayToUtf8(testArray);
      expect(actual).to.be.a('string');
      expect(actual).to.eql(expected);
    });
  });

  describe('getYoutubeTitleById()', function () {
    it('should return youtube title: \'BBC: The Secrets Of Quantum Physics | Science Documentary\'', function () {
      //var expected = '';
      dsaUtil.getYoutubeTitleById('2IScDRZa8aY', function (actual) {
        expect(actual).to.eql('BBC: The Secrets Of Quantum Physics | Science Documentary');
      });
    });
  });
});
