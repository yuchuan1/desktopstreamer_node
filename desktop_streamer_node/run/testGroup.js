var rva = require('../lib/index.js');
var rvaClient = new rva.RVAClient();
rvaClient.connect('192.168.1.10', 'Eddie', null);

rvaClient.on('connected', function(settings){
  console.log('connected:', settings);
});

rvaClient.on('invalidRVAVersion', function(){
  console.log('invalidRVAVersion event');
});

rvaClient.on('hostRequested', function(value){
  console.log('hostRequested:', value);
});

rvaClient.on('moderatorRequest', function(value){
  console.log('moderatorRequest:', value);
});

rvaClient.on('settingsUpdated', function(settings){
  console.log('settingsUpdated:', settings);
});

rvaClient.on('viewModeChanged', function(mode){
  console.log('viewModeChanged:', mode);
});

rvaClient.on('receivePreviewImage', function (previewImage) {
  console.log('receivePreviewImage event:', previewImage);
});

rvaClient.on('moderatorRequestApproved', function () {
  console.log('moderatorRequestApproved event');
  console.log('request approved');
});

rvaClient.on('moderatorRequestDenied', function () {
  console.log('moderatorRequestApproved event');
  console.log('request approved');
});

rvaClient.on('userListUpdated', function(userList){
  console.log('userListUpdated:', userList);
});

rvaClient.on('uploadGroupFileSuccess', function(group){
  console.log('UPLOAD_GROUP_FILE_SUCCESS');
  console.log(group);
});

rvaClient.on('removeGroupSuccess', function(){
  console.log('REMOVE_GROUP_SUCCESS');
});

rvaClient.on('viewerState', function(value){
  console.log('viewerState:', value);
  switch (value) {
    case 'pause':
      console.log('viewerState:', 'pause');
      break;
    case 'resume':
      console.log('viewerState:', 'resume');
      break;
    default:
      console.log('Error viewerState:', value);
  }
});

rvaClient.on('tabletsLocked', function(value){
  console.log('tabletsLocked');
  console.log(value);
});

rvaClient.on('disconnected', function(data){
  console.log('disconnected:', data);

});

rvaClient.on('error', function(err){
  console.log('error...');
  console.log(err);
  console.log(err.message);
  console.log(err.stack);
  switch (err.message) {
  case "PIN_INVALID":
    //connection_ip_wrong("PIN code invalid.");
    break;
  case "connect ETIMEDOUT":
   // connection_ip_wrong("Failed to connect");
    break;
  case "EPIPE":
    //console.log(err.message);
   // connection_ip_wrong(err.message);
    //connection_ip_wrong("Connection failed...");
    break;
  default:
    console.log(err);
   // connection_ip_wrong(err.message);
  }
});
