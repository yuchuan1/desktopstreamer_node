var rva = require('../lib/index.js');
var rvaClient = new rva.RVAClient();
rvaClient.on('connected', function (settings) {
  console.log('connected event');
  console.log(settings);
});

rvaClient.on('disconnected', function (settings) {
  console.log('disconnected event');
  console.log(settings);
});

rvaClient.on('logout', function () {
  console.log('logout event');

});

rvaClient.on('settingsUpdated', function (settings) {
  console.log('settingsUpdated event');
  console.log(settings);
});

rvaClient.on('userListUpdated', function (userList) {
    console.log('userListUpdated event');
    console.log(userList);
});

rvaClient.on('hostRequested', function (value) {
  console.log('hostRequested event');
  console.log(value);
});

rvaClient.on('moderatorRequest', function (value) {
  console.log('hostRequested event');
  console.log(value);
});

rvaClient.on('viewModeChanged', function (mode) {
  console.log('viewModeChanged event');
  console.log(mode);
});

rvaClient.on('receivePreviewImage', function (previewImage) {
  // a preview image would be saved to imgs/preview.png
  console.log('receivePreviewImage event');
  console.log(previewImage);
});

rvaClient.on('moderatorRequestTimeout', function () {
  console.log('moderatorRequestTimeout event');
  console.log('request timeout');
});

rvaClient.on('moderatorRequestApproved', function () {
  console.log('moderatorRequestApproved event');
  console.log('request approved');
});

rvaClient.on('error', function (err) {
  console.log('error event');
  console.log(err);
});

rvaClient.connect('192.168.1.10', 'myClient', null);
/*
var discovery = rva.RVADiscovery;
discovery.on('RvaDiscovery', function(value){
  console.log(value);
});
discovery.on('error', function(err){
  console.log('Error detected:' + err);
});

discovery.findAllRvas();

// to get last session's IP and user name
function lastSessionHandler(docs) {
  console.log(docs);
}

function errorHandler(err){
  console.log(err);
}

discovery.lastSession(errorHandler, lastSessionHandler);
// disconnect in 10 secs
//setTimeout(rvaClient.disconnect, 10000);
*/
