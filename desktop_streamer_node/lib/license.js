'use strict';
var settings = require('./settings.js'),
  fs = require('fs'),
  https = require('https'),
  ini = require('ini'),
  querystring = require('querystring'),
  licenseServer = settings.novoServer,
  logger = settings.logger,
  License = function(nativeService, nativePath) {
    this.nativeService = nativeService;
    this.nativePath = nativePath;
    logger.debug('init license, path: /');
  };

License.prototype.checkLicense = function checkLicense(cb) {
  var self = this;
  logger.debug('checkLicense function: ' + JSON.stringify(settings.currentDevice));
  if (settings.currentDevice.type === settings.deviceType.USB) {
    logger.debug('device is USB');
    // check if init file exist and correct
    self.nativeService.checkInitFile(function(result) {
      logger.debug('checkInitFile result: ' + result);
      /*
      if (process.platform === 'darwin') {
        self.nativePath = process.cwd().replace('/novo_desktop_streamer.app/Contents/Resources/app.nw', '/novo_desktop_streamer.app/Contents/Resources/app.nw/nativeApp/');
      }
      else if (process.platform === 'win32') {
        self.nativePath = process.cwd() + '/nativeApp/';
      }*/

      logger.debug('checkInitFile path: /');
      if (result === true) {
        self.checkIniFile(function(iniResult) {
          logger.debug('checkIniFile result: ' + iniResult);
          if (iniResult) {
            //read manufacturer from ini file
            var config, manufacturer, req, postData, options;
            if (process.platform === 'darwin') {
              logger.debug(process.cwd());
              config = ini.parse(fs.readFileSync('../../../../../License.ini', 'utf-8'));
            }
            if (process.platform === 'win32') {
              config = ini.parse(fs.readFileSync('/License.ini', 'utf-8'));
            }

            manufacturer = config.License.Manufacturer;
            logger.debug('manufacturer: ' + manufacturer + ' vendorId: ' + settings.currentDevice.vendorId + ' productId: ' + settings.currentDevice.productId + ' serial number: ' + settings.currentDevice.serialNumber);
            postData = querystring.stringify({
              productId: settings.currentDevice.productId,
              vendorId: settings.currentDevice.vendorId,
              serialNumber: settings.currentDevice.serialNumber,
              manufacturer: manufacturer
            });
            options = {
              hostname: licenseServer,
              port: 443,
              path: '/api/software/dsa/certificate',
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': postData.length
              }
            };

            req = https.request(options, function(res) {
              logger.debug('uploading device data result: ' + typeof(res.statusCode) + ' ' + res.statusCode);
              if (res.statusCode === 200 || res.statusCode === 408) {
                logger.debug('delete init and ini file');
                // delete init and ini file
                var list_file_to_delete;
                if (process.platform === 'darwin') {
                  list_file_to_delete = ['../../../../../init', '../../../../../License.ini'];
                }
                if (process.platform === 'win32') {
                  list_file_to_delete = ['/init', '/License.ini'];
                }
                list_file_to_delete.forEach(function(filename) {
                  fs.unlinkSync(filename);
                });
                // generate license file
                self.nativeService.generateLicenseFile(function(_result) {
                  logger.debug('generateLicenseFile:' + _result);
                  logger.debug('LICENSE_INSTALL_SUCCESS');
                  cb('LICENSE_INSTALL_SUCCESS');
                  // show license message (LICENSE_INSTALL_SUCCESS)
                });

              } else if (res.statusCode === 404) {
                // show SERVER_BUSY
                logger.debug('SERVER_BUSY');
                cb('SERVER_BUSY');
              } else if (res.statusCode === 406) {
                // show LICENSE_COUNT_OVER
                logger.debug('LICENSE_COUNT_OVER');
                cb('LICENSE_COUNT_OVER');
              } else if (res.statusCode === 407) {
                // show SET_DATA_FAILED
                logger.debug('SET_DATA_FAILED');
                cb('SET_DATA_FAILED');
              }
            });

            req.on('error', function(e) {
              logger.error('CONNECT_FAILED, problem with request: ' + e.message);
              // show CONNECT_FAILED
              cb('CONNECT_FAILED');
            });

            req.write(postData);
            req.end();
          } else {
            logger.debug('checkIniFile failed, nativePath: /');
            self.handleIniFilesNotCorrect(function(_result) {
              logger.debug('1 handleIniFilesNotCorrect result:' + _result);
              if (_result) {
                cb('LICENSE_APPROVED');
              } else {
                cb('NO_LICENSE');
              }
            });
          }
        });

      } else if (result === false) {
        // delete init and ini file
        logger.debug('checkInitFile failed, nativePath: /');
        self.handleIniFilesNotCorrect(function(_result) {
          logger.debug('2 handleIniFilesNotCorrect result:' + _result);
          if (_result) {
            cb('LICENSE_APPROVED');
          } else {
            cb('NO_LICENSE');
          }
        });
      }
    });
  } else if (settings.currentDevice.type === settings.deviceType.HDD) {
    logger.debug('device is HDD');
    /*
    if (process.platform === 'darwin') {
      self.nativePath = process.cwd().replace('/novo_desktop_streamer.app/Contents/Resources/app.nw', '/novo_desktop_streamer.app/Contents/Resources/app.nw/nativeApp/');
    } else if (process.platform === 'win32') {
      logger.debug('device is hdd, os is win32');
      self.nativePath = settings.dataPath + '/nativeApp/';
    }*/
    setTimeout(function() {
      self.nativeService.checkLicenseFile(function(result) {
        if (result === false) {
          cb('NO_LICENSE');
        } else {
          cb('LICENSE_APPROVED');
        }
      });
    }, 0);
  } else {
    cb('NO_LICENSE');
  }

};

License.prototype.checkIniFile = function checkIniFile(cb) {
  try {
    var iniFile = fs.statSync('../../../../../License.ini');
    logger.debug('checkIniFile:' + iniFile);
    if (iniFile.isFile()) {
      cb(true);
    }
  } catch (err) {
    if (err.code === 'ENOENT') {
      cb(false);
    } else {
      logger.error(err);
    }
    cb(false);
    logger.debug('checkIniFile path:' + process.cwd());
  }
};

License.prototype.deleteLicenseInitFiles = function deleteLicenseInitFiles(nativePath, cb) {
  logger.debug('deleteLicenseInitFiles');
  var list_file_to_delete = ['/init', '/License.ini'];
  if (process.platform === 'darwin') {
    list_file_to_delete = ['../../../../../init', '../../../../../License.ini'];
  }

  if (process.platform === 'win32') {
    list_file_to_delete = ['/init', '/License.ini'];
  }

  list_file_to_delete.forEach(function(filename) {
    fs.exists(filename, function(exists) {
      if (exists) {
        fs.unlinkSync(filename);
      }
    });
  });
  if (cb) {
    cb();
  }
};

License.prototype.handleIniFilesNotCorrect = function handleIniFilesNotCorrect(cb) {
  var self = this;
  logger.debug('handleIniFilesNotCorrect');
  self.deleteLicenseInitFiles(self.nativePath, function() {
    //check license file exist/correct or not?
    self.nativeService.checkLicenseFile(function(result) {
      logger.debug('checkLicenseFile: ' + result);
      if (result) {
        logger.debug('license is correct, run app, result: ' + JSON.stringify(result));
        if (cb) {
          cb(result);
        }
      } else {
        // get device info from server
        self.nativeService.getDeviceInfo(function(deviceInfo) {
          logger.debug('#license ' + JSON.stringify(deviceInfo));
          var options = {
              hostname: licenseServer,
              port: 443,
              path: '/api/software/dsa/certificate?productId=' + deviceInfo.productId + '&vendorId=' + deviceInfo.vendorId + '&serialNumber=' + deviceInfo.serialNumber,
              method: 'GET'
            },
            req = '';
          logger.debug('url: ' + options.path);
          req = https.request(options, function(res) {
            logger.debug('querying license result: ' + typeof(res.statusCode) + ' ' + res.statusCode);
            if (res.statusCode === 200) {
              res.on('data', function(chunk) {
                if (chunk.toString().trim() === '200') {
                  logger.debug('server confirmed license, generating license file');
                  self.nativeService.generateLicenseFile(function(generateLicenseResult) {
                    if (generateLicenseResult === 'successful') {
                      logger.debug('license file generated, continue app');
                      cb(true);
                    } else {
                      logger.error('error creating license file!');
                      cb(false);
                    }
                  });
                } else {
                  logger.debug('NO_LICENSE');
                  cb(false);
                }

              });
            } else {
              logger.debug('NO_LICENSE');
              cb(false);
            }
          });
          req.on('error', function(e) {
            logger.error('#handleIniFilesNotCorrect# problem with request: ' + e.message);
            logger.error('#handleIniFilesNotCorrect# CONNECT_FAILED');
            cb(false);
            // show CONNECT_FAILED
          });
          req.end();
        });
      }
    });
  });
};

exports.License = License;
