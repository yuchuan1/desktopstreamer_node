'use strict';
var settings = require('./settings.js'),
  fs = require('fs'),
  ini = require('ini'),
  http = require('http'),
  pjson = require('../../../package.json'),
  EventEmitter = require('events').EventEmitter,
  util = require('util'),
  md5 = require('spark-md5'),
  path = require('path'),
  url = require('url'),
  spawn = require('child_process').spawn,
  execSync = require('child_process').execSync,
  spawnSync = require('child_process').spawnSync,
  moment = require('moment'),
  AdmZip = require('adm-zip'),
  request = require('request'),
  dsaVersion = pjson.version,
  serverResponse,
  req,
  tempFile,
  updateFile = '',
  md5File = '',
  progressInterval,
  novotestExistsResult = false,
  logger = settings.logger;

var Update = function() {};

function novotestExists(cb) {
  logger.debug('novotestExists');
  if (process.platform === 'win32') {
    try {
      novotestExistsResult = fs.statSync(process.env.HOMEDRIVE + process.env.HOMEPATH + '/novotest');
      cb(novotestExistsResult);
    } catch (err) {
      cb(novotestExistsResult);
    }
  }

  if (process.platform === 'darwin') {
    logger.debug('detecting ' + process.env.HOME + '/Library/Logs/Novo/novotest');
    try {
      novotestExistsResult = fs.statSync(process.env.HOME + '/novotest');
      cb(novotestExistsResult);
    } catch (err) {
      cb(novotestExistsResult);
    }
  }
}

function setServerToChina(cb) {
  logger.debug('setServerToChina');
  novotestExists(function(novotestExists) {
    logger.debug('novotestExists: ' + novotestExists);
    if (novotestExists) {
      logger.debug('novotest detected...');
      request('http://121.41.41.1/api/application/newVersion/1', function(error, response/*, body*/) {
        if (error) {
          logger.debug('ali error: ' + JSON.stringify(error));
          settings.novoServer = 'staging.mynovo.delta.com.cn';
        }

        if (!error && response.statusCode === 200) {
          settings.novoServer = '121.41.41.1';
          logger.debug('novoServer set to 121.41.41.1');
          cb(null);
        } else {
          settings.novoServer = 'staging.mynovo.delta.com.cn';
          logger.debug('novoServer set to staging.mynovo.delta.com.cn');
          request('http://staging.mynovo.delta.com.cn/api/application/newVersion/1', function(error, response/*, body*/) {
		if(error){
			logger.debug('staging.mynovo.delta.com.cn error');
cb('staging.mynovo.delta.com.cn error');
		}
            if (!error && response.statusCode === 200) {
		logger.debug('staging.mynovo.delta.com.cn no error');
              cb(null);
            } else {
		logger.debug('staging.mynovo.delta.com.cn error');
              cb('staging.mynovo.delta.com.cn error');
            }
          });
          cb(null);
        }
      });
    } else {
      logger.debug('novotest not detected...');
      request('http://121.41.51.70/api/application/newVersion/1', function(error, response/*, body*/) {
        if (error) {
          logger.debug('ali error: ' + JSON.stringify(error));
          settings.novoServer = 'staging.mynovo.delta.com.cn';
          cb('ali error');
        }

        if (!error && response.statusCode === 200) {
          settings.novoServer = '121.41.51.70';
          logger.debug('novoServer set to 121.41.51.70');
          cb(null);
        } else {
          settings.novoServer = 'mynovo.delta.com.cn';
          logger.debug('novoServer set to mynovo.delta.com.cn');
          cb(null);
        }
      });
    }
  });
}

util.inherits(Update, EventEmitter);

function getMD5fromServer(Info_id, cb) {
  var options = {
    hostname: settings.novoServer,
    port: 80,
    path: '/api/download/md5/' + Info_id,
    method: 'GET'
  };

  md5File = settings.dataPath + '/serverMD5.txt';
  var tempMd5File = fs.createWriteStream(md5File);
  req = http.get(options, function(res) {
    logger.debug(JSON.stringify(options));
    logger.debug('http status: ' + res.statusCode);
    if (res.statusCode === 200) {
      res.on('data', function(d) {
        tempMd5File.write(d);
      });

      res.on('end', function() {
        tempMd5File.end();
      });
    } else if (res.statusCode === 303) {
      options.hostname = url.parse(res.headers.location).hostname;
      options.host = url.parse(res.headers.location).host;
      options.path = url.parse(res.headers.location).path;
      options.pathname = url.parse(res.headers.location).pathname;
      options.headers = {
        accept: '*/*'
      };
      http.get(res.headers.location, function(res) {
        res.on('data', function(d) {
          tempMd5File.write(d);
        });
        res.on('end', function() {
          tempMd5File.end();
        });
      }).on('error', function(e) {
        logger.error(e);
        tempMd5File.end();
      });
    } else {
      tempFile.end();
    }

  });
  req.end();
  req.on('error', function(e) {
    logger.error(e);
    tempMd5File.end();
  });

  tempMd5File.on('finish', function() {
    logger.debug('md5 file saved!');
    fs.stat(md5File, function(err, stats) {
      if (err)
        logger.error('error retrieving md5 from server', err);
      else if (stats.isFile()) {
        // open file and retrieve md5 string
        fs.readFile(md5File, function(err, data) {
          if (err) throw err;
          logger.debug(data.toString());
          cb(data.toString().substring(0, 32));
        });
      }
    });
  });
}

function getMD5(err, filePath, cb) {
  logger.debug('start getMD5 ' + filePath);
  var result = '';
  fs.readFile(filePath, function(err, buf) {
    if (err)
      logger.debug('md5 error: ' + JSON.stringify(err));
    result = md5.ArrayBuffer.hash(buf);
    logger.debug('getMD5: ', result);
    cb(result);
  });
}


Update.prototype.checkNewVersion = function checkNewVersion(err, cb) {
  var myOS = '';
  logger.debug('checkNewVersion function: ' + JSON.stringify(settings.currentDevice));
  if (process.platform === 'win32' && settings.currentDevice.type === settings.deviceType.HDD) {
    myOS = 'WIN';
  }
  if (settings.currentDevice.type === settings.deviceType.USB) {
    myOS = 'USB';
  }
  if (process.platform === 'darwin' && settings.currentDevice.type === settings.deviceType.HDD) {
    myOS = 'OSX';
  }

  setServerToChina(function(error) {

    var options = {
      hostname: settings.novoServer,
      port: 80,
      path: '/api/application/update/15/v' + dsaVersion + '?param=B380-v2.0&param=' + myOS,
      method: 'GET'
    };
    logger.debug('checkNewVersion path: ' + JSON.stringify(options));
    logger.debug('setServerToChina error: ' + JSON.stringify(error));
    logger.debug(typeof(error));
    if(error === null){
      http.get(options, function(res) {
        logger.debug('http get result: ' + res.statusCode);

        if (res.statusCode !== 200) {
          var result = {
            Info_id: '0'
          };
          cb(result);
        }

        res.on('data', function(d) {
          logger.debug('checkNewVersion response:' + d.toString());
          cb(JSON.parse(d.toString()));
        }).on('error', function(error) {
          logger.error('#update checkNewVersion request error!');
          logger.error(error);
          err(error);
          var result = {
            Info_id: '0'
          };
          cb(result);
        });
      }).on('error', function(error) {
        logger.error('#update checkNewVersion get error!');
        logger.error(error);
        err(error);
        var result = {
          Info_id: '0'
        };
        cb(result);
      });
    } else {
      var result = {
        Info_id: '0'
      };
      cb(result);
    }

  });
};

Update.prototype.validateExistingUpdateFile = function validateExistingUpdateFile(err, fileInfo, cb) {
  updateFile = settings.dataPath + '/' + fileInfo.AppName;
  fs.stat(updateFile, function(error, result) {
    logger.debug('check update file: ' + JSON.stringify(result));
    if (result)
      if (result.isFile()) {
        logger.debug('check existing update file, compare MD5 with server');
        getMD5fromServer(fileInfo.Info_id, function(serverMd5) {
          getMD5(function(err) {
            if (err)
              logger.error(err);
          }, updateFile, function(localMd5) {
            logger.debug('serverMd5: ' + serverMd5);
            logger.debug('localMd5: ' + localMd5);
            if (serverMd5 === localMd5) {
              cb(true);
            } else {
              logger.debug('MD5 does not match, download again');
              cb(false);
            }
          });
        });
      }
    if (process.platform === 'win32') {
      if (error) {
        if (error.code === 'ENOENT') {
          logger.debug('update file: ' + updateFile + ' not found, start downloading');
          cb(false);
        } else {
          logger.error('check update file error: ' + JSON.stringify(error));
          cb(false);
        }
      }
    }
    if (process.platform === 'darwin') {
      logger.debug('result === undefined:' + result === undefined);
      if (result === undefined) {
        logger.debug('update file: ' + updateFile + ' not found, start downloading');
        cb(false);
      }
    }
  });
};

Update.prototype.downloadNewVersion = function downloadNewVersion(err, fileInfo, cb) {
  var self = this;
  if (typeof fileInfo === 'undefined') {
    if (cb) {
      logger.debug('download failed, fileInfo is null');
      var error = new Error('download failed!');
      err(error);
    }
  }

  var progressReport = {
      totalBytes: 0,
      downloadedBytes: 0,
      downloadPercentage: 0,
      elapsedTime: 0,
      remainingBytes: 0,
      remainingTime: 0,
      speed: 0
    },
    options = {
      hostname: settings.novoServer,
      port: 80,
      path: '/api/download/file/' + fileInfo.Info_id,
      method: 'GET'
    };

  logger.debug('downloadNewVersion: ' + JSON.stringify(fileInfo) + ' from ' + settings.novoServer);

  updateFile = settings.dataPath + '/' + fileInfo.AppName;
  self.validateExistingUpdateFile(err, fileInfo, function(result) {
    if (result === true) {
      logger.debug('settings.currentDevice.type:' + settings.currentDevice.type);
      if (settings.currentDevice.type === settings.deviceType.HDD) {
        self.runUpdate(updateFile);
      } else if (settings.currentDevice.type === settings.deviceType.USB) {
        self.runUsbUpdate(updateFile, fileInfo);
      }
      cb('File exists and is validated');
    } else {
      tempFile = fs.createWriteStream(updateFile);
      req = http.get(options, function(res) {
        logger.debug('dowwnload path: ' + options.path);
        logger.debug('statusCode: ', res.statusCode);

        if (res.statusCode === 200) {
          logger.debug('headers: ', JSON.stringify(res.headers));
          logger.debug('total:' + res.headers['content-length']);
          var downloadStartTime = moment();
          serverResponse = res;
          progressReport.totalBytes = res.headers['content-length'];
          res.on('data', function(d) {
            tempFile.write(d);
            progressReport.downloadedBytes += d.length;
            progressReport.downloadPercentage = (progressReport.downloadedBytes / progressReport.totalBytes);
            progressReport.elapsedTime = moment().diff(downloadStartTime, 'seconds');
            progressReport.remainingBytes = progressReport.totalBytes - progressReport.downloadedBytes;
            progressReport.speed = (progressReport.downloadedBytes / 1024) / progressReport.elapsedTime;
            progressReport.remainingTime = (progressReport.remainingBytes / 1024) / progressReport.speed;
          });
          res.on('end', function() {
            logger.debug('http.get end received! ' + JSON.stringify(progressReport));
            tempFile.end();
          });
        } else if (res.statusCode === 303) {
          options.hostname = url.parse(res.headers.location).hostname;
          options.host = url.parse(res.headers.location).host;
          options.path = url.parse(res.headers.location).path;
          options.pathname = url.parse(res.headers.location).pathname;
          options.headers = {
            accept: '*/*'
          };
          http.get(res.headers.location, function(res) {
            logger.debug('cdn headers: ', JSON.stringify(res.headers));
            logger.debug('cdn total:' + res.headers['content-length']);
            progressReport.totalBytes = res.headers['content-length'];
            serverResponse = res;
            var downloadStartTime = moment();
            res.on('data', function(d) {
              tempFile.write(d);
              progressReport.downloadedBytes += d.length;
              progressReport.downloadPercentage = (progressReport.downloadedBytes / progressReport.totalBytes);
              progressReport.elapsedTime = moment().diff(downloadStartTime, 'seconds');
              progressReport.remainingBytes = progressReport.totalBytes - progressReport.downloadedBytes;
              progressReport.speed = (progressReport.downloadedBytes / 1024) / progressReport.elapsedTime;
              progressReport.remainingTime = (progressReport.remainingBytes / 1024) / progressReport.speed;
            });
            res.on('end', function() {
              logger.debug('http.get end received! ' + JSON.stringify(progressReport));
              tempFile.end();
            });
          }).on('error', function(e) {
            logger.debug(e);
            tempFile.end();
          });
        } else {
          tempFile.end();
        }
      });
      req.end();
      req.on('error', function(e) {
        clearInterval(progressInterval);
        tempFile.end();
        err(e);
      });

      progressInterval = setInterval(function() {
        if (progressReport.downloadPercentage > 0) {
          self.emit('downloadProgress', progressReport);
        }
      }, 1000);

      tempFile.on('finish', function() {
        clearInterval(progressInterval);
        self.emit('downloadProgress', progressReport);
        logger.debug('download finished');
        setTimeout(function() {
          logger.debug('making sure the file exists');
          fs.exists(updateFile, function(exists) {
            if (exists) {
              logger.debug(updateFile + ' exists');
              getMD5fromServer(fileInfo.Info_id, function(serverMd5) {
                getMD5(function(err) {
                  if (err)
                    logger.error(err);
                }, updateFile, function(localMd5) {
                  logger.debug('serverMd5: ' + serverMd5);
                  logger.debug('localMd5: ' + localMd5);
                  if (serverMd5 === localMd5) {
                    logger.debug('settings.currentDevice.type:' + settings.currentDevice.type);
                    if (settings.currentDevice.type === settings.deviceType.HDD) {
                      self.runUpdate(updateFile);
                    } else if (settings.currentDevice.type === settings.deviceType.USB) {
                      self.runUsbUpdate(updateFile, fileInfo);
                    }
                  }
                });
              });
            } else {
              logger.debug('file not found :' + updateFile);
            }
          });
        }, 700);

        cb('download completed!');
      }).on('error', function(error) {
        err(error);
      });
    }
  });
};


Update.prototype.cancelDownload = function cancelDownload() {
  logger.debug('cancel downloading ' + updateFile);
  clearInterval(progressInterval);
  req.abort();
  serverResponse.emit('end');
  tempFile.end();
  fs.unlink(updateFile, function(err) {
    if (err)
      logger.error(err);
    logger.debug('successfully deleted ' + updateFile);
  });
};

Update.prototype.generateUsbIniFile = function generateUsbIniFile(fileInfo) {
  var config;
  logger.debug('generate ini file');
  if (process.platform === 'win32') {
    config = {
      NewVersionName: fileInfo.Version,
      WinAppName: 'Launch Novo.exe',
      ApplicationDiskPath: path.normalize(path.parse(process.cwd()).root).replace(/\\/g, '/'),
      InstallPath: path.normalize(path.parse(process.cwd()).root).replace(/\\/g, '/'),
      UpgradeFileName: path.normalize(settings.dataPath + '/' + fileInfo.AppName).replace(/\\/g, '/'),
      ApplicationName: 'DesktopStreamer.exe:DSAService.exe:Launch Novo.exe',
      TargetFolder: 'usb'
    };
  } else if (process.platform === 'darwin') {
    var usbPath = '/' + process.cwd().split('/')[1] + '/' + process.cwd().split('/')[2];
    config = {
      NewVersionName: fileInfo.Version,
      WinAppName: 'Launch Novo.exe',
      ApplicationDiskPath: path.normalize(usbPath).replace(/\\/g, '/'),
      InstallPath: path.normalize(usbPath).replace(/\\/g, '/'),
      UpgradeFileName: path.normalize(settings.dataPath + '/' + fileInfo.AppName).replace(/\\/g, '/'),
      ApplicationName: 'DsaNativeService:nwjs',
      TargetFolder: 'usb'
    };
  }

  logger.debug(JSON.stringify(config));

  fs.writeFileSync(settings.dataPath + '/UpgradeInfo.ini', ini.stringify(config, {
    section: 'UpgradeInfo'
  }));
};

Update.prototype.runUsbUpdate = function runUsbUpdate(filePath, fileInfo) {
  var self = this,
    updateStartedEmitted = false,
    runUpdate = {
      pid: ''
    },
    fileName = '';
  logger.debug('runUsbUpdate ' + filePath);
  self.generateUsbIniFile(fileInfo);

  logger.debug('extracting zip... ');
  //extract installer Upgrade_InstallFree.app
  if (process.platform === 'win32') {
    fileName = 'Upgrade_InstallFreeWin.zip';
  } else if (process.platform === 'darwin') {
    fileName = 'Upgrade_InstallFree.app.zip';
  }

  var zip = new AdmZip(filePath);
  var zipEntries = zip.getEntries();
  if (process.platform === 'win32') {
    zipEntries.forEach(function(zipEntry) {
      if (zipEntry.entryName === fileName) {
        zip.extractEntryTo(fileName, settings.dataPath, false, true);
      }
    });
    logger.debug('zip file extracted, extract inner zip now');
    zip = new AdmZip(settings.dataPath + '/' + fileName);
    zipEntries = zip.getEntries();
    zipEntries.forEach(function(zipEntry) {
      if (zipEntry.entryName === 'Upgrade_InstallFree_Win/') {
        zip.extractEntryTo('Upgrade_InstallFree_Win/', settings.dataPath + '/Upgrade_InstallFree_Win/', false, true);
      }
    });

  } else if (process.platform === 'darwin') {
    logger.debug('filename: ' + fileName);
    zipEntries.forEach(function(zipEntry) {
      if (zipEntry.entryName === fileName) {
        zip.extractEntryTo(fileName, settings.dataPath, false, true);
      }
    });
    logger.debug('zip file extracted, extract inner zip now: ' + settings.dataPath + '/' + fileName);
    zip = new AdmZip(settings.dataPath + '/' + fileName);
    zip.extractAllTo(settings.dataPath, true);
    logger.debug('extracted, change mode now');

    execSync('chmod 777 ' + settings.dataPath + '/Upgrade_InstallFree.app/Contents/MacOS/Upgrade_InstallFree', function(error, stdout, stderr) {
      logger.debug(stdout);
      logger.debug(stderr);
      if (error !== null) {
        logger.debug(error);
      }
    });
  }

  logger.debug('starting updater... ' + process.platform);
  // start updater
  if (process.platform === 'win32') {
    process.chdir('c:/');
    runUpdate = spawn(process.env.SystemRoot + '\\System32\\cmd.exe', ['/c', settings.dataPath + '\\Upgrade_InstallFree_Win\\Upgrade_InstallFree.exe', 'ManualProtection', process.env.PUBLIC + '\\Novo\\UpgradeInfo.ini'], {
      detached: true
    });
  } else if (process.platform === 'darwin') {
    process.chdir('/');
    runUpdate = spawn('open', ['-a', settings.dataPath + '/Upgrade_InstallFree.app/', '--args', 'ManualProtection'], {
      detached: true
    });
  }

  setTimeout(function() {
    updateStartedEmitted = true;
    self.emit('updateStarted');
  }, 2000);
};

Update.prototype.runUpdate = function runUpdate(filePath) {
  var self = this,
    updateStartedEmitted = false,
    maxWaitTime = 50000,
    waitTime = 0,
    runUpdate;
  logger.debug('runUpdate ' + filePath);

  fs.stat(filePath, function(err, stats) {
    if (err)
      logger.error(err);
    else if (stats.isFile()) {
      var waitForUpdateRun = setInterval(function() {
        waitTime += 500;
        if (waitTime > maxWaitTime) {
          logger.debug('Cannot start update...');
          clearInterval(waitForUpdateRun);
        }

        logger.debug('waiting for installer to run...');
        if (runUpdate) {
          if (typeof runUpdate.pid !== 'undefined') {
            logger.debug('Spawned child pid: ' + runUpdate.pid);
            clearInterval(waitForUpdateRun);
            if (!updateStartedEmitted) {
              setTimeout(function() {
                updateStartedEmitted = true;
                self.emit('updateStarted');
              }, 1000);
            }
          }
        }
      }, 500);

      if (process.platform === 'win32') {
        runUpdate = spawn(process.env.SystemRoot + '\\System32\\cmd.exe', ['/c', filePath], {
          detached: true
        });
      } else if (process.platform === 'darwin') {
        logger.debug('trying to delete ' + settings.dataPath + '/DesktopStreamer.mpkg');
        spawnSync('rm', ['-rf', settings.dataPath + '/DesktopStreamer.mpkg/']);
        logger.debug('file deleted, opening ' + filePath);
        spawnSync('open', [filePath]);
        logger.debug('file extracted');
        setTimeout(function() {
          logger.debug('opening ' + settings.dataPath + '/DesktopStreamer.mpkg/');
          runUpdate = spawn('open', [settings.dataPath + '/DesktopStreamer.mpkg'], {
            detached: true
          });
        }, 2000);
      }
    }
  });
};

exports.Update = Update;
