'use strict';
var http = require('http'),
  util = require('util'),
  settings = require('./settings.js'),
  path = require('path'),
  fs = require('fs'),
  filePath,
  mimeNames = {
    '.css': 'text/css',
    '.html': 'text/html',
    '.js': 'application/javascript',
    '.3gp': 'video/3gpp',
    '.aac': 'audio/x-aac',
    '.mkv': 'video/x-matroska',
    '.mp3': 'audio/mpeg',
    '.mp4': 'video/mp4',
    '.mpg': 'video/mpeg',
    '.mp4a': 'audio/mp4',
    '.mp4s': 'application/mp4',
    '.mp4v': 'video/mp4',
    '.mov': 'video/quicktime',
    '.avi': 'video/x-msvideo',
    '.wma': 'audio/x-ms-wma',
    '.wmv': 'video/x-ms-wmv',
    '.flv': 'video/x-flv',
    '.ogg': 'application/ogg',
    '.ogv': 'video/ogg',
    '.oga': 'audio/ogg',
    '.txt': 'text/plain',
    '.wav': 'audio/x-wav',
    '.webm': 'video/webm'
  },
  logger = settings.logger;

function sendResponse(response, responseStatus, responseHeaders, readable) {
  response.writeHead(responseStatus, responseHeaders);

  if (readable === null) {
    response.end();
  } else {
    readable.once('readable', function() {
      logger.debug('data is readable');
      readable.pipe(response);
    });
    readable.once('end', function(){
      logger.debug('there will be no more data.');
      logger.debug('typeof readable: ' + typeof(readable));
      response.end();      
    });
    readable.once('close', function(){
      logger.debug('readable is closed.');
    });
    readable.once('error', function(err) {
      logger.debug('sendResponse readStream Error: ' + JSON.stringify(err));
      response.end();

    });
  }
  return null;
}

function getMimeNameFromExt(ext) {
  var result = mimeNames[ext.toLowerCase()];

  // It's better to give a default value.
  if (result === null) {
    result = 'application/octet-stream';
  }

  logger.debug('mime type: ' + result);
  return result;
}

function readRangeHeader(range, totalLength) {
  /*
   * Example of the method 'split' with regular expression.
   *
   * Input: bytes=100-200
   * Output: [null, 100, 200, null]
   *
   * Input: bytes=-200
   * Output: [null, null, 200, null]
   */
  if (typeof(range) === 'undefined') {
    return null;
  }

  if (range === null) {
    return null;
  }
  if (range.length === 0) {
    return null;
  }


  var array = range.split(/bytes=([0-9]*)-([0-9]*)/),
    start = parseInt(array[1]),
    end = parseInt(array[2]),
    result = {
      Start: isNaN(start) ? 0 : start,
      End: isNaN(end) ? (totalLength - 1) : end
    };

  if (!isNaN(start) && isNaN(end)) {
    result.Start = start;
    result.End = totalLength - 1;
  }

  if (isNaN(start) && !isNaN(end)) {
    result.Start = totalLength - end;
    result.End = totalLength - 1;
  }

  return result;
}

function VideoWebServer() {
  http.Server.call(this, this.handle);
}
util.inherits(VideoWebServer, http.Server);

VideoWebServer.prototype.start = function start(port, videoFilePath, done) {
  logger.debug('VideoWebServer port:' + port + ' path: ' + videoFilePath);
  filePath = videoFilePath;
  this.listen(port, function() {
    logger.debug('Listening for HTTP requests on port %d.', port);
    done();
  });
};

VideoWebServer.prototype.stop = function stop() {
  this.close(function() {
    logger.debug('VideoWebServer Stopped listening.');
  });
};

module.exports = VideoWebServer;

VideoWebServer.prototype.handle = function httpListener(request, response) {
  logger.debug('start httpListener filePath:' + filePath);
  // We will only accept 'GET' method. Otherwise will return 405 'Method Not Allowed'.
  if (request.method !== 'GET') {
    logger.debug('405: Method Not Allowed');
    sendResponse(response, 405, {
      Allow: 'GET'
    }, null);
    return null;
  }

  //var uri = url.parse(request.url).pathname;
  // logger.debug('uri:' + uri);
  // console.log('uri:' + uri);
  // var filename = initFolder + url.parse(request.url, true, true).pathname.split('/').join(path.sep);
  // var filename = path.join(process.cwd(), uri);
  //console.log('filename:' + filename);
  var filename = filePath,
    responseHeaders = {},
    stat = fs.statSync(filename),
    start, end,
    rangeRequest = readRangeHeader(request.headers.range, stat.size);
  logger.debug('filename:' + filename);
  if (!fs.existsSync(filename)) {
    logger.debug('file not found!');
    sendResponse(response, 404, null, null);
    return null;
  }
  logger.debug('request.headers:' + JSON.stringify(request.headers));
  if (rangeRequest) {
    logger.debug('rangeRequest: ' + JSON.stringify(rangeRequest));
  }

  // If 'Range' header exists, we will parse it with Regular Expression.
  if (rangeRequest === null) {
    responseHeaders['Content-Type'] = getMimeNameFromExt(path.extname(filename));
    responseHeaders['Content-Length'] = stat.size; // File size.
    responseHeaders['Accept-Ranges'] = 'bytes';

    logger.debug('return file directly, responseHeaders:');
    logger.debug(responseHeaders);
    //  If not, will return file directly.
    sendResponse(response, 200, responseHeaders, fs.createReadStream(filename, {bufferSize: 64 * 1024}));
    return null;
  }

  start = rangeRequest.Start;
  end = rangeRequest.End;

  // If the range can't be fulfilled.
  if (start >= stat.size || end >= stat.size) {
    logger.debug('Requested Range Not Satisfiable');
    // Indicate the acceptable range.
    responseHeaders['Content-Range'] = 'bytes */' + stat.size; // File size.
    logger.error('out of range error!');

    // Return the 416 'Requested Range Not Satisfiable'.
    sendResponse(response, 416, responseHeaders, null);
    return null;
  }

  // Indicate the current range.
  responseHeaders['Content-Range'] = 'bytes ' + start + '-' + end + '/' + stat.size;
  responseHeaders['Content-Length'] = start === end ? 0 : (end - start + 1);
  responseHeaders['Content-Type'] = getMimeNameFromExt(path.extname(filename));
  responseHeaders['Accept-Ranges'] = 'bytes';
  responseHeaders.Connection = 'keep-alive';
  responseHeaders['Cache-Control'] = 'no-cache';

  logger.debug('return partial Content, opening ' + filename + ' responseHeaders:' + JSON.stringify(responseHeaders) + 'start: ' + start + ' end: ' + end);
  // Return the 206 'Partial Content'.
  sendResponse(response, 206, responseHeaders, fs.createReadStream(filename, {
    bufferSize: 64 * 1024,
    start: start,
    end: end,
    autoClose: true
  }));
};
