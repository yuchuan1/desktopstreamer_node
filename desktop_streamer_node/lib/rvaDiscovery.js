'use strict';
var settings = require('./settings.js'),
  dgram = require('dgram'),
  ip = require('ip'),
  os = require('os'),
  EventEmitter = require('events').EventEmitter,
  util = require('util'),
//  discoverTimer,
  //runDiscovery = true,
  logger = settings.logger,
  db, db2,
  tempList = [],
  DiscoverClient = function(dsaDb, dsaDb2) {
    db = dsaDb;
    db2 = dsaDb2;
  },
  udpCloseTimeout;

util.inherits(DiscoverClient, EventEmitter);

DiscoverClient.prototype.scanRVA = function scanRVA() {
  //logger.debug('SCAN RVA STARTED');
  //var self = this;
  var ifaces = os.networkInterfaces();
  var ips = [];
  Object.keys(ifaces).forEach(function(ifname) {
    var alias = 0;

    ifaces[ifname].forEach(function(iface) {
      //logger.debug(JSON.stringify(iface));
      if ('IPv4' !== iface.family || iface.internal !== false || ifname.substring(0, 10) === 'VirtualBox' || ifname.substring(0, 6) === 'VMware') {
        // skip over internal (i.e. 127.0.0.1), vm, and non-ipv4 addresses
        return;
      }

      if (alias >= 1) {

        ips[ips.length] = iface.address;

      } else {

        ips[ips.length] = iface.address;
      }
    });
  });

  //logger.debug(JSON.stringify(ips));
  ips.forEach(function(ipAddress) {
    //logger.debug('SCAN RVA FOR IP ADDRESS = '+ipAddress);
    var broadcastAddress = ip.subnet(ipAddress, '255.255.255.0').broadcastAddress;
    var broadcastPort = settings.DISCOVERY_PORT;
    var client = dgram.createSocket({
      type: 'udp4',
      reuseAddr: true
    });
    var message = new Buffer('ipaddressssid');

    client.bind(broadcastPort, '0.0.0.0', function() {
      client.setBroadcast(true);
    });

    client.send(message, 0, message.length, broadcastPort, broadcastAddress, function(err) {
      if (err) {
        logger.error('1 DiscoverClient.prototype.discover: ' + JSON.stringify(err));
      }
      udpCloseTimeout = setTimeout(function() {
        //logger.debug('###### time to close udp');
        //logger.debug('SCAN RVA CLOSED');
        client.close();
      }, 7000);
    });

    client.on('error', function(err) {
      if (err) {
        logger.error('3 DiscoverClient.prototype.discover: ' + JSON.stringify(err));
      }
    });

    client.on('message', function(msg, rinfo) {
      var message = String(msg);
      //logger.debug(msg);
      if (message !== 'ipaddressssid') {
        var arr = message.split('_%&*_'),
          obj = {
            ip: rinfo.address,
            label: rinfo.address + ' (' + arr[0].substring(12, arr[0].length).trim() + ')',
            broadcast: true,
            userName: '',
          };
        if (obj.value !== '') {
          tempList.push(obj);
          // logger.debug(tempList);
        }
      }
    });
  });

};

DiscoverClient.prototype.findAllRvas = function findAllRvas() {
  var self = this;
  self.scanRVA();
  setInterval(function() {
    if (settings.fullMode < 1 && settings.splitMode < 1 && settings.screen_number === -2) {
      self.scanRVA();
    }
  }, 10000);

};

DiscoverClient.prototype.lastSession = function lastSession(errorCallback, callback) {
  var self = this;
  settings.runDiscovery = true;
  var doc = {};
  var docList = [];

  if (settings.currentDevice.type === settings.deviceType.USB) {
    try {
      db2.find({
        documentType: 'lastConnection'
      }, function(err, docs) {
        if (err !== null) {
          logger.debug('##### USB ERROR');
        }
        if (docs.length > 0) {
          doc.ip = docs[0].ip;
          db.find({
            documentType: 'lastConnection'
          }, function(err, docs) {
            if (err !== null) {
              logger.debug('##### HDD ERROR');
            }
            if (docs.length > 0) {
              doc.userName = docs[0].userName;
              docList.push(doc);
              callback(docList);
            }
          });
        } else {
          logger.debug('##### USB NEW FROM HDD');
          db.find({
            documentType: 'lastConnection'
          }, function(err, docs) {
            if (err !== null) {
              logger.debug('##### HDD ERROR');
            }
            if (docs.length > 0) {
              doc.ip = docs[0].ip;
              doc.userName = docs[0].userName;
              docList.push(doc);
              callback(docList);
            }
          });
        }
      });
    } catch (e) {
      logger.debug('db2 = ' + db2);
    }

  } else {
    db.find({
      documentType: 'lastConnection'
    }, function(err, docs) {
      if (err !== null) {
        logger.debug('##### HDD ERROR');
      }
      if (docs.length > 0) {
        doc.ip = docs[0].ip;
        doc.userName = docs[0].userName;
        logger.debug(docs);
        logger.debug(doc);
        docList.push(doc);
        callback(docList);
      }
    });
  }

  db.findOne({
    documentType: 'connectionHistory'
  }, function(error, docs) {
    if (error) {
      logger.error(error);
    }

    if (docs !== null) {
      var dosList = JSON.parse(JSON.stringify(docs.record));
      for (var i = 0; i < dosList.length; i++) {
        var record = dosList[i];
        record.broadcast = false;
        settings.connectionRecord.push(record);
        if (i === 9) {
          break;
        }
      }
    } else {
      settings.connectionRecord = [];
    }
    self.emit('RvaDiscovery', settings.connectionRecord);
  });

  setTimeout(function() {
    if (tempList.length > 0) {
      settings.runDiscovery = false;
      for (var i = 0; i < tempList.length; i++) {
        for (var j = 0; j < settings.connectionRecord.length; j++) {
          if (tempList[i].ip === settings.connectionRecord[j].ip) {
            tempList[i].broadcast = true;
            tempList[i].userName = settings.connectionRecord[j].userName;
            settings.connectionRecord.splice(j, 1);
            break;
          }
        }
        settings.connectionRecord.unshift(tempList[i]);
      }
      tempList = [];
      settings.runDiscovery = true;
      //      logger.debug(settings.connectionRecord);
      var doc2 = {
        documentType: 'connectionHistory',
        record: settings.connectionRecord,
      };

      db.update({
        documentType: 'connectionHistory'
      }, doc2, {
        upsert: true
      }, function(err) {
        if (err !== null) {

        }
      });
      self.emit('RvaDiscovery', settings.connectionRecord);
    }
  }, 1000);

  setInterval(function() {
    if (tempList.length > 0) {
      settings.runDiscovery = false;
      for (var i = 0; i < tempList.length; i++) {
        for (var j = 0; j < settings.connectionRecord.length; j++) {
          if (tempList[i].ip === settings.connectionRecord[j].ip) {
            tempList[i].broadcast = true;
            tempList[i].userName = settings.connectionRecord[j].userName;
            settings.connectionRecord.splice(j, 1);
            break;
          }
        }
        settings.connectionRecord.unshift(tempList[i]);
      }
      tempList = [];
      settings.runDiscovery = true;
      //      logger.debug(settings.connectionRecord);
      var doc2 = {
        documentType: 'connectionHistory',
        record: settings.connectionRecord,
      };

      db.update({
        documentType: 'connectionHistory'
      }, doc2, {
        upsert: true
      }, function(err) {
        if (err !== null) {

        }
      });
      self.emit('RvaDiscovery', settings.connectionRecord);
    }
  }, 5000);
};


exports.DiscoverClient = DiscoverClient;
