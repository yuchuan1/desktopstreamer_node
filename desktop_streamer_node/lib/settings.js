/*jshint -W030*/
var dataPath = '',
  logLevel = '',
  novoServer = '',
  novotestExists,
  fs = require('fs'),
  winston = require('winston'),
  moment = require('moment'),
  logger,
  maxLogFileSize = 2000000,
  path = require('path');

if (process.platform === 'win32'){
  dataPath = process.env.PUBLIC + '/Novo';
  try {
    novotestExists = fs.statSync(process.env.HOMEDRIVE + process.env.HOMEPATH + '/novotest');
    if (novotestExists.isFile()){
      logLevel = 'debug';
      novoServer = 'download.staging.launchnovo.com';
      logger = new(winston.Logger)({
        transports: [
          new(winston.transports.File)({
            name: 'info-file',
            filename: path.join(dataPath, 'dsa.log'),
            json: false,
            level: logLevel,
            timestamp: function(){
              return moment().format('MM-DD HH:mm:ss:SSS');
            },
            maxsize: maxLogFileSize
          }),
          new(winston.transports.Console)({
            level: logLevel,
            timestamp: function(){
              return moment().format('MM-DD HH:mm:ss:SSS');
            }
          })
        ]
      });
    }
  } catch (err){
    if (err.code === 'ENOENT'){
      logLevel = 'error';
      novoServer = 'download.launchnovo.com';
      logger = new(winston.Logger)({
        transports: [
          new(winston.transports.File)({
            name: 'info-file',
            filename: path.join(dataPath, 'dsa.log'),
            json: false,
            level: logLevel,
            timestamp: function(){
              return moment().format('MM-DD HH:mm:ss:SSS');
            },
            maxsize: maxLogFileSize
          }),
          new(winston.transports.Console)({
            level: logLevel,
            timestamp: function(){
              return moment().format('MM-DD HH:mm:ss:SSS');
            }
          })
        ]
      });
    } else {
      console.error(err);
    }
  }
}

if (process.platform === 'darwin'){
  dataPath = process.env.HOME + '/Library/Logs/Novo';
  try {
    novotestExists = fs.statSync(process.env.HOME + '/novotest');
    if (novotestExists.isFile()){
      logLevel = 'debug';
      novoServer = 'download.staging.launchnovo.com';
      logger = new(winston.Logger)({
        transports: [
          new(winston.transports.File)({
            name: 'info-file',
            filename: path.join(dataPath, 'dsa.log'),
            json: false,
            level: logLevel,
            timestamp: function(){
              return moment().format('MM-DD HH:mm:ss:SSS');
            },
            maxsize: maxLogFileSize
          }),
          new(winston.transports.Console)({
            level: logLevel,
            timestamp: function(){
              return moment().format('MM-DD HH:mm:ss:SSS');
            }
          })
        ]
      });
    }
  } catch (err){
    if (err.code === 'ENOENT'){
      novotestExists = false;
      logLevel = 'error';
      novoServer = 'download.launchnovo.com';
      logger = new(winston.Logger)({
        transports: [
          new(winston.transports.File)({
            name: 'info-file',
            filename: path.join(dataPath, 'dsa.log'),
            json: false,
            level: logLevel,
            timestamp: function(){
              return moment().format('MM-DD HH:mm:ss:SSS');
            },
            maxsize: maxLogFileSize
          }),
          new(winston.transports.Console)({
            level: logLevel,
            timestamp: function(){
              return moment().format('MM-DD HH:mm:ss:SSS');
            }
          })
        ]
      });
    } else {
      console.error(err);
    }
  }
}
console.log('logLevel:' + logLevel);
console.log('novoServer:' + novoServer);
exports.logLevel = logLevel;
exports.novoServer = novoServer;
exports.dataPath = dataPath;
exports.rvaIp = '';
exports.MAXIUM_MSG_LENGTH = 64;
exports.SOFTWARE_VERSION = 1234;
exports.MINIMUM_MSG_LENGTH = 4;
exports.DATA_PORT_MINIMUM_MSG_LENGTH = 8;

exports.UDP_SETTINGS_PORT = 20141;
exports.TCP_CONNECTION_PORT = 20121;
exports.DISCOVERY_PORT = 20124;

exports.REMOTE_CONTROL_PORT = 20122;
exports.TCP_DATA_PORT = 20123;
exports.TCP_VIDEO_PORT = 20126;
exports.DATA_SERVER_PORT = 20131;
exports.ANNOTATION_PORT = 20161;

exports.STREAMING_CONNECTION_PORT = 20130;
exports.HTTP_PORT = 12345;

exports.settingsTimeOut = 5000;
exports.dsaIp = '';
exports.clientName = 'default';
exports.pinRequired = false;
exports.pinCode = '';
exports.client_id = 0;
exports.user_id = '';
exports.state = 0;
exports.screen_number = -2;
exports.ssid = '';
exports.isDataConnected = false;
exports.isScreenConnected = false;
exports.network_mode = '';
exports.dev_name = '';
exports.isReconnect = false;
exports.rvaType = '';

exports.screenshot_userid = 0;
exports.groupMode = false;
exports.settingsPassword = '';

exports.screenWidth = 0;
exports.screenHeight = 0;
exports.numberOfScreens = 1;
exports.nvc_model = '';
exports.rva_version = 'v1.6.0';
exports.bsp_version = '';
exports.isVersionMatch = true;
exports.splitMode = 0; // 0 : stop, 1 : start, 2 : pause
exports.fullMode = 0; // 0 : stop, 1 : start, 2 : pause
exports.currentSplitPanel = 1;

exports.isImageSendingPause = false;
exports.presentationMode = {
  stop: 0,
  start: 1,
  pause: 2
};

exports.currentDevice = {
  type: '2',
  vendorId: '',
  productId: '',
  serialNumber: ''
};

exports.deviceType = {
  USB: '1',
  HDD: '2'
};

exports.screenType = {
  MAIN: '1',
  EXT: '2'
};

exports.currentScreen = this.screenType.MAIN;

exports.qr_ip = '';
exports.qr_ssid = '';
exports.qr_lan = '';
exports.qr_ImgURL = '';
exports.qr_Pin = '';
exports.qr_pin_status = false;
exports.cmd_keep_alive_timeout = false;
exports.enableKeepAlive = true;
exports.request_checkKeepAlive_timeout = false;
exports.send_request_timeout = true;

exports.novo_try = '';
exports.try_times = 0;
exports.request_try = '';
exports.connection_timeout = false;
exports.group = '';
exports.groupName = 'None';

exports.presentationMode = false;

//h.264 streaming settings
//exports.framerate = 24;
//exports.bitrate = 2048;

exports.RESOLUTION_SCREEN_W_720P = 1366;
exports.RESOLUTION_SCREEN_W_1080P = 1920;
exports.splitModeFrameRate = 1;
// for presentation mode v1.6
exports.BITRATE_HIGH_720P_V1 = 4096;
exports.BITRATE_NORMAL_720P_V1 = 4096;
exports.BITRATE_HIGH_1080P_V1 = 8192;
exports.BITRATE_NORMAL_1080P_V1 = 4096;

exports.FRMAERATE_HIGH_720P_V1 = 24;
exports.FRMAERATE_NORMAL_720P_V1 = 24;
exports.FRMAERATE_HIGH_1080P_V1 = 9;
exports.FRMAERATE_NORMAL_1080P_V1 = 9;

// for video mode  v1.6
exports.BITRATE_VIDEO_720P_V1 = 2048;
exports.BITRATE_VIDEO_1080P_V1 = 1760;

exports.FRMAERATE_VIDEO_720P_V1 = 24;
exports.FRMAERATE_VIDEO_1080P_V1 = 9;

// for presentation mode
exports.BITRATE_HIGH_720P = 8192;
exports.BITRATE_NORMAL_720P = 4096;
exports.BITRATE_HIGH_1080P = 8192;
exports.BITRATE_NORMAL_1080P = 4096;

exports.FRMAERATE_HIGH_720P = 30;
exports.FRMAERATE_NORMAL_720P = 30;
exports.FRMAERATE_HIGH_1080P = 30;
exports.FRMAERATE_NORMAL_1080P = 30;

// for video mode
exports.BITRATE_VIDEO_720P = 8192;
exports.BITRATE_VIDEO_1080P = 8192;

exports.FRMAERATE_VIDEO_720P = 30;
exports.FRMAERATE_VIDEO_1080P = 30;

exports.MAX_SCAN_IP_ADDRESSES = 10;

exports.youTubeVideoId = '';
exports.youTubeTitle = '';
exports.localVideoPath = '';
exports.videoPlaying = false;
exports.localStreamingThruProxy = true;
exports.tabletsLocked = false;

// ============================================ Luke's Code START ========================================= //
exports.isPreview = false;
exports.isGroupUpload = false;
exports.isVotingPolling = false;
exports.isFileSharing = false;
exports.isFileDownloading = false;
exports.isReadyToSend = true;
exports.isClosingSendFilepage = false;
exports.isSendToCancel = false;
exports.isCorpModeratorOn = false;
exports.uploadGroupData = '';
exports.keepAliveBack = false;
exports.needReconnection = false;
exports.disconnectEvent = 'Disconnection';
exports.connectionRecord = [];
exports.runDiscovery = true;
exports.LOOKUP_CONNECTION_PORT = 20191;
exports.isSupportMiracast = false;
exports.isSupportAirPlay = false;
exports.playMode = 0;
exports.isGrayoutAll = false;
exports.isLauncherCMDBack = true;
exports.isWebUrlStreaming = false;
exports.isRestoreScreenResolution = false;
exports.isConnectionApproval = true;
exports.urlList = [];
exports.lastPid = 0;
//============================================ Luke's Code END ========================================= //

exports.LED = {
  FULL_SCREEN_BLUE: 0x00,
  SPLIT_1: 0x01,
  SPLIT_2: 0x02,
  SPLIT_3: 0x03,
  SPLIT_4: 0x04,
  FULL_SCREEN_RED: 0x10,
  ALL: 0xFF
};

exports.LAUNCHER_CMD = {
  FULL_SCREEN: 0x00,
  SPLIT_1: 0x01,
  SPLIT_2: 0x02,
  SPLIT_3: 0x03,
  SPLIT_4: 0x04,
  DISCONNECT: 0x10,
  USB_PLUG_IN: 0x20,
  USB_PLUG_OUT: 0x21
};
exports.remoteControl = true;
exports.launcherEnabled = true;
exports.maxLogFileSize = maxLogFileSize;
exports.versionDevice = 'HDD';
exports.logger = logger;
exports.switchExtend = false;
