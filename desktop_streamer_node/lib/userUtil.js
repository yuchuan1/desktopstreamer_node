'use strict';
var settings = require('./settings.js'),
    userUtil = require('./userUtil.js'),
    common = require('./common.js'),
    path = require('path'),
    request = require('request'),
    _ = require('lodash'),
    moment = require('moment'),
    winston = require('winston'),
    logger = settings.logger;

exports.user_list = [];
exports.group_list = [];

exports.userdata = function userdata(uid, label, isHost, device, panel, isMyself, online, display_name, os_type) {
  var user = {};
  user.uid = uid;
  user.label = label;
  user.isHost = isHost;
  if (device == null) {
    user.device = getOSDeviceNameByType(os_type);
  } else {
    user.device = device;
  }
  user.panel = panel;
  user.isMyself = isMyself;
  user.online = online;
  user.display_name = display_name;
  user.os_type = os_type;

  return user;
};

exports.add_userdata = function add_userdata(uid, label, isHost, device, panel, isMyself, online, os_type) {
  if (settings.groupMode) {
    update_group_data(uid, label, isHost, device, panel, isMyself, online, os_type);
  } else {
    if(isNewUser(uid)) {
      var user = userUtil.userdata(uid, label, isHost, device, panel, isMyself, online, label, os_type);
      userUtil.user_list.push(user);
    } else {
      logger.debug("Client Id= "+uid+" Name = "+label+" is already in userlist");
  }
  }

  return user;
}

function update_group_data(uid, label, isHost, device, panel, isMyself, online, os_type) {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var update_user = userUtil.user_list[i];
    if (update_user.display_name.toUpperCase().replace(/\s/g, "") == label.toUpperCase().replace(/\s/g, "")) {
      var isMyself = false;
      //Add Function to create UserList
      if (update_user.uid == settings.client_id) {
        isMyself = true;
      }
      var user;
      if (update_user.os_type != os_type) {
        update_user.os_type = os_type;
      }
      for (var k = 1; k < userUtil.group_list.length; k = k + 2) {
        if (update_user.display_name.toUpperCase().replace(/\s/g, "") == userUtil.group_list[k].toUpperCase().replace(/\s/g, "")) {
          if (userUtil.group_list[k - 1] != userUtil.group_list[k]) {
            //Device = OS Type + (User's + Device name)
            var d_name = getOSDeviceNameByType(update_user.os_type) + " (" + userUtil.group_list[k - 1] + "'s " + userUtil.group_list[k] + ")";
            user = userUtil.userdata(uid, update_user.label, isHost, d_name, panel, isMyself, online, update_user.display_name, update_user.os_type);
          } else {
            user = userUtil.userdata(uid, update_user.label, isHost, null, panel, isMyself, online, update_user.display_name, update_user.os_type);
          }
          break;
        }
      }
      if (!user) {
        logger.debug(update_user.display_name);
        user = userUtil.userdata(uid, update_user.label, isHost, null, panel, isMyself, online, update_user.display_name, update_user.os_type);
      }
      userUtil.user_list[i] = user;
      break;
    }
  }
}

exports.update_userdata = function update_userdata(uid, is_host, panel, online) {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var update_user = userUtil.user_list[i];
    if (update_user.uid == uid) {
      var isMyself = false;
      //Add Function to create UserList
      if (update_user.uid == settings.client_id) {
        isMyself = true;
      }
      var user;
      if (settings.groupMode) {
        for (var k = 1; k < userUtil.group_list.length; k = k + 2) {
          if (update_user.display_name.toUpperCase().replace(/\s/g, "") == userUtil.group_list[k].toUpperCase().replace(/\s/g, "")) {
            if (userUtil.group_list[k - 1] != userUtil.group_list[k]) {
              //Device = OS Type + (User's + Device name)
              var d_name = getOSDeviceNameByType(update_user.os_type) + " (" + userUtil.group_list[k - 1] + "'s " + userUtil.group_list[k] + ")";
              user = userUtil.userdata(uid, update_user.label, is_host, d_name, panel, isMyself, update_user.online, update_user.display_name, update_user.os_type);
            } else {
              user = userUtil.userdata(uid, update_user.label, is_host, getOSDeviceNameByType(update_user.os_type), panel, isMyself, update_user.online, update_user.display_name, update_user.os_type);
            }
            break;
          }
        }
        if (!user) {
          user = userUtil.userdata(uid, update_user.label, is_host, getOSDeviceNameByType(update_user.os_type), panel, isMyself, online, update_user.display_name, update_user.os_type);
        }
      } else {
        user = userUtil.userdata(uid, update_user.label, is_host, update_user.device, panel, isMyself, update_user.online, update_user.display_name, update_user.os_type);
      }

      userUtil.user_list[i] = user;
      break;
    }
  }
}

exports.remove_userdata = function remove_userdata(uid) {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var remove_user = userUtil.user_list[i];
    if (remove_user.uid == uid) {
      if (settings.groupMode) {
        var user = userUtil.userdata("None", remove_user.label, false, getOSDeviceNameByType(remove_user.os_type), -1, false, false, remove_user.display_name, remove_user.os_type);
        userUtil.user_list[i] = user;
        break;
      } else {
        var r = userUtil.user_list.indexOf(remove_user);
        if (r != -1) {
          userUtil.user_list.splice(r, 1);
          break;
        }
      }

    }
  }
}

exports.refreshUserListByGroupList = function refreshUserListByGroupList() {
  // update_userdata
  for (var i = 1; i < userUtil.group_list.length; i = i + 2) {
    var pass = false;
    for (var j = 0; j < userUtil.user_list.length; j++) {
      var update_user = userUtil.user_list[j];
      if (update_user.label.toUpperCase().replace(/\s/g, "") == userUtil.group_list[i].toUpperCase().replace(/\s/g, "")) {
        // update online = true;
        if (userUtil.group_list[i - 1] != userUtil.group_list[i]) {
          //Device = OS Type + (User's + Device name)
          var d_name = getOSDeviceNameByType(update_user.os_type) + " (" + userUtil.group_list[i - 1] + "'s " + userUtil.group_list[i] + ")";
          user = userUtil.userdata(update_user.uid, userUtil.group_list[i - 1], update_user.isHost, d_name, update_user.panel, update_user.isMyself, true, userUtil.group_list[i], update_user.os_type);
        } else {
          user = userUtil.userdata(update_user.uid, userUtil.group_list[i - 1], update_user.isHost, null, update_user.panel, update_user.isMyself, true, userUtil.group_list[i], update_user.os_type);
        }
        userUtil.user_list[j] = user;
        pass = true;
        break;
      }
    }
    if (pass) {
      // already do update_user
    } else {
      // user is offline
      var user = userUtil.userdata("None", userUtil.group_list[i - 1], false, "", -1, false, false, userUtil.group_list[i], 0);
      userUtil.user_list.push(user);
    }
  }
}

exports.cleanUserOnlineStatus = function cleanUserOnlineStatus() {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var a = userUtil.user_list[i];
    var user = userUtil.userdata(a.uid, a.label, false, a.device, -1, false, false, a.display_name, a.os_type);
    userUtil.user_list[i] = user;
  }
}

exports.getSendNameById = function getSendNameById(uid) {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var user = userUtil.user_list[i];
    if (user.uid === uid) {
      return user.label;
    }
  }
}

exports.amIHost = function amIHost(uid) {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var user = userUtil.user_list[i];
    if (user.uid === uid) {
      return user.isHost;
    }
  }
}

exports.isHostOn = function isHostOn() {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var user = userUtil.user_list[i];
    if (user.isHost) {
      return user.isHost;
    }
  }
  return false;
}

exports.getHostName = function getHostName() {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var user = userUtil.user_list[i];
    if (user.isHost) {
      return user.label;
    }
  }
}

exports.getUserData = function getUserData(uid) {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var user = userUtil.user_list[i];
    if (user.uid === uid) {
      return user;
    }
  }
}

exports.getDispalyName = function getDispalyName(name) {
  if (settings.groupMode) {
    for (var i = 1; i < userUtil.group_list.length; i = i+2) {
      if (name.toUpperCase().replace(/\s/g, "") == userUtil.group_list[i].toUpperCase().replace(/\s/g, "")) {
        return userUtil.group_list[i-1];
      }
    }
  } else {
    return name;
  }
}


exports.getOnlineUserCounts = function getOnlineUserCounts() {
  if (settings.groupMode) {
    var count = 0;
    for (var i = 0; i < userUtil.user_list.length; i++) {
      var user = userUtil.user_list[i];
      if (user.online) {
        count++;
      }
    }
    return count;
  } else {
    return userUtil.user_list.length;
  }
}

function isNewUser(uid) {
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var userdata = userUtil.user_list[i];
    if (userdata.uid == uid) {
      return false;
    }
  }

  return true;
}

function getOSDeviceNameByType(os_type) {
  var device = "";
  if (os_type === common.OS_DEV_TYPE.OS_WINDOWS_PC) {
    device = "Win PC";
  } else if (os_type == common.OS_DEV_TYPE.OS_MAC_PC) {
    device = "Mac PC";
  } else if (os_type == common.OS_DEV_TYPE.OS_LINUX_PC) {
    device = "LINUX_PC";
  } else if (os_type == common.OS_DEV_TYPE.OS_UNIX_PC) {
    device = "UNIX_PC";
  } else if (os_type == common.OS_DEV_TYPE.OS_ANDROID_TABLET) {
    device = "Android Pad";
  } else if (os_type == common.OS_DEV_TYPE.OS_ANDROID_PHONE) {
    device = "Android Phone";
  } else if (os_type == common.OS_DEV_TYPE.OS_IOS_IPAD) {
    device = "iPad";
  } else if (os_type == common.OS_DEV_TYPE.OS_IOS_PHONE) {
    device = "iPhone";
  } else if (os_type == common.OS_DEV_TYPE.OS_CHROME_APP) {
    device = "Chromebook";
  } else if (os_type == common.OS_DEV_TYPE.OS_CHROME_APP_ON_WIN) {
    device = "Chrome on Win";
  } else if (os_type == common.OS_DEV_TYPE.OS_CHROME_APP_ON_MAC) {
    device = "Chrome on Mac";
  } else {
    device = "";
  }
  return device;
}

exports.couldBePresenter = function couldBePresenter(uid) {
  if (settings.rvaType === 'EDU') {
    for (var i = 0; i < userUtil.user_list.length; i++) {
      var user = userUtil.user_list[i];
      if (user.uid === uid) {
        return user.isHost;
      }
    }
  } else if (settings.rvaType === 'CORP') {
    if (settings.isCorpModeratorOn) {
      for (var i = 0; i < userUtil.user_list.length; i++) {
        var user = userUtil.user_list[i];
        if (user.uid === uid) {
          return user.isHost;
        }
      }
    } else {
      return true;
    }
  } else {
    return false;
  }

}

exports.getUserListStatus = function getUserListStatus() {
  var result = common.RVA_PLAY_STATUS.PENDING_ALL;
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var user = userUtil.user_list[i];
    if (user.panel === 0 && user.os_type < 5) {
      result = common.RVA_PLAY_STATUS.FULL_VIDEO;
      break;
    } else if (user.panel === 0 && user.os_type > 4) {
      result = common.RVA_PLAY_STATUS.FULL_IMAGE;
      break;
    } else if (user.panel > 0) {
      result = common.RVA_PLAY_STATUS.SPLIT_IMAGE;
      break;
    }
  }

  return result;
}

exports.getSplitModeRate = function getSplitModeRate() {
  var result = 0;
  var counts = 0;
  for (var i = 0; i < userUtil.user_list.length; i++) {
    var userdata = userUtil.user_list[i];
    if (userdata.panel > 0) {
      counts++;
    }
  }
  
  switch (counts) {
    case 1 :
      result = 20;
      break;
    case 2:
      result = 8;
      break;
    case 3:
      result = 3;
      break;
    case 4:
      result = 1;
      break;
    default:
      result = 1;
  }
  
  return result;
}
