/* jshint -W030 */
/* jshint -W040 */
'use strict';
var settings = require('./settings.js'),
  rvaSelf,
  dsaUtil = require('./dsaUtil.js'),
  userUtil = require('./userUtil.js'),
  net = require('net'),
  fs = require('fs'),
  semver = require('semver'),
  common = require('./common.js'),
  mkdirp = require('mkdirp'),
  path = require('path'),
  htmlencode = require('htmlencode'),
  Datastore = require('nedb'),
  EventEmitter = require('events').EventEmitter,
  util = require('util'),
  async = require('async'),
  xml2js = require('xml2js'),
  randomString = require('random-string'),
  VideoWebServer = require('./webServer.js'),
  os = require('os'),
  _ = require('lodash'),
  dgram = require('dgram'),
  isIp = require('is-ip'),
  //mp3Length = require('mp3Length'),
  mp3Duration = require('mp3-duration'),
  youTubeValidate = require('../youtube-validator'),
  //  request = require('request'),
  RVACommandClient,
  annotationClient,
  RVAConnected = false,
  dataServerSocket, monitoringFirmwareUpgradeInterval,
  RVAProxy1, RVAProxy2, keepAliveScheduler, keepAliveTimeout, connectTimeout,
  client, client2,
  videoWebServer,
  videoPlayMessage, videoPlayStatusCount = 0,
  videoPlayTimeUpdated = false,
  totalVideoDuration = 0,
  youTubePlaying = false,
  localVideoPlaying = false,
  isFirstTeacherPriorityError = true,
  previewTimer, splitScreenTimer, connectionRetry, framerate, bitrate,
  reconnectionTimeout, ks_rvTimeout,
  lic = require('../lib/license.js'),
  update = require('../lib/update.js'),
  updateChina = require('../lib/updateChina.js'),
  nativeService = require('dsa-native-service'),
  soundFlowerReminder = true,
  soundFlowerAllowed = false,
  soundFlowerEnabled = false,
  remoteMouseState = false,
  qLauncherExists = false,
  nativePath,
  nativeAlive = false,
  lastRequestTime = 0,
  thisRequestTime = 0,
  lastDuration = 0,
  durationDuplicateCount = 0,
  dsa_width = 0,
  dsa_height = 0,
  updateVersion,
  slice_byte = 0,
  handleBuffer = [],
  dEHBufer = [],
  launcherTimer,
  videoPlayPaused = false,
  qrImg = null,
  isStateChange = false,
  ledTimeout,
  dataExchangeRun = false,
  db = new Datastore({
    filename: path.join(settings.dataPath, 'dsa.db'),
    autoload: true
  }),
  db2,
  pjson = require('../../../package.json'),
  isSendingConnected = false,
  rvaDiscovery = require('./rvaDiscovery.js'),
  rvaDiscoveryClient = new rvaDiscovery.DiscoverClient(db, db2),
  logger = settings.logger,
  extendScreenStateBeforePause = 0,
  isResume = false;

process.on('uncaughtException', function(err) {
  logger.error('!!!! Uncaught errors !!!!');
  logger.error(err.stack);
});

logger.debug('process.execPath:' + process.execPath);
logger.debug('Current directory: ' + process.cwd());

if (process.platform === 'darwin') {
  nativePath = process.cwd().replace('/novo_desktop_streamer.app/Contents/Resources/app.nw', '/novo_desktop_streamer.app/Contents/Resources/app.nw/nativeApp/');
} else if (process.platform === 'win32') {
  nativePath = settings.dataPath + '/nativeApp/';
}

logger.debug('nativePath: ' + nativePath);
var license = new lic.License(nativeService, nativePath);
var autoUpdate; // = new update.Update();

setInterval(function() {
  global.gc();
}, 1000);

// launcher commands
logger.debug('settings.launcherEnabled: ' + settings.launcherEnabled + ' qLauncherExists:' + qLauncherExists);
if (settings.launcherEnabled === true) {
  //var self = this;
  nativeService.dsaServer.on('launcherCommand', function(command) {
    logger.debug('1 launcherCommand received: ' + command + ' native status:' + nativeAlive);
    if (nativeAlive) {
      if (command === settings.LAUNCHER_CMD.USB_PLUG_IN) {
        logger.debug('receive Launcher cmd usb plug in');
        if (RVAConnected) {
          nativeService.rvaConnectionState(0x01, function(result) {
            logger.debug('report rva connected to DSAService: ' + result);
          });
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledCycleFlash(1, 200);
          });
        } else {
          nativeService.rvaConnectionState(0x00, function(result) {
            logger.debug('report rva disconnected to DSAService: ' + result);
          });
          nativeService.ledOff(settings.LED.ALL, function() {
            logger.debug('LEDs are all off, start gradient effect now');
            nativeService.ledGradientFlash(settings.LED.FULL_SCREEN_RED, 5, 3000, function() {
              ledTimeout = setTimeout(function() {
                nativeService.ledOff(settings.LED.ALL, function() {
                  nativeService.ledOn(settings.LED.FULL_SCREEN_RED, 0);
                });
              }, 25000);
            });
          });
        }
      }
      /*
      if (!RVAConnected && (command === settings.LAUNCHER_CMD.SPLIT_1 || command === settings.LAUNCHER_CMD.SPLIT_2 || command === settings.LAUNCHER_CMD.SPLIT_3 || command === settings.LAUNCHER_CMD.SPLIT_4)) {
        logger.debug('gradient flash full screen red');
        nativeService.ledGradientFlash(settings.LED.FULL_SCREEN_RED, 3, 3000);
      }
      */
    }
  });
}

nativeService.dsaServer.once('nativeServerReady', function() {
  var self = this,
    runningFile = '';
  nativeAlive = true;

  logger.debug('checking license result... process.cwd(): ' + process.cwd());
  setTimeout(function() {
    async.series([
      function(done) {
        nativeService.getDeviceInfo(function(result) {
          logger.debug('3 getDeviceInfo: ' + JSON.stringify(result));
          settings.currentDevice.vendorId = result.vendorId;
          settings.currentDevice.productId = result.productId;
          settings.currentDevice.serialNumber = result.serialNumber;
          if (result.deviceType === settings.deviceType.USB) { //usb
            settings.currentDevice.type = settings.deviceType.USB;
            logger.debug('currentDevice.type USB...' + JSON.stringify(settings.currentDevice));
            db2 = new Datastore({
              filename: path.join('', 'dsa2.db'),
              autoload: true
            });
            rvaDiscoveryClient = new rvaDiscovery.DiscoverClient(db, db2);
            if (settings.versionDevice === 'HDD')
              self.emit('incorrectDevice');
            done();
          } else if (result.deviceType === settings.deviceType.HDD) { //hdd
            settings.currentDevice.type = settings.deviceType.HDD;
            logger.debug('currentDevice.type HDD...' + JSON.stringify(settings.currentDevice));
            db2 = new Datastore({
              filename: path.join(settings.dataPath, 'dsa2.db'),
              autoload: true
            });
            rvaDiscoveryClient = new rvaDiscovery.DiscoverClient(db, db2);
            if (settings.versionDevice === 'USB')
              self.emit('incorrectDevice');
            done();
          } else {
            logger.error('invalid device type: ' + result.deviceType);
          }
        });
      },
      function(done) {
        license.checkLicense(function(result) {
          switch (result) {
            case 'NO_LICENSE':
              self.emit('NO_LICENSE');
              break;
            case 'SERVER_BUSY':
              self.emit('SERVER_BUSY');
              break;
            case 'LICENSE_COUNT_OVER':
              self.emit('LICENSE_COUNT_OVER');
              break;
            case 'SET_DATA_FAILED':
              self.emit('SET_DATA_FAILED');
              break;
            case 'CONNECT_FAILED':
              self.emit('CONNECT_FAILED');
              break;
            case 'LICENSE_INSTALL_SUCCESS':
              self.emit('LICENSE_INSTALL_SUCCESS');
              break;
            case 'LICENSE_APPROVED':
              logger.debug('LICENSE_APPROVED');
              rvaSelf.getTeacherCredentialPassword(function(result) {
                rvaSelf.emit('teacherCredentialPassword', result);
              });

              rvaSelf.getTeacherCredentialSetting(function(result) {
                rvaSelf.emit('teacherCredentialSetting', result);
              });
              self.emit('LICENSE_APPROVED');
              break;
          }
          done();
        });
      },
      function(done) {
        if (process.platform === 'win32') {
          runningFile = settings.dataPath + '/appRunning~';
        } else if (process.platform === 'darwin') {
          runningFile = settings.dataPath + '/appRunning~';
        }

        if (runningFile !== '') {
          fs.writeFile(runningFile, '', function(err) {
            if (err) throw err;
            done();
          });
        }
      },
      function(done) {
        nativeService.getAirplaySupport(function(result) {
          logger.debug('airplay: ' + result);
          settings.isSupportAirPlay = result;
          done();
        });
      },
      function(done) {
        nativeService.getMiracastSupport(function(result) {
          logger.debug('miracast: ' + result);
          settings.isSupportMiracast = result;
          done();
        });
      },
      function(done) {
        if (process.platform === 'win32')
          nativeService.aeroModeDisable(function(result) {
            logger.debug('aeroModeDisable:' + JSON.stringify(result));
            done();
          });
        else
          done();
      },
      function(done) {
        nativeService.powerSavingDisable(function(result) {
          logger.debug('powerSavingDisable:' + JSON.stringify(result));
          done();
        });
      },
      function(done) {
        db.find({
          documentType: 'urlHistory'
        }, function(err, docs) {
          if (err !== null) {
            logger.debug('##### URL HISTORY GET ERROR');
          }
          if (docs.length > 0) {
            settings.urlList = docs[0].urlList;
            settings.lastPid = docs[0].urlList[docs[0].urlList.length - 1].pid;
          }
          done();
        });
      },
      function() {
        logger.debug('2 currentDevice.type: ' + JSON.stringify(settings.currentDevice));
        autoUpdate.checkNewVersion(function(error) {
          logger.error('autoUpdate.checkNewVersion error: ' + JSON.stringify(error));
        }, function(result) {
          logger.debug('autoupdate: ' + JSON.stringify(result));
          if (result.Info_id === '0') {
            logger.debug('no new version is available');
          } else {
            logger.debug('An upgrade is available... ', result);
            updateVersion = result;
            self.emit('newVersionAvailable');
          }
        });
      }
    ]);
  }, 1000);

});

nativeService.dsaServer.on('serviceError', function(err) {
  logger.error('serviceError: ' + JSON.stringify(err));
  if (err.code === 'ECONNRESET' || err.code === 'nativeDisconnect' || err.code === 'EPIPE')
    nativeAlive = false;
  if (settings.currentDevice.type === settings.deviceType.USB) {
    var self = this;
    logger.debug('##### USB PLUG OUT ERROR');
    self.disconnect('appEnd');
  }
});

var RVAClient = function(screen, lang) {
  console.time('RVAClient startup, lang: ' + lang);
  var self = this;
  rvaSelf = this;
  // Init
  logger.debug('init RVA client, lang: ' + lang);
  logger.debug('data path: ' + settings.dataPath);

  // set update server IP based on language
  if (lang.toUpperCase() === 'ZH-HANS' || lang.toUpperCase() === 'ZH-CN') {
    logger.debug('call updateChina');
    autoUpdate = new updateChina.Update();
  } else {
    logger.debug('call update');
    autoUpdate = new update.Update();
  }
  autoUpdate.on('downloadProgress', function(progress) {
    self.emit('newVersionDownloadProgress', progress);
  });

  autoUpdate.on('updateStarted', function() {
    logger.debug('updateStarted, shutting down DSA now...');
    self.disconnect('appEnd', function() {
      self.emit('shutDownDSA');
    });
  });

  videoWebServer = new VideoWebServer();
  //videoWebServer.start(settings.HTTP_PORT, 'D:\\testVideo\\Spy_2015_1080p_WEB-DL-MPEG-4_AVC_AAC_2_0_H264_KayOs_).mp4', function() {});

  if (screen) {
    if (typeof screen.width !== 'undefined') {
      settings.screenWidth = screen.width;
      settings.screenHeight = screen.height;
    } else if (typeof screen.length !== 'undefined') {
      if (screen.length > 0) {
        for (var i = 0; i < screen.length; i++) {
          if (screen[i].bounds.x === 0 && screen[i].bounds.y === 0) {
            settings.screenWidth = screen[i].bounds.width;
            settings.screenHeight = screen[i].bounds.height;
          }
        }
      } else {
        settings.screenWidth = 0;
        settings.screenHeight = 0;
      }
    } else {
      settings.screenWidth = 0;
      settings.screenHeight = 0;
    }
  }
  //console.log('screen size: w' + settings.screenWidth + ' h' + settings.screenHeight);
  //console.log('screen size: w' + settings.screenWidth + ' h' + settings.screenHeight);

  db.find({
    documentType: 'settings'
  }, function(err, docs) {
    if (docs.length === 0) {
      var initialDoc = {
        'documentType': 'settings',
        'projectionMode': 0,
        'videoQuality': 0,
        'refreshRate': 0,
        'remoteMouse': 0,
        'teacherCredentialEnabled': false,
        'teacherCredentialPassword': '',
        'lookupServer': '',
      };

      db.insert(initialDoc, function(err) {
        if (err !== null) {
          logger.error('RVAClient constructor' + JSON.stringify(err));
          this.emit('error', err);
        }
        logger.debug('Initial settings created!');
      });
    }
  });
  /*
      self.getTeacherCredentialPassword(function (result) {
        self.emit('teacherCredentialPassword', result);
      });

      self.getTeacherCredentialSetting(function (result) {
        self.emit('teacherCredentialSetting', result);
      });
    */

  nativeService.dsaServer.on('launcherCommand', function(command) {
    var couldSend = false;
    if (((settings.rvaType === 'EDU' && userUtil.amIHost(settings.client_id)) ||
        (settings.rvaType === 'CORP' && !settings.isCorpModeratorOn) ||
        (settings.rvaType === 'CORP' && userUtil.amIHost(settings.client_id))) && settings.isConnectionApproval) {
      couldSend = true;
    }
    switch (command) {
      case settings.LAUNCHER_CMD.FULL_SCREEN:
        logger.debug('received launcherCommand: FULL_SCREEN');
        if (RVAConnected) {
          if (couldSend) {
            if (nextCommand.length > 0) {
              if (nextCommand[nextCommand.length - 1] == 0) {
                addTime = true;
                logger.debug("##### Ignore command 0");
              } else {
                addTime = false;
                nextCommand.push(0);
              }
            } else {
              addTime = false;
              nextCommand.push(0);
              if (nextCommand.length > 0 && settings.isLauncherCMDBack) {
                self.sendNextLauncherCommand();
              }
            }
          }
        } else {
          if (isSendingConnected) {
            logger.debug('##### alreay send');
          } else {
            isSendingConnected = true;
            self.emit('connnectRVA');
          }
        }
        break;
      case settings.LAUNCHER_CMD.SPLIT_1:
        logger.debug('received launcherCommand: SPLIT_1 ');
        if (RVAConnected) {
          if (couldSend) {
            if (nextCommand.length > 0) {

              if (nextCommand[nextCommand.length - 1] == 1) {
                addTime = true;
                logger.debug("##### Ignore command 1");
              } else {
                nextCommand.push(1);
                addTime = false;
              }
            } else {
              addTime = false;
              nextCommand.push(1);
              if (nextCommand.length > 0 && settings.isLauncherCMDBack) {
                self.sendNextLauncherCommand();
              }
            }
          }
        }
        break;
      case settings.LAUNCHER_CMD.SPLIT_2:
        logger.debug('received launcherCommand: SPLIT_2 ');
        if (RVAConnected) {
          if (couldSend) {
            if (nextCommand.length > 0) {

              if (nextCommand[nextCommand.length - 1] == 2) {
                addTime = true;
                logger.debug("##### Ignore command 2");
              } else {
                addTime = false;
                nextCommand.push(2);
              }
            } else {
              addTime = false;
              nextCommand.push(2);
              if (nextCommand.length > 0 && settings.isLauncherCMDBack) {
                self.sendNextLauncherCommand();
              }
            }
          }
        }
        break;
      case settings.LAUNCHER_CMD.SPLIT_3:
        logger.debug('received launcherCommand: SPLIT_3 ');
        if (RVAConnected) {
          if (couldSend) {
            if (nextCommand.length > 0) {

              if (nextCommand[nextCommand.length - 1] == 3) {
                addTime = true;
                logger.debug("##### Ignore command 3");
              } else {
                addTime = false;
                nextCommand.push(3);
              }
            } else {
              addTime = false;
              nextCommand.push(3);
              if (nextCommand.length > 0 && settings.isLauncherCMDBack) {
                self.sendNextLauncherCommand();
              }
            }
          }
        }
        break;
      case settings.LAUNCHER_CMD.SPLIT_4:
        logger.debug('received launcherCommand: SPLIT_4 ');
        if (RVAConnected) {
          if (couldSend) {
            if (nextCommand.length > 0) {
              if (nextCommand[nextCommand.length - 1] == 4) {
                addTime = true;
                logger.debug("##### Ignore command 4");
              } else {
                addTime = false;
                nextCommand.push(4);
              }
            } else {
              addTime = false;
              nextCommand.push(4);
              if (nextCommand.length > 0 && settings.isLauncherCMDBack) {
                self.sendNextLauncherCommand();
              }
            }
          }
        }
        break;
      case settings.LAUNCHER_CMD.DISCONNECT:
        if (RVAConnected) {
          settings.isLauncherCMDBack = true;
          logger.debug('received launcherCommand: DISCONNECT');
          self.emit('disconnnectRVA');
          //self.disconnect('');
        }
        break;
      case settings.LAUNCHER_CMD.USB_PLUG_IN:
        logger.debug('received launcherCommand: USB_PLUG_IN');
        settings.isLauncherCMDBack = true;
        qLauncherExists = true;
        if (RVAConnected) {
          self.restoreQLauncherLocation();
        }
        break;
      case settings.LAUNCHER_CMD.USB_PLUG_OUT:
        logger.debug('received launcherCommand: USB_PLUG_OUT');
        qLauncherExists = false;
        if (settings.currentDevice.type === settings.deviceType.USB) {
          logger.debug('##### USB PLUG OUT ExIT');
          self.disconnect('appEnd');
        }
        break;
      default:
        break;
    }
    logger.debug("##### COMMAND LIST = " + nextCommand);
  });

  nativeService.dsaServer.on('ksrv_error', function(ks_error) {
    logger.debug('######## KS RV ERROR = ' + JSON.stringify(ks_error) + ' ' + ks_error.toString());
    if ((ks_error[9] === 128 && ks_error[10] === 9 && ks_error[11] === 3 && ks_error[12] === 9) || (ks_error[9] === 128 && ks_error[10] === 16 && ks_error[11] === 49 && ks_error[12] === 6)) {
      logger.debug('Extend screen error');
      if (settings.currentScreen === settings.screenType.MAIN) {
        self.extendScreen(0, 0);
      } else if (settings.currentScreen === settings.screenType.EXT) {
        self.extendScreen(1, 0);
      }
    } else if (ks_error.toString().indexOf('0x00000114')) {
      logger.debug('!!!  0x00000114 rva is ready for next command, only issue streaming stop after this event');
    } else {
      clearTimeout(ks_rvTimeout);
      ks_rvTimeout = setTimeout(function() {
        if (isStateChange) {
          logger.debug('##### STATE CHANGE IGNORE ERROR');
          isStateChange = false;
        } else {
          logger.debug('##### NATIVE VIDEO / IMAGE DISCONNECT -> RECONNECT');
          settings.needReconnection = true;
          settings.disconnectEvent = 'Reconnection';
          RVACommandClient.destroy();
          //Setup a final timeout for reconnection
          reconnectionTimeout = setTimeout(function() {
            self.checkReconnectionTimeout();
          }, 25000);

          connectionRetry = setInterval(function() {
            logger.debug('##### Start Reconnection');
            if (settings.needReconnection) {
              if (!RVACommandClient.destroyed) {
                RVACommandClient.destroy();
              }
              if (settings.pinRequired) {
                self.connect(settings.rvaIp, settings.clientName, settings.pinCode);
              } else {
                self.connect(settings.rvaIp, settings.clientName, null);
              }
            } else {
              logger.debug('##### clean connectionRetry');
              clearInterval(connectionRetry);
            }
          }, 5000);
        }
      }, 2000);
    }


  });
  console.timeEnd('RVAClient startup');
};
util.inherits(RVAClient, EventEmitter);

RVAClient.prototype.checkNewVersion = function checkNewVersion(err, cb) {
  logger.debug('1 checkNewVersion called');
  autoUpdate.checkNewVersion(function(error) {
    logger.error('autoUpdate.checkNewVersion error: ' + JSON.stringify(error));
    err(error);
  }, function(result) {
    logger.debug('1 result: ' + JSON.stringify(result));
    if (result.Info_id === '0') {
      logger.debug('1 no new version is available');
      cb(false);
    } else {
      logger.debug('1 An upgrade is available... ', result);
      cb(true);
    }
  });
};

RVAClient.prototype.beginDownloadUpdate = function beginDownloadUpdate(error, cb) {
  logger.debug('beginDownloadUpdate called');
  settings.isLauncherCMDBack = false;
  autoUpdate.downloadNewVersion(function(err) {
    logger.error('downloadNewVersion exception: ' + JSON.stringify(err));
    //error(err);
    cb(false);
  }, updateVersion, function(result) {
    logger.debug('download completed, ', result);
    cb(true);
  });
};

RVAClient.prototype.cancelDownloadUpdate = function cancelDownloadUpdate() {
  logger.debug('cancelDownloadUpdate called ');
  settings.isLauncherCMDBack = true;
  autoUpdate.cancelDownload();
};

RVAClient.prototype.getTeacherCredentialSetting = function getTeacherCredentialSetting(result) {
  db.find({
    documentType: 'settings'
  }, function(error, docs) {
    var temp = '';
    if (error)
      logger.error('getTeacherCredentialSetting' + JSON.stringify(error));

    if (docs.length > 0) {
      docs.forEach(function(item) {
        temp = item.teacherCredentialEnabled;
      });
      logger.debug('getTeacherCredentialSetting: ' + temp);
      result(temp);
    } else {
      result(temp);
    }
  });
};

RVAClient.prototype.setTeacherCredentialSetting = function setTeacherCredentialSetting(teacherCredentialEnabled) {
  var self = this;
  var field = {
    name: 'teacherCredentialEnabled',
    value: teacherCredentialEnabled
  };
  self.set(field);
};

RVAClient.prototype.getTeacherCredentialPassword = function getTeacherCredentialPassword(result) {
  db.find({
    documentType: 'settings'
  }, function(error, docs) {
    var temp = '';
    if (error)
      logger.error('getTeacherCredentialPassword' + JSON.stringify(error));

    if (docs.length > 0) {
      docs.forEach(function(item) {
        temp = item.teacherCredentialPassword;
      });
      result(temp);
    } else {
      result(temp);
    }
  });
};

RVAClient.prototype.setTeacherCredentialPassword = function setTeacherCredentialPassword(teacherCredentialPassword) {
  var self = this;
  var field = {
    name: 'teacherCredentialPassword',
    value: teacherCredentialPassword
  };
  self.set(field);
};

RVAClient.prototype.connectRvaProxy1 = function(rvaIp, videoUrl, done) {
  var self = this;
  global.gc();
  logger.debug('init RVAProxy1');
  if (typeof(RVAProxy1) !== 'undefined') {
    logger.debug('RVAProxy1 is defined, clearing proxy1');
    logger.debug('RVAProxy1.listenerCount: ' + EventEmitter.listenerCount(RVAProxy1, 'data'));
    RVAProxy1.destroy();
    RVAProxy1.removeAllListeners('data');
    global.gc();
    logger.debug('RVAProxy1.listenerCount after remove: ' + EventEmitter.listenerCount(RVAProxy1, 'data'));
    RVAProxy1 = null;
  }

  if (typeof(client) !== 'undefined') {
    logger.debug('client is defined, clearing proxy1');
    client.destroy();
    client.removeAllListeners('data');
    global.gc();
    client = null;
  }

  client = net.Socket();
  RVAProxy1 = new net.Socket();
  RVAProxy1.setNoDelay(true);
  RVAProxy1.connect(settings.STREAMING_CONNECTION_PORT, rvaIp, function connect() {
    logger.debug('RVA Proxy 1 Connection connected, STREAMING_CONNECTION_PORT: ' + settings.STREAMING_CONNECTION_PORT + ' rvaIp:' + rvaIp);
    done();
  });

  RVAProxy1.on('data', function(data) {
    logger.debug('RVA Proxy 1 Connection data received, data:' + data);
    client.connect(settings.HTTP_PORT, 'localhost', function connect() {
      client.write(data);
      client.on('data', function(httpdata) {
        if (RVAProxy1.writable) {
          RVAProxy1.write(httpdata, 'binary', function(err) {
            if (err) {
              httpdata = null;
              logger.debug('kill client and proxy1');
              //global.gc();
              //  client.end();
              client.removeAllListeners('data');
              client.destroy();
              //  RVAProxy1.end();
              RVAProxy1.removeAllListeners('data');
              RVAProxy1.destroy();
              global.gc();

            }
            /* else {
              logger.debug('data err: ' + err);
              logger.debug('data result: ' + result);
            }*/
          });
          /*
          RVAProxy1.write(httpdata, function(err, result) {
            logger.debug('data result: ' + result);
            if (err) {
              httpdata = null;
              client.end();
              RVAProxy1.end();
              RVAProxy1.destroy();
            }
          });
          */
        }
      });
    });
    client.on('error', function(err) {
      logger.debug('http client error! ' + JSON.stringify(err));
      //global.gc();
      client.removeAllListeners('data');
      client.end();
      RVAProxy1.removeAllListeners('data');
      RVAProxy1.end();
      RVAProxy1.destroy();
    });
    client.once('close', function() {
      logger.debug('proxy 1 http client clsoed!');
      client.removeAllListeners('data');
      client.destroy();
    });
  });

  RVAProxy1.once('error', function(err) {
    logger.error('RVA Proxy 1 Connection error' + JSON.stringify(err));
    //global.gc();
    if (client) {
      client.removeAllListeners('data');
      client.destroy();
    }
    RVAProxy1.removeAllListeners('data');
    RVAProxy1.end();
    RVAProxy1.destroy();
  });

  RVAProxy1.once('end', function() {
    logger.debug('RVA Proxy 1 Connection end received');
    if (client) {
      client.removeAllListeners('data');
      client.destroy();
    }
  });

  RVAProxy1.once('close', function() {
    logger.debug('RVA Proxy 1 Connection closed, localVideoPlaying ' + localVideoPlaying);
    if (localVideoPlaying && RVAProxy1.destroyed && process.platform === 'win32') {
      logger.debug('Reconnect proxy 1 (3)');
      self.connectRvaProxy1(rvaIp, videoUrl, function() {});
    } else {
      if (client) {
        client.removeAllListeners('data');
        client.destroy();
      }
      RVAProxy1.removeAllListeners('data');
      RVAProxy1.destroy();
    }
  });
};

RVAClient.prototype.connectRvaProxy2 = function(rvaIp, videoUrl, done) {
  var self = this;
  global.gc();
  logger.debug('init RVAProxy2');
  if (typeof(RVAProxy2) !== 'undefined') {
    logger.debug('RVAProxy2.listenerCount: ' + EventEmitter.listenerCount(RVAProxy2, 'data'));
    RVAProxy2.destroy();
    RVAProxy2.removeAllListeners('data');
    global.gc();
    RVAProxy2 = null;
  }
  if (typeof(client2) !== 'undefined') {
    client2.destroy();
    client2.removeAllListeners('data');
    global.gc();
    client2 = null;
  }
  client2 = net.Socket();
  RVAProxy2 = new net.Socket();
  RVAProxy2.setNoDelay(true);
  RVAProxy2.connect(settings.STREAMING_CONNECTION_PORT, rvaIp, function connect() {
    logger.debug('RVA Proxy 2 Connection connected');
    done();
  });

  RVAProxy2.on('data', function(data) {
    logger.debug('RVA Proxy2 Connection data received');
    client2.connect(settings.HTTP_PORT, 'localhost', function connect() {
      client2.write(data);
      client2.on('data', function(httpdata2) {
        if (RVAProxy2.writable) {
          RVAProxy2.write(httpdata2, 'binary', function(err) {
            if (err) {
              httpdata2 = null;
              logger.debug('kill client and proxy2');
              client2.end();
              client2.destroy();
              RVAProxy2.end();
              RVAProxy2.destroy();
            }
            /* else {
              logger.debug('data 2 err: ' + err);
              logger.debug('data 2 result: ' + result);
            }*/
          });
        }
      });
    });
    client2.on('error', function(err) {
      //global.gc();
      logger.debug('http client2 error! ' + JSON.stringify(err));
      client2.end();
      RVAProxy2.end();
      RVAProxy2.destroy();
    });
    client2.once('close', function() {
      logger.debug('http client2 closed!');
      client2.destroy();
    });
  });

  RVAProxy2.once('error', function(err) {
    //global.gc();
    logger.error('RVA Proxy 2 Connection error' + JSON.stringify(err));
    if (client2) {
      client2.destroy();
    }
    RVAProxy2.end();
    RVAProxy2.destroy();
  });

  RVAProxy2.once('end', function() {
    logger.debug('RVA Proxy 21 Connection end received');
    if (client2) {
      client2.destroy();
    }
  });

  RVAProxy2.once('close', function() {
    logger.debug('RVA Proxy 2 Connection closed , localVideoPlaying ' + localVideoPlaying);
    if (localVideoPlaying && RVAProxy2.destroyed && process.platform === 'win32') {
      logger.debug('Reconnect proxy 2 (3)');
      self.connectRvaProxy2(rvaIp, videoUrl, function() {});
    } else {
      if (client2) {
        client2.destroy();
      }
      RVAProxy2.destroy();
    }
  });
};

RVAClient.prototype.getDsaIp = function getDsaIp() {
  var self = this;
  logger.debug('rva ip ' + settings.rvaIp);
  var RvaIP = settings.rvaIp.split('.');
  var compare = RvaIP[0] + '.' + RvaIP[1] + '.' + RvaIP[2] + '.';
  var interfaces = os.networkInterfaces();
  var addresses = [];
  for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
      var address = interfaces[k][k2];
      if (address.family === 'IPv4' && !address.internal) {
        if (address.address.lastIndexOf(compare, 0) === 0)
          addresses.push(address.address);
      }
    }
  }
  if (addresses.length > 0)
    return addresses[0];
  else {
    logger.error('Cannot Detect DSA IP');
    self.emit('error', new Error('Cannot Detect DSA IP'));
  }
};

RVAClient.prototype.failedToConnect = function failedToConnect() {
  logger.debug('firing failedToConnect event');
  if (settings.launcherEnabled === true && qLauncherExists) {
    isSendingConnected = false;
    nativeService.ledOff(settings.LED.ALL, function() {
      logger.debug('LEDs are all off, start gradient effect now');
      nativeService.ledGradientFlash(settings.LED.FULL_SCREEN_RED, 5, 3000, function() {
        ledTimeout = setTimeout(function() {
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledOn(settings.LED.FULL_SCREEN_RED, 0);
          });
        }, 25000);
      });
    });
  }
  settings.disconnectEvent = 'ConnectionFail';
  RVACommandClient.destroy();
};

RVAClient.prototype.connect = function connect(rvaIp, clientName, pin, numberOfScreens) {
  var self = this;
  nativeService.saveDefaultExtendMode();
  settings.numberOfScreens = numberOfScreens;
  settings.isConnectionApproval = false;
  /*
    nativeService.setAudioMute(function(cb) {
      logger.debug('Mute: ' + cb);
    });
  */
  if (settings.launcherEnabled === true && qLauncherExists) {
    clearTimeout(ledTimeout);
    nativeService.ledOff(settings.LED.All, function() {
      nativeService.ledCycleFlash(100, 200);
    });
  }

  if (settings.needReconnection) {
    logger.debug('##### reconnection call connect');
    //Reconncetion does not need timeout check
  } else {
    clearTimeout(connectTimeout);
    connectTimeout = setTimeout(function() {
      self.failedToConnect(rvaIp);
    }, 5000);
    settings.pinCode = pin;
  }

  settings.rvaIp = rvaIp;
  settings.clientName = clientName;

  if (pin === null)
    settings.pinRequired = false;
  else
    settings.pinRequired = true;

  RVACommandClient = new net.Socket();
  RVACommandClient.setNoDelay(true);
  RVACommandClient.connect(settings.TCP_CONNECTION_PORT, settings.rvaIp, function connect() {
    self.sendCmdConnectionRequest(this);
  });

  RVACommandClient.on('connect', function() {
    logger.debug('RVA cmd port connected, clearing connection timeout');
    clearTimeout(connectTimeout);
  });

  RVACommandClient.on('data', function(data) {
    var receiveBuffer = new Uint8Array(data);
    handleBuffer.push.apply(handleBuffer, receiveBuffer);

    var sliceByte = 0;
    while (handleBuffer.length > 0) {
      sliceByte = self.receive_CMD_MESSAGE(handleBuffer, 0, this);
      if (sliceByte > handleBuffer.length) {
        logger.debug('Need more data');
        break;
      } else {
        handleBuffer.splice(0, sliceByte);
        if (handleBuffer.length === 0)
          break;
      }
    }
  });

  RVACommandClient.on('error', function(data) {
    logger.debug('##### RVAClient ERROR = ' + JSON.stringify(data.code));
    if (data.code === 'ECONNREFUSED' || data.code === 'ENETUNREACH') {
      settings.disconnectEvent = 'ConnectionFail';
      if (settings.launcherEnabled === true && qLauncherExists) {
        nativeService.ledOff(settings.LED.All, function() {
          nativeService.ledOn(settings.LED.FULL_SCREEN_RED, 20, function() {});
        });
      }
    } else if (data.code === 'ECONNRESET') {
      settings.disconnectEvent = 'ConnectionReset';
      if (settings.launcherEnabled === true && qLauncherExists) {
        nativeService.ledOff(settings.LED.All, function() {
          nativeService.ledOn(settings.LED.FULL_SCREEN_RED, 20, function() {});
        });
      }
    } else {
      settings.disconnectEvent = 'Disconnection';
    }
    logger.error('RVACommandClient Connection error');
  });

  RVACommandClient.on('end', function() {
    settings.disconnectEvent = 'Disconnection';
    logger.debug('RVA Command Connection end received');
  });

  RVACommandClient.on('close', function() {
    logger.debug('##### Close CMD Connection');
    var doc = {};
    switch (settings.disconnectEvent) {
      case 'ConnectionReset':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'connectReset'
        };
        break;
      case 'Reconnection':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'reconnect'
        };
        break;
      case 'Disconnection':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'disconnected'
        };
        break;
      case 'ConnectionFail':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'failedToConnect'
        };
        break;
      case 'reconnectFailed':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'Disconnection'
        };
        break;
      case 'invalidRVAVersion':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'invalidRVAVersion'
        };
        break;
      case 'PIN_INVALID':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'PIN_INVALID'
        };
        break;
      case 'NAME_NOT_IN_GROUP':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'NAME_NOT_IN_GROUP'
        };
        break;
      case 'DUPLICATED_NAME_IN_GROUP':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'DUPLICATED_NAME_IN_GROUP'
        };
        break;
      case 'REACH_MAX_NUMBER_USERS':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'REACH_MAX_NUMBER_USERS'
        };
        break;
      case 'NOVOCAST_REACH_MAX_NUMBER_USERS':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'NOVOCAST_REACH_MAX_NUMBER_USERS'
        };
        break;
      case 'applicationEnd':
        doc = {
          rvaIp: settings.rvaIp,
          port: settings.TCP_CONNECTION_PORT,
          event: 'applicationEnd'
        };
        break;
      default:
        break;
    }
    self.emit('disconnected', doc);

    if (settings.needReconnection) {
      // Reconnection will send old client id
    } else {
      //initial parameters
      settings.client_id = 0;
      settings.groupMode = false;
      settings.group = '';
      settings.screen_number = -2;
      settings.needReconnection = false;
      videoPlayTimeUpdated = false;

      //Check Data Exchange status
      settings.isPreview = false;
      settings.isGroupUpload = false;
      settings.isVotingPolling = false;
      settings.isFileSharing = false;
      settings.isFileDownloading = false;
      settings.isReadyToSend = true;
      settings.isClosingSendFilepage = false;
      settings.isSendToCancel = false;
      settings.try_times = 0;
      settings.playMode = 0;
      settings.isGrayoutAll = false;
      dEHBufer = [];

      //Q Launcher
      nativeService.ledOff(settings.LED.ALL);
      RVAConnected = false;

      userUtil.user_list = [];
      userUtil.group_list = [];
    }

    settings.fullMode = 0;
    settings.splitMode = 0;
    lastRequestTime = 0;
    thisRequestTime = 0;

    clearTimeout(keepAliveScheduler);
    clearTimeout(keepAliveTimeout);


    handleBuffer = [];
    settings.isConnectionApproval = true;
  });
};

RVAClient.prototype.receive_CMD_MESSAGE = function receive_CMD_MESSAGE(byteValues, startByte, client) {
  //To Check CMD is alive
  settings.keepAliveBack = true;
  var self = this;
  var msgLength = byteValues[startByte];
  var cmd = byteValues[startByte + 1];
  var userId = byteValues[startByte + 2];
  var arg = byteValues[startByte + 3];

  // if (cmd !== common.CMD.KEEP_ALIVE && cmd !== common.CMD.STREAMING_PLAY_TIME_UPDATE) {
  if (cmd === common.CMD.KEEP_ALIVE)
    logger.debug('receive_CMD_MESSAGE msgLength:' + msgLength + ' cmd:' + cmd + ' userId:' + userId + ' arg:' + arg);
  // }

  //if(1==0)
  switch (cmd) {
    case common.CMD.REMOTE_MOUSE_CONTROL:
      logger.verbose('Receive CMD REMOTE_MOUSE_CONTROL');
      self.receiveRemoteMouseControl(byteValues, startByte);
      break;
    case common.CMD.KEEP_ALIVE:
      // logger.debug('received keepalive');
      if (settings.enableKeepAlive) {
        self.receiveCmdKeepAlive();
      } else {
        logger.debug('keepalive disabled, but received keepalive from RVA!');
      }
      break;
    case common.CMD.VIEWER_REQUEST:
      logger.verbose('Receive CMD VIEWER_REQUEST');
      self.receive_CMD_VIEWER_REQUEST(byteValues, startByte);
      break;
    case common.CMD.MODERATOR_VIEWER_REQUEST:
      logger.verbose('Receive CMD MODERATOR_VIEWER_REQUEST');
      msgLength = self.receiveCmdModeratorViewerRequest(byteValues, startByte);
      break;
    case common.CMD.MODERATOR_VIEWER_STATE_CHANGE:
      logger.verbose('Receive CMD MODERATOR_VIEWER_STATE_CHANGE');
      self.receive_CMD_MODERATOR_VIEWER_STATE_CHANGE(byteValues, startByte);
      break;
    case common.CMD.REQUEST_TEACHER_PRIORITY:
      logger.verbose('Receive CMD CMD_REQUEST_TEACHER_PRIORITY');
      self.receive_CMD_REQUEST_TEACHER_PRIORITY(byteValues, startByte);
      break;
    case common.CMD.SCREEN_RESOLUTION:
      logger.verbose('Receive CMD SCREEN_RESOLUTION, rva version: ' + settings.rva_version);
      logger.debug('rva number:' + parseFloat(settings.rva_version.replace('v', '')));
      // if(parseFloat(settings.rva_version.replace('v', '')) < 2.0){
      self.receive_CMD_SCREEN_RESOLUTION(byteValues, startByte);
      // }
      break;
    case common.CMD.NETWORK_MODE:
      logger.verbose('Receive CMD NETWORK_MODE');
      self.receiveCmdNetworkMode(byteValues, startByte);
      break;
    case common.CMD.PIN_VALUE:
      logger.verbose('Receive CMD PIN_VALUE');
      self.receiveCmdPinValue(byteValues, startByte);
      break;
    case common.CMD.RVA_EDUCATION:
      logger.verbose('Receive CMD RVA_EDUCATION');
      if (settings.rvaType !== 'NOVOCAST') {
        settings.rvaType = 'EDU';
      }
      break;
    case common.CMD.RVA_ENTERPRISE_V2:
      logger.verbose('Receive CMD RVA_ENTERPRISE_V2');
      if (settings.rvaType !== 'NOVOCAST') {
        settings.rvaType = 'CORP';
      }
      break;
    case common.CMD.DISPLAY_SETTING:
      logger.verbose('Receive CMD DISPLAY_SETTING');
      self.receiveCmdDisplaySetting(byteValues, startByte);
      break;
    case common.CMD.DISPLAY_LANGUAGE:
      logger.verbose('Receive CMD DISPLAY_LANGUAGE');
      break;
    case common.CMD.RVA_VERSION:
      logger.verbose('Receive CMD RVA_VERSION, rva version: ' + settings.rva_version);
      self.receiveCmdRvaVersion(byteValues, startByte);
      break;
    case common.CMD.RVA_EDITION:
      logger.verbose('Receive CMD RVA_EDITION, rva edition: ' + JSON.stringify(byteValues));
      //self.receiveCmdRvaVersion(byteValues, startByte);
      break;
    case common.CMD.BSP_VERSION:
      logger.verbose('Receive CMD BSP_VERSION');
      self.receiveCmdBspVersion(byteValues, startByte);
      break;
    case common.CMD.NVC_MODEL:
      logger.verbose('Receive CMD NVC_MODEL');
      self.receiveCmdNvcModel(byteValues, startByte);
      break;
    case common.CMD.DEVICE_NAME:
      logger.verbose('Receive CMD DEVICE_NAME');
      self.receiveCmdDeviceName(byteValues, startByte);
      break;
    case common.CMD.PIN_REQUIRED:
      logger.verbose('Receive CMD PIN_REQUIRED');
      self.receiveCmdPinRequired(client);
      break;
    case common.CMD.PIN_NOT_REQUIRED:
      logger.verbose('Receive CMD PIN_NOT_REQUIRED');
      self.receiveCmdPinNotRequired(client);
      break;
    case common.CMD.UPLOAD_GROUP_DATA:
      logger.verbose('Receive CMD UPLOAD_GROUP_DATA');
      msgLength = self.receive_CMD_UPLOAD_GROUP_DATA(byteValues, startByte);
      break;
    case common.CMD.QR_CODE_IMAGE:
      logger.verbose('Receive CMD.QR_CODE_IMAGE');
      msgLength = self.receiveCmdQRCodeImage(byteValues, startByte);
      break;
    case common.CMD.CM_RECONNECTION_ALLOWED:
      logger.verbose('Receive CMD CM_RECONNECTION_ALLOWED');
      self.receive_CMD_RECONNECTION_ALLOWED();
      break;
    case common.CMD.CM_CONNECTION_APPROVAL:
      logger.verbose('Receive CMD CM_CONNECTION_APPROVAL');
      self.receiveCmdConnectionApproval(byteValues, startByte, client);
      break;
    case common.CMD.SPLIT_SCREEN_READY:
      logger.verbose('Receive CMD SPLIT_SCREEN_READY');
      self.receive_CMD_SPLIT_SCREEN_READY();
      break;
    case common.CMD.FULL_SCREEN_READY:
      logger.verbose('Receive CMD FULL_SCREEN_READY');
      self.receive_CMD_FULL_SCREEN_READY();
      break;
    case common.CMD.MODERATOR_BROADCAST_REQUEST:
      logger.verbose('Receive CMD MODERATOR_BROADCAST_REQUEST');
      self.receive_CMD_MODERATOR_BROADCAST_REQUEST();
      break;
    case common.CMD.CM_RESET_RVA:
      logger.verbose('Receive CMD CM_RESET_RVA');
      self.receive_CMD_RESET_RVA();
      break;
    case common.CMD.CM_RESET_DEVICE:
      logger.verbose('Receive CMD CM_RESET_DEVICE');
      self.receive_CMD_RESET_DEVICE();
      break;
    case common.CMD.MODERATOR_REQUEST:
      logger.verbose('Receive CMD MODERATOR_REQUEST');
      self.receive_CMD_MODERATOR_REQUEST();
      break;
    case common.CMD.MODERATOR_APPROVED:
      logger.verbose('Receive CMD MODERATOR_APPROVED');
      self.receive_CMD_MODERATOR_APPROVED();
      break;
    case common.CMD.MODERATOR_DENIED:
      logger.verbose('Receive CMD MODERATOR_DENIED');
      self.receive_CMD_MODERATOR_DENIED();
      break;
    case common.CMD.SCREENSHOT_REQUEST:
      logger.verbose('Receive CMD SCREENSHOT_REQUEST');
      //self.receive_CMD_SCREENSHOT_REQUEST();
      break;
    case common.CMD.SCREENSHOT_RESPONSE:
      logger.verbose('Receive CMD SCREENSHOT_RESPONSE');
      //self.receive_CMD_SCREENSHOT_RESPONSE();
      break;
    case common.CMD.SCREEN_LOCK_UNLOCK: // pending
      self.receive_CMD_SCREEN_LOCK_UNLOCK(byteValues);
      logger.verbose('Receive CMD SCREEN_LOCK_UNLOCK');
      break;
    case common.CMD.CM_VIEWER_PAUSE:
      logger.verbose('Receive CMD CM_VIEWER_PAUSE');
      self.receive_CMD_VIEWER_PAUSE();
      break;
    case common.CMD.CM_VIEWER_RESUME:
      logger.verbose('Receive CMD CM_VIEWER_RESUME');
      self.receive_CMD_VIEWER_RESUME();
      break;
    case common.CMD.VIEWER_RELEASE:
      logger.verbose('Receive CMD VIEWER_RELEASE');
      self.receive_CMD_VIEWER_RELEASE();
      break;
    case common.CMD.VIEWER_APPROVED:
      logger.verbose('Receive CMD VIEWER_APPROVED');
      self.receive_CMD_VIEWER_APPROVED(byteValues, startByte);
      break;
    case common.CMD.CM_VIEWER_STARTED:
      logger.verbose('Receive CMD CM_VIEWER_STARTED');
      self.receiveCmdViewerStarted();
      break;
    case common.CMD.REMOVE_REQUEST:
      logger.verbose('Receive CMD REMOVE_REQUEST');
      self.receiveCmdRemoveRequest(byteValues);
      break;
    case common.CMD.CM_INVALID_CMD:
      logger.verbose('Receive CMD CM_INVALID_CMD');
      self.receive_CMD_INVALID_CMD(byteValues, startByte);
      break;
    case common.CMD.CLIENT_STATUS_REFRESH:
      logger.verbose('Receive CMD CLIENT_STATUS_REFRESH');
      msgLength = self.receive_CMD_CLIENT_STATUS_REFRESH(byteValues, startByte);
      break;
    case common.CMD.PLAY_VIDEO:
      logger.verbose('Receive CMD PLAY_VIDEO');
      self.receive_CMD_PLAY_VIDEO(byteValues);
      break;
    case common.CMD.PLAY_YOUTUBE:
      logger.verbose('Receive CMD PLAY_YOUTUBE');
      self.receive_CMD_PLAY_YOUTUBE(byteValues);
      break;
    case common.CMD.YOUTUBE_HTTP_STREAMING_SETUP:
      logger.verbose('Receive CMD YOUTUBE_HTTP_STREAMING_SETUP');
      self.playVideo(byteValues, startByte);
      break;
    case common.CMD.POLLING_VOTING:
      logger.verbose('Receive CMD POLLING_VOTING');
      // msgLength = receive_CMD_POLLING_VOTING(byteValues, startByte);
      break;
    case common.CMD.CM_STATUS:
      logger.verbose('Receive CM_STATUS');
      msgLength = self.receiveCMStatus(byteValues, startByte);
      break;
    case common.CMD.SETTINGS_PASSWORD:
      logger.verbose('Receive SETTINGS_PASSWORD');
      self.receiveSettingsPassword(byteValues, startByte);
      break;
    case common.CMD.PREVIEW_DATA:
      logger.verbose('Receive PREVIEW_DATA');
      self.sendPreviewImage();
      break;
    case common.CMD.STREAMING_PLAY_TIME_UPDATE:
      //logger.verbose('Receive STREAMING_PLAY_TIME_UPDATE');
      self.handleStreamingPlayTimeUpdate(byteValues, startByte);
      break;
    case common.CMD.POLLING_VOTING_V2:
      self.receivePollingVotingV2(byteValues, startByte);
      break;
    case common.CMD.MODERATOR_RELEASED:
      self.receive_CMD_MODERATOR_RELEASED();
      break;
    case common.CMD.FILE_SHARING_DATA:
      self.receive_CMD_FILE_SHARING_DATA();
      break;
    case common.CMD.VIEWER_DENIED:
      self.receive_CMD_VIEWER_DENIED();
      break;
    case common.CMD.REMOTE_CONTROL_CMD:
      self.receive_REMOTE_CONTROL_CMD(byteValues);
      break;
    case common.CMD.URL_SHARING:
      msgLength = self.receive_CMD_URL_SHARING(byteValues, startByte);
      break;
    case common.CMD.BROADCAST_MSG:
      msgLength = self.receive_CMD_BROADCAST_MSG(byteValues, startByte);
      break;
    case common.CMD.RESPONDER:
      self.receive_CMD_RESPONDER(byteValues);
      break;
    case common.CMD.ANNOTATION:
      self.receive_CMD_ANNOTATION(byteValues);
      break;
    default:
      logger.verbose('Receive test CMD NOT_READY (' + cmd + ')');
      logger.debug('NOT_READY:' + common.CMD.SETTINGS_PASSWORD);
      // self.receive_CMD_NOT_READY(byteValues, startByte);
  }
  return startByte + msgLength;
};

RVAClient.prototype.handleStreamingPlayTimeUpdate = function handleStreamingPlayTimeUpdate(byteValues) {
  var self = this;
  /******************************* STREAMING_PLAY_TIME_UPDATE*******************************
    RVA should be sending this command every second after starting playing media file
   byte index:
   byte                   0      total packet length
   byte                   1      message type: STREAMING_PLAY_TIME_UPDATE
   byte                   2-3   short format integer, streaming_time
   ******************************************************************************************/
  var videoDuration = 0;
  //localPlayKeepAlive = null;

  logger.debug('received STREAMING_PLAY_TIME_UPDATE bytes: ' + JSON.stringify(byteValues));

  if (videoPlayStatusCount === 0) {
    totalVideoDuration = 256 * byteValues[2] + byteValues[3];
    logger.debug('RVA total duration: ' + totalVideoDuration + ' settings.localVideoPath:' + settings.localVideoPath);
  } else {
    videoDuration = 256 * byteValues[2] + byteValues[3];
    logger.debug('video play position: ' + videoDuration + ' second, last duration: ' + lastDuration);
    logger.debug('videoPlayPaused:' + videoPlayPaused);
    if (lastDuration === videoDuration) {
      durationDuplicateCount++;
      logger.debug('last duration = ' + lastDuration + ' duraion = ' + videoDuration);
      logger.debug('@@@  duration is the same as last one! ' + durationDuplicateCount + ' times!');
    }
    lastDuration = videoDuration;
  }

  videoPlayStatusCount++;

  if (totalVideoDuration <= 0)
    if (dsaUtil.endsWith(settings.localVideoPath, 'mp3')) {
      logger.debug('media is mp3, caculate length from client side...');
      mp3Duration(settings.localVideoPath, function(err, length) {
        if (err) {
          console.error(err.message);
          logger.error(err.message);
        }
        totalVideoDuration = length;
      });
    }

  var videoPercentage = videoDuration / totalVideoDuration;
  if (totalVideoDuration <= 0) {
    videoPercentage = 0;
    logger.error('totalVideoDuration is equal to or less than 0');
  }

  if (videoDuration < 0) {
    videoPercentage = 0;
  }

  videoPlayMessage = {
    status: 'playing',
    videoTitle: settings.youTubeTitle,
    total: totalVideoDuration,
    percentage: videoPercentage
  };

  if (!videoPlayPaused) {
    self.emit('videoPlayStatus', videoPlayMessage);
  }

  logger.debug('videoPlayMessage:' + JSON.stringify(videoPlayMessage));
  logger.debug('videoPlayStatusCount: ' + videoPlayStatusCount);
  logger.debug('durationDuplicateCount: ' + durationDuplicateCount);
  videoPlayTimeUpdated = true;
};

RVAClient.prototype.sendPreviewImage = function sendPreviewImage() {
  var self = this;
  logger.debug('##### sending preview image to moderator 279x174');
  nativeService.getPreviewImage(279, 174, settings.currentScreen, function(result) {
    self.openDataServerSocket(null, result, common.CMD.PREVIEW_DATA);
  });
};

RVAClient.prototype.receiveCmdRemoveRequest = function receiveCmdRemoveRequest(bValues) {
  var self = this;
  logger.verbose('Process CMD REMOVE REQUEST');
  var uid = bValues[2];
  if (uid !== null) {
    userUtil.remove_userdata(uid);
  }
  self.emit('userListUpdated', userUtil.user_list);
  self.changeSplitModeFramerate(function(callback) {
    if (callback === '200') {
      logger.debug('User quit to increase framerate');
    }
  });
};

RVAClient.prototype.receive_CMD_INVALID_CMD = function receive_CMD_INVALID_CMD(bValues, s_bytes) {
  var self = this;
  logger.verbose('Process CMD INVALID_CMD');

  var error_code = bValues[s_bytes + 3];
  switch (error_code) {
    case common.ERRORCODE.PIN_INVALID:
      settings.disconnectEvent = 'PIN_INVALID';
      break;
    case common.ERRORCODE.NAME_NOT_IN_GROUP:
      settings.disconnectEvent = 'NAME_NOT_IN_GROUP';
      break;
    case common.ERRORCODE.DUPLICATED_NAME_IN_GROUP:
      settings.disconnectEvent = 'DUPLICATED_NAME_IN_GROUP';
      break;
    case common.ERRORCODE.REACH_MAX_NUMBER_USERS:
      settings.disconnectEvent = 'REACH_MAX_NUMBER_USERS';
      break;
    case common.ERRORCODE.NOVOCAST_REACH_MAX_NUMBER_USERS:
      settings.disconnectEvent = 'NOVOCAST_REACH_MAX_NUMBER_USERS';
      logger.debug('NOVOCAST_REACH_MAX_NUMBER_USERS');
      break;
    default:
      var error = new Error('UNDEFINED_CMD', error_code);
      self.emit('error', error);
      logger.debug('Other Errorcode =' + error_code);
      logger.debug('[0]=' + bValues[s_bytes]);
      logger.debug('[1]=' + bValues[s_bytes + 1]);
      logger.debug('[2]=' + bValues[s_bytes + 2]);
      logger.debug('[3]=' + bValues[s_bytes + 3]);
  }
  RVACommandClient.end();
  RVACommandClient.destroy();
};

RVAClient.prototype.receive_CMD_CLIENT_STATUS_REFRESH = function receive_CMD_CLIENT_STATUS_REFRESH(bValues, s_bytes) {
  var self = this;

  var userId = bValues[s_bytes + 2];
  var state = bValues[s_bytes + 3];
  var device = bValues[s_bytes + 4];
  var j = 0;
  var length = bValues[s_bytes];
  var strBuf = new Uint8Array(bValues[s_bytes] - 5);
  for (var i = s_bytes + 5; i < length + s_bytes; i++) {
    strBuf[j] = bValues[i];
    j++;
  }

  var userName = dsaUtil.convertByteArrayToStringUtf8(strBuf);
  //settings.userName = userName;
  if (settings.client_id === 0) {
    if (userName === settings.clientName) {
      settings.client_id = userId;
    }
  }

  userUtil.add_userdata(userId, userName, false, null, -1, false, true, device);
  self.checkStateCase(userId, common.CMD.CLIENT_STATUS_REFRESH, state);

  return length;
};

RVAClient.prototype.send_CMD_SCREENSHOT_REQUEST = function send_CMD_SCREENSHOT_REQUEST(userid) {
  var self = this;
  logger.verbose('##### Send CMD SCREENSHOT REQUEST for userid: ' + userid);
  settings.screenshot_userid = userid;

  previewTimer = setTimeout(function() {
    logger.debug('triggering previewTimeout event!');
    self.emit('previewTimeout', userid);
  }, 10000);

  self.openDataServerSocket(common.CMD.PREVIEW_DATA, userid, null);
};

RVAClient.prototype.receive_CMD_VIEWER_PAUSE = function receive_CMD_VIEWER_PAUSE() {
  var self = this;
  logger.verbose('Received CMD_VIEWER_PAUSE, rva version: ' + settings.rva_version + ' rva type: ' + settings.rvaType + ' settings.fullMode = ' + settings.fullMode + ' switchExtend: ' + settings.switchExtend + ' settings.currentScreen: ' + settings.currentScreen);

  if (settings.rvaType === 'NOVOCAST') {
    setTimeout(function() {
      logger.debug('emit viewerState pause');
      self.emit('viewerState', 'pause');
      self.emit('screenPaused');
    }, 7000);
  } else {
    if (!settings.switchExtend || settings.fullMode === 0) {
      setTimeout(function() {
        logger.debug('emit viewerState pause');
        self.emit('viewerState', 'pause');
      }, 4000);
    }

    if (settings.switchExtend && settings.fullMode === 1 && settings.currentScreen == 2) {
      setTimeout(function() {
        logger.debug('emit viewerState pause');
        self.emit('viewerState', 'pause');
      }, 4000);
    }

    setTimeout(function() {
      self.emit('screenPaused');
    }, 4000);
  }

  settings.switchExtend = false;
  if (settings.fullMode > 0) {
    logger.debug('set settings.fullMode to 2');
    settings.fullMode = 2;
    nativeService.desktopStreamingStop(function(callback) {});
  }
  if (settings.splitMode > 0) {
    settings.splitMode = 2;
    nativeService.splitModePause();
  }

};

RVAClient.prototype.receive_CMD_VIEWER_RESUME = function receive_CMD_VIEWER_RESUME() {
  var self = this;
  logger.debug('Received CMD_VIEWER_RESUME, settings.splitMode: ' + settings.splitMode + ' settings.fullMode:' + settings.fullMode + ' settings.rva_version:' + settings.rva_version + 'extendScreenStateBeforePause: ' + extendScreenStateBeforePause);
  logger.debug('the type of extendScreenStateBeforePause is ' + typeof(extendScreenStateBeforePause));
  async.series([
    function(done) {
      switch (extendScreenStateBeforePause.toString()) {
        case '0': //mirror none
          done();
          break;
        case '1': //mirror main screen
          logger.debug('rva screenWidth: ' + settings.screenWidth + ' dsa width: ' + dsa_width);
          logger.debug('rva screenHeight: ' + settings.screenHeight + ' dsa width: ' + dsa_height);
          logger.debug('receive_CMD_VIEWER_RESUME case 1, close extended screen');
          nativeService.setExtendedScreenMode(0, 0, settings.screenWidth, settings.screenHeight, function(result) {
            logger.debug('setExtendedScreenMode result:' + result);
            done();
          });
          break;
        case '2': //mirror extend screen
          logger.debug('rva screenWidth: ' + settings.screenWidth + ' dsa width: ' + dsa_width);
          logger.debug('rva screenHeight: ' + settings.screenHeight + ' dsa width: ' + dsa_height);
          if (semver.gte(settings.rva_version, '2.0.0')) {
            if (settings.screenHeight === 720 || settings.screenHeight === 1080) {
              logger.debug('receive_CMD_VIEWER_RESUME case 2(mirror extend screen) setting extend screen to 1920x' + '1080');
              nativeService.setExtendedScreenMode(1, 0, 1920, 1080, function(result) {
                logger.debug('setExtendedScreenMode result:' + result);
                done();
              });
            } else {
              logger.debug('receive_CMD_VIEWER_RESUME case 2(mirror extend screen) setting extend screen to ' + settings.screenWidth + 'x' + settings.screenHeight);
              nativeService.setExtendedScreenMode(1, 0, settings.screenWidth, settings.screenHeight, function(result) {
                logger.debug('setExtendedScreenMode result:' + result);
                done();
              });
            }
          } else {
            logger.debug('rva v1.6 ');
            nativeService.getScreenResolution(function(result) {
              logger.debug('getScreenResolution: ' + JSON.stringify(result));
              dsa_width = result.width;
              dsa_height = result.height;
              logger.debug('rva v1.6 setExtendedScreenMode to ' + dsa_width + 'x' + dsa_height);
              nativeService.setExtendedScreenMode(1, 0, dsa_width, dsa_height, function(result) {
                logger.debug('setExtendedScreenMode result:' + result);
                done();
              });
            });
          }
          break;
      }
    },
    function(done) {
      // TODO: check main screen resolution, if larger than RVA, change resolution here
      logger.debug('check resolution -- receive_CMD_VIEWER_RESUME');
      if (settings.rvaType === 'NOVOCAST') {
        if (dsa_width > 1920 || dsa_height > 1080) {
          if (settings.screenWidth < dsa_width || settings.screenHeight < dsa_height) {
            nativeService.setScreenResolution(1920, 1080, function(result) {
              logger.debug('set pc resolution to ' + 1920 + 'x' + 1080);
              logger.debug(result.cmd + ' ' + result.data);
              settings.isRestoreScreenResolution = true;
            });
          }
        }
      } else if (semver.gte(settings.rva_version, '2.0.0')) {
        if (dsa_width > 1920 || dsa_height > 1080) {
          if (settings.screenWidth < dsa_width || settings.screenHeight < dsa_height) {
            nativeService.setScreenResolution(1920, 1080, function(result) {
              logger.debug('set pc resolution to ' + 1920 + 'x' + 1080);
              logger.debug(result.cmd + ' ' + result.data);
              settings.isRestoreScreenResolution = true;
            });
          }
        }
      } else {
        logger.debug('rva version is less than 2.0.0');
        logger.debug('rva screenWidth: ' + settings.screenWidth + ' dsa width: ' + dsa_width);
        logger.debug('rva screenHeight: ' + settings.screenHeight + ' dsa width: ' + dsa_height);
        logger.debug('setScreenResolution to ' + settings.screenWidth + 'x' + settings.screenHeight);
        if (dsa_width > settings.screenWidth || dsa_height > settings.screenHeight) {
          nativeService.setScreenResolution(settings.screenWidth, settings.screenHeight, function(result) {
            logger.debug('set pc resolution to ' + settings.screenWidth + 'x' + settings.screenHeight);
            logger.debug(result.cmd + ' ' + result.data);
            settings.isRestoreScreenResolution = true;
          });
        }
      }
      done();
    },
    function() {
      if (settings.rvaType === 'NOVOCAST') {
        setTimeout(function() {
          self.emit('viewerState', 'resume');
        }, 7000);
      } else {
        setTimeout(function() {
          self.emit('viewerState', 'resume');
        }, 4000);
      }
      if (settings.splitMode === 2) {
        settings.splitMode = 1;
        nativeService.splitModeResume();
      } else if (settings.fullMode === 2) {
        settings.fullMode = 1;
        self.initialStreamingServer();
      }
    }
  ]);
};

RVAClient.prototype.receiveCmdPinRequired = function receiveCmdPinRequired() {
  var self = this;
  logger.debug('Process CMD PIN REQUIRED, settings.pinCode:' + settings.pinCode);
  settings.pinRequired = true;
  self.emit('pinRequired', settings.pinCode);
};

RVAClient.prototype.send_CMD_PIN_REQUIRED = function send_CMD_PIN_REQUIRED() {
  logger.debug('send_CMD_PIN_REQUIRED');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.PIN_REQUIRED;
  buf[2] = settings.client_id;
  buf[3] = 0;
  RVACommandClient.write(buf);
};

RVAClient.prototype.send_CMD_PIN_NOT_REQUIRED = function send_CMD_PIN_NOT_REQUIRED() {
  logger.debug('send_CMD_PIN_NOT_REQUIRED');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.PIN_NOT_REQUIRED;
  buf[2] = settings.client_id;
  buf[3] = 0;
  RVACommandClient.write(buf);
};

/*************** STOP_PRESENTATION message format *************
 *   byte index:
 *   byte      0   one byte  --- packet length (4 bytes)
 *   byte      1:  one byte  --- message type:  STOP_PRESENTATION
 *   byte      2:  one byte  --- client ID if this command is from Moderator, or Sender ID if this command is from Presenter
 *   byte      3:  one byte  --- arg:  N/A
 *****************************************************/
RVAClient.prototype.stopPresentation = function send_CMD_STOP_PRESENTATION(client_id) {
  logger.debug('stop presentation');
  var self = this;
  if (typeof(client_id) === 'undefined') {
    client_id = settings.client_id;
  }
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.STOP_PRESENTATION;
  buf[2] = client_id;
  buf[3] = 0;
  //  RVACommandClient.write(buf);
  if (client_id != settings.client_id) {
    RVACommandClient.write(buf);
  } else {
    var range = 4000;
    var current_RVA_status = userUtil.getUserListStatus();
    logger.debug('CURRENT RVA STATUS = ' + current_RVA_status);

    if (current_RVA_status === common.RVA_PLAY_STATUS.FULL_VIDEO) {
      range = 5000;
    } else if (current_RVA_status === common.RVA_PLAY_STATUS.FULL_IMAGE) {
      range = 3000;
    } else if (current_RVA_status === common.RVA_PLAY_STATUS.SPLIT_IMAGE) {
      range = 3000;
    }

    var range_timeout = range;
    setTimeout(function() {
      logger.debug('##### Delay to send stop presentation');
      RVACommandClient.write(buf);
    }, range_timeout);
  }
};

RVAClient.prototype.receiveCMStatus = function receiveCMStatus(bValues, s_bytes) {
  var self = this;
  logger.debug('Process CMD_CM_STATUS');
  logger.debug(JSON.stringify(bValues));

  var msgLength = bValues[s_bytes];
  var cmd = bValues[s_bytes + 1];
  var userId = bValues[s_bytes + 2];
  var arg = bValues[s_bytes + 3];
  logger.debug('msgLength:' + msgLength + ' cmd:' + cmd + ' userId:' + userId + ' arg:' + arg);

  switch (arg) {
    case common.STATUSCODE.UPLOAD_GROUP_FILE_SUCCESS:
      logger.verbose('receive UPLOAD_GROUP_FILE_SUCCESS');
      self.emit('uploadGroupFileSuccess', settings.group);
      settings.groupMode = true;
      settings.groupName = settings.group.class;
      logger.debug(settings);
      self.emit('settingsUpdated', self.prepareSettingsForUI());
      break;
    case common.STATUSCODE.UPLOAD_GROUP_FILE_FAIL:
      logger.verbose('receive UPLOAD_GROUP_FILE_FAIL');
      self.emit('error', new Error('uploadGroupFileFail'));
      break;
    case common.STATUSCODE.REMOVE_GROUP_SUCCESS:
      logger.verbose('receive REMOVE_GROUP_SUCCESS');
      settings.groupMode = false;
      settings.groupName = 'None'; //TODO: Molly i18n
      settings.group = null;

      var host = userUtil.getUserData(settings.client_id);
      userUtil.user_list = [];
      userUtil.group_list = [];
      userUtil.add_userdata(host.uid, settings.clientName, true, null, host.panel, true, true, host.os_type);

      self.emit('settingsUpdated', self.prepareSettingsForUI());
      self.emit('userListUpdated', userUtil.user_list);
      self.emit('removeGroupSuccess', settings.group);

      break;
    case common.STATUSCODE.REMOVE_GROUP_FAIL:
      logger.verbose('receive REMOVE_GROUP_FAIL');
      break;
    case common.STATUSCODE.MODERATOR_REQUEST_SUCCESS:
      logger.verbose('receive MODERATOR_REQUEST_SUCCESS');
      settings.isCorpModeratorOn = true;
      self.emit('corpModeStatus', true);
      break;
    case common.STATUSCODE.MODERATOR_REQUEST_FAIL:
      logger.verbose('receive MODERATOR_REQUEST_FAIL');
      break;
    default:
      logger.debug('CMD_CM_STATUS unhandled');
      logger.debug('arg: ' + arg);
      break;
  }

  var j = 0;
  var length = bValues[s_bytes] + s_bytes;
  var strBuf = new Uint8Array(bValues[s_bytes] - 4);
  for (var i = s_bytes + 4; i < length; i++) {
    strBuf[j] = bValues[i];
    j++;
  }

  return length;
};

RVAClient.prototype.receiveCmdQRCodeImage = function receiveCmdQRCodeImage(bValues, s_bytes) {
  logger.debug('Process CMD QR_CODE_IMAGE');
  var imageDataSize = dsaUtil.convertBytesBigEndianToInt(
    bValues[s_bytes + 4], bValues[s_bytes + 5], bValues[s_bytes + 6], bValues[s_bytes + 7]);


  var length = imageDataSize + 8;
  logger.debug('imageDataSize= ' + length);

  var strBuf = new Buffer(imageDataSize);
  var j = 0;
  for (var i = 8; i < length; i++) {
    strBuf[j] = bValues[i];
    j++;
  }

  mkdirp('imgs', function(err) {
    if (err !== null) {
      logger.error('receiveCmdQRCodeImage mkdirp');
      logger.error(err.message);
    }
  });

  qrImg = strBuf.toString('base64');
  /*
    fs.writeFile('./imgs/qrcode.png', strBuf, function (err) {
      if (err !== null) {
        logger.error('receiveCmdQRCodeImage writeFile');
        logger.error(err.message);
      } else {
        logger.debug('The QR image file was saved!');
      }
    });*/

  return length;
};

RVAClient.prototype.receive_CMD_RECONNECTION_ALLOWED = function receive_CMD_RECONNECTION_ALLOWED() {
  logger.verbose('##### Receive CMD RECONNECTION ALLOWED');
  var self = this;

  settings.needReconnection = false;
  clearInterval(connectionRetry);
  clearTimeout(reconnectionTimeout);

  settings.try_times = 0;
  settings.isReconnect = true;

  if (settings.groupMode) {
    userUtil.cleanUserOnlineStatus();
  } else {
    userUtil.user_list = [];
  }

  self.emit('connected', self.prepareSettingsForUI());
  self.sendCmdClientStatusRefresh();
  self.sendCmdKeepAlive('receive_CMD_RECONNECTION_ALLOWED');
  settings.isConnectionApproval = true;
};

RVAClient.prototype.receive_CMD_VIEWER_REQUEST = function receive_CMD_VIEWER_REQUEST(bValues, s_bytes) {
  var self = this;
  logger.debug('Process CMD VIEWER_REQUEST');
  settings.screen_number = bValues[s_bytes + 3];
  if (settings.rvaType === 'EDU') {
    self.send_CMD_VIEWER_APPROVED();
  } else if (settings.rvaType === 'CORP') {
    self.emit('askPresentPage');
  }
};

RVAClient.prototype.send_CMD_MODERATOR_REQUEST_CORP = function send_CMD_MODERATOR_REQUEST_CORP() {
  logger.debug('Send CMD MODERATOR REQUEST');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.MODERATOR_REQUEST;
  buf[2] = settings.client_id;
  buf[3] = 0;
  RVACommandClient.write(buf);
};

RVAClient.prototype.send_CMD_MODERATOR_RELEASE = function send_CMD_MODERATOR_RELEASE() {
  logger.debug('Send CMD MODERATOR RELEASE');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.MODERATOR_RELEASE;
  buf[2] = settings.client_id;
  buf[3] = 0;
  RVACommandClient.write(buf);
};

RVAClient.prototype.receive_CMD_MODERATOR_RELEASED = function receive_CMD_MODERATOR_RELEASED() {
  var self = this;
  logger.debug('Process CMD MODERATOR RELEASED');
  settings.isCorpModeratorOn = false;
  //TODO: Call Molly there is no host now
  self.emit('corpModeStatus', false);
};

RVAClient.prototype.send_CMD_VIEWER_DENIED = function send_CMD_VIEWER_DENIED() {
  logger.debug('Send CMD VIEWER DENIED');
  if (settings.isCorpModeratorOn) {
    var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
    buf[0] = settings.MINIMUM_MSG_LENGTH;
    buf[1] = common.CMD.VIEWER_DENIED;
    buf[2] = settings.client_id;
    buf[3] = 0;

    RVACommandClient.write(buf);
  } else {
    //Nothing
  }
};

RVAClient.prototype.receive_CMD_VIEWER_DENIED = function receive_CMD_VIEWER_DENIED() {
  var self = this;
  if (settings.isCorpModeratorOn) {
    self.emit('requestDenied');
  }
};

RVAClient.prototype.send_CMD_VIEWER_APPROVED = function send_CMD_VIEWER_APPROVED() {
  logger.debug('Send CMD VIEWER APPROVED');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.VIEWER_APPROVED;
  buf[2] = settings.client_id;
  buf[3] = settings.screen_number;
  RVACommandClient.write(buf);
};

RVAClient.prototype.sendCmdClientStatusRefresh = function sendCmdClientStatusRefresh() {
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.CLIENT_STATUS_REFRESH;
  buf[2] = settings.client_id;
  buf[3] = 0;
  RVACommandClient.write(buf);
};

RVAClient.prototype.sendCmdRemoveRequest = function sendCmdRemoveRequest() {
  logger.debug('Process sendCmdRemoveRequest');
  if (!RVACommandClient.destroyed) {
    logger.debug('send CMD_REMOVE_REQUEST');
    var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
    buf[0] = settings.MINIMUM_MSG_LENGTH;
    buf[1] = common.CMD.REMOVE_REQUEST;
    buf[2] = 0;
    buf[3] = 0;
    if (!RVACommandClient.destroyed)
      RVACommandClient.write(buf);
  } else {
    logger.debug('No need to send CMD_REMOVE_REQUEST');
  }
};

RVAClient.prototype.receiveCmdNvcModel = function receiveCmdNvcModel(bValues, s_bytes) {
  logger.debug('Receive CMD NVC_MODEL');
  var j = 0;
  var length = bValues[s_bytes] + s_bytes;
  var strBuf = new Uint8Array(bValues[s_bytes] - 4);
  for (var i = s_bytes + 4; i < length; i++) {
    strBuf[j] = bValues[i];
    j++;
  }
  settings.nvc_model = dsaUtil.convertByteArrayToString(strBuf);
  logger.debug('NVC_MODEL:' + settings.nvc_model);
  if (settings.nvc_model === 'NovoConnect-B100') {
    settings.rvaType = 'NOVOCAST';
  } else {
    settings.rvaType = 'EDU';
  }
};

RVAClient.prototype.receiveCmdRvaVersion = function receiveCmdRvaVersion(bValues, s_bytes) {
  logger.debug('Receive CMD RVA_VERSION');
  var j = 0;
  var length = bValues[s_bytes] + s_bytes;
  var strBuf = new Uint8Array(bValues[s_bytes] - 4);
  for (var i = s_bytes + 4; i < length; i++) {
    strBuf[j] = bValues[i];
    j++;
  }

  settings.rva_version = dsaUtil.convertByteArrayToString(strBuf);
  var rva_version = dsaUtil.convertByteArrayToString(strBuf);
  var rvaNumber = parseFloat(rva_version.replace('v', ''));
  // For Demo only
  //  settings.rva_version = "v2.0";
  //  rvaNumber = 2.0;
  logger.debug('rva_version:' + settings.rva_version + ' rvaType:' + settings.rvaType);
  //if (settings.nvc_model == 'NovoConnect-B360' && (settings.rva_version == 'v1.3' || settings.rva_version == 'v1.4' || settings.rva_version == 'v1.5')) {
  logger.debug('settings.nvc_model: ' + settings.nvc_model);
  //  if (settings.nvc_model == 'NovoConnect-B360' && rvaNumber >= 1.6) {
  if (rvaNumber >= 1.6 && parseFloat(pjson.version) >= rvaNumber) {
    //  if (rvaNumber >= 1.6 && semver.gte(pjson.version, rva_version.replace('v', ''))) {
    settings.isVersionMatch = true;
    logger.debug('isVersionMatch= ' + settings.isVersionMatch);
  } else if (settings.rvaType === 'NOVOCAST') {
    settings.isVersionMatch = true;
  } else {
    settings.isVersionMatch = false;
  }
};

RVAClient.prototype.receiveCmdPinNotRequired = function receiveCmdPinNotRequired() {
  logger.debug('Process CMD PIN_NOT_REQUIRED');
  var self = this;
  settings.pinRequired = false;
  self.emit('pinRequired', false);
};

RVAClient.prototype.receiveCmdPinValue = function receiveCmdPinValue(bValues, s_bytes) {
  logger.debug('Process CMD PIN_VALUE');
  var pin_number = dsaUtil.convertBytesBigEndianToInt(
    bValues[s_bytes + 4], bValues[s_bytes + 5], bValues[s_bytes + 6], bValues[s_bytes + 7]);

  settings.pinCode = pin_number;
  logger.debug('PIN= ' + pin_number);
};

RVAClient.prototype.receiveSettingsPassword = function receiveSettingsPassword(bValues, s_bytes) {
  var passwordLength = 0;
  logger.debug('Process CMD SETTINGS_PASSWORD');
  if (bValues[3] === 1) {
    // password is set
    logger.debug('password is set');
    passwordLength = bValues[0] - settings.MINIMUM_MSG_LENGTH;
    logger.debug('password length is ' + passwordLength);

    var j = 0;
    var length = bValues[s_bytes] + s_bytes;
    var strBuf = new Uint8Array(bValues[s_bytes] - 4);
    for (var i = s_bytes + 4; i < length; i++) {
      strBuf[j] = bValues[i];
      j++;
    }

    settings.settingsPassword = dsaUtil.convertByteArrayToString(strBuf);

    logger.debug('password: ');
    logger.debug(settings.settingsPassword);

  } else {
    // password is not set
    logger.debug('password is not set');
    settings.settingsPassword = '';
  }

};

RVAClient.prototype.receive_CMD_SCREEN_RESOLUTION = function receive_CMD_SCREEN_RESOLUTION(bValues, s_bytes) {
  dsa_width = 0,
    dsa_height = 0;
  logger.debug('Process CMD SCREEN_RESOLUTION');
  settings.screenWidth = dsaUtil.convertBytesBigEndianToInt(
    bValues[s_bytes + 4], bValues[s_bytes + 5], bValues[s_bytes + 6], bValues[s_bytes + 7]);
  settings.screenHeight = dsaUtil.convertBytesBigEndianToInt(
    bValues[s_bytes + 8], bValues[s_bytes + 9], bValues[s_bytes + 10], bValues[s_bytes + 11]);
  logger.debug('rva screen_width= ' + settings.screenWidth + ' rva screen_height= ' + settings.screenHeight);
  settings.isRestoreScreenResolution = false;
  async.series([
    function(done) {
      nativeService.getScreenResolution(function(result) {
        logger.debug('getScreenResolution: ' + JSON.stringify(result));
        dsa_width = result.width;
        dsa_height = result.height;
        if (settings.rva_version === 'v1.6') {
          settings.rva_version = 'v1.6.0';
          done();
        } else
          done();
      });
    },
    function() {
      logger.debug('dsa w: ' + dsa_width + ' dsa h: ' + dsa_height);
      if (settings.rvaType === 'NOVOCAST') {
        if (dsa_width > 1920 || dsa_height > 1080) {
          if (settings.screenWidth < dsa_width || settings.screenHeight < dsa_height) {
            nativeService.setScreenResolution(1920, 1080, function(result) {
              logger.debug('set pc resolution to ' + 1920 + 'x' + 1080);
              logger.debug(result.cmd + ' ' + result.data);
              settings.isRestoreScreenResolution = true;
            });
          }
        }
      } else if (semver.gte(settings.rva_version, '2.0.0')) {
        if (dsa_width > 1920 || dsa_height > 1080) {
          if (settings.screenWidth < dsa_width || settings.screenHeight < dsa_height) {
            nativeService.setScreenResolution(1920, 1080, function(result) {
              logger.debug('set pc resolution to ' + 1920 + 'x' + 1080);
              logger.debug(result.cmd + ' ' + result.data);
              settings.isRestoreScreenResolution = true;
            });
          }
        }
      } else {
        logger.debug('rva version is less than 2.0.0');
        logger.debug('rva screenWidth: ' + settings.screenWidth + ' dsa width: ' + dsa_width);
        logger.debug('rva screenHeight: ' + settings.screenHeight + ' dsa width: ' + dsa_height);
        logger.debug('receive_CMD_SCREEN_RESOLUTION setScreenResolution to ' + settings.screenWidth + 'x' + settings.screenHeight);
        if (dsa_width > settings.screenWidth || dsa_height > settings.screenHeight) {
          nativeService.setScreenResolution(settings.screenWidth, settings.screenHeight, function(result) {
            logger.debug('set pc resolution to ' + settings.screenWidth + 'x' + settings.screenHeight);
            logger.debug(result.cmd + ' ' + result.data);
            settings.isRestoreScreenResolution = true;
          });
        }
      }
    }
  ]);
};

RVAClient.prototype.receiveCmdNetworkMode = function receiveCmdNetworkMode(bValues, s_bytes) {
  logger.debug('Process CMD NETWORK_MODE');
  if (bValues[s_bytes + 3] === 0) {
    settings.network_mode = 0; // Network mode
  } else if (bValues[s_bytes + 3] === 1) {
    settings.network_mode = 1; // Hotspot mode
  } else {
    settings.network_mode = 2; // Wifi turn off
  }

  logger.debug('network_mode: ' + settings.network_mode);

  //v1.5 support Dual IP address
  if (typeof settings.rva_version === 'undefined') {
    settings.rva_version = 'v1.4';
  }

  var rvaNumber = parseFloat(settings.rva_version.replace('v', ''));
  if (rvaNumber < 1.5) {
    var j = 0;
    var length = bValues[s_bytes] + s_bytes;
    var strBuf = new Uint8Array(bValues[s_bytes] - 4);
    for (var i = s_bytes + 4; i < length; i++) {
      strBuf[j] = bValues[i];
      j++;
    }

    settings.qr_ssid = settings.ssid = dsaUtil.convertByteArrayToString(strBuf);

    // new
  } else {
    // GET WIFI
    var ssid_length = bValues[4 + s_bytes];
    var strBuf = new Uint8Array(ssid_length);
    for (var i = 0; i < ssid_length; i++) {
      strBuf[i] = bValues[4 + i + 1 + s_bytes];
    }
    settings.qr_ssid = settings.ssid = dsaUtil.convertByteArrayToString(strBuf);



    var wifi_ip_length = bValues[4 + s_bytes + 1 + ssid_length];
    var strBuf2 = new Uint8Array(wifi_ip_length);
    for (var i = 0; i < wifi_ip_length; i++) {
      strBuf2[i] = bValues[4 + i + 1 + ssid_length + 1 + s_bytes];
      logger.debug(strBuf2[i]);
    }
    settings.qr_ip = dsaUtil.convertByteArrayToString(strBuf2);
    if (typeof settings.qr_ip === 'undefined') {
      settings.qr_ip = '';
    }

    var lan_ip_length = bValues[4 + s_bytes + 1 + ssid_length + 1 + wifi_ip_length];
    var strBuf3 = new Uint8Array(lan_ip_length);
    for (var i = 0; i < lan_ip_length; i++) {
      strBuf3[i] = bValues[4 + i + 1 + ssid_length + 1 + wifi_ip_length + 1 + s_bytes];
    }
    var strBuf3Result = dsaUtil.convertByteArrayToString(strBuf3);

    if (strBuf3Result !== '0.0.0.0') {
      settings.qr_lan = strBuf3Result;
    }
  }
};

RVAClient.receiveCmdNvcModel = function receiveCmdNvcModel(bValues, s_bytes) {
  logger.debug('Process CMD NVC_MODEL');
  var j = 0;
  var length = bValues[s_bytes] + s_bytes;
  var strBuf = new Uint8Array(bValues[s_bytes] - 4);
  for (var i = s_bytes + 4; i < length; i++) {
    strBuf[j] = bValues[i];
    j++;
  }
  settings.nvc_model = dsaUtil.convertByteArrayToString(strBuf);

  logger.debug('nvc_model= ' + settings.nvc_model);
};

RVAClient.prototype.receiveCmdBspVersion = function receiveCmdBspVersion(bValues, s_bytes) {
  logger.debug('Process CMD BSP_VERSION');
  var j = 0,
    length = bValues[s_bytes] + s_bytes,
    strBuf = new Uint8Array(bValues[s_bytes] - 4);
  for (var i = s_bytes + 4; i < length; i++) {
    strBuf[j] = bValues[i];
    j++;
  }
  settings.bsp_version = dsaUtil.convertByteArrayToString(strBuf);
  logger.debug('bsp_version= ' + settings.bsp_version);
};

RVAClient.prototype.receiveCmdDeviceName = function receiveCmdDeviceName(bValues, s_bytes) {
  logger.debug('Process CMD DEVICE_NAME');
  var j = 0;
  var length = bValues[s_bytes] + s_bytes;
  var strBuf = new Uint8Array(bValues[s_bytes] - 4);
  for (var i = s_bytes + 4; i < length; i++) {
    strBuf[j] = bValues[i];
    j++;
  }
  settings.dev_name = dsaUtil.convertByteArrayToStringUtf8(strBuf);
  logger.debug('dev_name= ' + settings.dev_name);
};

RVAClient.prototype.receiveCmdDisplaySetting = function receiveCmdDisplaySetting() {
  logger.debug('Process CMD DISPLAY_SETTING');
};

RVAClient.prototype.receiveRemoteMouseControl = function receiveRemoteMouseControl(bValues, s_bytes) {
  logger.debug('Process CMD REMOTE_MOUSE_CONTROL');
  async.series([
    function(done) {
      // command[3] = arg1 (arg1 == 0: enable remote mouse;  arg1 == 1: disable remote mouse; );
      logger.debug('[0]=' + bValues[s_bytes]);
      logger.debug('[1]=' + bValues[s_bytes + 1]);
      logger.debug('[2]=' + bValues[s_bytes + 2]);
      logger.debug('[3]=' + bValues[s_bytes + 3]);
      done();
    },
    function(done) {
      if (bValues[s_bytes + 3] === 0 && !remoteMouseState) {
        if (settings.remoteControl) {
          nativeService.remoteControlStart(settings.rvaIp, settings.REMOTE_CONTROL_PORT, function(result) {
            logger.debug('remote control enabled: ' + result);
            remoteMouseState = true;
            done();
          });
        } else
          done();
      } else if (bValues[s_bytes + 3] === 1 && remoteMouseState) {
        if (settings.remoteControl) {
          logger.debug('received CMD REMOTE_MOUSE_CONTROL - disable remote control');
          nativeService.remoteControlStop(function(result) {
            logger.debug('remote control disabled: ' + result);
            remoteMouseState = false;
            done();
          });
        } else
          done();
      } else {
        logger.debug('remote mouse no actions');
        done();
      }
    }
  ]);
};

RVAClient.prototype.remoteMouseStart = function remoteMouseStart() {
  if (settings.remoteControl) {
    nativeService.remoteControlStart(settings.rvaIp, settings.REMOTE_CONTROL_PORT, function(result) {
      logger.debug('remote control enabled: ' + result);
      remoteMouseState = true;
    });
  }
};

RVAClient.prototype.remoteMouseStop = function remoteMouseStop(cb) {
  if (settings.remoteControl) {
    logger.debug('received remoteMouseStop - disable remote control');
    nativeService.remoteControlStop(function(result) {
      logger.debug('remote control disabled: ' + result);
      remoteMouseState = false;
      if (cb)
        cb(result);
    });
  } else if (cb)
    cb(true);
};

RVAClient.prototype.receiveCmdKeepAlive = function receiveCmdKeepAlive() {
  var self = this;
  clearTimeout(keepAliveScheduler);
  if (!RVACommandClient.destroyed) {
    if (settings.network_mode === 0 || settings.network_mode === 2) {
      keepAliveScheduler = setTimeout(function() {
        // logger.debug('triggering sending keepalive');
        self.sendCmdKeepAlive();
      }, 10000, 'receiveCmdKeepAlive() wifi');
    } else {
      keepAliveScheduler = setTimeout(function() {
        self.sendCmdKeepAlive();
        //  logger.debug('triggering sending keepalive');
      }, 2500, 'receiveCmdKeepAlive() lan');
      // logger.debug('receiveCmdKeepAlive() lan');
    }
  }
};

RVAClient.prototype.disableSFReminder = function disableSFReminder() {
  soundFlowerReminder = false;
};

RVAClient.prototype.enableSFReminder = function disableSFReminder() {
  soundFlowerReminder = true;
};

RVAClient.prototype.SFAllowed = function SFAllowed() {
  var self = this;
  soundFlowerAllowed = true;
  logger.debug('soundFlowerAllowed: ' + soundFlowerAllowed);
  if (settings.fullMode > 0 && soundFlowerAllowed) {
    self.enableSoundFlowerState(true, function() {}, false);
  } else {
    self.enableSoundFlowerState(false, function() {}, false);
  }
};

RVAClient.prototype.SFNotAllowed = function SFNotAllowed() {
  soundFlowerAllowed = false;
  logger.debug('soundFlowerAllowed: ' + soundFlowerAllowed);
};

RVAClient.prototype.enableSoundFlowerState = function enableSoundFlowerState(permission, cb, connectionRequest) {
  var self = this;
  logger.debug('Process enableSoundFlowerState: ' + permission);
  if (permission === true) {
    soundFlowerEnabled = true;
    if (connectionRequest)
      self.sendCmdConnectionRequest();
    if (cb)
      nativeService.soundflowerStateEnable(cb);
    else
      nativeService.soundflowerStateEnable();
  } else {
    soundFlowerEnabled = false;
    if (connectionRequest)
      self.sendCmdConnectionRequest();
    if (cb)
      nativeService.soundflowerStateDisable(cb);
    else
      nativeService.soundflowerStateDisable();
  }
};

RVAClient.prototype.sendCmdSessionInfo = function sendCmdSessionInfo() {
  var buf = new Buffer(4);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.CONNECT_SESSION_INFO; // message type
  buf[2] = settings.client_id;
  buf[3] = 0;

  logger.debug('sendCmdSessionInfo uid: ' + settings.client_id + JSON.stringify(buf) + ' addr:' + RVACommandClient.remoteAddress + ' port:' + RVACommandClient.remotePort);

  RVACommandClient.write(buf);
};

RVAClient.prototype.closeSessionInfo = function closeSessionInfo() {
  var buf = new Buffer(4);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.CONNECT_SESSION_INFO; // message type
  buf[2] = settings.client_id;
  buf[3] = 1;

  logger.debug('closeSessionInfo uid: ' + settings.client_id + JSON.stringify(buf) + ' addr:' + RVACommandClient.remoteAddress + ' port:' + RVACommandClient.remotePort);

  RVACommandClient.write(buf);
};

RVAClient.prototype.sendCmdKeepAlive = function sendCmdKeepAlive() {
  var self = this;
  settings.keepAliveBack = false;
  var buf = new Buffer(4);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.KEEP_ALIVE; // message type
  buf[2] = 0;
  buf[3] = 0;

  var checkType = 2000;
  if (settings.network_mode === 1) {
    checkType = 2000;
  }
  if (settings.enableKeepAlive) {
    keepAliveTimeout = setTimeout(function() {
      self.checkKeepAlive();
    }, checkType);

    // logger.debug('sending keepalive');
    RVACommandClient.write(buf);
  } else {
    logger.debug('keepalive disabled!');
  }
};

RVAClient.prototype.checkKeepAlive = function checkKeepAlive() {
  var self = this;
  if (settings.keepAliveBack) {
    settings.needReconnection = false;
  } else {
    if (settings.try_times > 0) {
      async.series([
        function(done) {
          if (settings.fullMode > 0) {
            settings.fullMode = 0;
            logger.debug('checkKeepAlive desktopStreamingStop');
            nativeService.desktopStreamingStop(function() {
              if (remoteMouseState) {
                if (settings.remoteControl) {
                  nativeService.remoteControlStop(function(result) {
                    logger.debug('remote control disabled: ' + result);
                    remoteMouseState = false;
                  });
                }
              }
              nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
                logger.debug('checkKeepAlive setExtendedScreenMode result:' + result);
              });
              done();
            });
          } else if (settings.splitMode > 0) {
            settings.splitMode = 0;
            nativeService.splitModeStop(function(result) {
              logger.debug('##### closing native image result = ' + result);
              done();
            });
          } else {
            done();
          }
        },
        function(done) {
          settings.needReconnection = true;
          settings.disconnectEvent = 'Reconnection';
          RVACommandClient.destroy();
          //Setup a final timeout for reconnection
          reconnectionTimeout = setTimeout(function() {
            self.checkReconnectionTimeout();
          }, 25000);

          connectionRetry = setInterval(function() {
            logger.debug('##### Start Reconnection');
            if (settings.needReconnection) {
              if (!RVACommandClient.destroyed) {
                RVACommandClient.destroy();
              }
              if (settings.pinRequired) {
                self.connect(settings.rvaIp, settings.clientName, settings.pinCode);
              } else {
                self.connect(settings.rvaIp, settings.clientName, null);
              }
            } else {
              logger.debug('##### clean connectionRetry');
              clearInterval(connectionRetry);
            }
          }, 5000);
          done();
        }
      ]);
    } else {
      logger.debug('##### Send CMD Keep Alive again');
      settings.try_times = 1;
      self.sendCmdKeepAlive();
    }
  }
};

RVAClient.prototype.checkReconnectionTimeout = function checkReconnectionTimeout() {
  settings.disconnectEvent = 'Disconnection';
  if (settings.launcherEnabled === true && qLauncherExists) {
    nativeService.ledOff(settings.LED.ALL);
  }

  if (nativeAlive && settings.isRestoreScreenResolution) {
    nativeService.restoreResolution(function() {
      logger.debug('resolution restored when reconnection fail');
    });
  }

  if (!RVACommandClient.destroyed) {
    RVACommandClient.destroy();
  }

  logger.debug('#####Reconnection Timeout');
  settings.needReconnection = false;
  clearTimeout(keepAliveScheduler);
  clearTimeout(keepAliveTimeout);
  clearInterval(connectionRetry);
  clearTimeout(reconnectionTimeout);


};

RVAClient.prototype.sendCmdModeratorViewerRequest = function sendCmdModeratorViewerRequest() {
  logger.verbose('Send MODERATOR_VIEWER_REQUEST, settings.client_id:' + settings.client_id + ' settings.clientName:' + settings.clientName);
  //var buf = new  Uint8Array(settings.MAXIUM_MSG_LENGTH);
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  /*
  var length = 0;
  for (var i = 0; i < settings.MINIMUM_MSG_LENGTH; i++) {
    buf[i] = 0;
  }
  */
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.MODERATOR_VIEWER_REQUEST; // message type
  buf[2] = settings.client_id; // client ID
  buf[3] = 0; // state
  /*
  if(process.platform === 'darwin')
    buf[4] = common.OS_DEV_TYPE.OS_MAC_PC;
  if(process.platform === 'win32')
    buf[4] = common.OS_DEV_TYPE.OS_WINDOWS_PC;
  /*
  length = settings.clientName.trim().length;
  logger.debug('settings.clientName.trim().length: ' + length);

  if (length > 0) {
		var data = dsaUtil.convertStringToBytes(settings.clientName.trim());
		length = data.length;
		logger.debug(' data.length: ' +  length);
		for (var k = 0; k < length; k++) {
			buf[5+k] = data[k];
		}
	}

  buf[0] = length + settings.MINIMUM_MSG_LENGTH + 1;
  buf = buf.subarray(0, length + settings.MINIMUM_MSG_LENGTH + 1);
  */
  logger.debug('Send MODERATOR_VIEWER_REQUEST: ' + JSON.stringify(buf));
  RVACommandClient.write(buf);
};

RVAClient.prototype.initialImageServer = function initialImageServer(splitPanelNumber) {
  logger.debug('##### Call Native server image start, splitPanelNumber: ' + splitPanelNumber);
  var self = this;
  settings.currentSplitPanel = splitPanelNumber;
  logger.debug('currentSplitPanel: ' + settings.currentSplitPanel);
  if (settings.rva_version.substring(0, 3) === 'v1.') {
    settings.splitModeFrameRate = 1;
  }

  if (settings.rvaType === 'CORP') {
    logger.debug('##### qr_ip = ' + settings.qr_ip + ' ##### ssid = ' + settings.ssid + ' pin code = ' + settings.pinCode);
    var corpKey = dsaUtil.getCorporationKey(settings.qr_ip, settings.ssid, settings.pinCode + '');
    nativeService.splitModeStart(true, settings.rvaIp, settings.TCP_DATA_PORT, settings.splitModeFrameRate, corpKey, function(result) {
      if (result) {
        logger.debug('############# image start success success success');
        isResume = true;
      } else {
        logger.debug('############# image start fail fail fail');
        clearTimeout(keepAliveTimeout);
        settings.needReconnection = true;
        settings.disconnectEvent = 'Reconnection';
        if (settings.launcherEnabled === true && qLauncherExists) {
          nativeService.ledOn(settings.LED.FULL_SCREEN_RED, 20, function() {});
        }
        RVACommandClient.destroy();
        logger.debug('##### Start Reconnection');
        setTimeout(function() {
          nativeService.splitModeStop();
          if (settings.pinRequired) {
            self.connect(settings.rvaIp, settings.clientName, settings.pinCode);
          } else {
            self.connect(settings.rvaIp, settings.clientName, null);
          }
        }, 5000);
      }
    });
  } else {
    nativeService.splitModeStart(false, settings.rvaIp, settings.TCP_DATA_PORT, settings.splitModeFrameRate, '', function(result) {
      if (result) {
        logger.debug('############# image start success success success');
        isResume = true;
      } else {
        logger.debug('############# image start fail fail fail');
        clearTimeout(keepAliveTimeout);
        settings.needReconnection = true;
        settings.disconnectEvent = 'Reconnection';
        if (settings.launcherEnabled === true && qLauncherExists) {
          nativeService.ledOn(settings.LED.FULL_SCREEN_RED, 20, function() {});
        }
        RVACommandClient.destroy();
        logger.debug('##### Start Reconnection');
        setTimeout(function() {
          nativeService.splitModeStop();
          if (settings.pinRequired) {
            self.connect(settings.rvaIp, settings.clientName, settings.pinCode);
          } else {
            self.connect(settings.rvaIp, settings.clientName, null);
          }
        }, 5000);
      }
    });
  }

  if (process.platform === 'darwin') {
    self.enableSoundFlowerState(false, function() {}, false);
  }
};

RVAClient.prototype.initialStreamingServer = function initialStreamingServer() {
  var self = this;
  var rvaSetting;
  async.series([
    function(done) {
      if (!settings.isReconnect) {
        self.getSettings(function(setting) {
          rvaSetting = setting;
          if (typeof rvaSetting.videoQuality === 'undefined') {
            logger.debug('rvaSetting.videoQuality');
            logger.debug(rvaSetting.videoQuality);
            rvaSetting.videoQuality = '0';
            done();
          } else
            done();
        });
      } else {
        self.getSettings(function(setting) {
          rvaSetting = setting;
          if (typeof rvaSetting.videoQuality === 'undefined') {
            logger.debug('rvaSetting.videoQuality');
            logger.debug(rvaSetting.videoQuality);
            rvaSetting.videoQuality = '0';
            done();
          } else
            done();
        });
      }
    },
    function(done) {
      logger.debug('initialStreamingServer rvaSetting: ' + JSON.stringify(rvaSetting));
      if (!rvaSetting.projectionMode)
        rvaSetting.projectionMode = '0';
      // only determine peermedia params from UI when the connection mode is not reconnect so that the peermedia args remains the same during reconnection
      if (!settings.isReconnect) {
        // set bit rate and frame rate according to screen resolution and user settings
        logger.debug('screenWidth:' + settings.screenWidth + ' screenHeight:' + settings.screenHeight + 'rva version: ' + settings.rva_version);
        logger.debug('dsa width: ' + dsa_width);
        if (dsa_width < settings.RESOLUTION_SCREEN_W_720P) {
          logger.debug('screenWidth is less than or equal to RESOLUTION_SCREEN_W_720P');
          logger.debug('rvaSetting.videoQuality: ' + rvaSetting.videoQuality);
          if (rvaSetting.videoQuality === '1') //normal
          {
            if (rvaSetting.projectionMode === '0') {
              framerate = settings.FRMAERATE_NORMAL_720P;
              bitrate = settings.BITRATE_NORMAL_720P;
              if (settings.rva_version.substring(0, 3) === 'v1.') {
                framerate = settings.FRMAERATE_NORMAL_720P_V1;
                bitrate = settings.BITRATE_NORMAL_720P_V1;
              }
              logger.debug('BITRATE_NORMAL_720P  framerate: ' + framerate + ' bitrate: ' + bitrate);
            } else {
              framerate = settings.FRMAERATE_VIDEO_720P;
              bitrate = settings.BITRATE_VIDEO_720P;
              if (settings.rva_version.substring(0, 3) === 'v1.') {
                framerate = settings.FRMAERATE_VIDEO_720P_V1;
                bitrate = settings.BITRATE_VIDEO_720P_V1;
              }
              logger.debug('BITRATE_VIDEO_720P framerate: ' + framerate + ' bitrate: ' + bitrate);
            }
          } else { // high
            if (rvaSetting.projectionMode === '0') {
              framerate = settings.FRMAERATE_HIGH_720P;
              bitrate = settings.BITRATE_HIGH_720P;
              if (settings.rva_version.substring(0, 3) === 'v1.') {
                framerate = settings.FRMAERATE_HIGH_720P_V1;
                bitrate = settings.BITRATE_HIGH_720P_V1;
              }
              logger.debug('BITRATE_HIGH_720P framerate: ' + framerate + ' bitrate: ' + bitrate);
            } else {
              framerate = settings.FRMAERATE_VIDEO_720P;
              bitrate = settings.BITRATE_VIDEO_720P;
              if (settings.rva_version.substring(0, 3) === 'v1.') {
                framerate = settings.FRMAERATE_VIDEO_720P_V1;
                bitrate = settings.BITRATE_VIDEO_720P_V1;
              }
              logger.debug('BITRATE_VIDEO_720P  framerate: ' + framerate + ' bitrate: ' + bitrate);
            }
          }
        } else {
          logger.debug('screenWidth is larger than RESOLUTION_SCREEN_W_720P');
          if (rvaSetting.videoQuality === '1') //normal
          {
            if (rvaSetting.projectionMode === '0') {
              framerate = settings.FRMAERATE_NORMAL_1080P;
              bitrate = settings.BITRATE_NORMAL_1080P;
              if (settings.rva_version.substring(0, 3) === 'v1.') {
                framerate = settings.FRMAERATE_NORMAL_1080P_V1;
                bitrate = settings.BITRATE_NORMAL_1080P_V1;
              }
              logger.debug('BITRATE_NORMAL_1080P framerate: ' + framerate + ' bitrate: ' + bitrate);
            } else {
              framerate = settings.FRMAERATE_VIDEO_1080P;
              bitrate = settings.BITRATE_VIDEO_1080P;
              if (settings.rva_version.substring(0, 3) === 'v1.') {
                framerate = settings.FRMAERATE_VIDEO_1080P_V1;
                bitrate = settings.BITRATE_VIDEO_1080P_V1;
              }
              logger.debug('BITRATE_VIDEO_1080P');
            }
          } else { // high
            if (rvaSetting.projectionMode === '0') {
              framerate = settings.FRMAERATE_HIGH_1080P;
              bitrate = settings.BITRATE_HIGH_1080P;
              if (settings.rva_version.substring(0, 3) === 'v1.') {
                framerate = settings.FRMAERATE_HIGH_1080P_V1;
                bitrate = settings.BITRATE_HIGH_1080P_V1;
              }
              logger.debug('BITRATE_HIGH_1080P,  framerate: ' + framerate + ' bitrate: ' + bitrate);
            } else {
              framerate = settings.FRMAERATE_VIDEO_1080P;
              bitrate = settings.BITRATE_VIDEO_1080P;
              if (settings.rva_version.substring(0, 3) === 'v1.') {
                framerate = settings.FRMAERATE_VIDEO_1080P_V1;
                bitrate = settings.BITRATE_VIDEO_1080P_V1;
              }
              logger.debug('BITRATE_VIDEO_1080P framerate: ' + framerate + ' bitrate: ' + bitrate);
            }
          }
        }
      }

      var buf = new Uint8Array(4);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      logger.debug('rvaSetting.projectionMode: ' + rvaSetting.projectionMode);
      if (rvaSetting.projectionMode === '1') {
        if (process.platform === 'darwin') {
          if (soundFlowerAllowed) {
            self.enableSoundFlowerState(true, function() {}, false);
          }
        }
        buf[1] = common.CMD.AUDIO_STREAMING_ENABLE;
        logger.debug('send AUDIO_STREAMING_ENABLE');
        buf[2] = 0;
        buf[3] = 0;
        var node_buf = new Buffer(buf);
        RVACommandClient.write(node_buf);
      } else {
        buf[1] = common.CMD.AUDIO_STREAMING_DISABLE;
        logger.debug('send AUDIO_STREAMING_DISABLE');
        buf[2] = 0;
        buf[3] = 0;
        var node_buf = new Buffer(buf);
        RVACommandClient.write(node_buf);
      }


      if (settings.rvaType === 'CORP') {
        var corpKey = dsaUtil.getCorporationKey(settings.qr_ip, settings.ssid, settings.pinCode + '');
        logger.debug('calling desktopStreamingStart CORP version,rva version: ' + settings.rva_version + ' rvaIP: ' + settings.rvaIp + ' TCP_VIDEO_PORT:' + settings.TCP_VIDEO_PORT + ' framerate:' + framerate + ' bitrate: ' + bitrate + ' audio:' + rvaSetting.projectionMode + ' corp key:' + corpKey);
        nativeService.desktopStreamingStart(settings.rva_version, true, settings.rvaIp, settings.TCP_VIDEO_PORT, framerate, bitrate, rvaSetting.projectionMode, corpKey, function(result) {
          logger.debug('desktopStreamingStart: ' + JSON.stringify(result));
          if (result) {
            logger.debug('############# streaming start success success success');
            if (!isResume) {
              nativeService.setAudioMute(function(cb) {
                logger.debug('Mute: ' + cb);
              });
            }
            isResume = false;
            setTimeout(function() {
              logger.debug('desktopStreamingStarted event');
              self.emit('desktopStreamingStarted');
            }, 1000);

            if (parseFloat(settings.rva_version.replace('v', '')) >= 2.0 && settings.remoteControl) {
              logger.debug('settings.rva_version: ' + settings.rva_version + ' trying to turn on remote control');
              nativeService.remoteControlStart(settings.rvaIp, settings.REMOTE_CONTROL_PORT, function(result) {
                logger.debug('remote control enabled: ' + result);
                remoteMouseState = true;
              });
            }
          } else {
            logger.debug('streaming start failed.');
            if (settings.launcherEnabled === true && qLauncherExists) {
              nativeService.ledOn(settings.LED.FULL_SCREEN_RED, 20, function() {});
            }
          }
        });
      } else {
        logger.debug('calling desktopStreamingStart EDU version, rva version: ' + settings.rva_version + ' rvaIP: ' + settings.rvaIp + ' TCP_VIDEO_PORT:' + settings.TCP_VIDEO_PORT + ' framerate:' + framerate + ' bitrate: ' + bitrate + ' audio:' + rvaSetting.projectionMode);
        nativeService.desktopStreamingStart(settings.rva_version, false, settings.rvaIp, settings.TCP_VIDEO_PORT, framerate, bitrate, rvaSetting.projectionMode, 'key', function(result) {
          if (result) {
            logger.debug('############# streaming start success success success');
            logger.debug('settings.rva_version: ' + settings.rva_version + ' trying to turn on remote control, fullMode:' + settings.fullMode + ' isResume:' + isResume);
            if (!isResume) {
              nativeService.setAudioMute(function(cb) {
                logger.debug('Mute: ' + cb);
              });
            }
            isResume = false;
            setTimeout(function() {
              logger.debug('desktopStreamingStarted event');
              self.emit('desktopStreamingStarted');
            }, 1000);
            if (parseFloat(settings.rva_version.replace('v', '')) >= 2.0) {
              if (settings.remoteControl) {
                logger.debug('settings.rva_version: ' + settings.rva_version + ' trying to turn on remote control');
                nativeService.remoteControlStart(settings.rvaIp, settings.REMOTE_CONTROL_PORT, function(result) {
                  logger.debug('remote control enabled: ' + result);
                  remoteMouseState = true;
                });
              }
            }
          } else {
            logger.debug('############# streaming start fail fail fail');
            clearTimeout(keepAliveTimeout);
            settings.needReconnection = true;
            settings.disconnectEvent = 'Reconnection';
            if (settings.launcherEnabled === true && qLauncherExists) {
              nativeService.ledOn(settings.LED.FULL_SCREEN_RED, 20, function() {});
            }
            RVACommandClient.destroy();
            logger.debug('##### Start Reconnection');
            setTimeout(function() {
              logger.debug('Reconnection desktopStreamingStop');
              nativeService.desktopStreamingStop();
              if (settings.pinRequired) {
                self.connect(settings.rvaIp, settings.clientName, settings.pinCode);
              } else {
                self.connect(settings.rvaIp, settings.clientName, null);
              }
            }, 5000);
          }
        });
      }
      //      self.emit('PeerMediaApp', 'started');
      done();
    }
  ]);
};

RVAClient.prototype.receiveCmdViewerStarted = function receiveCmdViewerStarted() {
  var self = this;
  logger.verbose('##### Process CMD VIEWER STARTED');
  clearTimeout(splitScreenTimer);
  logger.debug('##### SCREEN NUMBER = ' + settings.screen_number);
  logger.debug('##### FullMode = ' + settings.fullMode);
  logger.debug('##### SplitMode = ' + settings.splitMode);

  async.series([
    function(done) {
      logger.debug('check resolution -- receiveCmdViewerStarted');
      if (settings.rvaType === 'NOVOCAST') {
        if (dsa_width > 1920 || dsa_height > 1080) {
          if (settings.screenWidth < dsa_width || settings.screenHeight < dsa_height) {
            nativeService.setScreenResolution(1920, 1080, function(result) {
              logger.debug('set pc resolution to ' + 1920 + 'x' + 1080);
              logger.debug(result.cmd + ' ' + result.data);
              settings.isRestoreScreenResolution = true;
            });
          }
        }
      } else if (semver.gte(settings.rva_version, '2.0.0')) {
        if (dsa_width > 1920 || dsa_height > 1080) {
          if (settings.screenWidth < dsa_width || settings.screenHeight < dsa_height) {
            nativeService.setScreenResolution(1920, 1080, function(result) {
              logger.debug('set pc resolution to ' + 1920 + 'x' + 1080);
              logger.debug(result.cmd + ' ' + result.data);
              settings.isRestoreScreenResolution = true;
            });
          }
        }
      } else {
        logger.debug('rva version is less than 2.0.0');
        logger.debug('rva screenWidth: ' + settings.screenWidth + ' dsa width: ' + dsa_width);
        logger.debug('rva screenHeight: ' + settings.screenHeight + ' dsa width: ' + dsa_height);
        logger.debug('receiveCmdViewerStarted setScreenResolution ' + settings.screenWidth + 'x' + settings.screenHeight);
        if (dsa_width > settings.screenWidth || dsa_height > settings.screenHeight) {
          nativeService.setScreenResolution(settings.screenWidth, settings.screenHeight, function(result) {
            logger.debug('set pc resolution to ' + settings.screenWidth + 'x' + settings.screenHeight);
            logger.debug(result.cmd + ' ' + result.data);
            settings.isRestoreScreenResolution = true;
          });
        }
      }
      done();
    },
    function() {
      if (settings.screen_number === -2 && settings.fullMode === 0 && settings.splitMode === 0) {
        logger.debug('##### First presenter set full screen');
        settings.fullMode = 1;
        settings.splitMode = 0;
        settings.screen_number = 0;
        setTimeout(function() {
          self.initialStreamingServer();
        }, 0);
      }
    }
  ])
};

RVAClient.prototype.receiveCmdConnectionApproval = function receiveCmdConnectionApproval(bValues, s_bytes) {
  var self = this;
  logger.verbose('Process CMD CONNECTION_APPROVAL');
  RVAConnected = true;
  settings.client_id = bValues[s_bytes + 2]; // This is real client_id from RVA
  settings.runDiscovery = false;
  clearTimeout(connectTimeout);

  if (userUtil.group_list.length > 0) {
    settings.groupMode = true;
    userUtil.refreshUserListByGroupList();
  } else {
    settings.groupMode = false;
    settings.groupName = 'None'; //TODO: Molly helps for i18n
  }

  if (settings.isVersionMatch) {
    var doc = {
      'documentType': 'lastConnection',
      'ip': settings.rvaIp,
      'label': settings.dev_name,
      'userName': settings.clientName,
    };

    //Update last connection
    db.update({
      documentType: 'lastConnection'
    }, doc, {
      upsert: true
    }, function(err, numReplaced) {
      if (err !== null) {

      }
      logger.debug('##### Update HDD last connection' + numReplaced);

    });

    if (settings.currentDevice.type === settings.deviceType.USB) {
      //Update last connection
      db2.update({
        documentType: 'lastConnection'
      }, doc, {
        upsert: true
      }, function(err, numReplaced) {
        if (err !== null) {

        }
        logger.debug('##### Update USB last connection' + numReplaced);

      });

    }



    var doc2 = {
      ip: settings.rvaIp,
      label: settings.dev_name,
      userName: settings.clientName,
      boardcast: false
    };
    //Update username in history
    var recordList = settings.connectionRecord;
    var isNew = true;
    for (var i = 0; i < recordList.length; i++) {
      var record = recordList[i];
      if (settings.rvaIp === record.ip) {
        isNew = false;
        record.userName = settings.clientName;
        break;
      }
    }
    if (isNew) {
      settings.connectionRecord.unshift(doc2);
    }


    self.sendCmdKeepAlive('receiveCmdConnectionApproval()');
    self.sendCmdModeratorViewerRequest();

    if (settings.launcherEnabled === true && qLauncherExists) {
      nativeService.rvaConnectionState(0x01, function(result) {
        logger.debug('report rva connected to DSAService: ' + result);
      });
    }
    self.emit('connected', self.prepareSettingsForUI());
    //Mac
    if (process.platform === 'darwin') {
      self.getSettings(function(setting) {
        if (setting.projectionMode === '1') {
          if (soundFlowerReminder) {
            self.emit('SoundFlowerWarning', false);
          }
        }
      });
    }
    setTimeout(function() {
      settings.isConnectionApproval = true;
      isSendingConnected = false;
    }, 3000);
  } else {
    settings.disconnectEvent = 'invalidRVAVersion';
    self.sendCmdRemoveRequest();
    RVACommandClient.destroy();
    settings.isConnectionApproval = true;
    isSendingConnected = false;
  }
};

RVAClient.prototype.receive_CMD_SPLIT_SCREEN_READY = function receive_CMD_SPLIT_SCREEN_READY() {
  logger.verbose('##### Process CMD SPLIT SCREEN READY');
  //DSA does not use this function...
};

RVAClient.prototype.receive_CMD_FULL_SCREEN_READY = function receive_CMD_FULL_SCREEN_READY() {
  logger.verbose('Receive CMD FULL SCREEN READY');
  //DSA does not use this function...
};

RVAClient.prototype.receive_CMD_MODERATOR_BROADCAST_REQUEST = function receive_CMD_MODERATOR_BROADCAST_REQUEST() {
  var self = this;
  logger.verbose('Received CMD MODERATOR BROADCASE REQUEST');
  if (settings.rvaType === 'EDU') {
    self.emit('moderatorRequest', true);
    self.emit('minimizedAttention');
  } else {
    // light up your location
    if (settings.launcherEnabled === true && qLauncherExists) {
      var user = userUtil.getUserData(settings.client_id);
      var location = user.panel;
      logger.debug('##### Receive moderator release location = ' + location);
      switch (location) {
        case 0:
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledOn(settings.LED.FULL_SCREEN, 0);
          });
          break;
        case 1:
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledOn(settings.LED.SPLIT_1, 0);
          });
          break;
        case 2:
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledOn(settings.LED.SPLIT_2, 0);
          });
          break;
        case 3:
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledOn(settings.LED.SPLIT_3, 0);
          });
          break;
        case 4:
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledOn(settings.LED.SPLIT_4, 0);
          });
          break;
        default:
          nativeService.ledOff(settings.LED.ALL, function() {

          });
      }
    }
  }
  //TODO: Tell Molly close voitng answer
  self.emit('votingHandler', 'closeVotingWindow');
};

RVAClient.prototype.receive_CMD_RESET_RVA = function receive_CMD_RESET_RVA() {
  var self = this;
  logger.verbose('Received CMD RESET RVA');
  self.disconnect();
};

RVAClient.prototype.receive_CMD_RESET_DEVICE = function receive_CMD_RESET_DEVICE() {
  var self = this;
  logger.verbose('Received CMD RESET DEVICE');
  self.disconnect();
};

RVAClient.prototype.receive_CMD_MODERATOR_REQUEST = function receive_CMD_MODERATOR_REQUEST() {
  var self = this;
  logger.verbose('Receive CMD MODERATOR REQUEST');
  self.emit('moderatorRequest', true);
  self.emit('minimizedAttention');
};

RVAClient.prototype.receiveCmdModeratorViewerRequest = function receiveCmdModeratorViewerRequest(bValues, s_bytes) {
  var self = this;
  logger.debug('Process CMD_MODERATOR_VIEWER_REQUEST');
  settings.send_request_timeout = false;

  var uid = bValues[s_bytes + 2];
  var state = bValues[s_bytes + 3];
  var device = bValues[s_bytes + 4];

  var j = 0;
  var length = bValues[s_bytes];
  var strBuf = new Uint8Array(bValues[s_bytes] - 5);
  for (var i = s_bytes + 5; i < length + s_bytes; i++) {
    strBuf[j] = bValues[i];
    j++;
  }
  var userName = dsaUtil.byteArrayToUtf8(strBuf);

  userUtil.add_userdata(uid, userName, false, null, -1, false, true, device);
  self.checkStateCase(uid, common.CMD.MODERATOR_VIEWER_REQUEST, state);

  return length;
};


RVAClient.prototype.sendCmdToConnectionMgr = function sendCmdToConnectionMgr(cmd, arg1, arg2) {
  logger.debug('sendCmdToConnectionMgr cmd: ' + cmd + ' arg1:' + arg1 + ' arg1:' + arg2);
  logger.debug('cmd: ' + cmd);
  logger.debug('convertIntToBytesBigEndian cmd: ' + dsaUtil.convertIntToBytesBigEndian(cmd));
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  // buf[1] = dsaUtil.convertIntToBytesBigEndian(cmd);
  buf[1] = cmd;
  buf[2] = arg1;
  buf[3] = arg2;

  RVACommandClient.write(buf);
};

RVAClient.prototype.sendCmdConnectionRequest = function sendCmdConnectionRequest() {
  logger.debug('send CMD CONNECTION_REQUEST');
  var buf = new Buffer(settings.MAXIUM_MSG_LENGTH);
  for (var i = 0; i < settings.MAXIUM_MSG_LENGTH; i++) {
    buf[i] = 0;
  }

  buf[1] = common.CMD.CM_CONNECTION_REQUEST; // message type
  buf[2] = settings.client_id; // client ID
  buf[3] = settings.state; // state

  var dataPortArray = dsaUtil.convertIntToBytesBigEndian(settings.pinCode);
  buf[4] = dataPortArray[0];
  buf[5] = dataPortArray[1];
  buf[6] = dataPortArray[2];
  buf[7] = dataPortArray[3];

  var pversion = dsaUtil.convertIntToBytesBigEndian(settings.SOFTWARE_VERSION);
  buf[8] = pversion[0];
  buf[9] = pversion[1];
  buf[10] = pversion[2];
  buf[11] = pversion[3];

  if (process.platform === 'darwin')
    buf[12] = common.OS_DEV_TYPE.OS_MAC_PC;
  else if (process.platform === 'win32') {
    buf[12] = common.OS_DEV_TYPE.OS_WINDOWS_PC; //OS_WINDOWS_PC; //OS_DEV_TYPE.OS_CHROME_PC; // ChromeBook = 9
    if (settings.localStreamingThruProxy === false) {
      buf[12] = common.OS_DEV_TYPE.OS_MAC_PC;
    }
  } else
    buf[12] = common.OS_DEV_TYPE.OS_LINUX_PC;

  if (settings.pinRequired) {
    buf[13] = 1;
  } else {
    buf[13] = 0;
  }

  //width
  var width = dsaUtil.convertIntToBytesBigEndian(settings.screenWidth);
  logger.debug('settings.screenWidth:' + settings.screenWidth);
  buf[14] = width[0];
  buf[15] = width[1];
  buf[16] = width[2];
  buf[17] = width[3];

  //height
  var height = dsaUtil.convertIntToBytesBigEndian(settings.screenHeight);
  logger.debug('settings.screenHeight:' + settings.screenHeight);
  buf[18] = height[0];
  buf[19] = height[1];
  buf[20] = height[2];
  buf[21] = height[3];

  var clientLength = dsaUtil.lengthInUtf8Bytes(settings.clientName);
  logger.debug('client name length:' + clientLength);

  if (clientLength > 0) {
    var data = dsaUtil.utf8ToByteArray(settings.clientName);
    clientLength = data.length;

    for (var k = 0; k < clientLength; k++) {
      buf[22 + k] = data[k];
    }
  }

  buf[0] = clientLength + settings.MINIMUM_MSG_LENGTH + 18;
  //buf = buf.subarray(0, clientLength + settings.MINIMUM_MSG_LENGTH + 18);
  buf = buf.slice(0, clientLength + settings.MINIMUM_MSG_LENGTH + 18);
  // creating node buffer
  //logger.debug('RVACommandClient.destroyed: ' + RVACommandClient.destroyed);
  // logger.debug(RVACommandClient);
  // logger.debug(buf);
  //var node_buf = new Buffer(buf);

  RVACommandClient.write(buf);
};
RVAClient.prototype.receive_CMD_MODERATOR_VIEWER_STATE_CHANGE = function receive_CMD_MODERATOR_VIEWER_STATE_CHANGE(bValues, s_bytes) {
  logger.verbose('Process CMD MODERATOR VIEWER STATE CHANGE');
  var self = this;
  var change_uid = bValues[s_bytes + 2];
  var change_state = bValues[s_bytes + 3];

  self.checkStateCase(change_uid, common.CMD.MODERATOR_VIEWER_STATE_CHANGE, change_state);
};

RVAClient.prototype.receive_CMD_REQUEST_TEACHER_PRIORITY = function receive_CMD_REQUEST_TEACHER_PRIORITY(bValues, s_bytes) {
  logger.verbose('Process CMD_REQUEST_TEACHER_PRIORITY');
  var self = this;
  var arg = bValues[s_bytes + 3];
  logger.debug('arg: ' + arg);
  switch (arg) {
    case common.TEACHER_CREDENTIAL_CMD.PASSWORD_CORRECT:
      logger.debug('Teacher credential password correct');
      break;
    case common.TEACHER_CREDENTIAL_CMD.PASSWORD_INCORRECT:
      logger.debug('Teacher credential password incorrect');
      if (isFirstTeacherPriorityError) {
        isFirstTeacherPriorityError = false;
        self.emit('teacherCredentialPasswordError', 2);
      } else {
        self.emit('teacherCredentialPasswordError', 1);
      }
      break;
    case common.TEACHER_CREDENTIAL_CMD.REQUEST:
      // check moderator_credential status
      logger.debug('Receive Teacher credential request');
      self.check_as_Moderator();
      break;
    case common.TEACHER_CREDENTIAL_CMD.NO_PERMISSION:
      logger.debug('Receive Teacher credential no permission');
      self.emit('teacherCredentialNoPermission');

      break;
    case common.TEACHER_CREDENTIAL_CMD.NO_PERMISSION_VOTING_ON:
      logger.debug('Receive Teacher credential no permission voting on');
      self.emit('teacherCredentialNoPermissionVotingOn');

      break;
    case common.TEACHER_CREDENTIAL_CMD.NO_PERMISSION_TEACHER_ON:
      logger.debug('Receive Teacher credential no permission teacher on');
      self.emit('teacherCredentialNoPermissionTeacherOn');

      break;
    default:
      logger.debug('CMD REQUEST TEACHER PRIORITY DEFAULT');
  }
};

RVAClient.prototype.send_CMD_REQUEST_TEACHER_PRIORITY = function send_CMD_REQUEST_TEACHER_PRIORITY(arg1, credential_password) {
  logger.debug('Process send_CMD_REQUEST_TEACHER_PRIORITY');
  logger.debug('pw: ' + credential_password);
  var length = credential_password.length;
  logger.debug('credential_password password length: ' + length);

  var buf = new Buffer(length + settings.MINIMUM_MSG_LENGTH);
  buf[0] = length + settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.REQUEST_TEACHER_PRIORITY;
  buf[2] = settings.client_id;
  buf[3] = arg1;

  if (length > 0) {
    var data = dsaUtil.convertStringToBytes(credential_password);
    logger.debug('credential_password data:');
    logger.debug(data);
    length = data.length;

    for (var k = 0; k < length; k++) {
      buf[settings.MINIMUM_MSG_LENGTH + k] = data[k];
    }
  }
  RVACommandClient.write(buf);

};

RVAClient.prototype.check_as_Moderator = function check_as_Moderator() {
  var self = this;
  logger.debug('Process CMD_REQUEST_TEACHER_PRIORITY');
  self.getTeacherCredentialSetting(function(passwordRequired) {
    logger.debug('passwordRequired: ' + passwordRequired);
    if (passwordRequired) {
      if (settings.groupMode) {
        logger.debug('No teacher credential in group mode');
      } else {
        self.getTeacherCredentialPassword(function(password) {
          logger.debug('password: ' + password);
          self.send_CMD_REQUEST_TEACHER_PRIORITY(2, password);
        });
      }
    }
  });
};

RVAClient.prototype.revalidateTeacherCredentialPassword = function revalidateTeacherCredentialPassword(password) {
  var self = this;
  logger.debug('Process revalidateTeacherCredentialPassword');
  self.setTeacherCredentialPassword(password);
  self.check_as_Moderator();
};

/**
 * Disconnect RVA
 */
RVAClient.prototype.disconnect = function disconnect(flag, cb) {
  var self = this;
  logger.debug('Disconnect RVA, flag: ' + flag);
  settings.runDiscovery = true;
  settings.tabletsLocked = false;
  settings.currentScreen = settings.screenType.MAIN;
  nextCommand = [];
  if (flag !== 'appEnd') {
    clearTimeout(keepAliveScheduler);
    clearTimeout(keepAliveTimeout);
    isStateChange = true;
    logger.debug('Disconnect RVA, flag is not appEnd');
    logger.debug('nativeAlive: ' + nativeAlive);

    async.series([
      function(done) {
        logger.debug('disconnect step 1 -- restoreAudioMute');
        nativeService.restoreAudioMute(function() {
          done();
        });
      },
      function(done) {
        logger.debug('disconnect step 2 -- ledOff, ledFlash');
        if (nativeAlive && qLauncherExists) {
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledFlash(settings.LED.FULL_SCREEN_BLUE, 0, 500, function() {
              done();
            });
          });
        } else {
          done();
        }
      },
      function(done) {
        logger.debug('disconnect step 3 -- disable sound flower');
        if (process.platform === 'darwin') {
          // determine if sound flower is on, if it is, then turn it off, if it is not, skip this step
          if (nativeAlive) {
            nativeService.getSoundflowerState(function(result) {
              logger.debug('SF result: ' + result);
              if (result !== 'SOUNDFLOWER_OUTPUT_DISABLE') {
                logger.debug('trying to disable SF');
                nativeService.soundflowerStateDisable(function() {
                  logger.debug('SF disabled');
                  done();
                });
              } else {
                done();
              }
            });
          } else {
            done();
          }
        } else if (process.platform === 'win32') {
          logger.debug('OS is win32, no need to disable sound flower');
          done();
        }
      },
      function(done) {
        logger.debug('disconnect step 4 -- remote mouse stop');
        if (nativeAlive) {
          logger.debug('sending remote mouse stop cmd...');
          self.remoteMouseStop(function(result) {
            logger.debug('remote mouse stop: ' + result);
            remoteMouseState = false;
            done();
          });
        } else {
          done();
        }
      },
      function(done) {
        logger.debug('disconnect step 5 -- Sending Remove Request and disconnect RVA');
        self.sendCmdRemoveRequest();
        settings.disconnectEvent = 'Disconnection';
        RVACommandClient.destroy();
        done();
      },
      function(done) {
        logger.debug('disconnect step 6 -- Stop streaming...');
        if (nativeAlive) {
          logger.debug('Stop streaming...');
          if (settings.fullMode > 0) {
            settings.fullMode = 0;
            nativeService.desktopStreamingStop(function(result) {
              logger.debug('Streaming stop result = ' + result);
              done();
            });
          } else if (settings.splitMode > 0) {
            settings.splitMode = 0;
            logger.debug('Stop split screen server...');
            nativeService.splitModeStop(function(result) {
              logger.debug('Split stop result = ' + result);
              done();
            });
          } else {
            logger.debug('##### Nothing need to close');
            done();
          }
        } else {
          done();
        }
      },
      /* The correct sequence is to call setExtendedScreenMode after calling desktopStreamingStop, if desktopStreamingStop is needed */
      function(done) {
        logger.debug('disconnect step 7 -- disable extended screen');
        if (nativeAlive) {
          nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
            logger.debug('disconnect setExtendedScreenMode result:' + result);
            done();
          });
        } else {
          done();
        }
      },
      function(done) {
        logger.debug('disconnect step 8 -- Restoring main screen resolution...');
        if (nativeAlive && settings.isRestoreScreenResolution) {
          nativeService.restoreResolution(function() {
            logger.debug('main screen resolution restored');
            done();
          });
        } else {
          done();
        }
      },
      function(done) {
        logger.debug('disconnect step 9 -- disconnect successful, light up LED');
        if (nativeAlive && qLauncherExists) {
          logger.debug('ledGradientFlash  FULL_SCREEN_RED');
          nativeService.ledGradientFlash(settings.LED.FULL_SCREEN_RED, 5, 3000, function() {
            ledTimeout = setTimeout(function() {
              nativeService.ledOff(settings.LED.ALL, function() {
                nativeService.ledOn(settings.LED.FULL_SCREEN_RED, 0);
              });
            }, 25000);
            done();
          });
        } else {
          done();
        }
      },
      function(done) {
        logger.debug('disconnect step 10 -- send rva disconnected state to DSAService');
        if (nativeAlive) {
          nativeService.rvaConnectionState(0x00, function(result) {
            logger.debug('report rva disconnected to DSAService: ' + result);
            done();
          });
        } else {
          done();
        }
      }
    ]);

  } else {
    logger.debug('Disconnect RVA, flag is appEnd, nativeAlive: ' + nativeAlive);
    async.series([
      function(done) {
        nativeService.restoreAudioMute(function() {
          done();
        });
      },
      function(done) {
        if (nativeAlive) {
          if (settings.launcherEnabled === true && qLauncherExists) {
            logger.debug('Disconnect RVA, turn all LEDs off');
            nativeService.ledOff(settings.LED.ALL, function() {
              logger.debug('LED off off off');
              done();
            });
          } else {
            done();
          }
        } else {
          done();
        }
      },
      function(done) {
        if (process.platform === 'darwin') {
          if (nativeAlive) {
            // determine if sound flower is on, if it is, then turn it off, if it is not, skip this step
            nativeService.getSoundflowerState(function(result) {
              logger.debug('SF result: ' + result);
              if (result !== 'SOUNDFLOWER_OUTPUT_DISABLE') {
                logger.debug('trying to disable SF');
                nativeService.soundflowerStateDisable(function() {
                  logger.debug('SF disabled');
                  done();
                });
              } else
                done();
            });
          } else {
            done();
          }
        } else if (process.platform === 'win32') {
          logger.debug('OS is win32');
          done();
        }
      },
      function(done) {
        if (nativeAlive) {
          logger.debug('sending remote mouse stop cmd...');
          self.remoteMouseStop(function(result) {
            logger.debug('remote mouse stop: ' + result);
            remoteMouseState = false;
            done();
          });
        } else {
          done();
        }
      },
      function(done) {
        if (nativeAlive) {
          logger.debug('Stop streaming...');
          if (settings.fullMode > 0) {
            settings.fullMode = 0;
            nativeService.desktopStreamingStop(function(result) {
              logger.debug('Streaming stop result = ' + result);
              done();
            });
          } else if (settings.splitMode > 0) {
            settings.splitMode = 0;
            logger.debug('Stop split screen server...');
            nativeService.splitModeStop(function(result) {
              logger.debug('Split stop result = ' + result);
              done();
            });
          } else {
            logger.debug('##### Nothing need to close');
            done();
          }
        } else {
          done();
        }
      },
      function(done) {
        logger.debug('restore resolution');
        if (nativeAlive && settings.isRestoreScreenResolution) {
          nativeService.restoreResolution(function() {
            logger.debug('resolution restored process.platform: ' + process.platform);
            done();
          });
        } else {
          done();
        }
      },
      function(done) {
        logger.debug('disable extended screen');
        if (nativeAlive) {
          nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
            logger.debug('disconnect setExtendedScreenMode result:' + result);
            done();
          });
        } else {
          done();
        }
      },
      function(done) {
        logger.debug('remove request');
        if (RVACommandClient)
          if (!RVACommandClient.destroyed) {
            self.sendCmdRemoveRequest();
          }
        done();
      },
      function(done) {
        logger.debug('enable power saving');
        if (nativeAlive) {
          nativeService.powerSavingEnable(function() {
            logger.debug('power saving enabled');
            done();
          });
        } else {
          done();
        }
      },
      function(done) {
        logger.debug('kill native, nativeAlive: ' + nativeAlive);
        //delay for 1 second to wait for native to finish all its jobs
        setTimeout(function() {
          logger.debug('kill native, wait for 1 second');
          if (nativeAlive) {
            nativeService.exit(done());
          } else {
            done();
          }
        }, 1000);
      },
      function(done) {
        logger.debug('applicationEnd event');
        settings.disconnectEvent = 'applicationEnd';
        if (RVACommandClient) {
          logger.debug('RVACommandClient.destroyed: ' + RVACommandClient.destroyed);
          if (!RVACommandClient.destroyed) {
            self.sendCmdRemoveRequest();
            RVACommandClient.destroy();
            done();
          } else {
            done();
          }
        } else {
          done();
        }
      },
      function() {
        rvaSelf.emit('dsaQuit');
        if (cb)
          cb();
      }
    ]);
  }
};

RVAClient.prototype.prepareSettingsForUI = function prepareSettingsForUI() {
  logger.debug('prepareSettingsForUI: ' + settings.rvaType);
  if (settings.rvaType !== 'NOVOCAST') {
    var cName = '';
    if (settings.groupMode) {
      cName = userUtil.getSendNameById(settings.client_id);
    } else {
      cName = settings.clientName;
    }
    var doc = {
      clientName: cName,
      message: 'You are connected.', //TODO: Molly i18n
      rvaIP: settings.rvaIp,
      rvaType: settings.rvaType,
      rvaVersion: parseFloat(settings.rva_version.replace('v', '')),
      pinRequired: settings.pinRequired,
      pinCode: settings.pinCode,
      ssid: settings.qr_ssid.replace('"', '').replace('"', ''),
      wifi: {
        IP: settings.qr_ip,
        ssid: settings.qr_ssid.replace('"', '').replace('"', '')
      },
      lanIP: settings.qr_lan,
      groupMode: settings.groupMode,
      groupName: settings.groupName,
      dataPath: settings.dataPath,
      qrImg: qrImg
    };
    return doc;
  } else {
    var doc = {
      clientName: settings.clientName,
      message: 'You are connected.', //TODO: Molly i18n
      rvaIP: settings.rvaIp,
      rvaType: settings.rvaType,
      rvaVersion: parseFloat(settings.rva_version.replace('v', '')),
      pinRequired: settings.pinRequired,
      pinCode: settings.pinCode,
      groupMode: false,
      dataPath: settings.dataPath
    };
    logger.debug(JSON.stringify(doc));
    return doc;
  }
};

RVAClient.prototype.checkStateCase = function checkStateCase(userid, cmd, arg) {
  var self = this;
  if (userid === settings.client_id) {
    logger.debug('##### set isStateChange = true');
    self.sendNextLauncherCommand();
    isStateChange = true;
  }
  logger.debug('checkStateCase userid: ' + userid + ' cmd: ' + cmd + ' arg: ' + arg);
  clearTimeout(splitScreenTimer);
  self.emit('stopWaitingPageWithTimer');
  switch (arg) {
    case common.STATE.VIEWER_PENDING:
      logger.debug('STATE.VIEWER_PENDING');
      self.receiveStateViewerPending(userid, cmd);
      break;
    case common.STATE.VIEWER_HOLDER:
      logger.debug('STATE.VIEWER_HOLDER');
      self.receive_STATE_VIEWER_HOLDER(userid, cmd);
      break;
    case common.STATE.MODERATOR_HOLDER:
      logger.debug('STATE.MODERATOR_HOLDER');
      self.receive_STATE_MODERATOR_HOLDER(userid, cmd);
      break;
    case common.STATE.MODERATOR_RELEASE:
      logger.debug('STATE.MODERATOR_RELEASE');
      break;
    case common.STATE.MODERATOR_HOLDER_VIEWER_HOLDER:
      logger.debug('STATE.MODERATOR_HOLDER_VIEWER_HOLDER');
      self.receive_STATE_MODERATOR_HOLDER_VIEWER_HOLDER(userid, cmd);
      break;
    case common.STATE.READY_FOR_DISPLAY_SCREENSHOT:
      logger.debug('STATE.READY_FOR_DISPLAY_SCREENSHOT');
      self.receive_READY_FOR_DISPLAY_SCREENSHOT(userid, cmd);
      break;
    case common.STATE.VIEWER_HOLDER_1:
      logger.debug('STATE.VIEWER_HOLDER_1');
      self.receive_VIEWER_HOLDER(userid, cmd, 1);
      break;
    case common.STATE.VIEWER_HOLDER_2:
      logger.debug('STATE.VIEWER_HOLDER_2');
      self.receive_VIEWER_HOLDER(userid, cmd, 2);
      break;
    case common.STATE.VIEWER_HOLDER_3:
      logger.debug('STATE.VIEWER_HOLDER_3');
      self.receive_VIEWER_HOLDER(userid, cmd, 3);
      break;
    case common.STATE.VIEWER_HOLDER_4:
      logger.debug('STATE.VIEWER_HOLDER_4');
      self.receive_VIEWER_HOLDER(userid, cmd, 4);
      break;
    case common.STATE.MODERATOR_VIEWER_HOLDER_1:
      logger.debug('STATE.MODERATOR_VIEWER_HOLDER_1');
      self.receive_MODERATOR_VIEWER_HOLDER(userid, cmd, 1);
      break;
    case common.STATE.MODERATOR_VIEWER_HOLDER_2:
      logger.debug('STATE.MODERATOR_VIEWER_HOLDER_2');
      self.receive_MODERATOR_VIEWER_HOLDER(userid, cmd, 2);
      break;
    case common.STATE.MODERATOR_VIEWER_HOLDER_3:
      logger.debug('STATE.MODERATOR_VIEWER_HOLDER_3');
      self.receive_MODERATOR_VIEWER_HOLDER(userid, cmd, 3);
      break;
    case common.STATE.MODERATOR_VIEWER_HOLDER_4:
      logger.debug('STATE.MODERATOR_VIEWER_HOLDER_4');
      self.receive_MODERATOR_VIEWER_HOLDER(userid, cmd, 4);
      break;
    default:
      logger.debug('Others, userid:' + userid + ' cmd: ' + cmd + ' arg: ' + arg);
  }

  self.changeSplitModeFramerate(function(callback) {
    if (callback === '200') {
      console.log('Check Framerate');
    } else {

    }
  });

  if (userid === settings.client_id) {
    self.sendNextLauncherCommand();
  }
};

RVAClient.prototype.receiveStateViewerPending = function receiveStateViewerPending(userid /*, cmd*/ ) {
  var self = this;
  userUtil.update_userdata(userid, false, -1, true);
  settings.isCorpModeratorOn = userUtil.isHostOn();
  if (userid === settings.client_id) {
    self.emit('hostRequested', false);
    self.emit('viewerState', 'resume');
    settings.screen_number = -1;
    if (settings.launcherEnabled === true && qLauncherExists) {
      if (settings.rvaType === 'CORP') {
        if (settings.isCorpModeratorOn) {
          nativeService.ledOff(settings.LED.ALL);
        } else {
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledGradientFlash(settings.LED.FULL_SCREEN_BLUE, 3, 3000);
          });
        }
      } else {
        nativeService.ledOff(settings.LED.ALL);
      }
    }

    if (settings.fullMode > 0) {
      settings.fullMode = 0;
      nativeService.desktopStreamingStop(function() {
        if (remoteMouseState) {
          if (settings.remoteControl) {
            nativeService.remoteControlStop(function(result) {
              logger.debug('remote control disabled: ' + result);
              remoteMouseState = false;
            });
          }
        }
        nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
          logger.debug('receiveStateViewerPending setExtendedScreenMode result:' + result);
        });
      });
    } else if (settings.splitMode > 0) {
      settings.splitMode = 0;
      nativeService.splitModeStop();
    }
    if (process.platform === 'darwin') {
      if (soundFlowerAllowed) {
        self.enableSoundFlowerState(false, function() {}, false);
      }
    }
    settings.isLauncherCMDBack = true;
    clearTimeout(launcherTimer);
  }
  self.updateRemotePanel(0);
  self.emit('userListUpdated', userUtil.user_list);
};

RVAClient.prototype.receive_READY_FOR_DISPLAY_SCREENSHOT = function receive_READY_FOR_DISPLAY_SCREENSHOT(userid) {
  logger.debug('Userid = ' + userid + ' is ready for display screenshot');
};

RVAClient.prototype.receive_STATE_VIEWER_HOLDER = function receive_STATE_VIEWER_HOLDER(userid) {
  var self = this;
  userUtil.update_userdata(userid, false, 0, true);
  settings.isCorpModeratorOn = userUtil.isHostOn();

  if (userid === settings.client_id) {
    self.emit('hostRequested', false);
    self.emit('viewerState', 'resume');
    settings.screen_number = 0;
    if (settings.launcherEnabled === true && qLauncherExists) {
      if (settings.rvaType === 'CORP') {
        if (settings.isCorpModeratorOn) {
          nativeService.ledOff(settings.LED.ALL);
        } else {
          nativeService.ledOff(settings.LED.ALL, function() {
            nativeService.ledOn(settings.LED.FULL_SCREEN_BLUE, 0);
          });
        }
      }
    }

    if (settings.splitMode > 0) {
      settings.splitMode = 0;
      logger.debug('##### STOP IMAGE');
      nativeService.splitModeStop();
    }
    if (settings.fullMode < 1) {
      settings.fullMode = 1;
      logger.debug('##### START STREAMING');
      self.initialStreamingServer();
    }
    settings.isLauncherCMDBack = true;
    clearTimeout(launcherTimer);
  }

  self.updateRemotePanel(0);
  self.emit('viewModeChanged', 'full');
  self.emit('userListUpdated', userUtil.user_list);
};

RVAClient.prototype.receive_STATE_MODERATOR_HOLDER = function receive_STATE_MODERATOR_HOLDER(userid, cmd) {
  var self = this;
  logger.debug('Process receive_STATE_MODERATOR_HOLDER');
  userUtil.update_userdata(userid, true, -1, true);
  if (settings.rvaType === 'CORP') {
    settings.isCorpModeratorOn = true;
  }

  if (userid === settings.client_id) {
    self.emit('hostRequested', true);
    if (cmd !== common.CMD.MODERATOR_VIEWER_REQUEST) {
      settings.screen_number = -1;

      if (settings.launcherEnabled === true && qLauncherExists && nativeAlive) {
        nativeService.ledOff(settings.LED.ALL, function() {
          nativeService.ledGradientFlash(settings.LED.FULL_SCREEN_BLUE, 3, 3000);
        });
      }

      if (settings.fullMode > 0) {
        settings.fullMode = 0;
        logger.debug('##### STOP STREAMING');
        nativeService.desktopStreamingStop(function() {
          if (remoteMouseState) {
            if (settings.remoteControl) {
              nativeService.remoteControlStop(function(result) {
                logger.debug('remote control disabled: ' + result);
                remoteMouseState = false;
              });
            }
          }
          nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
            logger.debug('receive_STATE_MODERATOR_HOLDER setExtendedScreenMode result:' + result);
          });
        });

      }
      if (settings.splitMode > 0) {
        settings.splitMode = 0;
        logger.debug('##### STOP IMAGE');
        nativeService.splitModeStop();
      }
      self.emit('viewerState', 'resume');
    }
    settings.isLauncherCMDBack = true;
    clearTimeout(launcherTimer);
  } else {
    if (settings.launcherEnabled === true && qLauncherExists && nativeAlive) {
      nativeService.ledOff(settings.LED.ALL);
    }
  }

  self.updateRemotePanel(0);
  self.emit('userListUpdated', userUtil.user_list);
};

RVAClient.prototype.receive_STATE_MODERATOR_HOLDER_VIEWER_HOLDER = function receive_STATE_MODERATOR_HOLDER_VIEWER_HOLDER(userid, cmd) {
  var self = this;
  userUtil.update_userdata(userid, true, 0, true);
  if (settings.rvaType === 'CORP') {
    settings.isCorpModeratorOn = true;
  }
  if (userid === settings.client_id) {
    self.emit('hostRequested', true);
    settings.screen_number = 0;
    if (cmd !== common.CMD.MODERATOR_VIEWER_REQUEST) {
      if (settings.launcherEnabled === true && qLauncherExists && nativeAlive) {
        nativeService.ledOff(settings.LED.ALL, function() {
          nativeService.ledOn(settings.LED.FULL_SCREEN_BLUE, 0);
        });
      }

      if (settings.splitMode > 0) {
        settings.splitMode = 0;
        logger.debug('##### STOP IMAGE');
        nativeService.splitModeStop();
      }
      if (settings.fullMode < 1) {
        settings.fullMode = 1;
        logger.debug('##### START STREAMING');
        self.initialStreamingServer();
      }
      self.emit('viewerState', 'resume');
    }
    settings.isLauncherCMDBack = true;
    clearTimeout(launcherTimer);
  } else {
    if (settings.launcherEnabled === true && qLauncherExists && nativeAlive) {
      nativeService.ledOff(settings.LED.ALL);
    }
  }

  self.updateRemotePanel(0);
  self.emit('viewModeChanged', 'full');
  self.emit('userListUpdated', userUtil.user_list);
};

RVAClient.prototype.receive_VIEWER_HOLDER = function receive_VIEWER_HOLDER(userid, cmd, splitPanelNumber) {
  var self = this;
  userUtil.update_userdata(userid, false, splitPanelNumber, true);
  settings.isCorpModeratorOn = userUtil.isHostOn();
  if (userid === settings.client_id) {
    self.emit('hostRequested', false);
    if (cmd !== common.CMD.MODERATOR_VIEWER_REQUEST) {
      settings.screen_number = splitPanelNumber;
      if (settings.launcherEnabled === true && qLauncherExists) {
        if (settings.rvaType === 'CORP' && !settings.isCorpModeratorOn) {
          switch (splitPanelNumber) {
            case 1:
              nativeService.ledOff(settings.LED.ALL, function() {
                nativeService.ledOn(settings.LED.SPLIT_1, 0, function() {
                  logger.debug('SPLIT_SCREEN_BLUE_1 on 50ms ');
                });
              });
              break;
            case 2:
              nativeService.ledOff(settings.LED.ALL, function() {
                nativeService.ledOn(settings.LED.SPLIT_2, 0, function() {
                  logger.debug('SPLIT_SCREEN_BLUE_2 on 50ms ');
                });
              });
              break;
            case 3:
              nativeService.ledOff(settings.LED.ALL, function() {
                nativeService.ledOn(settings.LED.SPLIT_3, 0, function() {
                  logger.debug('SPLIT_SCREEN_BLUE_3 on 50ms ');
                });
              });
              break;
            case 4:
              nativeService.ledOff(settings.LED.ALL, function() {
                nativeService.ledOn(settings.LED.SPLIT_4, 0, function() {
                  logger.debug('SPLIT_SCREEN_BLUE_4 on 50ms ');
                });
              });
              break;
          }
        }
      }
      if (settings.fullMode > 0) {
        settings.fullMode = 0;
        logger.debug('##### STOP STREAMING');
        nativeService.desktopStreamingStop(function() {
          logger.debug('split mode 1 - disable remote control');
          if (remoteMouseState) {
            if (settings.remoteControl) {
              nativeService.remoteControlStop(function(result) {
                logger.debug('remote control disabled: ' + result);
                remoteMouseState = false;
              });
            }
          }
          /*
          nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
            logger.debug('receive_VIEWER_HOLDER setExtendedScreenMode result:' + result);
          });*/
        });
      }
      if (settings.splitMode < 1) {
        settings.splitMode = 1;
        logger.debug('##### START IMAGE');
        self.initialImageServer(splitPanelNumber);
      } else if (settings.splitMode === 2) {
        logger.debug('##### RESUME IMAGE');
        nativeService.splitModeResume();
      }
      self.emit('viewerState', 'resume');
    }
    settings.isLauncherCMDBack = true;
    clearTimeout(launcherTimer);
  }

  self.updateRemotePanel(0);
  self.emit('viewModeChanged', 'split');
  self.emit('userListUpdated', userUtil.user_list);
};

RVAClient.prototype.receive_MODERATOR_VIEWER_HOLDER = function receive_MODERATOR_VIEWER_HOLDER(userid, cmd, location) {
  var self = this;
  userUtil.update_userdata(userid, true, location, true);
  if (settings.rvaType === 'CORP') {
    settings.isCorpModeratorOn = true;
  }
  if (userid === settings.client_id) {
    self.emit('hostRequested', true);
    if (cmd !== common.CMD.MODERATOR_VIEWER_REQUEST) {
      settings.screen_number = location;
      if (settings.launcherEnabled === true && qLauncherExists) {
        switch (location) {
          case 1:
            nativeService.ledOff(settings.LED.ALL, function() {
              nativeService.ledOn(settings.LED.SPLIT_1, 0, function() {
                logger.debug('SPLIT_SCREEN_BLUE_1 on 50ms ');
              });
            });
            break;
          case 2:
            nativeService.ledOff(settings.LED.ALL, function() {
              nativeService.ledOn(settings.LED.SPLIT_2, 0, function() {
                logger.debug('SPLIT_SCREEN_BLUE_2 on 50ms ');
              });
            });
            break;
          case 3:
            nativeService.ledOff(settings.LED.ALL, function() {
              nativeService.ledOn(settings.LED.SPLIT_3, 0, function() {
                logger.debug('SPLIT_SCREEN_BLUE_3 on 50ms ');
              });
            });
            break;
          case 4:
            nativeService.ledOff(settings.LED.ALL, function() {
              nativeService.ledOn(settings.LED.SPLIT_4, 0, function() {
                logger.debug('SPLIT_SCREEN_BLUE_4 on 50ms ');
              });
            });
            break;
        }
      }

      if (settings.fullMode > 0) {
        settings.fullMode = 0;
        logger.debug('##### STOP STREAMING');
        nativeService.desktopStreamingStop(function() {
          logger.debug('split mode 1 - disable remote control');
          if (remoteMouseState) {
            if (settings.remoteControl) {
              nativeService.remoteControlStop(function(result) {
                logger.debug('remote control disabled: ' + result);
                remoteMouseState = false;
              });
            }
          }
          nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
            logger.debug('receive_MODERATOR_VIEWER_HOLDER setExtendedScreenMode result:' + result);
          });
        });
      }
      if (settings.splitMode < 1) {
        settings.splitMode = 1;
        logger.debug('##### START IMAGE');
        self.initialImageServer(location);
      } else if (settings.splitMode === 2) {
        logger.debug('##### RESUME IMAGE');
        nativeService.splitModeResume();
      }
    }
    self.emit('viewerState', 'resume');
    settings.isLauncherCMDBack = true;
    clearTimeout(launcherTimer);
  } else {
    if (settings.launcherEnabled === true && qLauncherExists && nativeAlive) {
      nativeService.ledOff(settings.LED.ALL);
    }
  }

  self.updateRemotePanel(0);
  self.emit('viewModeChanged', 'split');
  self.emit('userListUpdated', userUtil.user_list);
};

RVAClient.prototype.send_CMD_VIEWER_REQUEST = function send_CMD_VIEWER_REQUEST(userId, screen_location) {
  var self = this;
  logger.debug('send_CMD_VIEWER_REQUEST, userid: ' + userId + ' panel:' + screen_location);
  settings.currentSplitPanel = screen_location;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.VIEWER_REQUEST;
  buf[2] = userId;
  buf[3] = screen_location;

  var range = 5000;
  if (semver.gte(settings.rva_version, '2.1.0')) {
    var current_RVA_status = userUtil.getUserListStatus();
    logger.debug('CURRENT RVA STATUS = ' + current_RVA_status);
    var request_device = userUtil.getUserData(parseInt(userId));
    if (current_RVA_status === common.RVA_PLAY_STATUS.FULL_VIDEO) {
      logger.debug('###### VIDEO -> any = 5 + 1 seconds');
      range = 5000;
    } else if (current_RVA_status === common.RVA_PLAY_STATUS.FULL_IMAGE) {
      if (request_device.os_type < 5 && parseInt(screen_location) === 0) {
        logger.debug('###### FULL IMAGE -> VIDEO = 5 + 1 seconds');
        range = 5000;
      } else if (parseInt(screen_location) > -1) {
        logger.debug('###### FULL IMAGE -> FULL IMAGE / SPLIT IMAGE = 2 seconds');
        range = 2000;
      }
    } else if (current_RVA_status === common.RVA_PLAY_STATUS.SPLIT_IMAGE) {
      if (request_device.os_type < 5 && parseInt(screen_location) === 0) {
        logger.debug('###### SPLIT IMAGE -> VIDEO = 5 + 1 seconds');
        range = 5000;
      } else if (request_device.os_type > 4 && parseInt(screen_location) === 0) {
        logger.debug('###### SPLIT IMAGE -> FULL IMAGE = 2 seconds');
        range = 2000;
      } else if (parseInt(screen_location) > 0) {
        logger.debug('###### SPLIT IMAGE -> SPLIT IMAGE = 1 seconds');
        range = 1100;
      }
    } else if (current_RVA_status === common.RVA_PLAY_STATUS.PENDING_ALL) {
      if (request_device.os_type < 5 && parseInt(screen_location) === 0) {
        logger.debug('###### HOME PENDING -> VIDEO = 5 + 1 seconds');
        range = 5000;
      } else if (parseInt(screen_location) > -1) {
        logger.debug('###### HOME PENDING -> FULL IMAGE / SPLIT IMAGE = 3 seconds');
        range = 3000;
      }
    }
  }

  var range_timeout = range + 1000;
  thisRequestTime = new Date().getTime();

  logger.debug('##### range = ' + range);
  logger.debug('##### thisRequest = ' + thisRequestTime);
  logger.debug('##### lastRequestTime = ' + lastRequestTime);

  if (-(lastRequestTime - thisRequestTime) > range) {
    logger.debug('##### (1) send to RVA');
    RVACommandClient.write(buf);
    lastRequestTime = new Date().getTime();
    logger.debug('setTimeout for screen mode change');
    splitScreenTimer = setTimeout(function() {
      logger.debug('triggering changeScreenModeTimeout event!');
      self.emit('changeScreenModeTimeout', userId);
      self.sendNextLauncherCommand();
    }, range_timeout);
    if (settings.launcherEnabled === true && qLauncherExists) {
      launcherTimer = setTimeout(function() {
        self.restoreQLauncherLocation();
      }, range_timeout);
    }
  } else {
    var nextTime = range - thisRequestTime + lastRequestTime;
    setTimeout(function() {
      logger.debug('##### (2) send to RVA');
      RVACommandClient.write(buf);
      lastRequestTime = new Date().getTime();
      logger.debug('setTimeout for screen mode change');
      splitScreenTimer = setTimeout(function() {
        logger.debug('triggering changeScreenModeTimeout event!');
        self.restoreQLauncherLocation();
      }, range_timeout);
    }, nextTime + 200);
  }
};


RVAClient.prototype.receive_CMD_VIEWER_APPROVED = function receive_CMD_VIEWER_APPROVED(bValues) {
  var self = this;
  logger.debug('Receive CMD VIEWER APPROVED');
  var userid = bValues[2];
  var location = bValues[3];
  self.send_CMD_VIEWER_START(userid, location);
};

RVAClient.prototype.send_CMD_VIEWER_START = function send_CMD_VIEWER_START(userid, location) {
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.CM_VIEWER_START; // message type
  buf[2] = userid;
  buf[3] = location;
  RVACommandClient.write(buf);
};

RVAClient.prototype.receive_CMD_VIEWER_RELEASE = function receive_CMD_VIEWER_RELEASE() {
  logger.debug('Process CMD VIEWER RELEASE');
  var self = this;
  settings.screen_number = -1;
  if (settings.fullMode > 0) {
    settings.fullMode = 0;
    nativeService.desktopStreamingStop(function(cb) {
      if (remoteMouseState) {
        if (settings.remoteControl) {
          nativeService.remoteControlStop(function(result) {
            logger.debug('remote control disabled: ' + result);
            remoteMouseState = false;
          });
        }
      }
      nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
        logger.debug('receive_CMD_VIEWER_RELEASE setExtendedScreenMode result:' + result);
      });
    });
  }
  if (settings.splitMode > 0) {
    settings.splitMode = 0;
    nativeService.splitModeStop();
  }

  if (process.platform === 'darwin') {
    self.enableSoundFlowerState(false, function() {}, false);
  }
};

RVAClient.prototype.closeDataConnection = function closeDataConnection() {
  logger.debug('closeDataConnection');
  nativeService.desktopStreamingStop();
};

RVAClient.prototype.send_CMD_MODERATOR_DENIED = function send_CMD_MODERATOR_DENIED() {
  logger.debug('send_CMD_MODERATOR_DENIED');
  var buffer = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buffer[0] = settings.MINIMUM_MSG_LENGTH;
  buffer[1] = common.CMD.MODERATOR_DENIED;
  buffer[2] = settings.client_id;
  buffer[3] = 0;
  RVACommandClient.write(buffer);
};

RVAClient.prototype.send_CMD_MODERATOR_APPROVED = function send_CMD_MODERATOR_APPROVED() {
  logger.debug('send_CMD_MODERATOR_APPROVED');
  var buffer = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buffer[0] = settings.MINIMUM_MSG_LENGTH;
  buffer[1] = common.CMD.MODERATOR_APPROVED;
  buffer[2] = settings.client_id;
  buffer[3] = 0;
  RVACommandClient.write(buffer);

};

RVAClient.prototype.send_CMD_MODERATOR_REQUEST = function send_CMD_MODERATOR_REQUEST(userid) {
  logger.debug('send_CMD_MODERATOR_REQUEST');
  var buffer = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buffer[0] = settings.MINIMUM_MSG_LENGTH;
  buffer[1] = common.CMD.MODERATOR_REQUEST;
  buffer[2] = userid;
  buffer[3] = 0;
  RVACommandClient.write(buffer);
  // moderatorRequestTimeout = setTimeout(self.emit('moderatorRequestTimeout'), 25000);
};

RVAClient.prototype.receive_CMD_MODERATOR_APPROVED = function receive_CMD_MODERATOR_APPROVED() {
  var self = this;
  logger.debug('Receive CMD MODERATOR APPROVED');
  //  clearTimeout(moderatorRequestTimeout);
  self.emit('moderatorRequestApproved');
};

RVAClient.prototype.receive_CMD_MODERATOR_DENIED = function receive_CMD_MODERATOR_DENIED() {
  var self = this;
  logger.debug('Receive CMD MODERATOR DENIED');
  // clearTimeout(moderatorRequestTimeout);
  self.emit('moderatorRequestDenied');
};

/**
 * Set RVA settings
 * @param {string} field - an object with a name field and a value field, current valid names includes (projectionMode, videoQuality, refreshRate & remoteMouse)
 */
RVAClient.prototype.set = function set(field) {
  var self = this;
  logger.debug('update settings in db:');
  logger.debug('field: ' + field.name);
  logger.debug('value: ' + field.value);
  switch (field.name) {
    case 'projectionMode':
      logger.debug('set projectionMode: ' + field.value);
      if (RVACommandClient) {
        if (!RVACommandClient.destroyed) {
          self.pauseScreen();
          isStateChange = true;
          var buffer = new Buffer(4);
          buffer[0] = settings.MINIMUM_MSG_LENGTH;
          if (field.value === '1') {
            buffer[1] = common.CMD.AUDIO_STREAMING_ENABLE; // message type AUDIO_STREAMING_ENABLE AUDIO_STREAMING_DISABLE
            logger.debug('send AUDIO_STREAMING_ENABLE');
            //Mac             if (process.platform === 'darwin') {
            if (process.platform === 'darwin') {
              if (soundFlowerReminder) {
                self.emit('SoundFlowerWarning', false);
              } else {
                if (soundFlowerAllowed) {
                  self.enableSoundFlowerState(true, function() {}, false);
                } else {
                  self.enableSoundFlowerState(false, function() {}, false);
                }
              }
            }
          } else {
            buffer[1] = common.CMD.AUDIO_STREAMING_DISABLE;
            logger.debug('send AUDIO_STREAMING_DISABLE');
            //Mac             if (process.platform === 'darwin') {
            if (process.platform === 'darwin') {
              self.enableSoundFlowerState(false, function() {}, false);
            }
          }
          buffer[2] = 0;
          buffer[3] = 0;
          RVACommandClient.write(buffer);
        }
        setTimeout(function() {
          logger.debug('#resume screen');
          self.resumeScreen();
        }, 5000);
      }

      db.update({
        documentType: 'settings'
      }, {
        $set: {
          projectionMode: field.value
        }
      }, {
        upsert: true
      }, function(err) {
        if (err !== null) {
          logger.error(err.message);
          self.emit('error', err);
        }
      });
      break;
    case 'videoQuality':
      db.update({
        documentType: 'settings'
      }, {
        $set: {
          videoQuality: field.value
        }
      }, {
        upsert: true
      }, function(err) {
        if (err !== null) {
          logger.error(err.message);
          self.emit('error', err);
        }
      });
      break;
    case 'refreshRate':
      db.update({
        documentType: 'settings'
      }, {
        $set: {
          refreshRate: field.value
        }
      }, {
        upsert: true
      }, function(err) {
        if (err !== null) {
          logger.error(err.message);
          self.emit('error', err);
        }
      });
      break;
    case 'teacherCredentialPassword':
      db.update({
        documentType: 'settings'
      }, {
        $set: {
          teacherCredentialPassword: field.value
        }
      }, {
        upsert: true
      }, function(err) {
        if (err !== null) {
          logger.error(err.message);
          self.emit('error', err);
        }
        self.emit('teacherCredentialPassword', field.value);
      });
      break;
    case 'teacherCredentialEnabled':
      db.update({
        documentType: 'settings'
      }, {
        $set: {
          teacherCredentialEnabled: field.value
        }
      }, {
        upsert: true
      }, function(err) {
        if (err !== null) {
          logger.error(err.message);
          self.emit('error', err);
        }
        self.emit('teacherCredentialSetting', field.value);
      });
      break;
    case 'remoteMouse':
      db.update({
        documentType: 'settings'
      }, {
        $set: {
          remoteMouse: field.value
        }
      }, {
        upsert: true
      }, function(err) {
        logger.debug('setting remoteMouse to ' + field.value);
        if (err !== null) {
          logger.error(err.message);
          self.emit('error', err);
        }

        db.find({
          documentType: 'settings'
        }, function(err, docs) {
          if (err !== null) {
            logger.error(err.message);
            self.emit('error', err);
          } else {
            logger.debug(docs);
          }

        });
      });
      break;
    case 'lookupServer':
      db.update({
        documentType: 'settings'
      }, {
        $set: {
          lookupServer: field.value
        }
      }, {
        upsert: true
      }, function(err) {
        logger.debug('setting lookupServer to ' + field.value);
        if (err !== null) {
          logger.error(err.message);
          self.emit('error', err);
        }

        db.find({
          documentType: 'settings'
        }, function(err, docs) {
          if (err !== null) {
            logger.error(err.message);
            self.emit('error', err);
          } else {
            logger.debug(docs);
          }

        });
      });
      break;

    default:
      var error = new Error(field.name + ' is not a valide field name. Valid names are projectionMode, videoQuality, refreshRate or remoteMouse');
      self.emit('error', error);
      logger.error(field.name + ' is not a valide field name. Valid names are projectionMode, videoQuality, refreshRate or remoteMouse');
  }
};

RVAClient.prototype.receive_CMD_UPLOAD_GROUP_DATA = function receive_CMD_UPLOAD_GROUP_DATA(bValues, s_bytes) {
  logger.verbose('Receive CMD UPLOAD GROUP DATA');
  var self = this;
  var xml_length = dsaUtil.convertBytesBigEndianToInt(bValues[s_bytes + 4], bValues[s_bytes + 5], bValues[s_bytes + 6], bValues[s_bytes + 7]);
  if (xml_length + 8 > bValues.length) {
    logger.debug('return ' + (xml_length + 8));
    return xml_length + 8;
  } else {
    var j = 0;
    var length = xml_length + 8;
    var strBuf = new Uint8Array(xml_length);
    for (var i = 8 + s_bytes; i < length + s_bytes; i++) {
      strBuf[j] = bValues[i];
      j++;
    }
    var receive_xml = dsaUtil.byteArrayToUtf8(strBuf);

    self.getGroupList(receive_xml);

    return length;
  }
};

RVAClient.prototype.getGroupList = function getGroupList(xml_str) {
  var parser = new xml2js.Parser({
    explicitArray: false
  });

  async.series([
    function(done) {
      parser.parseString(xml_str, function(err, result) {
        result.documentType = 'group';
        settings.group = result.group;
        //settings.groupMode = true;
        settings.groupName = result.group.class;

        //Teacher first
        var t_name = result.group.teacher.name,
          t_device = result.group.teacher.device;
        if (typeof t_name === 'undefined' || t_name === '') {
          t_name = t_device;
        }
        if (typeof t_device === 'undefined' || t_device === '') {
          t_device = t_name;
        }

        userUtil.group_list.push(t_name, t_device);
        logger.debug(JSON.stringify(result.group.student));
        //logger.debug('aaaa '+result.group.student[].name);

        var students = (result.group.student.name) ? [result.group.student] : result.group.student;
        if (students !== null) {
          for (var i = 0; i < students.length; i++) {
            var student = students[i];
            var s_name = student.name;
            var s_device = student.device;
            if (typeof s_name === 'undefined' || s_name === '') {
              s_name = s_device;
            }
            if (typeof s_device === 'undefined' || s_device === '') {
              s_device = s_name;
            }
            userUtil.group_list.push(s_name, s_device);
          }
        }
        done();
      });
    }
  ]);
};
/**
 * Return RVA Settings
 * @returns {object} RVA Settings - projectionMode, videoQuality, refreshRate or remoteMouse
 */
RVAClient.prototype.getSettings = function getSettings(settings) {
  var self = this;
  // logger.debug('get settings');
  db.find({
    documentType: 'settings'
  }, function(err, docs) {
    if (err !== null) {
      logger.error(err.message);
      self.emit('error', err);
      return null;
    } else {
      // logger.debug('settings: ');
      // logger.debug(docs[0]);
      settings(docs[0]);
    }
  });
};

RVAClient.prototype.getGroups = function getGroups(groups) {
  //logger.debug('get group list');
  db.find({
    documentType: 'group'
  }, function(err, docs) {
    groups(docs);
  });
};

RVAClient.prototype.setGroup = function setGroup(groupName) {
  var self = this;
  logger.debug('##### setGroup');
  if (settings.isVotingPolling) {
    logger.debug('##### You cant load a group when voting is in progress');
    self.emit('votingStartingException', 'setgroup');
  } else {
    var builder = new xml2js.Builder(),
      groupXML, groupName, group, teacherName, teacherDevice, studentList, isOK = 1,
      count = 0;
    db.find({
      $and: [{
        documentType: 'group'
      }, {
        'group.class': groupName
      }]
    }, function(err, docs) {
      teacherName = docs[0].group.teacher.name;
      teacherDevice = docs[0].group.teacher.device;
      if (typeof teacherDevice === 'undefined') {
        teacherDevice = '';
      }
      studentList = docs[0].group.student;
      //Step1 check teacher role
      if (teacherDevice !== null && teacherDevice.length > 0) {
        if (teacherDevice.toLowerCase().replace(/ /ig, '') === settings.clientName.toLowerCase().replace(/ /ig, '')) {
          isOK = 0;
        } else {
          isOK = 1;
        }
      } else if (teacherName.toLowerCase().replace(/ /ig, '') === settings.clientName.toLowerCase().replace(/ /ig, '')) {
        isOK = 0;
      }
      //Setp2 check if duplicate name and device
      if (isOK === 0) {
        var list1 = [];
        if (teacherName === teacherDevice) {
          list1.push(teacherName);
        } else {
          list1.push(teacherName);
          list1.push(teacherDevice);
        }

        for (var i = 0; i < studentList.length; i++) {
          var ss = studentList[i];
          if (ss.name === ss.device) {
            list1.push(ss.name);
          } else {
            list1.push(ss.name);
            if (typeof ss.device === 'undefined') {

            } else {
              list1.push(ss.device);
            }
          }
        }
        for (var k = 0; k < list1.length; k++) {
          count = 0;
          for (var q = 0; q < list1.length; q++) {
            if (list1[k].trim().length > 0) {
              if (list1[k] === list1[q]) {
                count++;
              }
              if (count > 1) {
                isOK = 2;
                break;
              }
            }
          }
          if (isOK === 2) {
            break;
          }
        }
      }
      if (isOK === 0) {
        async.series([
          function(done) {
            db.find({
              $and: [{
                documentType: 'group'
              }, {
                'group.class': groupName
              }]
            }, function(err, docs) {
              settings.group = docs[0].group;
              group = docs[0].group;
              done();
            });
          },
          function(done) {
            groupName = group.class;
            groupXML = builder.buildObject(group).replace('<root>', '<group>').replace('</root>', '</group>');
            done();
          },
          function(done) {
            //        self.openDataServerSocket(done, common.DataServerMode.groupFileUpload);
            self.openDataServerSocket(common.CMD.UPLOAD_GROUP_DATA_V2, groupXML, null);
            done();
          }
        ]);
      } else if (isOK === 2) {
        logger.debug('##### duplicate username');
        self.emit('duplicateNameInGroup');
      } else {
        logger.debug('notGroupTeacher, expected: ' + settings.clientName + ' actual: ' + teacherName);
        self.emit('notGroupTeacher');
      }

    });
  }
};
/**
 * Import group XML file  - Convert the XML file to JSON format and save it to the database
 * @param {file} file group XML file
 */
RVAClient.prototype.importXMLFile = function importXMLFile(file, done) {
  logger.debug('importXMLFile');
  var self = this;
  fs.readFile(file.path, function(err, data) {
    if (err)
      self.emit('error', err);

    self.importGroupXML(function(error) {
      self.emit('error', error);
    }, data, done);

  });
};

RVAClient.prototype.importGroupXML = function importGroupXML(error, data, done) {
  var parser = new xml2js.Parser({
    explicitArray: false
  });
  logger.debug('importGroupXML');
  parser.parseString(data, function(err, result) {
    result.documentType = 'group';
    logger.debug('group xml update');
    logger.debug(result);
    var groupName = result.group.class;
    db.update({
      $and: [{
        documentType: 'group'
      }, {
        'group.class': groupName
      }]
    }, result, {
      upsert: true
    }, function(err, numReplaced, upsert) {
      logger.debug('group xml inserted');
      logger.debug(upsert);

      if (err !== null) {
        logger.error(JSON.stringify(err));
        error(err);
      }

      done();

    });
  });
};

RVAClient.prototype.setGroupXML = function setGroupXML(error, data, done) {
  logger.debug('setGroupXML');
  var self = this,
    parser = new xml2js.Parser({
      explicitArray: false
    });

  parser.parseString(data, function(err, result) {
    result.documentType = 'group';
    logger.debug('group xml update');
    logger.debug(result);
    //var groupName = result.group.class;
    settings.group = result.group;
    settings.groupMode = true;
    settings.groupName = result.group.class;

    logger.debug('group teacher:');
    logger.debug(result.group.teacher.name);

    logger.debug('current user: ' + settings.clientName);

    var userFound = false;
    var userMatched = {};

    if (result.group.teacher.name.toLowerCase().replace(/ /ig, '') === settings.clientName.toLowerCase().replace(/ /ig, '')) {
      userFound = true;
      userMatched.name = result.group.teacher.name;
      userMatched.device = result.group.teacher.device;
    }

    if (!userFound)
      _(result.group.student).forEach(function(student) {
        if (student.name.toLowerCase().replace(/ /ig, '') === settings.clientName.toLowerCase().replace(/ /ig, '')) {
          userFound = true;
          userMatched.name = student.name;
          userMatched.device = student.device;
        }
      });

    logger.debug('user match:');
    logger.debug(userMatched);

    if (result.group.teacher.name.toLowerCase().replace(/ /ig, '') !== userMatched.name.toLowerCase().replace(/ /ig, '')) {
      self.addUser(-1, result.group.teacher.name, false, result.group.teacher.device, -1, false);
    }
    _(result.group.student).forEach(function(student) {
      if (student.name.toLowerCase().replace(/ /ig, '') !== userMatched.name.toLowerCase().replace(/ /ig, ''))
        self.addUser(-1, student.name, false, student.device, -1, false);
    });

    /*
     var user = {
    'documentType': 'user',
    'uid': uid,
    'label': userName,
    'isHost': isHost,
    'device': device,
    'panel': panel,
    'isMyself': isMyself,
    'online': false
  };
  */

    // self.addUser(uid, userName, isHost, device, -1, false);
    logger.debug('group students:');
    logger.debug(result.group.student);


    if (done)
      done();

    if (err !== null) {
      logger.error(JSON.stringify(err));
      error(err);
    }
  });
};

RVAClient.prototype.updateGroup = function updateGroup(group) {
  var self = this;
  logger.debug('update group');
  logger.debug(group.group.class);
  group.documentType = 'group';

  db.update({
    $and: [{
      documentType: 'group'
    }, {
      'group.class': group.group.class
    }]
  }, group, {
    upsert: true
  }, function(err, numReplaced) {
    logger.debug('updated ' + group.class + ' ' + numReplaced);
    if (err !== null) {
      logger.error(err.message);
      self.emit('error', err);
    }
  });
};

RVAClient.prototype.exportGroupXML = function exportGroupXML(group, exportFile) {
  logger.debug('export group xml file');
  var self = this;

  var builder = new xml2js.Builder();
  var xml = builder.buildObject(group).replace('<root>', '<group>').replace('</root>', '</group>').replace(/^\s*[\r\n]/gm, '').trim();

  logger.debug('xml');
  logger.debug(xml);

  fs.writeFile(exportFile.path, xml, function(err) {
    if (err !== null) {
      logger.error(JSON.stringify(err));
      self.emit('error', err);
    }

    logger.debug('XML File exported!');
  });
};

RVAClient.prototype.reloadGroupList = function reloadGroupList(groups) {
  logger.debug('reloadGroupList, returning all groups');
  var self = this;
  var tempJSON = [];

  db.find({
    documentType: 'group'
  }, function(err, docs) {
    if (err !== null) {
      logger.error(JSON.stringify(err));
      self.emit('error', err);
    }

    async.series([
      function(done) {
        _(docs).forEach(function(doc) {
          delete doc.documentType;
          delete doc._id;
          tempJSON.push(doc);
        });
        done();
      },
      function(done) {
        if (tempJSON !== '') {
          logger.debug('Returning JSON: ' + JSON.stringify(tempJSON));
        }

        groups(tempJSON);
        done();
      }
    ]);
  });
};

RVAClient.prototype.deleteGroup = function deleteGroup(groupName) {
  db.remove({
    $and: [{
      documentType: 'group'
    }, {
      'group.class': groupName
    }]
  }, {
    multi: true
  }, function(err, numRemoved) {
    logger.debug('Deleted ' + numRemoved + ' groups');
  });
};

RVAClient.prototype.unloadGroup = function unloadGroup() {
  var self = this;
  if (settings.isVotingPolling) {
    logger.debug('##### You cant de-select the group when voting is in progress');
    self.emit('votingStartingException', 'removegroup');
  } else {
    logger.debug('unloadGroup cmd:' + common.CMD.UPLOAD_GROUP_DATA_V2);
    var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
    buf[0] = settings.MINIMUM_MSG_LENGTH;
    buf[1] = common.CMD.UPLOAD_GROUP_DATA_V2;
    buf[2] = settings.client_id;
    buf[3] = 1;
    RVACommandClient.write(buf);
  }
};

RVAClient.prototype.terminateSession = function terminateSession() {
  var self = this;
  logger.debug('##### Send CMD REQUEST SESSION END');
  if (settings.isVotingPolling) {
    //TODO: Tell Molly grid out [start] button
  }

  if (process.platform === 'darwin') {
    self.enableSoundFlowerState(false, function() {}, false);
  }

  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.REQUEST_SESSION_END;
  buf[2] = settings.client_id;
  buf[3] = 0;
  RVACommandClient.write(buf);
};

RVAClient.prototype.pauseScreen = function pauseScreen() {
  logger.debug('##### Send CMD CM VIEWER PAUSE');
  var self = this;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.CM_VIEWER_PAUSE;
  buf[2] = settings.client_id;
  buf[3] = 0;
  RVACommandClient.write(buf);

  /*
    result:
    0 is no mirror screen.
    1 is mirror main screen.
    2 is mirror extend screen.
  */
  self.getCurrentScreen(function(cb) {
    extendScreenStateBeforePause = cb;
  });

  isStateChange = true;
};

RVAClient.prototype.resumeScreen = function resumeScreen() {
  //var self = this;
  logger.debug('send resumeScreen, rva version: ' + settings.rva_version);
  isResume = true;
  if (settings.rvaType !== 'NOVOCAST') {
    // if rva is 1.6 then change resolution again to avoid pink screen
    if (settings.rva_version.indexOf('v1.6.') === 0) {
      async.series([
        function(done) {
          nativeService.getScreenResolution(function(result) {
            logger.debug('getScreenResolution: ' + JSON.stringify(result));
            dsa_width = result.width;
            dsa_height = result.height;
            if (settings.rva_version === 'v1.6') {
              settings.rva_version = 'v1.6.0';
              done();
            } else
              done();
          });
        }
      ]);
    }
  }

  logger.debug('sending CM_VIEWER_RESUME from resumeScreen');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.CM_VIEWER_RESUME;
  buf[2] = settings.client_id;
  buf[3] = 0;
  RVACommandClient.write(buf);
};

/*
  result:
  0 is no mirror screen.
  1 is mirror main screen.
  2 is mirror extend screen.
*/
RVAClient.prototype.getCurrentScreen = function getCurrentScreen(cb) {
  logger.debug('get current screen...');
  if (settings.fullMode === 1 || settings.splitMode === 1) {
    nativeService.getCurrentScreen(function(result) {
      logger.debug('current screen: ' + result + ' settings.currentScreen:' + settings.currentScreen);
      if (cb) {
        cb(result);
      }
    });
  } else {
    logger.debug('current screen: ' + settings.currentScreen);
    if (cb) {
      cb(settings.currentScreen);
    }
  }
};

RVAClient.prototype.extendScreen = function extendScreen(enable, direction) {
  var self = this;
  settings.switchExtend = true;
  logger.debug('extendScreen start');
  logger.debug(JSON.stringify(settings));
  //if (semver.lt(settings.rva_version, '2.0.0') && settings.fullMode > 0) {
  if (settings.fullMode > 0) {
    // if screen is not paused
    if (settings.fullMode !== 2) {
      logger.debug('calling pauseScreen');
      self.pauseScreen();
      self.once('screenPaused', function() {
        logger.debug('screenPaused event triggered');
        if (typeof(enable) === 'undefined') {
          enable = 0;
        }

        if (typeof(direction) === 'undefined') {
          direction = 0;
        }

        if (enable === 0) {
          settings.switchExtend = false;
          settings.currentScreen = settings.screenType.MAIN;
          extendScreenStateBeforePause = settings.screenType.MAIN;
        } else if (enable === 1) {
          settings.currentScreen = settings.screenType.EXT;
          extendScreenStateBeforePause = settings.screenType.EXT;
        }

        logger.debug('extendScreen settings.fullMode: ' + settings.fullMode + ' settings.splitMode:' + settings.splitMode + 'settings.currentScreen:' + settings.currentScreen);

        if (settings.fullMode === 2) {
          logger.debug('full mode screen is paused, call resume...');
          setTimeout(function() {
            logger.debug('#resume screen');
            self.resumeScreen();
          }, 5000);
        }
      });
    } else {
      // if screen is already paused
      logger.debug('screen is already paused');
      if (typeof(enable) === 'undefined') {
        enable = 0;
      }

      if (typeof(direction) === 'undefined') {
        direction = 0;
      }

      if (enable === 0) {
        settings.currentScreen = settings.screenType.MAIN;
        extendScreenStateBeforePause = settings.screenType.MAIN;
      } else if (enable === 1) {
        settings.currentScreen = settings.screenType.EXT;
        extendScreenStateBeforePause = settings.screenType.EXT;
      }

      logger.debug('extendScreen settings.fullMode: ' + settings.fullMode + ' settings.splitMode:' + settings.splitMode + 'settings.currentScreen:' + settings.currentScreen);

      if (settings.fullMode === 2) {
        logger.debug('full mode screen is paused, call resume...');
        setTimeout(function() {
          logger.debug('#resume screen');
          self.resumeScreen();
        }, 5000);
      }
    }



  } else { // fullmode <= 0
    if (typeof(enable) === 'undefined') {
      enable = 0;
    }

    if (typeof(direction) === 'undefined') {
      direction = 0;
    }

    if (enable === 0) {
      settings.currentScreen = settings.screenType.MAIN;
      extendScreenStateBeforePause = settings.screenType.MAIN;
      /*
      nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
        logger.debug('setExtendedScreenMode flag result:' + result);
      });*/
    } else if (enable === 1) {
      settings.currentScreen = settings.screenType.EXT;
      extendScreenStateBeforePause = settings.screenType.EXT;
    }

    logger.debug('extendScreen settings.fullMode: ' + settings.fullMode + ' settings.splitMode:' + settings.splitMode + 'settings.currentScreen:' + settings.currentScreen);
    if (settings.splitMode > 0) {
      async.series([
        function(done) {
          if (settings.splitMode === 2) {
            logger.debug('split screen is paused, call resume...');
            self.resumeScreen();
            done();
          } else {
            done();
          }
        },
        function(done) {
          nativeService.splitModeStop(function(result) {
            logger.debug('splitModeStop result: ' + result);
            done();
          });
        },
        function(done) {
          nativeService.setExtendedScreenMode(enable, 0, settings.screenWidth, settings.screenHeight, function(result) {
            logger.debug('setExtendedScreenMode result:' + result);
            done();
          });
        },
        function() {
          self.initialImageServer(settings.currentSplitPanel);
          self.emit('viewerState', 'resume');
        }
      ]);
    }
  }

};

RVAClient.prototype.streamingSetup = function streamingSetup() {
  logger.debug('streamingSetup');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.YOUTUBE_HTTP_STREAMING_SETUP;
  buf[2] = settings.client_id;
  buf[3] = common.STREAM_CMD.STOPPING;

  logger.debug('sending buffer');
  logger.debug(JSON.stringify(buf));

  RVACommandClient.write(buf);
};

RVAClient.prototype.playVideo = function playVideo(byteValues, startByte) {
  var self = this;
  logger.debug('Process playVideo');
  logger.debug(JSON.stringify(byteValues));

  // stop peer media
  nativeService.desktopStreamingStop(function(result) {
    logger.debug('Stop streaming when playing video or Youtube result = ' + result);
    if (remoteMouseState) {
      if (settings.remoteControl) {
        nativeService.remoteControlStop(function(result) {
          logger.debug('remote control disabled: ' + result);
          remoteMouseState = false;
        });
      }
    }
  });

  var msgLength = byteValues[startByte];
  var cmd = byteValues[startByte + 1];
  var userId = byteValues[startByte + 2];
  var arg = byteValues[startByte + 3];

  logger.debug('msgLength:' + msgLength + ' cmd:' + cmd + ' userId:' + userId + ' arg:' + arg);

  switch (arg) {
    case common.PLAY_CMD.PLAY:
      if (cmd === common.CMD.YOUTUBE_HTTP_STREAMING_SETUP) {
        if (settings.playVideoMode === 'YouTube') {
          logger.debug('play youtube video, id:' + settings.youTubeVideoId);
          settings.youTubeTitle = dsaUtil.getYoutubeTitleById(settings.youTubeVideoId, function(title) {
            logger.debug('youTube video title: ' + title);
            settings.youTubeTitle = title;
            logger.debug('arg: ' + arg);
            self.sendPlayYouTubeCommand(arg);
          });
        } else {
          logger.debug('local video');
          self.sendPlayLocalVideoCommand(arg);
        }

      }
      break;

    default:
      logger.debug('COMMAND NOT HANDLED! cmd: ' + cmd);
      break;
  }
};

RVAClient.prototype.sendPlayLocalVideoCommand = function sendPlayLocalVideoCommand(playCommand) {
  var self = this;
  logger.debug('send local Video command:' + playCommand);
  var dsaIp = self.getDsaIp();
  var videoWebUrl = 'localhost:' + settings.HTTP_PORT + '/';
  videoWebUrl = videoWebUrl + encodeURIComponent(randomString({
    length: 20
  }));
  logger.debug('domSafeRandomGuid length: ' + videoWebUrl.length);
  videoWebUrl = videoWebUrl.substring(0, 247);
  logger.debug('length: ' + videoWebUrl.length + ' videoWebUrl: ' + videoWebUrl);
  logger.debug('url with file name: ' + videoWebUrl);
  logger.debug('settings.youTubeTitle: ' + settings.youTubeTitle);
  logger.debug('settings.localVideoPath: ' + settings.localVideoPath);
  durationDuplicateCount = 0;
  lastDuration = 0;

  switch (playCommand) {
    case common.PLAY_CMD.PLAY:
      logger.debug('sending PLAY_CMD.PLAY');
      localVideoPlaying = true;
      // start http server
      settings.dsaIp = dsaIp;
      async.series([
        function(done) {
          if (settings.isWebUrlStreaming) {
            logger.debug('##### Web Streaming No local web server');
            done();
          } else {
            console.time('start web server');
            console.log('start web server: ' + settings.localVideoPath);
            videoWebServer.start(settings.HTTP_PORT, settings.localVideoPath, function() {
              console.timeEnd('start web server');
              done();
            });
          }
        },
        function(done) {
          if (settings.isWebUrlStreaming) {
            done();
          } else {
            // connect to proxy 1 socket
            if (settings.localStreamingThruProxy === false) {
              done();
            } else {
              if (process.platform === 'win32') {
                self.connectRvaProxy1(settings.rvaIp, videoWebUrl, done);
              } else {
                done();
              }
            }
          }
        },
        function(done) {
          if (settings.isWebUrlStreaming) {
            done();
          } else {
            // connect to proxy 2 socket
            if (settings.localStreamingThruProxy === false) {
              done();
            } else {
              if (process.platform === 'win32') {
                self.connectRvaProxy2(settings.rvaIp, videoWebUrl, done);
              } else {
                done();
              }
            }
          }
        },
        function() {
          // send play command send play command: http://
          if (process.platform === 'darwin')
            videoWebUrl = videoWebUrl.replace('localhost', dsaIp);

          logger.debug('send play command: http://' + videoWebUrl);
          var overSend = false;
          var length = 0;
          var buf = new Uint8Array(400);
          for (var i = 0; i < 400; i++) {
            buf[i] = 0;
          }
          var data = dsaUtil.convertStringToBytes('http://' + videoWebUrl);
          if (settings.isWebUrlStreaming) {
            if (settings.youTubeTitle.startsWith('https://drive.google.com')) {
              data = dsaUtil.convertStringToBytes('web->' + dsaUtil.getGoogleDriveURL(settings.youTubeTitle));
            } else if (settings.youTubeTitle.startsWith('https://www.dropbox.com')) {
              data = dsaUtil.convertStringToBytes('web->' + dsaUtil.getDropboxURL(settings.youTubeTitle));
            } else {
              data = dsaUtil.convertStringToBytes('web->' + settings.youTubeTitle);
            }
          }

          length = data.length;
          if (length + settings.MINIMUM_MSG_LENGTH >= 127) {
            var data_length = length;
            var extention_length = dsaUtil.convertIntToBytesBigEndian(data_length);
            if (extention_length !== null && extention_length.length === 4) {
              for (var j = 0; j < 4; j++) {
                buf[j + 4] = extention_length[j];
              }
              for (var q = 0; q < length; q++) {
                buf[8 + q] = data[q];
              }
              overSend = true;
              buf[0] = 127;
            }
          } else {
            buf[0] = length + settings.MINIMUM_MSG_LENGTH;
            for (var i = 0; i < length; i++) {
              buf[4 + i] = data[i];
            }
            overSend = false;
          }

          buf[1] = common.CMD.PLAY_VIDEO;
          buf[2] = 0;
          buf[3] = common.PLAY_CMD.START;

          if (overSend) {
            buf = buf.subarray(0, length + 8);
          } else {
            buf = buf.subarray(0, length + 4);
          }

          var node_buf = new Buffer(buf);
          RVACommandClient.write(node_buf);
          settings.fullMode = 0;
          nativeService.desktopStreamingStop(function() {
            if (remoteMouseState) {
              if (settings.remoteControl) {
                nativeService.remoteControlStop(function(result) {
                  logger.debug('remote control disabled: ' + result);
                  remoteMouseState = false;
                });
              }
            }
            nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
              logger.debug('sendPlayLocalVideoCommand setExtendedScreenMode result:' + result);
            });
          });
        }
      ]);
      break;

    case common.PLAY_CMD.EXIT:
      logger.debug('send common.PLAY_CMD.EXIT');
      localVideoPlaying = false;
      videoPlayStatusCount = 0;
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PLAY_VIDEO;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.EXIT;
      RVACommandClient.write(buf);
      break;

    case common.PLAY_CMD.FORWARD:
      logger.debug('send common.PLAY_CMD.FORWARD');
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PLAY_VIDEO;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.FORWARD;
      RVACommandClient.write(buf);
      break;

    case common.PLAY_CMD.REVERSE:
      logger.debug('send common.PLAY_CMD.REVERSE');
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PLAY_VIDEO;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.REVERSE;
      RVACommandClient.write(buf);
      break;

    case common.PLAY_CMD.PAUSE:
      logger.debug('process common.PLAY_CMD.PAUSE');
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PLAY_VIDEO;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.PAUSE;
      RVACommandClient.write(buf);
      break;

    case common.PLAY_CMD.RESUME:
      logger.debug('send common.PLAY_CMD.RESUME');
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PLAY_VIDEO;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.RESUME;
      RVACommandClient.write(buf);
      break;
  }
};

RVAClient.prototype.sendPlayYouTubeCommand = function sendPlayYouTubeCommand(playCommand /*, cb*/ ) {
  logger.debug('send YouTube Video command:' + playCommand);
  switch (playCommand) {
    case common.PLAY_CMD.PLAY:
      logger.debug('sending PLAY_CMD.PLAY');
      var overSend = false;
      var length = 0;
      var buf = new Uint8Array(settings.MAXIUM_MSG_LENGTH);
      for (var i = 0; i < settings.MAXIUM_MSG_LENGTH; i++) {
        buf[i] = 0;
      }
      var data = dsaUtil.convertStringToBytes(settings.youTubeVideoId);
      length = data.length;
      if (length + settings.MINIMUM_MSG_LENGTH >= 127) {
        var data_length = length;
        var extention_length = dsaUtil.convertIntToBytesBigEndian(data_length);
        if (extention_length !== null && extention_length.length === 4) {
          for (var j = 0; j < 4; j++) {
            buf[j + 4] = extention_length[j];
          }
          for (var q = 0; q < length; q++) {
            buf[8 + q] = data[q];
          }
          overSend = true;
        }
      } else {
        buf[0] = length + settings.MINIMUM_MSG_LENGTH;
        for (var i = 0; i < length; i++) {
          buf[4 + i] = data[i];
        }
        overSend = false;
      }

      buf[1] = common.CMD.PLAY_YOUTUBE;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.START;

      if (overSend) {
        buf = buf.subarray(0, length + 8);
      } else {
        buf = buf.subarray(0, length + 4);
      }

      var node_buf = new Buffer(buf);
      // logger.debug(JSON.stringify(buf));

      RVACommandClient.write(node_buf);

      //Close Streaming
      settings.fullMode = 0;
      nativeService.desktopStreamingStop(function() {
        if (remoteMouseState) {
          if (settings.remoteControl) {
            nativeService.remoteControlStop(function(result) {
              logger.debug('remote control disabled: ' + result);
              remoteMouseState = false;
            });
          }
        }
        nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
          logger.debug('sendPlayYouTubeCommand setExtendedScreenMode result:' + result);
        });
      });

      break;

    case common.PLAY_CMD.EXIT:
      logger.debug('process common.PLAY_CMD.EXIT');
      localVideoPlaying = false;
      videoPlayStatusCount = 0;
      var overSend = false;
      var length = 0;
      var buf = new Uint8Array(settings.MAXIUM_MSG_LENGTH);
      for (var i = 0; i < settings.MAXIUM_MSG_LENGTH; i++) {
        buf[i] = 0;
      }
      if (settings.youTubeVideoId)
        var data = dsaUtil.convertStringToBytes(settings.youTubeVideoId);
      else
        var data = dsaUtil.convertStringToBytes('http://' + settings.dsaIp + ':' + settings.HTTP_PORT + '/');
      length = data.length;
      if (length + settings.MINIMUM_MSG_LENGTH >= 127) {
        var data_length = length;
        var extention_length = dsaUtil.convertIntToBytesBigEndian(data_length);
        if (extention_length !== null && extention_length.length === 4) {
          for (var j = 0; j < 4; j++) {
            buf[j + 4] = extention_length[j];
          }
          for (var q = 0; q < length; q++) {
            buf[8 + q] = data[q];
          }
          overSend = true;
        }
      } else {
        buf[0] = length + settings.MINIMUM_MSG_LENGTH;
        for (var i = 0; i < length; i++) {
          buf[4 + i] = data[i];
        }
        overSend = false;
      }

      buf[1] = common.CMD.PLAY_YOUTUBE;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.EXIT;

      if (overSend) {
        buf = buf.subarray(0, length + 8);
      } else {
        buf = buf.subarray(0, length + 4);
      }

      var node_buf = new Buffer(buf);
      RVACommandClient.write(node_buf, 'UTF8', function() {
        logger.debug('buffer sent');
      });
      break;

    case common.PLAY_CMD.FORWARD:
      logger.debug('process common.PLAY_CMD.FORWARD');
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PLAY_YOUTUBE;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.FORWARD;
      RVACommandClient.write(buf);
      break;

    case common.PLAY_CMD.REVERSE:
      logger.debug('process common.PLAY_CMD.REVERSE');
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PLAY_YOUTUBE;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.REVERSE;
      RVACommandClient.write(buf);
      break;

    case common.PLAY_CMD.PAUSE:
      logger.debug('process common.PLAY_CMD.PAUSE');
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PLAY_YOUTUBE;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.PAUSE;

      RVACommandClient.write(buf);
      break;

    case common.PLAY_CMD.RESUME:
      logger.debug('process common.PLAY_CMD.RESUME');
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PLAY_YOUTUBE;
      buf[2] = 0;
      buf[3] = common.PLAY_CMD.RESUME;

      RVACommandClient.write(buf);
      break;
  }
};

RVAClient.prototype.resumeYouTubeVideo = function resumeYouTubeVideo() {
  var self = this;
  videoPlayPaused = false;
  if (settings.playVideoMode === 'YouTube') {
    logger.debug('Resume YouTube Video');
    if (youTubePlaying)
      self.sendPlayYouTubeCommand(common.PLAY_CMD.RESUME);
  } else if (settings.playVideoMode === 'Local') {
    logger.debug('Resume local Video');
    self.sendPlayLocalVideoCommand(common.PLAY_CMD.RESUME);
  }
};

RVAClient.prototype.pauseYouTubeVideo = function pauseYouTubeVideo() {
  var self = this;
  videoPlayPaused = true;
  if (settings.playVideoMode === 'YouTube') {
    logger.debug('Pause YouTube Video');
    if (youTubePlaying)
      self.sendPlayYouTubeCommand(common.PLAY_CMD.PAUSE);
  } else if (settings.playVideoMode === 'Local') {
    logger.debug('Pause local Video');
    self.sendPlayLocalVideoCommand(common.PLAY_CMD.PAUSE);
  }
};

RVAClient.prototype.forwardYouTubeVideo = function forwardYouTubeVideo() {
  var self = this;
  if (settings.playVideoMode === 'YouTube') {
    logger.debug('Forward YouTube Video');
    if (youTubePlaying)
      self.sendPlayYouTubeCommand(common.PLAY_CMD.FORWARD);
  } else if (settings.playVideoMode === 'Local') {
    logger.debug('Forward local Video');
    self.sendPlayLocalVideoCommand(common.PLAY_CMD.FORWARD);
  }
};

RVAClient.prototype.reverseYouTubeVideo = function reverseYouTubeVideo() {
  var self = this;
  if (settings.playVideoMode === 'YouTube') {
    logger.debug('Reverse YouTube Video');
    if (youTubePlaying)
      self.sendPlayYouTubeCommand(common.PLAY_CMD.Reverse);
  } else if (settings.playVideoMode === 'Local') {
    logger.debug('Reverse local Video');
    self.sendPlayLocalVideoCommand(common.PLAY_CMD.Reverse);
  }
};

RVAClient.prototype.stopYouTubeVideo = function stopYouTubeVideo() {
  var self = this;
  youTubePlaying = false;
  localVideoPlaying = false;
  videoPlayPaused = false;
  videoPlayStatusCount = 0;
  logger.debug('set youTubePlaying to ' + youTubePlaying);
  if (settings.playVideoMode === 'YouTube') {
    logger.debug('Stop YouTube Video!');
    self.sendPlayYouTubeCommand(common.PLAY_CMD.EXIT);
  } else if (settings.playVideoMode === 'Local') {
    logger.debug('Stop Local Video');
    if (process.platform === 'win32') {
      if (RVAProxy1) {
        if (!RVAProxy1.destroyed) {
          RVAProxy1.end();
          RVAProxy1.destroy();
        }
      }
      if (RVAProxy2) {
        if (!RVAProxy2.destroyed) {
          RVAProxy2.end();
          RVAProxy2.destroy();
        }
      }
    }

    setTimeout(function() {
      self.sendPlayLocalVideoCommand(common.PLAY_CMD.EXIT);
    }, 1000);
  }
};

RVAClient.prototype.sendStreamingPercentToRVA = function sendStreamingPercentToRVA(percent) {
  logger.debug('percent: ' + percent);
  logger.debug('totalVideoDuration: ' + totalVideoDuration);

  var seconds = percent * totalVideoDuration;
  logger.debug('seconds: ' + seconds);

  var data3 = Math.floor(seconds / 256);
  var data4 = Math.floor(seconds % 256);

  var buffer = new Buffer(4);
  buffer[0] = settings.MINIMUM_MSG_LENGTH;
  buffer[1] = common.CMD.STREAMING_PLAY_TIME_UPDATE;
  buffer[2] = data3;
  buffer[3] = data4;

  RVACommandClient.write(buffer);

};

RVAClient.prototype.playYouTubeVideo = function playYouTubeVideo(youTubeUrl) {
  var self = this;
  logger.debug('play YouTube Video');
  logger.debug(youTubeUrl);
  isResume = true;
  youTubeValidate.validateUrl(youTubeUrl, function(res, err) {
    if (res !== null && err === null) {
      settings.youTubeVideoId = dsaUtil.getYoutubeId(youTubeUrl);
      dsaUtil.validateYouTubeID(settings.youTubeVideoId, function(isValidVid) {
        if (isValidVid) {
          youTubePlaying = true;
          settings.playVideoMode = 'YouTube';
          nativeService.desktopStreamingStop(function() {
            if (remoteMouseState) {
              if (settings.remoteControl) {
                nativeService.remoteControlStop(function(result) {
                  logger.debug('remote control disabled: ' + result);
                  remoteMouseState = false;
                });
              }
            }
          });

          self.streamingSetup();
        } else {
          youTubePlaying = false;
          settings.playVideoMode = '';
          logger.debug('youtube ID error, vid: ' + settings.youTubeVideoId);
          logger.debug(err);
          self.emit('youTubeUrlError');
        }
      });
    } else {
      logger.debug('youtube url error');
      logger.debug(err);
      self.emit('youTubeUrlError');
    }
  });
};

RVAClient.prototype.playLocalVideo = function playLocalVideo(videoFile) {
  var self = this;
  logger.debug('play local Video');
  logger.debug(videoFile);
  isResume = true;
  settings.playVideoMode = 'Local';
  settings.youTubeTitle = settings.localVideoPath = videoFile;
  settings.youTubeTitle = htmlencode.htmlEncode(settings.youTubeTitle);
  nativeService.desktopStreamingStop(function(result) {
    if (result === 'successful') {
      if (remoteMouseState) {
        if (settings.remoteControl) {
          nativeService.remoteControlStop(function(result) {
            logger.debug('remote control disabled: ' + result);
            remoteMouseState = false;
          });
        }
      }
      self.streamingSetup();
    }
  });
  //self.streamingSetup();
  // start http server, setup appropriate route to the video file and serve the file.

  //settings.youTubeVideoId = dsaUtil.getYoutubeId(youTubeUrl);
  //self.streamingSetup();
};

RVAClient.prototype.receive_CMD_PLAY_VIDEO = function receive_CMD_PLAY_VIDEO(byteValues) {
  var self = this;
  logger.debug('process receive_CMD_PLAY_VIDEO');
  var videoPlayStatus = byteValues[3];

  logger.debug('videoPlayStatus:' + videoPlayStatus);
  logger.debug('YouTube Title: ' + settings.youTubeTitle);
  logger.debug('settings.playVideoMode: ' + settings.playVideoMode);

  switch (videoPlayStatus) {

    case common.PLAY_CMD.READY_TO_SHOW:
      videoPlayMessage = {
        status: 'playing',
        videoTitle: settings.youTubeTitle,
        total: 0,
        percentage: 0
      };
      settings.videoPlaying = true;
      logger.debug('readytoshow videoPlayStatus: ' + JSON.stringify(videoPlayMessage));
      self.emit('videoPlayStatus', videoPlayMessage);
      break;

    case common.PLAY_CMD.EXIT:
      /*************** YOUTUBE_HTTP_STREAMING_SETUP message format *************
      *   byte      index:
      *   byte      0  one byte  --- packet length (4 bytes)
      *   byte      1: one byte  --- message type:  YOUTUBE_HTTP_STREAMING_SETUP
      *   byte      2: one byte  --- client ID
      *   byte      3: one byte  --- arg: 0  stop video view only (from DSA to RVA);
      *   arg: 1 video view stopped(from RVA to DSA);  arg: 2 start video view(from DSA to RVA) arg: 3 unknown error(from RVA to DSA)
      /*************** YOUTUBE_HTTP_STREAMING_SETUP message format *************/
      var buffer = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buffer[0] = settings.MINIMUM_MSG_LENGTH;
      buffer[1] = common.CMD.YOUTUBE_HTTP_STREAMING_SETUP;
      buffer[2] = settings.client_id;
      buffer[3] = 2;
      logger.debug('send YOUTUBE_HTTP_STREAMING_SETUP message PLAY_CMD.EXIT, play mode: ' + settings.playVideoMode);

      localVideoPlaying = false;
      videoPlayTimeUpdated = false;
      if (settings.playVideoMode !== 'YouTube') {

        videoWebServer.stop();
      }

      videoPlayStatusCount = 0;
      videoPlayMessage = {
        status: 'stopped'
      };

      logger.debug('stopped videoPlayStatus: ' + JSON.stringify(videoPlayMessage));
      self.once('desktopStreamingStarted', function() {
        setTimeout(function() {
          self.emit('videoPlayStatus', videoPlayMessage);
        }, 1000);
      });

      if (settings.videoPlaying) {
        RVACommandClient.write(buffer);
      }

      settings.videoPlaying = false;

      setTimeout(function() {
        if (settings.fullMode < 1) {
          settings.fullMode = 1;
          logger.debug('resume desktop streaming');
          self.initialStreamingServer();
        }
      }, 1000);
      break;

    case common.PLAY_CMD.UNKNOWN_ERROR:
      logger.debug('UNKNOWN_ERROR');
      var buffer = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buffer[0] = settings.MINIMUM_MSG_LENGTH;
      buffer[1] = common.CMD.YOUTUBE_HTTP_STREAMING_SETUP;
      buffer[2] = settings.client_id;
      buffer[3] = 2;
      logger.debug('send YOUTUBE_HTTP_STREAMING_SETUP message');
      RVACommandClient.write(buffer);
      settings.videoPlaying = false;

      videoPlayTimeUpdated = false;
      localVideoPlaying = false;
      videoPlayStatusCount = 0;
      videoPlayMessage = {
        status: 'stopped'
      };

      if (settings.playVideoMode !== 'YouTube')
        videoWebServer.stop();

      if (settings.fullMode < 1) {
        settings.fullMode = 1;
        setTimeout(function() {
          self.initialStreamingServer();
        }, 1000);
      }

      self.once('desktopStreamingStarted', function() {
        // self.emit('videoPlayStatus', videoPlayMessage);
        self.emit('videoError', 'video.errorMsg');
      });
      //self.emit('videoError', 'video.errorMsg');
      break;

    default:
      //videoWebServer.stop();
      break;
  }

};

RVAClient.prototype.receive_CMD_PLAY_YOUTUBE = function receive_CMD_PLAY_YOUTUBE(byteValues) {
  var self = this;
  logger.debug('process CMD_PLAY_YOUTUBE');
  var videoPlayStatus = byteValues[3];

  async.series([
    function(done) {
      if (settings.youTubeVideoId) {
        logger.debug('youTubeVideoId');
        logger.debug(settings.youTubeVideoId);
        settings.youTubeTitle = dsaUtil.getYoutubeTitleById(settings.youTubeVideoId, function(title) {
          logger.debug('youTube video title: ' + title);
          settings.youTubeTitle = title;
          done();
        });
      } else {
        logger.debug('localVideoPath: ' + settings.localVideoPath);
        settings.youTubeTitle = htmlencode.htmlEncode(settings.localVideoPath);
        done();
      }

    },
    function(done) {
      logger.debug('receive_CMD_PLAY_YOUTUBE videoPlayStatus: ' + videoPlayStatus);
      logger.debug('video title:' + settings.youTubeTitle);
      switch (videoPlayStatus) {
        case common.PLAY_CMD.READY_TO_SHOW:
          self.emit('videoPlayStatus', {
            status: 'playing',
            videoTitle: settings.youTubeTitle
          });
          settings.videoPlaying = true;
          break;
        case common.PLAY_CMD.EXIT:
          logger.debug('YOUTUBE PLAY_CMD.EXIT');

          /*************** YOUTUBE_HTTP_STREAMING_SETUP message format *************
          *   byte      index:
          *   byte      0  one byte  --- packet length (4 bytes)
          *   byte      1: one byte  --- message type:  YOUTUBE_HTTP_STREAMING_SETUP
          *   byte      2: one byte  --- client ID
          *   byte      3: one byte  --- arg: 0  stop video view only (from DSA to RVA);
          *   arg: 1 video view stopped(from RVA to DSA);  arg: 2 start video view(from DSA to RVA) arg: 3 unknown error(from RVA to DSA)
          /*************** YOUTUBE_HTTP_STREAMING_SETUP message format *************/
          var buffer = new Buffer(settings.MINIMUM_MSG_LENGTH);
          buffer[0] = settings.MINIMUM_MSG_LENGTH;
          buffer[1] = common.CMD.YOUTUBE_HTTP_STREAMING_SETUP;
          buffer[2] = settings.client_id;
          buffer[3] = 2;

          localVideoPlaying = false;
          videoPlayStatusCount = 0;
          self.emit('videoPlayStatus', {
            status: 'stopped'
          });
          if (settings.playVideoMode !== 'YouTube')
            videoWebServer.stop();

          self.once('desktopStreamingStarted', function() {
            setTimeout(function() {
              self.emit('videoPlayStatus', videoPlayMessage);
            }, 1000);
          });
          if (settings.videoPlaying) {
            RVACommandClient.write(buffer);
          }
          settings.videoPlaying = false;
          if (settings.fullMode < 1) {
            settings.fullMode = 1;
            setTimeout(function() {
              logger.debug('YOUTUBE resume desktop streaming');
              self.initialStreamingServer();
            }, 1000);
          }
          break;
        case common.PLAY_CMD.UNKNOWN_ERROR:
          logger.debug('UNKNOWN_ERROR');
          var buffer = new Buffer(settings.MINIMUM_MSG_LENGTH);
          buffer[0] = settings.MINIMUM_MSG_LENGTH;
          buffer[1] = common.CMD.YOUTUBE_HTTP_STREAMING_SETUP;
          buffer[2] = settings.client_id;
          buffer[3] = 2;
          logger.debug('send YOUTUBE_HTTP_STREAMING_SETUP message');
          RVACommandClient.write(buffer);
          settings.videoPlaying = false;
          if (settings.playVideoMode !== 'YouTube') {
            videoWebServer.stop();
          }

          localVideoPlaying = false;
          videoPlayStatusCount = 0;

          self.emit('videoError');

          self.once('desktopStreamingStarted', function() {
            setTimeout(function() {
              self.emit('videoPlayStatus', videoPlayMessage);
            }, 1000);
          });

          // self.emit('error', new Error('Unknown video play error'));
          if (settings.fullMode < 1) {
            settings.fullMode = 1;
            setTimeout(function() {
              logger.debug('YOUTUBE resume desktop streaming');
              self.initialStreamingServer();
            }, 1000);
          }
          break;
      }
      done();
    }
  ]);
};

RVAClient.prototype.receive_CMD_SCREEN_LOCK_UNLOCK = function receive_CMD_SCREEN_LOCK_UNLOCK(bytes) {
  logger.debug('Receive CMD SCREEN LOCK UNLOCK');
  var self = this;
  if (bytes[3] === 0) {
    logger.debug('Screen is locked');
    settings.tabletsLocked = true;
  } else {
    logger.debug('Screen is not locked');
    settings.tabletsLocked = false;
  }

  logger.debug('settings.tabletsLocked is set to ' + settings.tabletsLocked);
  if (settings.tabletsLocked) {
    self.emit('tabletsLocked', settings.tabletsLocked);
    // setTimeout(self.emit('tabletsLocked', settings.tabletsLocked).bind(this), 4000);
  } else {
    self.emit('tabletsLocked', settings.tabletsLocked);
    // setTimeout(self.emit('tabletsLocked', settings.tabletsLocked).bind(this), 4000);
  }
};

RVAClient.prototype.sendLockUnlock = function sendLockUnlock() {
  logger.debug('Send CMD SCREEN LOCK UNLOCK, settings.tabletsLocked: ' + settings.tabletsLocked);
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.SCREEN_LOCK_UNLOCK;
  buf[2] = 0;

  if (settings.tabletsLocked)
    buf[3] = 1;
  else
    buf[3] = 0;

  RVACommandClient.write(buf);
};

function handleSettingsResult(self, result, reboot) {
  logger.debug('result: ' + result);
  logger.debug('reboot: ' + reboot);
  switch (result) {
    case common.SETTINGS_CMD.STATUS_CODE.SUCCESS:
      self.emit('SETTING_CHANGE_SUCCESS');
      if (reboot === common.SETTINGS_CMD.STATUS_CODE.REBOOT_DEVICE_NEEDED)
        self.emit('REBOOT_DEVICE_NEEDED');
      break;
    case common.SETTINGS_CMD.STATUS_CODE.FAIL:
      var detailedError = reboot;
      logger.error('SETTING_FAIL detailedError: ' + detailedError);
      switch (detailedError) {
        case common.SETTINGS_CMD.STATUS_CODE.FIRMWARE_IS_UP_TO_DATE:
          self.emit('SETTING_FIRMWARE_IS_UP_TO_DATE');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.NO_PERMISSION:
          self.emit('SETTING_NO_PERMISSION');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.RESET_RVA_NOT_ALLOWED_WITH_ONLINE_USERS:
          self.emit('RESET_RVA_NOT_ALLOWED_WITH_ONLINE_USERS');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.RESET_DEVICE_NOT_ALLOWED_WITH_ONLINE_USERS:
          self.emit('RESET_DEVICE_NOT_ALLOWED_WITH_ONLINE_USERS');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.CHANGE_PASSWORD_NOT_ALLOWED_WITH_ONLINE_USERS:
          self.emit('CHANGE_PASSWORD_NOT_ALLOWED_WITH_ONLINE_USERS');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.NEW_PASSWORD_IS_NOT_MEET_REQUIREMENT:
          self.emit('NEW_PASSWORD_IS_NOT_MEET_REQUIREMENT');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.CHANGE_WIFI_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS:
          self.emit('CHANGE_WIFI_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.CHANGE_DEV_NAME_NOT_ALLOWED_WITH_ONLINE_USERS:
          self.emit('CHANGE_DEV_NAME_NOT_ALLOWED_WITH_ONLINE_USERS');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.SERVER_NOT_FOUND:
          self.emit('SERVER_NOT_FOUND');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.UNKNOWN_REASONS:
          self.emit('UNKNOWN_REASONS');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.UPGRADE_NOT_ALLOWED_WITH_ONLINE_USERS:
          self.emit('UPGRADE_NOT_ALLOWED_WITH_ONLINE_USERS');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.CHANGE_DISPLAY_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS:
          self.emit('CHANGE_DISPLAY_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS');
          break;
        case common.SETTINGS_CMD.STATUS_CODE.CHANGE_LANGUAGE_NOT_ALLOWED_WITH_ONLINE_USERS:
          self.emit('CHANGE_LANGUAGE_NOT_ALLOWED_WITH_ONLINE_USERS');
          break;
        default:
          break;
      }
      break;
    case common.SETTINGS_CMD.IN_PROGRESS:
      self.emit('SETTING_IN_PROGRESS');
      break;
    default:
      break;
  }
}

/*****************
  RVA Settings functions
*****************/

RVAClient.prototype.send_CMD_RESET_RVA = function send_CMD_RESET_RVA(rvaIP) {
  var settingsClient = dgram.createSocket('udp4');
  logger.verbose('Send CMD RESET RVA');
  logger.debug('UDP message sent to ' + rvaIP + ':' + settings.UDP_SETTINGS_PORT);

  var self = this;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.RESET_RVA;
  buf[2] = 0;
  buf[3] = 0;

  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  logger.debug('timeout is set to ' + settings.settingsTimeOut);
  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('SETTINGS_TIMEOUT timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });

  settingsClient.on('error', function(err) {
    logger.error('rva error:\n' + err.stack);
    self.emit('UNKNOWN_REASONS');
    settingsClient.close();
  });
};

RVAClient.prototype.getHdmiCec = function getHdmiCec(rvaIP, done) {
  logger.verbose('Get HDMI_CEC');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.HDMI_CEC;
  buf[2] = 0; //read
  buf[3] = 0;

  var HdmiCec = false;
  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    HdmiCec.timeout = true;
    logger.debug('HdmiCec timeout!');
    settingsClient.close();
    done(HdmiCec);
  }, 5000);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    HdmiCec = msg[3];
    logger.debug('HdmiCec: ' + HdmiCec);
    settingsClient.close();
    done(HdmiCec);
  });
};

RVAClient.prototype.setHdmiCec = function setHdmiCec(cec, rvaIP) {
  var self = this;
  logger.debug('send HdmiCec:' + cec + ' typeof ' + typeof(cec) + ' rva ip:' + rvaIP);

  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.HDMI_CEC;
  buf[2] = 1; //set
  if (cec === 'false') {
    logger.debug('set cec off');
    buf[3] = 0;
  } else {
    logger.debug('set cec on');
    buf[3] = 1;
  }

  logger.debug(JSON.stringify(buf));
  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  logger.debug('set timeout for changing HdmiCec');
  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('changing HdmiCec timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    logger.debug('clear HdmiCec timeout');
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });
};

RVAClient.prototype.getLanguage = function getLanguage(rvaIP, done) {
  logger.verbose('Get language');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.DISPLAY_LANGUAGE;
  buf[2] = 0;
  buf[3] = 0;

  var language = {
    'value': 0,
    'timeout': false
  };

  logger.debug('set timeout: ' + settings.settingsTimeOut);

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    language.timeout = true;
    logger.debug('getLanguage timeout!');
    settingsClient.close();
    done(language);
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    language.value = msg[3];
    logger.debug('language: ' + language.value);
    settingsClient.close();
    done(language);
  });
};

RVAClient.prototype.getRoomName = function getRoomName(rvaIP, done) {
  logger.verbose('Get room name');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.DEVICE_NAME;
  buf[2] = 0;
  buf[3] = 0;

  var roomName = {
    'value': '',
    'timeout': false
  };

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    roomName.timeout = true;
    logger.debug('getRoomName timeout!');
    settingsClient.close();
    done(roomName);
  }, 5000);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    var roomNameLength = msg[4];
    var j = 0;
    var strBuf = new Uint8Array(roomNameLength);
    for (var i = 4; i < roomNameLength + 5; i++) {
      strBuf[j] = msg[i];
      j++;
    }

    roomName.value = dsaUtil.byteArrayToUtf8(strBuf);
    logger.debug('roomName: ' + roomName.value);
    settingsClient.close();
    done(roomName);
  });
};

RVAClient.prototype.getDisplaySetting = function getDisplaySetting(rvaIP, done) {
  logger.verbose('Get display setting');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.DISPLAY_SETTING;
  buf[2] = 0;
  buf[3] = 0;

  var displaySetting = {
    'value': 0,
    'timeout': false
  };

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    displaySetting.timeout = true;
    logger.debug('getDisplaySetting timeout!');
    done(displaySetting);
  }, 5000);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    logger.debug('display setting: ' + msg[3]);
    displaySetting.value = msg[3];
    settingsClient.close();
    done(displaySetting);
  });

};

RVAClient.prototype.getSupportedResolutions = function getSupportedResolutions(rvaIP, done) {
  logger.verbose('Get all supported resolutions');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.DISPLAY_SETTING;
  buf[2] = 2;
  buf[3] = 0;

  var result = [];

  var displaySetting = {
    'value': result,
    'timeout': false
  };

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    displaySetting.timeout = true;
    logger.debug('displaySetting timeout!');
    done(displaySetting);
  }, 5000);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    displaySetting.timeout = false;
    logger.debug('received all supported resoltions');
    var packet_size = msg[0];
    var resBuf = new Uint8Array(packet_size - 4);
    for (var i = 0; i < packet_size; i++) {
      resBuf[i] = msg[i + 4];
    }
    var resolution = dsaUtil.byteArrayToUtf8(resBuf);
    logger.debug('All resoultion supported = ' + resolution);
    var resolutionArray = resolution.split(';');
    logger.debug(JSON.stringify(resolutionArray));
    for (var j = 1; j < resolutionArray.length - 1; j++) {
      result.push(resolutionArray[j].split(',')[1]);
    }
    displaySetting.value = result;
    settingsClient.close();
    done(displaySetting);
  });

};

RVAClient.prototype.sendDisplaySetting = function sendDisplaySetting(displaySetting, rvaIP) {
  var settingsClient = dgram.createSocket('udp4');
  logger.verbose('Set display mode to ' + displaySetting);
  logger.debug('UDP message sent to ' + rvaIP + ':' + settings.UDP_SETTINGS_PORT);

  var self = this;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.DISPLAY_SETTING;
  buf[2] = 1;
  buf[3] = displaySetting;

  logger.debug('set timeout to: ' + settings.settingsTimeOut);
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });
};

RVAClient.prototype.send_CMD_RESET_DEVICE = function send_CMD_RESET_DEVICE(rvaIP) {
  logger.verbose('Send CMD RESET DEVICE');
  var settingsClient = dgram.createSocket('udp4');
  var self = this;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.RESET_DEVICE;
  buf[2] = 0;
  buf[3] = 0;

  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('RESET DEVICE timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });
};

RVAClient.prototype.startMonitoringFirmwareUpgrade = function startMonitoringFirmwareUpgrade(rvaIP) {
  logger.verbose('Start Monitoring Firmware Upgrade');
  var self = this;
  monitoringFirmwareUpgradeInterval = setInterval(function() {
    self.rvaUpgradeQuery(rvaIP);
  }, 1000);
};

RVAClient.prototype.stopMonitoringFirmwareUpgrade = function stopMonitoringFirmwareUpgrade() {
  logger.verbose('stop Monitoring Firmware Upgrade');
  if (monitoringFirmwareUpgradeInterval)
    if (monitoringFirmwareUpgradeInterval !== '')
      clearInterval(monitoringFirmwareUpgradeInterval);
};

RVAClient.prototype.rvaUpgradeQuery = function rvaUpgradeQuery(rvaIP) {
  // logger.verbose("Send CMD RVA UPGRADE query");
  var self = this;
  var buffer = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buffer[0] = settings.MINIMUM_MSG_LENGTH;
  buffer[1] = common.SETTINGS_CMD.RVA_UPGRADE;
  buffer[2] = 101;
  buffer[3] = 0;

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buffer, 0, buffer.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  settingsClient.on('message', function(msg) {

    // logger.debug('Received %d bytes from %s:%d\n', msg.length, rinfo.address, rinfo.port);

    var arg = msg[3];

    if (arg === common.FIRMWARE_DOWNLOAD_STATUS.UPGRADE_MSG_NO_INTERNET ||
      arg === common.FIRMWARE_DOWNLOAD_STATUS.UPGRADE_MSG_UPGRADE_UNAVAILABLE ||
      arg === common.FIRMWARE_DOWNLOAD_STATUS.UPGRADE_MSG_DOWNLOAD_COMPLETED ||
      arg === common.FIRMWARE_DOWNLOAD_STATUS.UPGRADE_MSG_DOWNLOAD_ERROR ||
      arg === common.FIRMWARE_DOWNLOAD_STATUS.UPGRADE_MSG_REBOOT ||
      arg === common.FIRMWARE_DOWNLOAD_STATUS.UPGRADE_MSG_INSTALL ||
      arg === common.FIRMWARE_DOWNLOAD_STATUS.UPGRADE_MSG_CLEAR) {
      self.stopMonitoringFirmwareUpgrade();
    }

    switch (arg) {
      /*
      case 1:
        self.emit('UPGRADE_MSG_START');
        break;
      case 2:
        self.emit('UPGRADE_MSG_NO_INTERNET');
        break;
      case 3:
        self.emit('UPGRADE_MSG_UPGRADE_AVAILABLE');
        break;
        */
      case 4:
        self.emit('SETTING_FIRMWARE_IS_UP_TO_DATE');
        break;
        /*
        case 5:
          self.emit('UPGRADE_MSG_DOWNLOAD_IN_PROGRESS');
          break;
        case 6:
          self.emit('UPGRADE_MSG_DOWNLOAD_COMPLETED');
          break;
        case 7:
          self.emit('UPGRADE_MSG_DOWNLOAD_ERROR');
          break;
        case 8:
          self.emit('UPGRADE_MSG_INSTALL');
          break;
        case 9:
          self.emit('UPGRADE_MSG_REBOOT');
          break;
        case 10:
          self.emit('UPGRADE_MSG_CLEAR');
          break;
          */
      default:
        self.emit('UPGRADE_IN_PROGRESS');
        break;
    }


    settingsClient.close();
  });

};

/*************** RVA_UPGRADE message format *************************************
 *   byte index:
 *   byte      0   one byte  --- packet length (MSG_HEADER_LENGTH ==4)
 *   byte      1:  one byte  --- message type: RVA_UPGRADE
 *   byte      2:  one byte  --- arg1:
 *   byte      3:  one byte  --- arg2:
 *   FROM RMGR: arg1 --> (arg1==0, from RDM, arg1==1 from RVA or DSA)
 *              arg2 --> Not used
 *   FROM RVA:  arg1 --> arg1; status code
 *                       arg2 --> detailed status code
 *   Note: this message is write only
 ********************************************************************************/
RVAClient.prototype.rvaUpgrade = function rvaUpgrade(rvaIP) {
  logger.verbose('Send CMD RVA UPGRADE');
  logger.debug('UDP message sent to ' + rvaIP + ':' + settings.UDP_SETTINGS_PORT);
  var self = this;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.RVA_UPGRADE;
  buf[2] = 1;
  buf[3] = 0;

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    self.startMonitoringFirmwareUpgrade(rvaIP);
    if (err)
      logger.debug('UDP error: ' + err);

  });

  // var timeout = setTimeout(function () {
  //  self.emit('SETTINGS_TIMEOUT');
  // logger.debug('rva upgrade timeout!');
  //settingsClient.close();
  // }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    // clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    logger.debug('result:' + result);
    logger.debug('reboot:' + reboot);
    self.stopMonitoringFirmwareUpgrade();
    settingsClient.close();
    // handleSettingsResult(self, result, reboot);
  });

  settingsClient.on('error', function(err) {
    logger.error('rva error:' + JSON.stringify(err));
    self.emit('UNKNOWN_REASONS');
    settingsClient.close();
  });

};

RVAClient.prototype.sendDeviceName = function sendDeviceName(deviceName, rvaIP) {
  var self = this;
  logger.debug('send Device Name:' + deviceName);

  var data = dsaUtil.utf8ToByteArray(deviceName);
  var headerLength = 5;
  var length = data.length;

  logger.debug('name length: ' + length);
  var buffer = new Buffer(headerLength + length);
  buffer[0] = headerLength + length;
  buffer[1] = common.SETTINGS_CMD.DEVICE_NAME;
  buffer[2] = 1;
  buffer[3] = 0;
  buffer[4] = length;


  for (var i = 0; i < length; i++) {
    buffer[headerLength + i] = data[i];
  }

  logger.debug(JSON.stringify(buffer));

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buffer, 0, buffer.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  logger.debug('set timeout for changing device name');
  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('changing device name timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    logger.debug('clear device name timeout');
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });
};

RVAClient.prototype.sendHotspotSettings = function sendHotspotSettings(channel, routing, rvaIP) {
  logger.verbose('Set hotspotSettings to channel:' + channel + ' routing enabled: ' + routing + ' rvaIP: ' + rvaIP);
  logger.debug('channel.length: ' + channel.toString().length);
  var self = this;
  var channelLength = channel.toString().length;
  routing = routing.toString();
  logger.debug('routing.length: ' + routing.toString().length);
  var routingLength = routing.toString().length;
  var bufferSize = 5 + channelLength + 1 + routingLength;
  logger.debug('packat length: ' + bufferSize);
  var count = 0;
  var buf = new Buffer(bufferSize);
  buf[0] = bufferSize;
  buf[1] = common.SETTINGS_CMD.WIFI_SETTING;
  buf[2] = 3;
  buf[3] = 0;
  buf[4] = channelLength;
  for (var i = 0; i < channelLength; i++) {
    logger.debug('setting channel buffer ' + (i + 5) + ' to ' + dsaUtil.convertStringToBytes(channel)[i] + ' where i =' + i);
    buf[i + 5] = dsaUtil.convertStringToBytes(channel)[i];
    count = i + 5;
    logger.debug('count: ' + count);
  }
  var pos = count + 1;
  logger.debug('setting buffer ' + pos + ' to ' + routingLength);
  buf[pos] = routingLength;
  for (var j = 0; j < routingLength; j++) {
    logger.debug('setting routing buffer ' + (pos + 1 + j) + ' to ' + dsaUtil.convertStringToBytes(routing)[j] + ' where j =' + j);
    buf[pos + 1 + j] = dsaUtil.convertStringToBytes(routing)[j];
  }
  logger.debug('hotspotSettings:' + JSON.stringify(buf));

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });

};

RVAClient.prototype.sendWifiSettings = function sendWifiSettings(ssid, wifiMode, wifiPassword, rvaIP) {
  logger.verbose('Set WifiSettings to ssid:' + ssid + ' wifiMode: ' + wifiMode + ' password: ' + wifiPassword);
  logger.debug('UDP message sent to ' + rvaIP + ':' + settings.UDP_SETTINGS_PORT);

  var self = this,
    buf,
    bufferSize;

  var data = dsaUtil.convertStringToBytes(ssid);
  logger.debug('ssid.length: ' + ssid.length);
  logger.debug('wifiPassword.length: ' + wifiPassword.length);

  if (wifiPassword.length > 0)
    bufferSize = settings.MINIMUM_MSG_LENGTH + ssid.length + wifiPassword.length + 2;
  else
    bufferSize = settings.MINIMUM_MSG_LENGTH + ssid.length + 1;

  buf = new Buffer(bufferSize);

  buf[0] = bufferSize;
  buf[1] = common.SETTINGS_CMD.WIFI_SETTING;
  buf[2] = wifiMode;
  buf[3] = 0;

  var data = dsaUtil.convertStringToBytes(ssid);
  buf[4] = data.length;

  var j = 5;
  for (var i = 0; i < data.length; i++) {
    buf[j] = data[i];
    j++;
  }

  if (wifiPassword.length > 0) {
    buf[j] = wifiPassword.length;
    j = j + 1;
    data = dsaUtil.convertStringToBytes(wifiPassword);
    for (var i = 0; i < data.length; i++) {
      buf[j] = data[i];
      j++;
    }
  }

  logger.debug(JSON.stringify(buf));

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });
};

RVAClient.prototype.getAllData = function getAllData(rvaVersion, rvaIP, cb) {
  logger.verbose('Send CMD ALL_DATA ' + new Date().toLocaleString());
  if (parseFloat(rvaVersion) > 2.0) {
    logger.debug('rva version is greater than 2.0.0');
  } else {
    logger.debug('rva version is less than 2.0.0');
  }

  var self = this;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.ALL_DATA;
  buf[2] = 0;
  buf[3] = 0;

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var allDataTimeout = setTimeout(function() {
    logger.debug('all data timeout ' + new Date().toLocaleString());
    self.emit('invalidIp');
  }, 3000);

  var options = {
    rvaVersion: 1.6,
    wifiType: 0,
    ssid: '',
    display: 1,
    room: 'Meeing Room Name',
    language: 0,
    enablePassword: false,
    rvaPassword: '',
    resolution: [], // added after RVA 2.1 (included)
    // below is after RVA 2.2.0 (included)
    channel: 6,
    channelList: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], // [1,2,3,4,...,11,12,13] US: 11, the rest: 13
    routing: false, // false: Disable true: Enable
    hdmiCec: true, // false: Disable true: Enable
  };

  settingsClient.once('message', function(msg) {
    clearTimeout(allDataTimeout);
    logger.debug('slicing all_data, getting rid of all data header...' + new Date().toLocaleString());
    logger.debug('parsing settings_password...');
    msg = msg.slice(4, msg.length);
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));
    var passwordLength = msg[0];
    logger.debug('password length: ' + passwordLength);

    if (passwordLength === 4) {
      logger.debug('password is not set');
      msg = msg.slice(4, msg.length);
      options.enablePassword = false;
    } else {
      options.enablePassword = true;
      passwordLength = passwordLength - 4;
      logger.debug('password is set length:' + passwordLength);
      var passwordBuf = new Uint8Array(passwordLength);
      for (var i = 0; i < passwordLength; i++) {
        logger.debug('set passwordBuf[' + (i) + '] to ' + msg[i + 4]);
        passwordBuf[i] = msg[i + 4];
      }
      options.rvaPassword = dsaUtil.convertByteArrayToString(passwordBuf);
      logger.debug('password is ' + dsaUtil.convertByteArrayToString(passwordBuf));
      msg = msg.slice(4 + passwordLength, msg.length);
    }
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));

    logger.debug('parsing display_setting...');
    options.display = msg[3];
    logger.debug('display_settings: ' + msg[3]);
    msg = msg.slice(4, msg.length);
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));

    logger.debug('parsing DISPLAY_LANGUAGE...');
    logger.debug('DISPLAY_LANGUAGE: ' + msg[3]);
    options.language = msg[3];
    msg = msg.slice(4, msg.length);
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));

    logger.debug('parsing RVA_EDITION...');
    logger.debug('RVA_EDITION: ' + msg[2]);
    options.rvaEdition = msg[2];
    msg = msg.slice(4, msg.length);
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));

    if (parseFloat(rvaVersion) > 2.0) {
      logger.debug('parsing HDMI_CEC...');
      logger.debug('HDMI_CEC: ' + msg[3]);
      if (msg[3] === 0) {
        options.hdmiCec = false;
      } else {
        options.hdmiCec = true;
      }
      msg = msg.slice(4, msg.length);
      logger.debug('data length: ' + msg.length);
      logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));
    }

    logger.debug('parsing RVA_VERSION...');
    var rvaVersionLength = msg[0] - 4;
    logger.debug('RVA_VERSION length: ' + rvaVersionLength);
    var rvaVersionBuf = new Buffer(rvaVersionLength);
    for (var i = 0; i < rvaVersionLength; i++) {
      rvaVersionBuf[i] = msg[4 + i];
    }
    var rvaVersion = dsaUtil.byteArrayToUtf8(rvaVersionBuf).replace('v', '');
    var rvaMajor = rvaVersion.split('.').slice(0, 1);
    var rvaMinor = rvaVersion.split('.').slice(1, 2);
    rvaVersion = rvaMajor + '.' + rvaMinor;
    logger.debug('rvaVersionBuf: ' + JSON.stringify(rvaVersionBuf));
    logger.debug('RVA_VERSION: ' + dsaUtil.convertByteArrayToString(rvaVersionBuf));
    options.rvaVersion = rvaVersion;
    msg = msg.slice(4 + rvaVersionLength, msg.length);
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));

    logger.debug('parsing BSP_VERSION...');
    logger.debug('BSP_VERSION lengh:' + (msg[0] - 4));
    var bspBuf = new Buffer(msg[0] - 4);
    for (var i = 0; i < (msg[0] - 4); i++) {
      bspBuf[i] = msg[4 + i];
    }
    logger.debug('bspBuf: ' + JSON.stringify(bspBuf));
    options.bspVersion = dsaUtil.convertByteArrayToString(bspBuf);
    logger.debug('BSP_VERSION: ' + dsaUtil.convertByteArrayToString(bspBuf));
    msg = msg.slice(msg[0], msg.length);
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));

    logger.debug('parsing NVC_MODEL...');
    logger.debug('NVC_MODEL lengh:' + msg[0]);
    var nvcBuf = new Buffer(msg[0] - 4);
    for (var i = 0; i < msg[0] - 4; i++) {
      nvcBuf[i] = msg[4 + i];
    }
    logger.debug('nvcBuf: ' + JSON.stringify(nvcBuf));
    options.nvcModel = dsaUtil.convertByteArrayToString(nvcBuf);
    logger.debug('NVC_MODEL: ' + dsaUtil.convertByteArrayToString(nvcBuf));
    msg = msg.slice(msg[0], msg.length);
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));

    logger.debug('parsing SW_EDITION...');
    logger.debug('SW_EDITION lengh:' + msg[0]);
    var swBuf = new Buffer(msg[0] - 4);
    for (var i = 0; i < msg[0] - 4; i++) {
      swBuf[i] = msg[4 + i];
    }
    logger.debug('swBuf: ' + JSON.stringify(swBuf));
    logger.debug('SW_EDITION: ' + dsaUtil.convertByteArrayToString(swBuf));
    if (dsaUtil.convertByteArrayToString(swBuf).toString().indexOf('-US-') < 0) {
      options.channelList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    } else {
      options.channelList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    }
    msg = msg.slice(msg[0], msg.length);
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));

    logger.debug('parsing NVC_MODEL...');
    logger.debug('NVC_MODEL lengh:' + msg[0]);
    var modeleBuf = new Buffer(msg[0] - 4);
    for (var i = 0; i < msg[0] - 4; i++) {
      modeleBuf[i] = msg[4 + i];
    }
    logger.debug('modeleBuf: ' + JSON.stringify(modeleBuf));
    options.nvcModel = dsaUtil.convertByteArrayToString(modeleBuf);
    logger.debug('NVC MODEL: ' + dsaUtil.convertByteArrayToString(modeleBuf));
    msg = msg.slice(msg[0], msg.length);
    logger.debug('data length: ' + msg.length);
    logger.debug('ALL_DATA sliced: ' + JSON.stringify(msg));

    logger.debug('parsing DEVICE_NAME...');
    logger.debug('DEVICE_NAME lengh:' + msg[0]);
    var deviceBuf = new Buffer(msg[0] - 4);
    for (var i = 0; i < msg[0] - 4; i++) {
      deviceBuf[i] = msg[4 + i];
    }
    options.room = dsaUtil.convertByteArrayToString(deviceBuf);
    logger.debug('Device name: ' + dsaUtil.convertByteArrayToString(deviceBuf));

    if (cb) {
      cb(options);
    }
    settingsClient.close();
  });

  settingsClient.on('error', function(err) {
    logger.error('RVA get all data error:' + JSON.stringify(err));
    clearTimeout(timeout);
    self.emit('UNKNOWN_REASONS');
    settingsClient.close();
  });
};

RVAClient.prototype.getSwEdition = function getSwEdition(rvaIP, cb) {
  logger.verbose('Send CMD SW EDITION');
  logger.debug('UDP message sent to ' + rvaIP + ':' + settings.UDP_SETTINGS_PORT);
  var self = this;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.SW_EDITION;
  buf[2] = 0;
  buf[3] = 0;

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var getRvaVersionTimer = setTimeout(function() {
    logger.debug('getSwEdition timeout!');
    self.emit('invalidIp');
  }, 3000);

  settingsClient.once('message', function(msg) {
    clearTimeout(getRvaVersionTimer);
    logger.debug('sw edition:' + JSON.stringify(msg));
    logger.debug('sw edition:' + msg);
    if (cb) {
      cb(msg);
    }
    settingsClient.close();
    // handleSettingsResult(self, result, reboot);
  });

  settingsClient.on('error', function(err) {
    logger.error('RVA VERSION error:' + JSON.stringify(err));
    self.emit('UNKNOWN_REASONS');
    settingsClient.close();
  });

};

RVAClient.prototype.getRvaVersion = function getRvaVersion(rvaIP, cb) {
  logger.verbose('Send CMD RVA VERSION');
  logger.debug('UDP message sent to ' + rvaIP + ':' + settings.UDP_SETTINGS_PORT);
  var self = this;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.RVA_VERSION;
  buf[2] = 0;
  buf[3] = 0;

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var getRvaVersionTimer = setTimeout(function() {
    logger.debug('getRvaVersion timeout!');
    self.emit('invalidIp');
  }, 3000);

  settingsClient.once('message', function(msg) {
    clearTimeout(getRvaVersionTimer);
    var rvaVersionLength = msg[4];
    var j = 0;
    var strBuf = new Uint8Array(rvaVersionLength);
    for (var i = 4; i < rvaVersionLength + 5; i++) {
      strBuf[j] = msg[i];
      j++;
    }
    var rvaVersion = dsaUtil.byteArrayToUtf8(strBuf).replace('v', '');
    var rvaMajor = rvaVersion.split('.').slice(0, 1);
    var rvaMinor = rvaVersion.split('.').slice(1, 2);
    rvaVersion = rvaMajor + '.' + rvaMinor;

    cb(rvaVersion);
    settingsClient.close();
    // handleSettingsResult(self, result, reboot);
  });

  settingsClient.on('error', function(err) {
    logger.error('RVA VERSION error:' + JSON.stringify(err));
    self.emit('UNKNOWN_REASONS');
    settingsClient.close();
  });

};

RVAClient.prototype.getNetworkInterface1Info = function getNetworkInterface1Info(rvaIP, done) {
  logger.debug('get NETWORK_INTERFACE_1');
  var self = this;
  if (isIp(rvaIP)) {
    var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
    buf[0] = settings.MINIMUM_MSG_LENGTH;
    buf[1] = common.SETTINGS_CMD.NETWORK_INTERFACE_1;
    buf[2] = 0;
    buf[3] = 0;

    var settingsClient = dgram.createSocket('udp4');
    settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
      if (err)
        logger.debug('UDP error: ' + err);
    });

    var hotspotSettings = {
      'channel': 6,
      'enabled': false
    };

    var timeout = setTimeout(function() {
      hotspotSettings.timeout = true;
      logger.debug('hotspotSettings timeout!');
      settingsClient.close();
      done(hotspotSettings);
    }, settings.settingsTimeOut);

    settingsClient.on('message', function(msg) {
      logger.debug('NETWORK_INTERFACE_1 response');
      logger.debug(msg);

      clearTimeout(timeout);

      hotspotSettings.channel = msg[2];
      if (msg[3] === 0) {
        hotspotSettings.enabled = false;
      } else {
        hotspotSettings.enabled = true;
      }
      done(hotspotSettings);
    });
  } else {
    logger.debug('getNetworkInterface1Info timeout!');
    self.emit('invalidIp');
  }
};

RVAClient.prototype.getNetworkInterfaceInfo = function getNetworkInterfaceInfo(rvaIP, done) {
  logger.debug('get NETWORK_INTERFACE');
  var self = this;
  if (isIp(rvaIP)) {
    var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
    buf[0] = settings.MINIMUM_MSG_LENGTH;
    buf[1] = common.SETTINGS_CMD.NETWORK_INTERFACE;
    buf[2] = 0;
    buf[3] = 0;

    logger.debug(JSON.stringify(buf));
    var settingsClient = dgram.createSocket('udp4');
    settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
      if (err)
        logger.debug('UDP error: ' + err);
    });

    var networkSettings = {
      'wifiType': 0,
      'ssid': '',
      'wifiIP': '',
      'lanIP': '',
      'timeout': false
    };

    var timeout = setTimeout(function() {
      networkSettings.timeout = true;
      logger.debug('timeout!');
      settingsClient.close();
      done(networkSettings);
    }, settings.settingsTimeOut);

    settingsClient.on('message', function(msg) {
      logger.debug('NETWORK_INTERFACE response');
      logger.debug(msg);

      clearTimeout(timeout);

      networkSettings.wifiType = msg[2];
      var ssidLength = msg[4];

      var j = 0;
      var strBuf = new Uint8Array(ssidLength);
      for (var i = 5; i < ssidLength + 5; i++) {
        strBuf[j] = msg[i];
        j++;
      }

      networkSettings.ssid = dsaUtil.convertByteArrayToString(strBuf);
      networkSettings.ssid = networkSettings.ssid.replace(/(^|\W)"/g, '').replace(/"($|\W)/g, '');
      logger.debug('ssid: ' + networkSettings.ssid);
      var wifiIPLength = msg[ssidLength + 5];

      j = 0;
      strBuf = new Uint8Array(wifiIPLength);
      for (var i = ssidLength + 6; i < wifiIPLength + ssidLength + 6; i++) {
        strBuf[j] = msg[i];
        j++;
      }

      networkSettings.wifiIP = dsaUtil.convertByteArrayToString(strBuf);

      var lanIPLength = msg[wifiIPLength + ssidLength + 5];
      logger.debug('lanIPLength: ' + lanIPLength);
      j = 0;
      strBuf = new Uint8Array(lanIPLength);
      for (var i = wifiIPLength + ssidLength + 6; i < lanIPLength + wifiIPLength + ssidLength + 6; i++) {
        strBuf[j] = msg[i];
        j++;
      }

      networkSettings.lanIP = dsaUtil.convertByteArrayToString(strBuf);

      settingsClient.close();
      done(networkSettings);

    });
  } else {
    logger.debug('getNetworkInterfaceInfo timeout');
    self.emit('invalidIp');
  }
};

RVAClient.prototype.getSettingsPassword = function getSettingsPassword(rvaIP, done) {
  logger.debug('get settings password');
  var self = this;
  var password = {
    'enabled': false,
    'value': '',
    'timeout': false
  };

  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.SETTINGS_PASSWORD;
  buf[2] = 0;
  buf[3] = 0;

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    logger.debug('timeout!');
    self.emit('SETTINGS_TIMEOUT');
    password.timeout = true;
    settingsClient.close();
    done(password);
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    logger.debug('password bytes: ' + JSON.stringify(msg));
    var length = msg[0];
    if (length === settings.MINIMUM_MSG_LENGTH) {
      settingsClient.close();
      logger.debug('password is disabled');
      password.enabled = false;
      done(password);
    } else {
      var j = 0;
      var passwordLength = length - settings.MINIMUM_MSG_LENGTH;
      var strBuf = new Uint8Array(passwordLength);
      for (var i = settings.MINIMUM_MSG_LENGTH; i < length; i++) {
        strBuf[j] = msg[i];
        j++;
      }
      logger.debug('returning ' + dsaUtil.convertByteArrayToString(strBuf));
      settingsClient.close();
      password.value = dsaUtil.convertByteArrayToString(strBuf);
      password.enabled = true;
      done(password);
    }
  });

};

RVAClient.prototype.setSettingsPassword = function setSettingsPassword(settingsPassword, rvaIP) {
  var self = this;
  logger.debug('set settings password:' + settingsPassword);

  var data = dsaUtil.utf8ToByteArray(settingsPassword);
  var headerLength = 5;
  var length = data.length;
  logger.debug('password length: ' + length);

  var buffer = new Buffer(headerLength + length);
  buffer[0] = headerLength + length;
  buffer[1] = common.SETTINGS_CMD.SETTINGS_PASSWORD;
  buffer[2] = 1;
  buffer[3] = 0;
  buffer[4] = length;

  for (var i = 0; i < length; i++) {
    buffer[headerLength + i] = data[i];
  }

  logger.debug(JSON.stringify(buffer));

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buffer, 0, buffer.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    logger.debug('settings password response');
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });
};

RVAClient.prototype.disableSettingsPassword = function disableSettingsPassword(rvaIP) {
  var self = this;
  logger.debug('Disable settings password');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.SETTINGS_PASSWORD;
  buf[2] = 2;
  buf[3] = 0;

  logger.debug(JSON.stringify(buf));
  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    logger.debug('settings password response');
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });
};

RVAClient.prototype.sendLanguage = function sendLanguage(language, rvaIP) {
  var self = this;
  var lang = common.LANGUAGE.ENGLISH;
  logger.debug('Send language: ' + language);
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.SETTINGS_CMD.DISPLAY_LANGUAGE;

  switch (language) {
    case 0:
      lang = common.LANGUAGE.ENGLISH;
      logger.debug('Setting language to English');
      break;
    case 1:
      lang = common.LANGUAGE.CHINESE_TRADITIONAL;
      logger.debug('Setting language to Traditional Chinese');
      break;
    case 2:
      lang = common.LANGUAGE.CHINESE_SIMPLIFIED;
      logger.debug('Setting language to Simplified Chinese');
      break;
    case 3:
      lang = common.LANGUAGE.JAPANESE;
      logger.debug('Setting language to Japanese');
      break;
    case 4:
      lang = common.LANGUAGE.FRENCH;
      logger.debug('Setting language to French');
      break;
    case 5:
      lang = common.LANGUAGE.GERMAN;
      logger.debug('Setting language to German');
      break;
    case 6:
      lang = common.LANGUAGE.SPANISH;
      logger.debug('Setting language to Spanish');
      break;
    case 7:
      lang = common.LANGUAGE.RUSSIAN;
      logger.debug('Setting language to Russian');
      break;
    case 8:
      lang = common.LANGUAGE.POLISH;
      logger.debug('Setting language to POLISH');
      break;
  }

  buf[2] = 1;
  buf[3] = language;

  var settingsClient = dgram.createSocket('udp4');
  settingsClient.send(buf, 0, buf.length, settings.UDP_SETTINGS_PORT, rvaIP, function(err) {
    if (err)
      logger.debug('UDP error: ' + err);
  });

  var timeout = setTimeout(function() {
    self.emit('SETTINGS_TIMEOUT');
    logger.debug('timeout!');
    settingsClient.close();
  }, settings.settingsTimeOut);

  settingsClient.on('message', function(msg) {
    clearTimeout(timeout);
    var result = msg[2],
      reboot = msg[3];
    settingsClient.close();
    handleSettingsResult(self, result, reboot);
  });

};

// ====================================================== Luke's Code START =============================== //
// ==== Voting Polling parameters
var votingJsonNum = 0;
var userAnswer = 0;
// ==== File Sharing parameters
var upload_report = {};
var sendFileObject;
var captureScreenshot = false;
var receiveFileUser = 0;
var screenCaptureTimeout;
var isSendAnswer = false;
var answer_time = 0;
// TODO: Molly call this function when moderator click [Start] / [Answer On]
RVAClient.prototype.callVotingStart = function callVotingStart(json_Question) {
  var self = this;
  logger.debug('##### UI call starting voting');
  var qq = JSON.parse(JSON.stringify(json_Question).replace('data:image/png;base64,', '').replace('data:image/jpeg;base64,', '').replace('data:image/jpg;base64,', ''));
  if (qq.correct === '') {
    isSendAnswer = false;
    answer_time = 0;
  } else {
    isSendAnswer = true;
  }
  var number = qq.num - 1;
  qq.num = number;
  var result = '[' + JSON.stringify(qq) + ']';
  var sendArray = dsaUtil.utf8ToByteArray(result);
  self.openDataServerSocket(common.CMD.POLLING_VOTING_V2, sendArray, null);
};
//TODO: Molly call this function when moderator click [Stop]
RVAClient.prototype.callVotingStop = function callVotingStop() {
  var self = this;
  logger.debug('##### UI call stoping voting');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.POLLING_VOTING_V2;
  buf[2] = settings.client_id;
  buf[3] = 2;
  RVACommandClient.write(buf);
  settings.isVotingPolling = false;
  self.checkExchangeDataNeedClose();
};

RVAClient.prototype.sendVotingPollingStart = function sendVotingPollingStart() {
  logger.debug('##### Send voting start to all client');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.POLLING_VOTING_V2;
  buf[2] = settings.client_id;
  buf[3] = 0;
  RVACommandClient.write(buf);
};

// TODO: Molly call this function when client send answer
RVAClient.prototype.sendVotingAnswer = function sendVotingAnswer(num, answer) {
  var self = this;
  logger.debug('##### UI send answer voting');
  var sendAns;
  userAnswer = answer;
  logger.debug('##### Number = ' + num);
  if (num !== votingJsonNum) {
    logger.debug('##### Wrong question number');
  } else {
    if (answer.length > 2) {
      logger.debug('##### type of file');
      sendAns = '{' + '\"num\" : \"' + num + '\", \"user_name\" : \"' + settings.clientName + '\", \"answer\" : \"' + answer + '\"}';
      var json_answer = dsaUtil.utf8ToByteArray(sendAns);
      self.openDataServerSocket(common.CMD.POLLING_VOTING_V2, json_answer, null);
    } else {
      logger.debug('##### type of integer');
      sendAns = '{' + '\"num\" : \"' + num + '\", \"user_name\" : \"' + settings.clientName + '\", \"answer\" : ' + answer + '}';
      var json_answer = dsaUtil.utf8ToByteArray(sendAns);
      self.openDataServerSocket(common.CMD.POLLING_VOTING_V2, json_answer, null);
    }
  }
};

RVAClient.prototype.receivePollingVotingV2 = function receivePollingVotingV2(bValues, s_bytes) {
  var self = this;
  logger.debug('##### Receive CMD Polling Voting V2');
  var status = bValues[s_bytes + 3];
  if (status === 0) {
    logger.debug('##### Voting Start !');
    self.openDataServerSocket(null, null, common.CMD.POLLING_VOTING_V2);
  } else if (status === 2) {
    logger.debug('##### Voting Stop !');
    // TODO: Tell Molly voting stop now
    self.emit('votingHandler', 'stopAnswerQuestion');
  }
};

/**
 * arg1 = send to RVA, arg2 = data, arg3 = Receive from RVA
 */
RVAClient.prototype.openDataServerSocket = function openDataServerSocket(arg1, arg2, arg3) {
  logger.debug('##### call Data Exchange server CMD');
  var self = this;
  if (typeof dataServerSocket === 'undefined' || dataServerSocket.destroyed) {
    logger.debug('###### initial new server');
    dataServerSocket = new net.Socket();
    dataServerSocket.setNoDelay(true);
    dataExchangeRun = true;
    dataServerSocket.connect(settings.DATA_SERVER_PORT, settings.rvaIp, function() {
      logger.debug('##### Connected to dataServerSocket');
      self.sendDataExchangeCMD(arg1, arg2, arg3);
    });
    dataServerSocket.on('data', function(data) {
      var receiveBuffer = new Uint8Array(data);
      dEHBufer.push.apply(dEHBufer, receiveBuffer);
      if (dataExchangeRun) {
        while (dEHBufer.length >= dsaUtil.convertBytesBigEndianToInt(dEHBufer[0], dEHBufer[1], dEHBufer[2], dEHBufer[3])) {
          dataExchangeRun = false;
          slice_byte = self.receiveDataServerSocket(dEHBufer, 0);
          if (slice_byte > dEHBufer.length) {
            dataExchangeRun = true;
            break;
          } else {
            dEHBufer.splice(0, slice_byte);
            dataExchangeRun = true;
          }
          if (dEHBufer.length === 0) {
            dataExchangeRun = true;
            break;
          }
        }
      }
    });

    dataServerSocket.on('end', function() {
      logger.debug('dataServerSocket end received');
    });

    dataServerSocket.on('error', function(error) {
      logger.error('dataServerSocket error: ' + JSON.stringify(error));
    });

    dataServerSocket.on('close', function() {
      logger.info('Client disconnected dataServerSocket connection');
      settings.isPreview = false;
      settings.isGroupUpload = false;
      settings.isVotingPolling = false;
      settings.isFileSharing = false;
      settings.isFileDownloading = false;
      settings.isReadyToSend = true;
      settings.isClosingSendFilepage = false;
      settings.isSendToCancel = false;
    });
  } else {
    logger.debug('##### dataServerSocket reuse , continue to send');
    self.sendDataExchangeCMD(arg1, arg2, arg3);
  }
};

RVAClient.prototype.receiveDataServerSocket = function receiveDataServerSocket(bValues, sByte) {
  var self = this;
  logger.debug('##### Data Exchange Socket receive');
  var packet_length = dsaUtil.convertBytesBigEndianToInt(bValues[sByte],
    bValues[sByte + 1], bValues[sByte + 2], bValues[sByte + 3]);
  if (packet_length === settings.DATA_PORT_MINIMUM_MSG_LENGTH) {
    var cmdCode = bValues[sByte + 4];
    switch (cmdCode) {
      case common.CMD.PREVIEW_DATA:
        logger.debug('##### receive data preview status code');
        settings.isPreview = false;
        if (bValues[sByte + 6] === common.STATUSCODE.FAIL) {
          logger.debug('##### Receive preview data fail');
          logger.debug('##### Error code = ' + bValues[sByte + 7]);
        }
        self.checkExchangeDataNeedClose();
        break;
      case common.CMD.UPLOAD_GROUP_DATA_V2:
        settings.isGroupUpload = false;
        logger.debug('##### receive UPLOAD GROUP DATA V2 STATUS CODE');
        logger.debug('##### 6 = ' + bValues[sByte + 6]);
        logger.debug('##### 7 = ' + bValues[sByte + 7]);
        if (bValues[sByte + 6] === common.STATUSCODE.UPLOAD_GROUP_FILE_SUCCESS) {
          self.resetDataExchangeServerStatus();
          settings.groupMode = false;
          var host = userUtil.getUserData(settings.client_id);
          userUtil.user_list = [];
          userUtil.group_list = [];
          userUtil.add_userdata(settings.client_id, host.label, true, host.device, host.panel, true, true, host.os_type);
          self.getGroupList(settings.uploadGroupData);
          settings.groupMode = true;
          userUtil.refreshUserListByGroupList();
          self.emit('settingsUpdated', self.prepareSettingsForUI());
          self.emit('userListUpdated', userUtil.user_list);
          self.setGroupMode(0);
        } else {
          // TODO: send to Molly fail to upload group
          settings.groupMode = false;
          settings.groupName = '';
          settings.group = '';
        }
        break;
      case common.CMD.POLLING_VOTING_V2:
        logger.debug('##### Receive Voting Polling status');
        logger.debug('##### Status code = ' + bValues[sByte + 6]);
        logger.debug('##### Error code = ' + bValues[sByte + 7]);
        if (userUtil.amIHost(settings.client_id)) {
          // Moderator send voting question
          settings.isVotingPolling = true;
          if (bValues[sByte + 6] === common.STATUSCODE.SUCCESS && bValues[sByte + 7] === common.STATUSCODE.SUCCESS) {
            // TODO: tell Molly voting start success
            self.emit('votingStartSuccess');
            if (isSendAnswer) {
              answer_time = answer_time + 1;
              if (answer_time === 1) {
                self.sendVotingPollingStart();
              } else {
                logger.debug('##### no more !!!!');
              }
            } else {
              self.sendVotingPollingStart();
            }
          } else {
            // TODO: tell Molly voting start fail
            settings.isVotingPolling = false;
            self.emit('votingStartFail');
            self.checkExchangeDataNeedClose();
          }
        } else {
          // Client send voting answer
          settings.isVotingPolling = false;
          if (bValues[sByte + 6] === common.STATUSCODE.SUCCESS && bValues[sByte + 7] === common.STATUSCODE.SUCCESS) {
            // TODO: tell Molly send answer success
            var dataObj = {
              result: true,
              //            message: 'Your answer is submitted successfully'
              message: 'voting.submitSuccess'
            };
            self.emit('votingHandler', 'submitAnswerResult', dataObj);
          } else {
            // TODO: tell Molly send answer fail
            var dataObj = {
              result: false,
              message: 'voting.submitFail'
            };
            self.emit('votingHandler', 'submitAnswerResult', dataObj);
          }
          self.checkExchangeDataNeedClose();
        }
        break;
      case common.CMD.FILE_SHARING_DATA:
        logger.debug('##### Receive File Sharing');
        settings.isFileSharing = false;
        self.receiveCMDFileSharing(bValues);
        break;
      default:
        break;
    }
  } else {
    var cmdCode = bValues[sByte + 4];
    switch (cmdCode) {
      case common.CMD.PREVIEW_DATA:
        settings.isPreview = false;
        self.receivePreviewDataFromDataExchangeServer(bValues);
        break;
      case common.CMD.POLLING_VOTING_V2:
        logger.debug('##### Receive Voting Polling with Data');
        if (userUtil.amIHost(settings.client_id)) {
          // Moderator receive client's answer
          settings.isVotingPolling = true;
          self.receiveVotingPollingAnswer(bValues);
        } else {
          // Client receive moderator's question
          settings.isVotingPolling = false;
          self.receiveVotingPollingQuestion(bValues);
        }
        break;
      case common.CMD.FILE_SHARING_DATA:
        logger.debug('##### Receive File Sharing with Data');
        settings.isFileSharing = false;
        self.receiveCMDFileSharingWithData(bValues);
        settings.isFileDownloading = false;
        self.checkExchangeDataNeedClose();
        break;
      default:
        break;

    }
  }

  //  logger.debug('##### return length = ' + sByte + packet_length);
  return sByte + packet_length;
};

RVAClient.prototype.receiveVotingPollingAnswer = function receiveVotingPollingAnswer(
  receiveBuf) {
  logger.debug('##### Receive client answer');
  var self = this;
  var ansBuf = new Uint8Array(receiveBuf.length - 8);
  for (var i = 0; i < receiveBuf.length; i++) {
    ansBuf[i] = receiveBuf[i + 8];
  }

  var anwserJson = dsaUtil.convertByteArrayToStringUtf8(ansBuf);
  try {
    var answer = JSON.parse(anwserJson);
    answer.num = parseInt(answer.num) + 1;
    answer.user_name = userUtil.getDispalyName(answer.user_name);
    self.emit('votingReceiveAnswer', answer);
  } catch (e) {
    logger.debug('##### JSON ERROR');
    logger.debug(anwserJson);
  }



};

RVAClient.prototype.receiveVotingPollingQuestion = function receiveVotingPollingQuestion(
  receiveBuf) {
  logger.debug('##### Receive moderator question');
  var self = this;
  var queBuf = new Uint8Array(receiveBuf.length - 8);
  for (var i = 0; i < receiveBuf.length; i++) {
    queBuf[i] = receiveBuf[i + 8];
  }

  var question_json = dsaUtil.convertByteArrayToStringUtf8(queBuf);
  try {
    votingJsonNum = JSON.parse(question_json)[0].num;
    self.checkVotingAnswer(JSON.parse(question_json)[0]);
    // TODO: Luke check voting answer if need...
    // TODO: Molly display question ...
    self.checkExchangeDataNeedClose();
  } catch (e) {
    logger.debug('##### JSON ERROR');
    logger.debug(question_json);
  }


};

RVAClient.prototype.checkVotingAnswer = function checkVotingAnswer(result) {
  var self = this;
  if (result.correct === null || result.correct === '') {
    //    if (lastQuestionNumber !== votingJsonNum) {
    logger.debug('##### Clean user answer');
    userAnswer = 0;
    //    }
    var data = {
      isReady: true,
      question: result
    };
    self.emit('goVoting', data);
  } else if (result.type === 6 || result.type === 1) {
    // ThumbUpDown and OpenEnd didn't check correct answer
    var data = {
      isReady: true,
      question: result
    };
    self.emit('goVoting', data);
  } else {
    var correct = parseInt(result.correct);
    if (userAnswer === 0) {
      var dataObj = {
        flag: 3
      };
      self.emit('votingHandler', 'showAnswerResultDialog', dataObj);
    } else if (parseInt(userAnswer) === correct) {
      var dataObj = {
        flag: 1
      };
      self.emit('votingHandler', 'showAnswerResultDialog', dataObj);
    } else {
      var correctAnswer = dsaUtil.parseVotingAnswerType(correct);
      var dataObj = {
        flag: 2,
        correctAnswer: correctAnswer
      };
      self.emit('votingHandler', 'showAnswerResultDialog', dataObj);
    }
  }
};

RVAClient.prototype.receivePreviewDataFromDataExchangeServer = function receivePreviewDataFromDataExchangeServer(
  receiveBuf) {
  var self = this;
  logger.debug('##### Receive PREVIEW DATA from exchange ');
  clearTimeout(previewTimer);
  var screenBuf = new Uint8Array(receiveBuf.length - 8);
  for (var i = 0; i < receiveBuf.length; i++) {
    screenBuf[i] = receiveBuf[i + 8];
  }

  var img = dsaUtil.covertArrayToBase64(screenBuf);
  logger.debug('##### ' + img);
  var data = {
    src: img,
    uid: settings.screenshot_userid
  };
  self.emit('receivePreviewImage', data);
  self.checkExchangeDataNeedClose();
};

RVAClient.prototype.checkExchangeDataNeedClose = function checkExchangeDataNeedClose() {
  if (settings.isPreview || settings.isGroupUpload || settings.isVotingPolling || settings.isFileSharing) {
    logger.debug('##### Exchange data server is still using');
  } else {
    logger.debug('##### Close Exchange data server while is no use');
    dataServerSocket.destroy();
  }
};

RVAClient.prototype.sendDataExchangeCMD = function sendDataExchangeCMD(arg1, arg2, arg3) {
  var self = this;
  // Receive from RVA
  if (arg3 !== null) {
    switch (arg3) {
      case common.CMD.PREVIEW_DATA:
        logger.debug('##### With Preview');
        settings.isPreview = true;
        self.sendPreviewDataMessage(settings.client_id, 1, arg2);
        break;
      case common.CMD.POLLING_VOTING_V2:
        logger.debug('##### WITH POLLING VOTING');
        settings.isVotingPolling = true;
        self.sendVotingPollingData(3, arg2);
        break;
      case common.CMD.UPLOAD_GROUP_DATA_V2:
        break;
      case common.CMD.FILE_SHARING_DATA:
        logger.debug('##### WITH FILE SHARING');
        settings.isFileSharing = true;
        self.sendCMDFileSharing(common.FS_CMD.DOWNLOAD, 0, 0, null);
        break;
      default:
        logger.debug('##### wrong send data exchange CMD');
    }
  } else {
    // Send to RVA
    switch (arg1) {
      case common.CMD.PREVIEW_DATA:
        logger.debug('##### With Preview');
        settings.isPreview = true;
        self.sendPreviewDataMessage(arg2, 0, null);
        break;
      case common.CMD.POLLING_VOTING_V2:
        logger.debug('##### WITH POLLING VOTING');
        settings.isVotingPolling = true;
        // TODO: check isHost
        if (userUtil.amIHost(settings.client_id)) {
          // Voting start
          self.sendVotingPollingData(0, arg2);
        } else {
          // Send Answer
          self.sendVotingPollingData(1, arg2);
        }
        break;
      case common.CMD.UPLOAD_GROUP_DATA_V2:
        logger.debug('##### With Group upload');
        settings.isGroupUpload = true;
        self.sendGroupDataMessage(arg2);
        break;
      case common.CMD.FILE_SHARING_DATA:
        logger.debug('##### With File Sharing');
        upload_report = {};
        settings.isSendToCancel = false;
        self.sendCMDFileSharing(common.FS_CMD.STATUS, 0, null, null);
        break;
      default:
        logger.debug('##### wrong receive data exchange CMD');
    }
  }
};

RVAClient.prototype.setGroupMode = function setGroupMode(arg) {
  logger.debug('#### set group mode = ' + arg);
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.UPLOAD_GROUP_DATA_V2;
  buf[2] = settings.client_id;
  buf[3] = arg;
  RVACommandClient.write(buf);
};

/**
 * arg1 = 0 : polling request, arg1 = 1 : polling response, arg1 = 2 : polling
 * stop, arg1 = 3 : request from data
 */
RVAClient.prototype.sendVotingPollingData = function sendVotingPollingData(arg1, arg2) {
  var data_length = 0;
  if (arg2 === null) {
    // Polling request with no data
    data_length = 8;
  } else {
    data_length = 8 + arg2.length;
  }

  var buf = new Buffer(data_length);
  var data_size = dsaUtil.convertIntToBytesBigEndian(data_length);
  buf[0] = data_size[0];
  buf[1] = data_size[1];
  buf[2] = data_size[2];
  buf[3] = data_size[3];
  buf[4] = common.CMD.POLLING_VOTING_V2;
  buf[5] = settings.client_id;
  buf[6] = arg1;
  buf[7] = 0;
  if (data_length > settings.DATA_PORT_MINIMUM_MSG_LENGTH) {
    for (var i = 0; i < arg2.length; i++) {
      buf[i + 8] = arg2[i];
    }
  }
  dataServerSocket.write(buf);
};

RVAClient.prototype.sendPreviewDataMessage = function sendPreviewDataMessage(
  userid, arg1, arg2) {
  var data_length = 0;
  if (arg2 === null) {
    // Moderator request preview image
    data_length = 8;
  } else {
    data_length = 8 + arg2.length;
  }
  async.series([function(done) {
    var buf = new Buffer(data_length);
    var data_size = dsaUtil.convertIntToBytesBigEndian(data_length);
    buf[0] = data_size[0];
    buf[1] = data_size[1];
    buf[2] = data_size[2];
    buf[3] = data_size[3];
    buf[4] = common.CMD.PREVIEW_DATA;
    buf[5] = settings.client_id;
    buf[6] = arg1;
    buf[7] = 0;
    if (data_length > settings.DATA_PORT_MINIMUM_MSG_LENGTH) {
      for (var i = 0; i < arg2.length; i++) {
        buf[i + 8] = arg2[i];
      }
      dataServerSocket.write(buf);
      done();
    } else {
      dataServerSocket.write(buf);
      done();
    }
  }, function(done) {
    if (data_length === settings.DATA_PORT_MINIMUM_MSG_LENGTH) {
      var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
      buf[0] = settings.MINIMUM_MSG_LENGTH;
      buf[1] = common.CMD.PREVIEW_DATA;
      buf[2] = userid;
      buf[3] = 0;

      RVACommandClient.write(buf);
      done();
    } else {
      done();
    }
  }]);
};

RVAClient.prototype.sendGroupDataMessage = function sendGroupDataMessage(arg) {
  settings.uploadGroupData = arg;
  var data = dsaUtil.utf8ToByteArray(arg);
  var data_size = data.length + settings.DATA_PORT_MINIMUM_MSG_LENGTH;

  var buf = new Buffer(data_size);
  var sizeArray = dsaUtil.convertIntToBytesBigEndian(data_size);
  buf[0] = sizeArray[0];
  buf[1] = sizeArray[1];
  buf[2] = sizeArray[2];
  buf[3] = sizeArray[3];

  buf[4] = common.CMD.UPLOAD_GROUP_DATA_V2;
  buf[5] = settings.client_id;
  buf[6] = 0;
  buf[7] = 0;

  for (var i = 0; i < data.length; i++) {
    buf[settings.DATA_PORT_MINIMUM_MSG_LENGTH + i] = data[i];
  }

  dataServerSocket.write(buf);
};

RVAClient.prototype.receiveFileSharingData = function receiveFileSharingData() {
  logger.debug('##### receive CMD FILE DOWNLOAD');
  var self = this;
  settings.isFileDownloading = true;
  self.openDataServerSocket(null, null, common.CMD.FILE_SHARING_DATA);
};

// new file sharing

RVAClient.prototype.initialFileSharing = function initialFileSharing(arg) {
  logger.debug('##### initial CMD FILE SHARING');
  var self = this;
  var data = {};
  if (arg !== null) {
    captureScreenshot = false;
    logger.debug('##### send select file mode');
    sendFileObject = arg;
    data.filename = sendFileObject.name;
    self.emit('askSendFilePage', data);
  } else {
    captureScreenshot = true;
    logger.debug('##### send screenshot file mode');
    async.series([
      function(done) {
        var fName = dsaUtil.getFilenameByCurrentTime();
        data.filename = fName;
        screenCaptureTimeout = setTimeout(function() {
          nativeService.getPreviewImage(settings.screenWidth, settings.screenHeight, settings.currentScreen, function(result) {
            sendFileObject = {
              'name': fName,
              'bytearray': result
            };
            data.image = result.toString('base64');
            done();
          });
        }, 3000);
      },
      function(done) {
        self.emit('askSendFilePage', data);
        done();
      }
    ]);
  }
};

RVAClient.prototype.confirmToSharingFile = function confirmToSharingFile() {
  logger.debug('##### user confirm to start file sharing');
  var self = this;
  if (captureScreenshot) {
    clearTimeout(screenCaptureTimeout);
  }
  self.openDataServerSocket(common.CMD.FILE_SHARING_DATA, null, null);
};

RVAClient.prototype.isFileReadyToSend = function isFileReadyToSend() {
  var result = settings.isReadyToSend;
  return result;
};

RVAClient.prototype.isFileDownloading = function isFileDownloading() {
  var result = settings.isFileDownloading;
  return result;
};

RVAClient.prototype.sendCMDFileSharing = function sendCMDFileSharing(arg1, arg2, name_data, file_data) {
  var data_length = 0;
  if (file_data !== null) {
    data_length = 8 + 1 + name_data.length + file_data.length;
  } else {
    data_length = 8;
  }

  var buf = new Buffer(data_length);
  var data_size = dsaUtil.convertIntToBytesBigEndian(data_length);
  buf[0] = data_size[0];
  buf[1] = data_size[1];
  buf[2] = data_size[2];
  buf[3] = data_size[3];

  buf[4] = common.CMD.FILE_SHARING_DATA;
  buf[5] = settings.client_id;
  buf[6] = arg1;
  buf[7] = arg2;

  if (data_length > 8) {
    //filename
    var name_length = name_data.length;
    buf[8] = name_length;
    for (var i = 0; i < name_length; i++) {
      buf[9 + i] = name_data[i];
    }
    //filedata
    for (var k = 0; k < file_data.length; k++) {
      buf[9 + name_length + k] = file_data[k];
    }

    dataServerSocket.write(buf);
  } else {
    dataServerSocket.write(buf);
  }
};

RVAClient.prototype.receiveCMDFileSharing = function receiveCMDFileSharing(receive_buffer) {
  logger.debug('##### Receive CMD FILE SHARING');
  var self = this;
  var fs_cmd = receive_buffer[6];
  switch (fs_cmd) {
    case common.FS_CMD.AVAILABLE:
      logger.debug('##### Receive FS_CMD AVAILABLE');
      settings.isReadyToSend = true;
      logger.debug(sendFileObject.name);
      var filename_data = dsaUtil.utf8ToByteArray(sendFileObject.name);
      var homePath = process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'];
      if (captureScreenshot) {
        async.series([
          function(done) {
            mkdirp(homePath + '/GroupShare', function(err) {
              if (err !== null) {
                logger.error('##### sendScreen mkdirp error');
                self.emit('error', err);
              }
              done();
            });
          },
          function(done) {
            fs.writeFile(homePath + '/GroupShare/' + sendFileObject.name, sendFileObject.bytearray, function(err) {
              if (err !== null) {
                logger.error('##### sendScreen writeFile error');
              } else {
                self.sendCMDFileSharing(common.FS_CMD.UPLOAD, 0, filename_data, sendFileObject.bytearray);
                done();
              }
            });
          }
        ]);
      } else {
        async.series([
          function(done) {
            fs.readFile(sendFileObject.path, function(err, data) {
              if (err !== null) {
                self.emit('error', err);
              } else {
                self.sendCMDFileSharing(common.FS_CMD.UPLOAD, 0, filename_data, data);
                done();
              }
            });
          },
          function(done) {
            upload_report.percentage = 50;
            upload_report.finished = 0;
            // Corporation or Education

            if (settings.rvaType === 'EDU') {
              if (userUtil.amIHost(settings.client_id)) {
                upload_report.total = userUtil.getOnlineUserCounts() - 1;
                receiveFileUser = userUtil.getOnlineUserCounts() - 1;
              } else {
                upload_report.total = 1;
                receiveFileUser = 1;
              }
            } else if (settings.rvaType === 'CORP') {
              if (settings.isCorpModeratorOn) {
                if (userUtil.amIHost(settings.client_id)) {
                  upload_report.total = userUtil.getOnlineUserCounts() - 1;
                  receiveFileUser = userUtil.getOnlineUserCounts() - 1;
                } else {
                  upload_report.total = 1;
                  receiveFileUser = 1;
                }
              } else {
                upload_report.total = userUtil.getOnlineUserCounts() - 1;
                receiveFileUser = userUtil.getOnlineUserCounts() - 1;
              }
            }
            self.emit('sendingFilePage', upload_report);
            done();
          }
        ]);
      }
      break;
    case common.FS_CMD.BUSY:
      logger.debug('##### Receive FS_CMD BUSY');
      settings.isFileSharing = false;
      self.checkExchangeDataNeedClose();
      self.emit('systemBusyPage');
      break;
    case common.FS_CMD.STATUS:
      var result = receive_buffer[7];
      if (result === common.STATUSCODE.SUCCESS) {
        if (settings.isSendToCancel) {
          settings.isReadyToSend = true;
          settings.isFileDownloading = false;
          logger.debug('##### Send CMD FILE SHARING DATA CANCEL SUCCESS');
        } else {
          settings.isFileSharing = true;
          settings.isReadyToSend = false;
          upload_report.percentage = 100;
          upload_report.finished = 0;

          // Corporation or Education
          if (settings.rvaType === 'EDU') {
            if (userUtil.amIHost(settings.client_id)) {
              upload_report.total = userUtil.getOnlineUserCounts() - 1;
              receiveFileUser = userUtil.getOnlineUserCounts() - 1;
            } else {
              upload_report.total = 1;
              receiveFileUser = 1;
            }
          } else if (settings.rvaType === 'CORP') {
            if (settings.isCorpModeratorOn) {
              if (userUtil.amIHost(settings.client_id)) {
                upload_report.total = userUtil.getOnlineUserCounts() - 1;
                receiveFileUser = userUtil.getOnlineUserCounts() - 1;
              } else {
                upload_report.total = 1;
                receiveFileUser = 1;
              }
            } else {
              upload_report.total = userUtil.getOnlineUserCounts() - 1;
              receiveFileUser = userUtil.getOnlineUserCounts() - 1;
            }
          }
          if (!settings.isClosingSendFilepage) {
            self.emit('sendingFilePage', upload_report);
          }
          self.sendCMDFileSharingStart();
        }
      } else if (result === common.STATUSCODE.FAIL) {
        logger.debug('##### CMS STATUS FAIL');
        settings.isFileDownloading = false;
        settings.isReadyToSend = true;
        settings.isClosingSendFilepage = false;
        settings.isFileSharing = false;
        self.checkExchangeDataNeedClose();
      }
      break;
    case common.FS_CMD.PROGRESS:
      logger.debug('##### Receive FS_CMD PROGRESS');
      if (settings.isSendToCancel) {
        logger.debug('##### But File already cancel');
        break;
      } else {
        settings.isFileSharing = true;
        upload_report.finished = receive_buffer[7];
        logger.debug('##### upload file 100% and send to ' + receive_buffer[7] + '/' + receiveFileUser + ' participarts last time');
        if (receive_buffer[7] >= receiveFileUser) {
          logger.debug('##### All download complete');
          settings.isReadyToSend = true;
          settings.isFileDownloading = false;
          settings.isFileSharing = false;
          self.emit('closeSendingFilePage');
          self.checkExchangeDataNeedClose();
        } else {
          if (!settings.isClosingSendFilepage) {
            self.emit('sendingFilePage', upload_report);
          }
        }
      }
      break;
    case common.FS_CMD.CANCEL:
      logger.debug('##### Receive FS_CMD CANCEL');
      settings.isFileSharing = false;
      settings.isFileDownloading = false;
      self.checkExchangeDataNeedClose();
      break;
    default:
      break;
  }
};

RVAClient.prototype.sendCMDFileSharingStart = function sendCMDFileSharingStart() {
  logger.debug('Send CMD File Sharing Start');
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.FILE_SHARING_DATA;
  buf[2] = settings.client_id;
  buf[3] = 0;

  RVACommandClient.write(buf);
};

RVAClient.prototype.setBackGroundFileSending = function setBackGroundFileSending(arg) {
  settings.isClosingSendFilepage = arg;
};

RVAClient.prototype.receive_CMD_FILE_SHARING_DATA = function receive_CMD_FILE_SHARING_DATA() {
  logger.debug('##### Receive File downloading command');
  var self = this;
  settings.isFileDownloading = true;
  self.openDataServerSocket(null, null, common.CMD.FILE_SHARING_DATA);
};

RVAClient.prototype.receiveCMDFileSharingWithData = function receiveCMDFileSharingWithData(arg) {
  logger.debug('##### Receive Sharing File Data');
  var self = this;
  var total_length = dsaUtil.convertBytesBigEndianToInt(arg[0], arg[1], arg[2], arg[3]);
  var send_id = arg[5];
  var send_name = '';
  if (send_id === 255) {
    send_name = 'NovoPro';
  } else {
    send_name = userUtil.getSendNameById(send_id);
  }
  var homePath = process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'];
  var filename_length = arg[8];
  var filename_buf = new Uint8Array(filename_length);
  for (var j = 0; j < filename_length; j++) {
    filename_buf[j] = arg[j + 9];
  }
  var receive_filename = dsaUtil.byteArrayToUtf8(filename_buf);
  logger.debug('Receive Filename = ' + receive_filename);
  var filedata_length = total_length - 8 - 1 - filename_length;
  var filedata_offset = 8 + 1 + filename_length;
  var filedata_buf = new Uint8Array(filedata_length);
  for (var k = 0; k < filedata_length; k++) {
    filedata_buf[k] = arg[filedata_offset + k];
  }
  var node_buf = new Buffer(filedata_buf);

  async.series([
    function(done) {
      mkdirp(homePath + '/GroupShare', function(err) {
        if (err !== null) {
          logger.error('##### receiveFileSharing mkdirp error');
          self.emit('error', err);
        }
        done();
      });
    },
    function() {
      fs.writeFile(homePath + '/GroupShare/' + send_name + '_' + receive_filename, node_buf, function(err) {
        if (err !== null) {
          logger.error('##### receiveFileSharing writeFile error');
        } else {
          var data = {};
          data.filename = receive_filename;
          data.sender = send_name;
          self.emit('fileReceived', data);
          self.emit('minimizedAttention');
        }
      });
    }
  ]);

};

RVAClient.prototype.askSendAnotherFilePage = function askSendAnotherFilePage() {
  var self = this;
  logger.debug('##### askSendAnotherFilePage');
  self.emit('askSendAnotherFilePage', upload_report);
};

RVAClient.prototype.send_CMD_FILE_SHARING_DATA_CANCEL = function send_CMD_FILE_SHARING_DATA_CANCEL() {
  logger.debug('##### Send CMD File Sharing Cancel');
  settings.isSendToCancel = true;
  settings.isReadyToSend = true;
  settings.isFileDownloading = false;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.FILE_SHARING_DATA;
  buf[2] = settings.client_id;
  buf[3] = 1;

  RVACommandClient.write(buf);
};

RVAClient.prototype.openSendingFilePage = function openSendingFilePage() {
  var self = this;
  self.emit('sendingFilePage', upload_report);
};

RVAClient.prototype.resetDataExchangeServerStatus = function resetDataExchangeServerStatus() {
  var self = this;
  settings.isPreview = false;
  settings.isGroupUpload = false;
  settings.isVotingPolling = false;
  settings.isFileSharing = false;
  settings.isFileDownloading = false;
  settings.isReadyToSend = true;
  settings.isClosingSendFilepage = false;
  settings.isSendToCancel = false;
  self.checkExchangeDataNeedClose();
};

RVAClient.prototype.checkIsVoting = function checkIsVoting() {
  return settings.isVotingPolling;
};

RVAClient.prototype.checkIsFileSharing = function checkIsFileSharing() {
  var result = settings.isFileSharing || settings.isFileDownloading;
  return result;
};

RVAClient.prototype.soundFlowerReminderOff = function soundFlowerReminderOff() {
  logger.debug('soundFlowerReminderOff');
  db.find({
    documentType: 'soundFlowerReminderSettings'
  }, function(err, docs) {
    if (docs.length === 0) {
      var initialDoc = {
        'documentType': 'soundFlowerReminderSettings',
        'soundFlowerReminder': false
      };
      db.insert(initialDoc, function(err) {
        if (err !== null) {
          logger.error(err.message);
        }
        logger.debug('Initial soundFlowerReminderSettings created!');
      });
    }
  });
};

var ipList = [];

//NovoLookup Server
RVAClient.prototype.saveLookupServerIP = function saveLookupServerIP(ip) {
  var self = this;
  var field = {
    name: 'lookupServer',
    value: ip
  };
  self.set(field);
};


RVAClient.prototype.startLookupServerChecking = function startLookupServerChecking() {
  var self = this;
  setInterval(function() {
    self.getSettings(function(setting) {
      if (setting.lookupServer !== '' && settings.client_id === 0) {
        self.initialLookupClient(setting.lookupServer);
      }
    });
  }, 10000);

};

RVAClient.prototype.initialLookupClient = function initialLookupClient(serverAddress) {
  var self = this;
  var lookupClient = new net.Socket();
  lookupClient.setNoDelay(true);
  lookupClient.connect(settings.LOOKUP_CONNECTION_PORT, serverAddress, function connect() {
    var message = 'GetList';
    var buf = dsaUtil.utf8ToByteArray(message);
    var node_buf = new Buffer(buf);
    lookupClient.write(node_buf);
  });

  lookupClient.on('connect', function() {

  });

  lookupClient.on('data', function(data) {
    var receiveBuffer = new Uint8Array(data);
    var ipdata = dsaUtil.byteArrayToUtf8(receiveBuffer);
    self.getLookupList(ipdata);

  });

  lookupClient.on('error', function(error) {
    logger.error('lookupClient error: ' + JSON.stringify(error));
  });

  lookupClient.on('end', function() {

  });

  lookupClient.on('close', function() {
    self.emit('getLookupServer', ipList);
    lookupClient.destroy();
  });
};

RVAClient.prototype.getLookupList = function getLookupList(xml_str) {
  var self = this,
    parser = new xml2js.Parser({
      explicitArray: false
    });

  async.series([
    function(done) {
      parser.parseString(xml_str, function(err, result) {
        var devices = result.NovoLookup.Devices;
        var i_list = [];

        var devObj = JSON.parse(JSON.stringify(devices));
        for (var i = 0; i < devObj.Device.length; i++) {
          var devName = devObj.Device[i].$.DeviceName;
          if (devObj.Device[i].$.DeviceName.length === 0) {
            devName = devObj.Device[i].$.ID;
          }
          var ipObj = {
            ip: devObj.Device[i].$.IP,
            deviceName: devName,
            status: devObj.Device[i].$.Status
          };
          i_list.push(ipObj);
        }
        ipList = i_list;
        self.emit('getLookupServer', i_list);

        done();
      });
    }
  ]);
};

RVAClient.prototype.send_CMD_REMOTE_CONTROL = function send_CMD_REMOTE_CONTROL(mode) {
  logger.debug('##### Send Remote control mode = ' + mode);
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.REMOTE_CONTROL_CMD;
  buf[2] = 0;
  switch (mode) {
    case common.REMOTE_CONTROL_CMD.STATUS_REQUEST:
      buf[3] = 0;
      break;
    case common.REMOTE_CONTROL_CMD.RVA_HOME:
      buf[3] = 40;
      break;
    case common.REMOTE_CONTROL_CMD.ESS_CAST:
      buf[3] = 50;
      break;
  }

  RVACommandClient.write(buf);
};

RVAClient.prototype.receive_REMOTE_CONTROL_CMD = function receive_REMOTE_CONTROL_CMD(bValues) {
  var self = this;
  logger.debug('##### RECEIVE REMOTE CONTROL CMD');
  //settings.network_mode = 1; hotspot mode
  var arg = bValues[3];
  logger.debug('##### arg = ' + arg);

  self.remoteMouseStop(function(result) {
    logger.debug('remote mouse stop: ' + result);
    remoteMouseState = false;
  });

  switch (arg) {
    case common.REMOTE_CONTROL_CMD.RVA_HOME_SUCCESS:
      self.updateRemotePanel(arg);
      break;
    case common.REMOTE_CONTROL_CMD.RVA_HOME_FAIL:
      self.updateRemotePanel(arg);
      break;
    case common.REMOTE_CONTROL_CMD.ESS_CAST_SUCCESS:
      self.updateRemotePanel(arg);
      break;
    case common.REMOTE_CONTROL_CMD.ESS_CAST_FAIL:
      self.updateRemotePanel(arg);
      break;
    case common.REMOTE_CONTROL_CMD.NORMAL_MODE_IN_USE:
      self.updateRemotePanel(arg);
      break;
    default:
      logger.debug('REMOTE CONTROL CMD unhandled');
      logger.debug('arg: ' + arg);
      break;
  }
};


RVAClient.prototype.updateRemotePanel = function updateRemotePanel(status) {
  var self = this;
  var modePanelData = {};
  switch (status) {
    case common.REMOTE_CONTROL_CMD.RVA_HOME_SUCCESS:
      modePanelData.grayOut = false;
      modePanelData.lightOn = true;
      if (settings.fullMode > 0) {
        isStateChange = true;
        settings.fullMode = 0;
        nativeService.desktopStreamingStop(function() {
          if (remoteMouseState) {
            if (settings.remoteControl) {
              nativeService.remoteControlStop(function(result) {
                logger.debug('remote control disabled: ' + result);
                remoteMouseState = false;
              });
            }
          }
          nativeService.setExtendedScreenMode(0, 0, 0, 0, function(result) {
            logger.debug('updateRemotePanel setExtendedScreenMode result:' + result);
          });
        });
      } else if (settings.splitMode > 0) {
        isStateChange = true;
        settings.splitMode = 0;
        nativeService.splitModeStop();
      }
      self.emit('remoteStatusUpdate', modePanelData);
      break;
    case common.REMOTE_CONTROL_CMD.RVA_HOME_FAIL:
      modePanelData.grayOut = false;
      break;
    case common.REMOTE_CONTROL_CMD.ESS_CAST_SUCCESS:
      modePanelData.grayOut = false;
      modePanelData.lightOn = false;
      if (settings.fullMode < 1) {
        settings.fullMode = 1;
        self.initialStreamingServer();
      }
      self.emit('remoteStatusUpdate', modePanelData);
      break;
    case common.REMOTE_CONTROL_CMD.ESS_CAST_FAIL:
      modePanelData.grayOut = false;
      modePanelData.lightOn = true;
      self.emit('remoteStatusUpdate', modePanelData);
      break;
    case common.REMOTE_CONTROL_CMD.STATUS_REQUEST:
      var user = userUtil.getUserData(settings.client_id);
      if (typeof user !== 'undefined') {
        if (settings.rvaType === 'EDU') {
          if (user.isHost) {
            // EDU Moderator_Holder, Moderator_Holder_Viewer_Holder 0 ~ 4
            modePanelData.grayOut = false;
            modePanelData.lightOn = false;
          } else {
            if (user.panel === 0) {
              //EDU Viewer_Holder
              if (settings.isSupportAirPlay) {
                modePanelData.grayOut = false;
                modePanelData.lightOn = false;
              } else if (settings.isSupportMiracast) {
                if (settings.network_mode === 1) {
                  //HotSpot mode
                  modePanelData.grayOut = true;
                } else {
                  modePanelData.grayOut = false;
                  modePanelData.lightOn = false;
                }
              } else {
                modePanelData.grayOut = true;
              }
            } else {
              modePanelData.grayOut = true;
            }
          }
        } else if (settings.rvaType === 'CORP') {
          if (settings.isCorpModeratorOn) {
            if (user.isHost) {
              modePanelData.grayOut = false;
              modePanelData.lightOn = false;
            } else if (user.panel === 0) {
              if (settings.isSupportAirPlay) {
                modePanelData.grayOut = false;
                modePanelData.lightOn = false;
              } else if (settings.isSupportMiracast) {
                if (settings.network_mode === 1) {
                  //HotSpot mode
                  modePanelData.grayOut = true;
                } else {
                  modePanelData.grayOut = false;
                  modePanelData.lightOn = false;
                }
              } else {
                modePanelData.grayOut = true;
              }
            } else {
              modePanelData.grayOut = true;
            }
          } else {
            modePanelData.grayOut = false;
            modePanelData.lightOn = false;
          }
        } else if (settings.rvaType === 'NOVOCAST') {
          if (settings.isSupportMiracast || settings.isSupportAirPlay) {
            modePanelData.grayOut = false;
            modePanelData.lightOn = false;
          } else {
            modePanelData.grayOut = true;
          }
        }
      } else {
        modePanelData.grayOut = false;
        modePanelData.lightOn = false;
      }

      self.emit('remoteStatusUpdate', modePanelData);
      break;
    case common.REMOTE_CONTROL_CMD.NORMAL_MODE_IN_USE:
      logger.debug('##### REMOTE_CONTROL_CMD.NORMAL_MODE_IN_USE');
      if (settings.isSupportMiracast || settings.isSupportAirPlay) {
        modePanelData.grayOut = false;
        modePanelData.lightOn = false;
      } else {
        modePanelData.grayOut = true;
      }
      self.emit('remoteStatusUpdate', modePanelData);
      break;
  }
};

RVAClient.prototype.restoreQLauncherLocation = function restoreQLauncherLocation() {
  settings.isLauncherCMDBack = true;
  var user = userUtil.getUserData(settings.client_id);
  var location = user.panel;
  logger.debug('##### Restore Q Launcher location = ' + location);
  if (user.isHost || (settings.rvaType === 'CORP' && !settings.isCorpModeratorOn)) {
    switch (location) {
      case 0:
        nativeService.ledOff(settings.LED.ALL, function() {
          nativeService.ledOn(settings.LED.FULL_SCREEN, 0);
        });
        break;
      case 1:
        nativeService.ledOff(settings.LED.ALL, function() {
          nativeService.ledOn(settings.LED.SPLIT_1, 0);
        });
        break;
      case 2:
        nativeService.ledOff(settings.LED.ALL, function() {
          nativeService.ledOn(settings.LED.SPLIT_2, 0);
        });
        break;
      case 3:
        nativeService.ledOff(settings.LED.ALL, function() {
          nativeService.ledOn(settings.LED.SPLIT_3, 0);
        });
        break;
      case 4:
        nativeService.ledOff(settings.LED.ALL, function() {
          nativeService.ledOn(settings.LED.SPLIT_4, 0);
        });
        break;
      default:
        nativeService.ledOff(settings.LED.ALL);
    }
  } else {
    nativeService.ledOff(settings.LED.ALL);
  }

};

RVAClient.prototype.setLocalStreamingType = function setLocalStreamingType(type) {
  if (type === 'web') {
    settings.isWebUrlStreaming = true;
  } else {
    settings.isWebUrlStreaming = false;
  }
};

RVAClient.prototype.sendURLSharing = function sendURLSharing(url) {
  logger.debug('##### Send URL = ' + url);
  var overSend = false;
  var length = 0;
  var buf = new Uint8Array(512);
  for (var i = 0; i < 512; i++) {
    buf[i] = 0;
  }
  var data = dsaUtil.utf8ToByteArray(url);
  length = data.length;
  if (length + settings.MINIMUM_MSG_LENGTH >= 127) {
    var data_length = length;
    var extention_length = dsaUtil.convertIntToBytesBigEndian(data_length);
    if (extention_length !== null && extention_length.length === 4) {
      for (var j = 0; j < 4; j++) {
        buf[j + 4] = extention_length[j];
      }
      for (var q = 0; q < length; q++) {
        buf[8 + q] = data[q];
      }
      overSend = true;
      buf[0] = 127;
    }
  } else {
    buf[0] = length + settings.MINIMUM_MSG_LENGTH;
    for (var i = 0; i < length; i++) {
      buf[4 + i] = data[i];
    }
    overSend = false;
  }

  buf[1] = common.CMD.URL_SHARING;
  buf[2] = settings.client_id; //client_id
  buf[3] = 0;

  if (overSend) {
    buf = buf.subarray(0, length + 8);
    logger.debug('##### Send URL SHARING >= 127');
  } else {
    buf = buf.subarray(0, length + 4);
    logger.debug('##### Send URL SHARING < 127');
  }

  var node_buf = new Buffer(buf);
  RVACommandClient.write(node_buf);
};

RVAClient.prototype.receive_CMD_URL_SHARING = function receive_CMD_URL_SHARING(bValues, s_bytes) {
  logger.debug('##### Receive URL SHARING');
  var self = this;
  var length = bValues[s_bytes];
  var send_id = bValues[s_bytes + 2];
  var send_name = userUtil.getSendNameById(send_id);
  var receive_url = '';
  if (length === 127) {
    var sharing_length = dsaUtil.convertBytesBigEndianToInt(bValues[s_bytes + 4], bValues[s_bytes + 5], bValues[s_bytes + 6], bValues[s_bytes + 7]);
    var strBuf = new Uint8Array(sharing_length);
    var j = 0;
    for (var i = 8; i < sharing_length; i++) {
      strBuf[j] = bValues[i];
      j++;
    }
    receive_url = dsaUtil.byteArrayToUtf8(strBuf);
    length = sharing_length + 8;
  } else {
    var j = 0;
    var strBuf = new Uint8Array(length - 4);
    for (var i = 0; i < length - 4; i++) {
      strBuf[j] = bValues[i + 4];
      j++;
    }
    receive_url = dsaUtil.byteArrayToUtf8(strBuf);
  }
  var urlObject = {};
  urlObject.pid = settings.lastPid + 1;
  settings.lastPid = urlObject.pid;
  urlObject.sender = send_name;
  urlObject.url = receive_url;
  var nowTime = new Date();
  urlObject.timestamp = nowTime;
  settings.urlList.push(urlObject);

  var doc = {
    documentType: 'urlHistory',
    urlList: settings.urlList,
  };

  db.update({
    documentType: 'urlHistory'
  }, doc, {
    upsert: true
  }, function(err) {
    if (err !== null) {

    }
  });
  var data = {};
  data.url = receive_url;
  data.sender = send_name;
  self.emit('urlReceived', data);

  return length;
};

//--------------------------------------- annotation ---------------------------------------------------- //
RVAClient.prototype.receive_CMD_BROADCAST_MSG = function receive_CMD_BROADCAST_MSG(bValues, s_bytes) {
  logger.debug('##### Receive BROADCAST MSG');
  //  var self = this;
  var length = bValues[s_bytes];
  //var send_type = bValues[s_bytes + 2]; // 0 sends message to all clients, 1 to all clients including itself
  if (length === 127) {
    var sharing_length = dsaUtil.convertBytesBigEndianToInt(bValues[s_bytes + 4], bValues[s_bytes + 5], bValues[s_bytes + 6], bValues[s_bytes + 7]);
    //    var strBuf = new Uint8Array(sharing_length);
    //    var j = 0;
    //    for (var i = 8; i < sharing_length; i++) {
    //      strBuf[j] = bValues[i];
    //      j++;
    //    }
    //    var receive_url = dsaUtil.byteArrayToUtf8(strBuf);
    //    var data = {};
    //    data.url = receive_url;
    //    data.sender = send_name;
    //    self.emit('urlReceived', data);
    return sharing_length + 8;

  } else {
    //    var j = 0;
    //    var strBuf = new Uint8Array(length - 4);
    //    for (var i = 0; i < length - 4; i++) {
    //      strBuf[j] = bValues[i+4];
    //      j++;
    //    }
    //    var receive_url = dsaUtil.byteArrayToUtf8(strBuf);
    //    var data = {};
    //    data.url = receive_url;
    //    data.sender = send_name;
    //    logger.debug(JSON.stringify(data));
    //    self.emit('urlReceived', data);

    return length;
  }
};

RVAClient.prototype.receive_CMD_RESPONDER = function receive_CMD_RESPONDER( /*bValues*/ ) {
  logger.debug('##### Receive CMD RESPONDER');
};

/*************** 8 bytes fixed(header) message format **************************
 *   byte index:
 *   byte 	0 -3  	four bytes  --- total packet length
 *   byte 	4:  one byte  --- message type:  see CMCommand class
 *   byte 	5:  one byte  --- client ID
 *   byte    6:  one byte  --- arg1: sub-CMD (ANNOTATION_CMD)
 *   byte 	7  	one byte  --- arg2  ERRORCODE if FAIL
 * Note: the field must fill 0 (zero) if it is not applied.
 *****************************************************/
RVAClient.prototype.sendInitialAnnotation = function sendInitialAnnotation(role) {
  logger.debug('send initial annotation');
  var buf = new Buffer(8);
  var messageLength = dsaUtil.convertIntToBytesBigEndian(8);
  for (var i = 0; i < messageLength.length; i++) {
    buf[i] = messageLength[i];
  }
  buf[4] = role; //common.OP_CMD.ANNOTATOR;
  buf[5] = settings.client_id;
  buf[6] = 0;
  buf[7] = 0;
  annotationClient.write(buf);
};

// 3
RVAClient.prototype.receive_CMD_ANNOTATION = function receive_CMD_ANNOTATION(bValues) {
  var self = this;
  logger.debug('##### Receive CMD ANNOTATION: ' + JSON.stringify(bValues));
  // 4
  annotationClient = new net.Socket();
  annotationClient.setNoDelay(true);
  annotationClient.connect(settings.ANNOTATION_PORT, settings.rvaIp, function connect() {
    logger.debug('RVA annotation port connected (receive_CMD_ANNOTATION)');
    // 5
    self.sendInitialAnnotation(common.OP_CMD.PRESENTER);
  });

  annotationClient.on('data', function(data) {
    logger.debug('annotation data received: ' + JSON.stringify(data));
  });

  annotationClient.on('error', function(error) {
    logger.error('annotation error received: ' + JSON.stringify(error));
  });

};

// 2
RVAClient.prototype.startAnnotation = function startAnnotation() {
  logger.debug('Start annotation');
  var self = this;
  var buf = new Buffer(settings.MINIMUM_MSG_LENGTH);
  buf[0] = settings.MINIMUM_MSG_LENGTH;
  buf[1] = common.CMD.ANNOTATION; // message type
  buf[2] = 0;
  buf[3] = 0;
  RVACommandClient.write(buf);

  // 4
  annotationClient = new net.Socket();
  annotationClient.setNoDelay(true);
  annotationClient.connect(settings.ANNOTATION_PORT, settings.rvaIp, function connect() {
    logger.debug('RVA annotation port connected (startAnnotation)');
    // 5
    self.sendInitialAnnotation(common.OP_CMD.ANNOTATOR);
  });

  annotationClient.on('data', function(data) {
    logger.debug('annotation data received: ' + JSON.stringify(data));
  });

  annotationClient.on('error', function(error) {
    logger.error('annotation error received: ' + JSON.stringify(error));
  });
};

/*************** ANNOTATION message (For RVA) format *************
 *   byte index:
 *   byte 	0 -3  	four bytes  --- total packet length (8 bytes)
 *   byte 	4:  one byte  --- message type:  ANNOTATION
 *   byte 	5:  one byte  --- client ID
 *   byte    6:  one byte  --- arg1: OP_CMD.SCREEN_IMG (see OP_CMD class)
 *   byte 	7  	one byte  --- arg2  N/A
 *   byte 	8 - N  image file bytes stream up to 1024x1024 bytes
 * Note: the field must fill 0 (zero) if it is not applied.
 *****************************************************/
RVAClient.prototype.sendAnnotationScreenImage = function sendAnnotationScreenImage(imageData) {
  logger.debug('send annotation image, image size: ' + imageData.length);
  if (imageData.length > (1024 * 1024)) {
    logger.debug('image size is too big, it must be smaller than 1024 * 1024');
  } else {
    var packetLength = 8 + imageData.length;
    var buf = new Buffer(packetLength);
    var messageLength = dsaUtil.convertIntToBytesBigEndian(packetLength);
    for (var i = 0; i < messageLength.length; i++) {
      buf[i] = messageLength[i];
    }

    buf[4] = common.CMD.ANNOTATION;
    buf[5] = settings.client_id;
    buf[6] = common.OP_CMD.SCREEN_IMG;
    buf[7] = 0;

    for (var i = 8; i < imageData.length; i++) {
      buf[i] = imageData[i];
    }
    annotationClient.write(buf);
  }
};

//--------------------------------------- end annotation ---------------------------------------------------- //

RVAClient.prototype.getURLHistory = function getURLHistory() {
  return settings.urlList;
};

RVAClient.prototype.removeURLHistory = function removeURLHistory(pid) {
  //var self = this;
  var url_list = settings.urlList;
  if (pid === -1) {
    url_list = [];
  } else {
    for (var i = 0; i < url_list.length; i++) {
      var remove_data = url_list[i];
      if (remove_data.pid === pid) {
        var r = url_list.indexOf(remove_data);
        if (r !== -1) {
          url_list.splice(r, 1);
          break;
        }
      }
    }
  }
  settings.urlList = url_list;
  var doc = {
    documentType: 'urlHistory',
    urlList: settings.urlList,
  };
  db.update({
    documentType: 'urlHistory'
  }, doc, {
    upsert: true
  }, function(err) {
    if (err !== null) {
      logger.debug(JSON.stringify(err));
    }
  });
};

RVAClient.prototype.extendedScreenSupport = function extendedScreenSupport(cb) {
  nativeService.getExtendedScreenSupport(function(extendedScreenSupport) {
    logger.debug('ExtendedScreenSupport: ' + extendedScreenSupport);
    cb(extendedScreenSupport);
  });
};

RVAClient.prototype.changeSplitModeFramerate = function changeSplitModeFramerate(callback) {
  var rvaNumber = parseFloat(settings.rva_version.replace('v', ''));
  if (rvaNumber > 1.6) {
    if (settings.client_id === 0) {
      callback('400');
    } else {
      var mydata = userUtil.getUserData(settings.client_id);
      if (mydata.panel > 0) {
        logger.debug('User is playing splitmode');
        var rate = userUtil.getSplitModeRate();
        logger.debug('Now rate = ' + rate);
        nativeService.setSplitFrameRate(rate, function(cb) {
          if (cb) {
            callback('200');
          }
        });
      } else {
        callback('400');
      }
    }
  } else {
    callback('400');
  }
};
var nextCommand = [];
var nextCmd;
var addTime = false;
RVAClient.prototype.sendNextLauncherCommand = function sendNextLauncherCommand() {
  var self = this;
  setTimeout(function() {
    if (nextCommand.length === 0) {
      logger.debug("YYYYY No next Command");
    } else {
      logger.debug("YYYYY Command List Before = " + nextCommand);
      nextCommand.splice(0, nextCommand.length - 1);
      logger.debug("YYYYY Next CMD = " + nextCommand[0]);
      nextCmd = nextCommand[0];
      if (typeof nextCmd !== 'undefined') {
        settings.isLauncherCMDBack = false;
        switch (nextCmd) {
          case 0:
            nextCommand.shift();
            nativeService.ledOff(settings.LED.ALL, function() {
              nativeService.ledGradientFlash(settings.LED.FULL_SCREEN_BLUE, 60, 500);
            });
            if (addTime) {
              setTimeout(function() {
                logger.debug("YYYYY MODE SWITCH 0 + addTime");
                addTime = false;
                self.emit('modeSwitch', 0);
              }, 1500);
            } else {
              logger.debug("YYYYY MODE SWITCH 0");
              self.emit('modeSwitch', 0);
            }

            break;
          case 1:
            nextCommand.shift();
            nativeService.ledOff(settings.LED.ALL, function() {
              nativeService.ledGradientFlash(settings.LED.SPLIT_1, 60, 500);
            });
            if (addTime) {
              setTimeout(function() {
                logger.debug("YYYYY MODE SWITCH 1 + addTime");
                addTime = false;
                self.emit('modeSwitch', 1);
              }, 1500);
            } else {
              logger.debug("YYYYY MODE SWITCH 1");
              self.emit('modeSwitch', 1);
            }

            break;
          case 2:
            nextCommand.shift();
            nativeService.ledOff(settings.LED.ALL, function() {
              nativeService.ledGradientFlash(settings.LED.SPLIT_2, 60, 500);
            });
            if (addTime) {
              setTimeout(function() {
                logger.debug("YYYYY MODE SWITCH 2 + addTime");
                addTime = false;
                self.emit('modeSwitch', 2);
              }, 1500);
            } else {
              logger.debug("YYYYY MODE SWITCH 2");
              self.emit('modeSwitch', 2);
            }

            break;
          case 3:
            nextCommand.shift();
            nativeService.ledOff(settings.LED.ALL, function() {
              nativeService.ledGradientFlash(settings.LED.SPLIT_3, 60, 500);
            });
            if (addTime) {
              setTimeout(function() {
                logger.debug("YYYYY MODE SWITCH 3 + addTime");
                addTime = false;
                self.emit('modeSwitch', 3);
              }, 1500);
            } else {
              logger.debug("YYYYY MODE SWITCH 3");
              self.emit('modeSwitch', 3);
            }

            break;
          case 4:
            nextCommand.shift();
            nativeService.ledOff(settings.LED.ALL, function() {
              nativeService.ledGradientFlash(settings.LED.SPLIT_4, 60, 500);
            });
            if (addTime) {
              setTimeout(function() {
                logger.debug("YYYYY MODE SWITCH 4 + addTime");
                addTime = false;
                self.emit('modeSwitch', 4);
              }, 1500);
            } else {
              logger.debug("YYYYY MODE SWITCH 4");
              self.emit('modeSwitch', 4);
            }

            break;
        };
        logger.debug("##### Command List After = " + nextCommand);
      }
    }
  }, 250);

};

// ====================================================== Luke's Code END
exports.RVAClient = RVAClient;
exports.nativeService = nativeService;
exports.settings = settings;
exports.RVADiscovery = rvaDiscoveryClient;
exports.lastSession = rvaDiscovery.lastSession;
exports.dataPath = settings.dataPath;
exports.db = db;
exports.db2 = db2;
