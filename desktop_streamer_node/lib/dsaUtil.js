/* jshint -W079 */
'use strict';
var settings = require('./settings.js'),
  request = require('request'),
  _ = require('lodash'),
  logger = settings.logger,
  btoa = require('btoa');

/**
 * Convert Integer to Big Endian Value
 * @param intValue
 * @returns {Int8Array}
 */
exports.convertIntToBytesBigEndian = function convertIntToBytesBigEndian(intValue) {
  var result = new Int8Array(4);
  result[3] = 0xff & intValue;
  result[2] = 0xff & (intValue >> 8);
  result[1] = 0xff & (intValue >> 16);
  result[0] = 0xff & (intValue >> 24);
  return result;
};

function checkOffset(offset, ext, length) {
  if (offset + ext > length) {
    throw new RangeError('index out of range');
  }
}

exports.readUIntBE = function(data, offset, byteLength, noAssert) {
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;
  if (!noAssert) {
    checkOffset(offset, byteLength, data.length);
  }

  var val = data[offset + --byteLength],
    mul = 1;
  while (byteLength > 0 && (mul *= 0x100)) {
    val += data[offset + --byteLength] * mul;
  }

  return val;
};

exports.writeUIntBE = function(data, value, offset, byteLength) {
  value = +value;
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;
  //  if (!noAssert)
  //    checkInt(this, value, offset, byteLength, Math.pow(2, 8 * byteLength), 0);

  var i = byteLength - 1,
    mul = 1;
  data[offset + i] = value;
  while (--i >= 0 && (mul *= 0x100)) {
    data[offset + i] = (value / mul) >>> 0;
  }

  return data;
  //return offset + byteLength;
};

/**
 * Convert 4Bytes Big Endian arrays to Integer
 * @param byte1
 * @param byte2
 * @param byte3
 * @param byte4
 * @returns
 */
exports.convertBytesBigEndianToInt = function convertBytesBigEndianToInt(byte1, byte2, byte3, byte4) {
  var firstByte = 0xff & byte1,
    secondByte = 0xff & byte2,
    thirdByte = 0xff & byte3,
    fourthByte = 0xff & byte4,
    result = ((firstByte << 24) | (secondByte << 16) | (thirdByte << 8) | fourthByte) & 0xFFFFFFFF;
  return result;
};

/**
 * Covert String to bytes arrays
 * @param str
 * @returns {Array}
 */
exports.convertStringToBytes = function convertStringToBytes(str) {
  var bytes = [],
    i = 0;
  for (i = 0; i < str.length; ++i) {
    bytes.push(str.charCodeAt(i));
  }

  return bytes;
};

/**
 * Covert String to bytes arrays
 * @param array
 * @returns {String}
 */
exports.convertByteArrayToString = function convertByteArrayToString(array) {
  var result = '',
    i = 0;
  for (i = 0; i < array.length; i++) {
    result += String.fromCharCode(array[i]);
  }

  return result;
};

/**
 * Conver String to bytes arrays with UTF-8 Decode
 * @param arrayBuffer
 * @returns {String}
 */
exports.convertByteArrayToStringUtf8 = function convertByteArrayToStringUtf8(arrayBuffer) {
  var result = '',
    i = 0,
    c = 0,
    c2 = 0,
    c3 = 0,
    data = new Uint8Array(arrayBuffer);

  // If we have a BOM skip it
  if (data.length >= 3 && data[0] === 0xef && data[1] === 0xbb && data[2] === 0xbf) {
    i = 3;
  }

  while (i < data.length) {
    c = data[i];

    if (c < 128) {
      result += String.fromCharCode(c);
      i++;
    } else if (c > 191 && c < 224) {
      if (i + 1 >= data.length) {
        throw 'UTF-8 Decode failed. Two byte character was truncated.';
      }
      c2 = data[i + 1];
      result += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
      i += 2;
    } else {
      if (i + 2 >= data.length) {
        throw 'UTF-8 Decode failed. Multi byte character was truncated.';
      }
      c2 = data[i + 1];
      c3 = data[i + 2];
      result += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      i += 3;
    }
  }
  return result;
};

exports.utf8ToByteArray = function(str) {
  var byteArray = [],
    i = 0,
    j = 0,
    h = '';
  for (i = 0; i < str.length; i++) {
    if (str.charCodeAt(i) <= 0x7F) {
      byteArray.push(str.charCodeAt(i));
    } else {
      h = encodeURIComponent(str.charAt(i)).substr(1).split('%');
      for (j = 0; j < h.length; j++) {
        byteArray.push(parseInt(h[j], 16));
      }
    }
  }
  return byteArray;
};

exports.byteArrayToUtf8 = function(byteArray) {
  var str = '',
    i = 0;
  for (i = 0; i < byteArray.length; i++) {
    str += byteArray[i] <= 0x7F ?
      byteArray[i] === 0x25 ? '%25' : // %
      String.fromCharCode(byteArray[i]) :
      '%' + byteArray[i].toString(16).toUpperCase();
  }

  return decodeURIComponent(str);
};

/**
 * Covert array bytes to base64
 * @param bytes
 * @returns
 */
exports.covertArrayToBase64 = function covertArrayToBase64(bytes) {
  var binary = '',
    len = bytes.byteLength,
    i = 0;
  for (i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return btoa(binary);
};

/**
 * Get Youtube URL
 * @param url
 * @returns
 */
exports.getYoutubeId = function getYoutubeId(url) {
  var result = url.replace('https://www.youtube.com/watch?v=', '').replace(/&.*/g, '');
  return result;
};

function replaceAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace);
}

exports.getYoutubeTitleById = function getYoutubeTitleById(videoID, returnTitle) {
  var arr;
  request.get('https://download.launchnovo.com/api/youtube/decode', function(error, response, body) {
    var obj = JSON.parse(body);
    request.get('http://www.youtube.com/get_video_info?eurl=http://kej.tw/&sts=' + obj.sts + '&video_id=' + videoID, function(_err, _response, _body) {
      var title = 'YouTube Video';
      if (!_err && _response.statusCode === 200) {
        arr = _body.split('&');
        _.forEach(arr, function(n) {
          if (n.indexOf('title=') > -1) {
            var vArr = n.split('=');
            if (vArr.length >= 2) {
              title = replaceAll('\\+', ' ', decodeURIComponent(vArr[1]));
            } else {
              logger.debug('title not found in the title array');
            }
          }
        });
        returnTitle(title);
      }
    });
  });
};

exports.validateYouTubeID = function validateYouTubeID(vid, callback) {
  logger.debug('dsaUtil validateYouTubeID');
  request.get('http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=' + vid + '&format=json', function(error, response, body) {
    var result;
    if (response.statusCode === 404) {
      result = false;
    } else {
      if (error) {
        logger.debug('validateYouTubeID response:' + response.statusCode + 'validateYouTubeID body:' + body);
      }
      result = true;
    }
    logger.debug('validateYouTubeID result is ' + result);
    callback(result);

  });
};

/**
 * Creates a new Uint8Array based on two different ArrayBuffers
 *
 * @private
 * @param {ArrayBuffers} buffer1 The first buffer.
 * @param {ArrayBuffers} buffer2 The second buffer.
 * @return {ArrayBuffers} The new ArrayBuffer created out of the two.
 */
exports.appendBuffer = function(buffer1, buffer2) {
  var tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
  tmp.set(new Uint8Array(buffer1), 0);
  tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
  return tmp.buffer;
};

exports.lengthInUtf8Bytes = function lengthInUtf8Bytes(str) {
  // Matches only the 10.. bytes that are non-initial characters in a multi-byte sequence.
  var m = encodeURIComponent(str).match(/%[89ABab]/g);
  return str.length + (m ? m.length : 0);
};

// =================================== Luke's Code Start ================================ //
exports.parseVotingAnswerType = function(type) {
  var result = '';
  switch (type) {
    case 1:
      result = 'TRUE';
      break;
    case 2:
      result = 'FALSE';
      break;
    case 5:
      result = 'A';
      break;
    case 6:
      result = 'B';
      break;
    case 7:
      result = 'C';
      break;
    case 8:
      result = 'D';
      break;
    case 9:
      result = 'E';
      break;
  }
  return result;
};

exports.getFilenameByCurrentTime = function() {
  var thisTime = new Date(),
    month, date, hour, minute, second;
  if (thisTime.getMonth() + 1 < 10) {
    month = '0' + (thisTime.getMonth() + 1);
  } else {
    month = thisTime.getMonth() + 1;
  }
  if (thisTime.getDate() < 10) {
    date = '0' + (thisTime.getDate());
  } else {
    date = thisTime.getDate();
  }
  if (thisTime.getHours() < 10) {
    hour = '0' + (thisTime.getHours());
  } else {
    hour = thisTime.getHours();
  }
  if (thisTime.getMinutes() < 10) {
    minute = '0' + (thisTime.getMinutes());
  } else {
    minute = thisTime.getMinutes();
  }
  if (thisTime.getSeconds() < 10) {
    second = '0' + (thisTime.getSeconds());
  } else {
    second = thisTime.getSeconds();
  }

  return thisTime.getFullYear() + '-' + month + '-' + date + '_' + hour + '-' + minute + '-' + second + '.jpg';
};

function toUTF8Array(str) {
  var utf8 = [],
    i = 0,
    charcode = '';
  for (i = 0; i < str.length; i++) {
    charcode = str.charCodeAt(i);
    if (charcode < 0x80) {
      utf8.push(charcode);
    } else if (charcode < 0x800) {
      utf8.push(0xc0 | (charcode >> 6),
        0x80 | (charcode & 0x3f));
    } else if (charcode < 0xd800 || charcode >= 0xe000) {
      utf8.push(0xe0 | (charcode >> 12),
        0x80 | ((charcode >> 6) & 0x3f),
        0x80 | (charcode & 0x3f));
    } else {
      i++;
      // UTF-16 encodes 0x10000-0x10FFFF by
      // subtracting 0x10000 and splitting the
      // 20 bits of 0x0-0xFFFFF into two halves
      charcode = 0x10000 + (((charcode & 0x3ff) << 10) | (str.charCodeAt(i) & 0x3ff));
      utf8.push(0xf0 | (charcode >> 18),
        0x80 | ((charcode >> 12) & 0x3f),
        0x80 | ((charcode >> 6) & 0x3f),
        0x80 | (charcode & 0x3f));
    }
  }
  return utf8;
}

exports.getCorporationKey = function(ip_address, ssid, pin) {
  var buf = new Int8Array(16),
    ip_data = '',
    length = 0,
    i = 0,
    j = 0,
    k = 0,
    ssid_data = '',
    pin_data = '';
  if (ip_address.length > 0) {
    ip_data = toUTF8Array(ip_address);
    length = ip_data.length;
    if (ip_data.length > 16) {
      length = 16;
    }
    for (i = 0; i < length; i++) {
      buf[i] = ip_data[i];
    }
  }

  if (ssid.length > 0) {
    ssid_data = toUTF8Array(ssid);
    length = ssid_data.length;
    if (ssid_data.length > 16) {
      length = 16;
    }
    for (k = 0; k < length; k++) {
      buf[15 - k] = (ssid_data[k] + buf[k]) >> 1;
    }
  }

  if (pin.length > 0) {
    pin_data = toUTF8Array(pin);
    length = pin_data.length;
    if (length > 4) {
      length = 4;
    }
    for (j = 0; j < length; j++) {
      buf[j * 4] = (pin_data[j] + buf[j * 4]) >> 1;
    }
  }

  return buf;
};

exports.endsWith = function endsWith(str, suffix) {
  return str.indexOf(suffix, str.length - suffix.length) !== -1;
};

exports.getGoogleDriveURL = function getGooglDrvieURL(url) {
  var result = '',
    end = 0;
  if (url.substring(0, 33) === 'https://drive.google.com/open?id=') {
    result = url.substring(33, url.length);
  } else if (url.substring(0, 32) === 'https://drive.google.com/file/d/') {
    end = url.indexOf('/view');
    result = url.substring(32, end);
  }
  return 'https://drive.google.com/uc?id=' + result + '&export=open';
};

exports.getDropboxURL = function getDropboxURL(url) {
  return url.replace('dl=0', 'dl=1');
};
// =================================== Luke's Code End ================================ //
