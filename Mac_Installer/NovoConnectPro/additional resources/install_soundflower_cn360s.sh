#!/bin/sh

os_version=$(/usr/bin/sw_vers -productVersion)
echo "version:" $os_version;

#App Nap disabled
cp com.yourcompany.NovoConnect\ Desktop\ Streamer.plist ~/Library/Preferences/

# copy license.app to tmp directory
cp license.app "/tmp"

/usr/bin/hdiutil attach Soundflower-1.6.6b.dmg
#cd /Volumes/Soundflower-1.6.6/
#ls -al

osascript Uninstall\ Soundflower.scpt
os_version=$(/usr/bin/sw_vers -productVersion)

if [ "$os_version" > 10.7 ]; then
    echo "OS X 10.0 and later"
    cd /Volumes/Soundflower-1.6.6/
    sudo /usr/sbin/installer -allowUntrusted -pkg Soundflower.pkg -target /
else
    echo "OS X 10.6.x"
    cd /Volumes/Soundflower-1.6.6/
    sudo /usr/sbin/installer -allow -pkg Soundflower.pkg -target /
fi

# generate license file
cd /tmp
./license.app HDD
cp "license.lic" ~/Library/Preferences/

#cd /Volumes/

#/usr/bin/hdiutil detach /Volumes/Soundflower-1.6.6/