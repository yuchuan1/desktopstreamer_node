#!/bin/sh

 os_version=$(/usr/bin/sw_vers -productVersion)
 echo "version:" $os_version;

 #App Nap disabled
 #cp com.yourcompany.NovoConnect\ Desktop\ Streamer.plist ~/Library/Preferences/
 cp com.yourcompany.Novo\ Desktop\ Streamer.plist ~/Library/Preferences/

 # copy license.app to tmp directory
 cp license.app "/tmp"

 /usr/bin/hdiutil attach Soundflower-2.0b2.dmg
 cd /Volumes/Soundflower-2.0b2/
 osascript Uninstall\ Soundflower.scpt
 os_version=$(/usr/bin/sw_vers -productVersion)

 #if [ "$os_version" > 10.7 ]; then
 	#echo "OS X 10.7 and later"
    #cd /Volumes/Soundflower-2.0b2/
    #sudo /usr/sbin/installer -allowUntrusted -pkg Soundflower.pkg -target /
 #else
 	#echo "OS X 10.6.x"
    #cd /Volumes/Soundflower-1.6.6/
    sudo /usr/sbin/installer -allow -pkg Soundflower.pkg -target /
 #fi

# preparing to generate license file
#test -d ~/AppData/ || mkdir ~/AppData/
#test -d ~/AppData/Local/ || mkdir ~/AppData/Local/
#test -d ~/AppData/Local/Mac_Resource/ || mkdir ~/AppData/Local/Mac_Resource/

# generate license file
cd /tmp
./license.app HDD
cp "license.lic" ~/Library/Preferences/

sudo chmod -R 777 /Applications/Novo\ Desktop\ Streamer.app

defaults write com.apple.dock ResetLaunchPad -bool true; killall Dock
