#!/bin/bash

DSA_Version=$1
if [ "$DSA_Version" == "" ]; then
	echo "command:"
	echo "GenerateVersion.sh DSA_Version SVN_Revision"
	exit -1
fi

SVN_Revision=$2
if [ "$SVN_Revision" == "" ]; then
	echo "command:"
	echo "GenerateVersion.sh DSA_Version SVN_Revision"
	exit -1
fi


file="../DSMainApp/utilities/CApplicationVersion.h"
touch $file

echo DSA_Version=$DSA_Version

echo SVN_Revision=$SVN_Revision


echo "#ifndef CAPPLICATION_VERSION_H" > $file
echo "#define CAPPLICATION_VERSION_H" >> $file
echo  >> $file
echo '#include "qglobal.h"' >> $file
echo  >> $file
echo '#define TEXT_DEFAULT_START_VER_NUMBER       "'$DSA_Version'"'>> $file
echo '#define TEXT_CURRENT_VERSION_NUMBER         "'$DSA_Version.$SVN_Revision'"'>> $file
echo '#define TEXT_CURRENT_VERSION_NUMBER_TEST    "'$DSA_Version.$SVN_Revision-Test'"'>> $file
echo  >> $file
echo '#endif // CAPPLICATION_VERSION_H' >> $file
   
exit 0
