#!/bin/sh

#  auto_build.sh
#
#
#  Created by Asurada_MacBook on 2/10/14.
#
   
MAC_APP_EXT='app'
ZIP_FILE_EXT='zip'
SH_FILE_EXT='sh'
LIB_FILE_EXT='dylib'
LIB_FILE_NAME='libnvc_crypto'
PKG_PROJ_FILE_EXT='pkgproj'
PKG_FILE_EXT='mpkg'
INSTALLER_DIR='../Mac_Installer'

DSA_APP_FOLDER='../build/darwin/hdd'

INSTALLER_NAME='exe_installer'
PACKAGE_NAME='NovoConnect_jenkins_local'
LAUNCH_APP_NAME='Novo Desktop Streamer'
PKG_OUT_NAME='DesktopStreamer'
NODE_APP_NAME='DesktopStreamer'


DSA_Version=$1
if [ "$DSA_Version" == "" ]; then
	echo "command:"
	echo "auto_build.sh DSA_Version SVN_Revision BUILD_TYPE=server_build | local_build BUILD_NUMBER"
	exit -1
fi

SVN_Revision=$2
if [ "$SVN_Revision" == "" ]; then
	echo "command:"
	echo "auto_build.sh DSA_Version SVN_Revision BUILD_TYPE=server_build | local_build BUILD_NUMBER"
	exit -1
fi
echo $SVN_Revision


BUILD_NUMBER=$4
if [ "$BUILD_NUMBER" == "" ]; then
	echo "command:"
	echo "auto_build.sh DSA_Version SVN_Revision BUILD_NUMBER BUILD_TYPE=server_build | local_build BUILD_NUMBER"
	exit -1
fi
echo $BUILD_NO

BUILD_TYPE=$3
if [ "$BUILD_TYPE" == "" ]; then
	echo "command:"
	echo "auto_build.sh DSA_Version SVN_Revision BUILD_TYPE=server_build | local_build BUILD_NUMBER"
	exit -1
fi
echo $BUILD_TYPE





# remove old app and clean old object files
#rm -rf $DSA_APP_FOLDER
#mkdir $DSA_APP_FOLDER

# build node.js application
#
#
#

# copy DesktopStreamer.app to Installer folder
#echo "copy Novo Desktop Streamer.app from " "$DSA_APP_FOLDER/$MAC_APP_NAME.$MAC_APP_EXT" " to " "$INSTALLER_DIR"
#rm -r "$INSTALLER_DIR/$MAC_APP_NAME.$MAC_APP_EXT"
#cp -r "$DSA_APP_FOLDER/$MAC_APP_NAME.$MAC_APP_EXT" "$INSTALLER_DIR"


#build Novo Desktop Streamer
LAUNCH_PROJ_DIR='Novo Desktop Streamer'

echo "change directory to Novo Desktop Streamer"
cd "../../$LAUNCH_PROJ_DIR"

# copy DesktopStreamer.app into Novo Desktop Streamer.app folder
test -e "Novo Desktop Streamer/Resources/DesktopStreamer.app" && rm -r "Novo Desktop Streamer/Resources/DesktopStreamer.app"
echo "copy DesktopStreamer.app from " "$DSA_APP_FOLDER/$NODE_APP_NAME.$MAC_APP_EXT" " to " "$LAUNCH_PROJ_DIR"
cp -r "$DSA_APP_FOLDER/$NODE_APP_NAME.$MAC_APP_EXT" "$LAUNCH_APP_NAME/Resources/"


#cd "$IF_PROJ_DIR"
xcrun agvtool new-version -all "$DSA_Version"
xcrun agvtool new-marketing-version "$DSA_Version"
/usr/bin/xcodebuild -target "$LAUNCH_PROJ_DIR" clean
/usr/bin/xcodebuild -target "$LAUNCH_PROJ_DIR"

#check return error code
LAUNCH_BUILD_STATUS=$?
if [ $LAUNCH_BUILD_STATUS -ne 0 ]; then
	echo "*****************************************************"
	echo "Novo Desktop Streamer build fail"
	echo "*****************************************************"
	exit $LAUNCH_BUILD_STATUS
fi

cp -r "build/Release/$LAUNCH_APP_NAME.$MAC_APP_EXT" "../desktopstreamer_node/Mac_Installer"


# copy an installation shell script to DSA
#echo "copy upgrade shell script from " "$INSTALLER_DIR/$INSTALLER_NAME.$SH_FILE_EXT" "to" "$INSTALLER_DIR/$MAC_APP_NAME.$MAC_APP_EXT/Contents/MacOS/"
#cp -r "$INSTALLER_DIR/$INSTALLER_NAME.$SH_FILE_EXT" "$INSTALLER_DIR/$MAC_APP_NAME.$MAC_APP_EXT/Contents/MacOS/"


# package the DSA.app
echo "change directory to" "$INSTALLER_DIR/NovoConnectPro" "and launch packages"
cd "../desktopstreamer_node/Mac_Installer/$INSTALLER_DIR/NovoConnectPro"

/usr/local/bin/packagesbuild "$PACKAGE_NAME.$PKG_PROJ_FILE_EXT"

# zip
PACKAGES_BUILD_FOLDER='build'

echo "change directory to" "$INSTALLER_DIR/NovoConnectPro/build" 

cd "$PACKAGES_BUILD_FOLDER"

echo "generate" "$PKG_OUT_NAME.$ZIP_FILE_EXT"
#rm "$MAC_APP_NAME.$ZIP_FILE_EXT"
/usr/bin/zip -r "$PKG_OUT_NAME.$ZIP_FILE_EXT" "$PKG_OUT_NAME.$PKG_FILE_EXT"
rm -rf "../../$INSTALLER_DIR/$PKG_OUT_NAME.$PKG_FILE_EXT"
mv "$PKG_OUT_NAME.$PKG_FILE_EXT" "../../$INSTALLER_DIR/"
mv "$PKG_OUT_NAME.$ZIP_FILE_EXT" "$PKG_OUT_NAME.$DSA_Version.$SVN_Revision.$BUILD_NUMBER.zip"
echo "$PKG_OUT_NAME.$DSA_Version.$SVN_Revision.$BUILD_NUMBER.zip"

FTP_APP_NAME='NovoDesktopStreamer'
cd "../../$INSTALLER_DIR"
/usr/bin/zip -r "$FTP_APP_NAME.$MAC_APP_EXT.zip" "$LAUNCH_APP_NAME.$MAC_APP_EXT" 


BUILD_TYPE=$3
FTP_PATH=''

if [ "$BUILD_TYPE" == "server_build" ]; then 
	echo "DSA2 server_build"
	# upgrade to ftp server
	DATE=$(date +%Y%m%d)    
	echo $DATE
	FTP_PATH='ESS/EngineeringReleases/NovoPro/0_release_temp'
	#FTP_PATH='ESS/EngineeringReleases/NovoPro'
	echo $FTP_PATH 
	#echo "$PKG_OUT_NAME.$DSA_Version.$SVN_Revision.$BUILD_NUMBER.zip"
	echo "$FTP_APP_NAME.$MAC_APP_EXT.zip"
	#../../ftp_upload.sh $FTP_PATH $DATE "$PKG_OUT_NAME.$DSA_Version.$SVN_Revision.$BUILD_NUMBER.zip"
	./ftp_upload.sh $FTP_PATH $DATE "$FTP_APP_NAME.$MAC_APP_EXT.zip"
else
	#BUILD_TYPE='default'
	echo "local build DSA"
	# upgrade to ftp server
	DATE=$(date +%Y%m%d)    
	echo $DATE
	FTP_PATH='ESS/EngineeringReleases/NovoPro/local_build'
	echo $FTP_PATH 
	echo "$FTP_APP_NAME.$MAC_APP_EXT.zip"
	#./ftp_upload.sh $FTP_PATH $DATE "$FTP_APP_NAME.$MAC_APP_EXT.zip"
fi 

