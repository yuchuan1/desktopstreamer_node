/* jshint -W079 */
/* jshint -W109 */
'use strict';
var nodemailer = require('nodemailer'),
  smtpTransport = require('nodemailer-smtp-transport'),
//  moment = require('moment'),
  transporter = nodemailer.createTransport(smtpTransport({
    host: 'TWTPECAS01.delta.corp',
    port: 25,
    tls: {rejectUnauthorized: false}
  }));

/* only send out email when it is Thursday
if (moment().format('d') === 4) {
  transporter.sendMail({
    from: 'eddie.chen@deltaww.com',
    to: 'eddie.chen@deltaww.com, molly.h@deltaww.com, luke.yang@deltaww.com, zoe.yr.chen@deltaww.com, daisy.t.tai@deltaww.com, ivan.shih@deltaww.com, jim.lin@deltaww.com, SIVIA.LO@deltaww.com, James.Lu@deltaww.com, Botao.Lin@deltaww.com, sasa.hsu@deltaww.com, Kevin.K.Wang@deltaww.com',
    subject: 'DSA2 Engineering Release Notice - ' + 'dsa2_setup_' + process.platform + '_' + process.env.SVN_REVISION + '_' + process.env.BUILD_NUMBER + '.exe',
    text: 'hello',
    html: "Hi, <br>A new DSA2 engineering release is available. <br><br><a href='http://10.120.136.134:8080/job/DSA2/" + process.env.BUILD_NUMBER + "/artifact/build/win32/'>PC HDD</a><br><br><a href='http://172.16.5.221:8080/job/dsa2/'>MAC HDD</a><br><br><a href='http://10.120.136.134:8080/job/DSA2/" + process.env.BUILD_NUMBER + "/artifact/build/usb/'>MAC and PC USB</a><br><br>For PC HDD <br> run setup.exe <br><br> for PC USB <br> Please extract content of the zip file to the root folder of your USB disc and run DesktopStreamerPro.exe <br><br> Thanks <br> Eddie"
  }, function (error, info) {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: ' + info.response);
  });
} else {*/
  //console.log("Today is not Thursday, I ain't sending out emails to all users!!");
  transporter.sendMail({
    from: 'eddie.chen@deltaww.com',
    to: 'eddie.chen@deltaww.com, molly.h@deltaww.com, luke.yang@deltaww.com, vince.lo@deltaww.com',
    subject: 'DSA2 build Notice - ' + "dsa2_setup_" + process.platform + '_' + process.env.SVN_REVISION + "_" + process.env.BUILD_NUMBER + ".exe",
    text: 'hello',
    html: "Hi, <br>A new DSA2 engineering release is available. <br><br><a href='http://10.120.136.134:8080/job/DSA2/lastSuccessfulBuild/artifact/build/win32/'>PC HDD</a><br><br><a href='http://172.16.5.221:8080/job/dsa2/'>MAC HDD</a><br><br><a href='http://10.120.136.134:8080/job/DSA2/lastSuccessfulBuild/artifact/build/usb/'>MAC and PC USB</a><br><br>For PC HDD <br> run setup.exe <br><br> for PC USB <br> Please extract content of the zip file to the root folder of your USB disc and run DesktopStreamerPro.exe <br><br> Thanks <br> Eddie"
  }, function (error, info) {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: ' + info.response);
  });
//}
//eddie.chen@deltaww.com; molly.h@deltaww.com; luke.yang@deltaww.com; zoe.yr.chen@deltaww.com; daisy.t.tai@deltaww.com; ivan.shih@deltaww.com; turbine.lu@deltaww.com; jim.lin@deltaww.com; TERENCE.CHENG@deltaww.com; James.Lu@delta-corp.com; Botao.Lin@delta-corp.com; sasa.hsu@deltaww.com; Kevin.K.Wang@delta-corp.com
