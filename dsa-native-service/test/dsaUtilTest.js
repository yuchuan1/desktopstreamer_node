var chai = require('chai'),
  assert = chai.assert,
  expect = chai.expect,
  dsaUtil = require('../dsaUtil.js');

describe('dsaUtil', function () {
  describe('utf8ToByteArray()', function () {
    it('should return a byteArray representing the UTF8 string', function () {
      var testString = 'this is a test!';
      var expected = [116, 104, 105, 115, 32, 105, 115, 32, 97, 32, 116, 101, 115, 116, 33];
      var actual = dsaUtil.utf8ToByteArray(testString);
      expect(actual).to.be.an('array');
      expect(actual).to.eql(expected);
    })
  });

  describe('utf8ToByteArray()', function () {
    it('should return a byteArray representing the UTF8 string in Chinese', function () {
      var testString = '中文測試!';
      var expected = [228, 184, 173, 230, 150, 135, 230, 184, 172, 232, 169, 166, 33];
      var actual = dsaUtil.utf8ToByteArray(testString);
      expect(actual).to.be.an('array');
      expect(actual).to.eql(expected);
    })
  });

  describe('byteArrayToUtf8()', function () {
    it('should return an UTF8 string', function () {
      var testArray = [116, 104, 105, 115, 32, 105, 115, 32, 97, 32, 116, 101, 115, 116, 33];
      var expected = 'this is a test!';
      var actual = dsaUtil.byteArrayToUtf8(testArray);
      expect(actual).to.be.a('string');
      expect(actual).to.eql(expected);
    })
  });

  describe('byteArrayToUtf8()', function () {
    it('should return an UTF8 string in Chinese', function () {
      var testArray = [228, 184, 173, 230, 150, 135, 230, 184, 172, 232, 169, 166, 33];
      var expected = '中文測試!';
      var actual = dsaUtil.byteArrayToUtf8(testArray);
      expect(actual).to.be.a('string');
      expect(actual).to.eql(expected);
    })
  });

  describe('readUIntBE(data, 0, 2)', function () {
    it('0x0500 should equal to 1280', function () {
      var data = Buffer(2);
      data[0] = 0x05;
      data[1] = 0x00;
      var actual = dsaUtil.readUIntBE(data, 0, 2);
      expect(actual).to.eql(1280);
    })
  });

  describe('readUIntBE(data, 0, 2)', function () {
    it('0x0500 should equal to 720', function () {
      var data = new Buffer(2);
      data[0] = 0x02;
      data[1] = 0xd0;
      var actual = dsaUtil.readUIntBE(data, 0, 2);
      expect(actual).to.eql(720);
    })
  });

  describe('writeUIntBE(data, 1280, 0, 2)', function () {
    it('0x0500 should equal to 1280', function () {
      var data = new Buffer(2);
      var actual = dsaUtil.writeUIntBE(data, 1280, 0, 2);
      var expected = new Buffer(2);
      expected[0] = 0x05;
      expected[1] = 0x00;
      expect(data).to.eql(expected);
    })
  });

  describe('writeUIntBE(data, 720, 0, 2)', function () {
    it('0x0500 should equal to 1280', function () {
      var data = new Buffer(2);
      var actual = dsaUtil.writeUIntBE(data, 720, 0, 2);
      var expected = new Buffer(2);
      expected[0] = 0x02;
      expected[1] = 0xd0;
      expect(data).to.eql(expected);
    })
  });

})
