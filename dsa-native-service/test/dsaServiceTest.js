var chai = require('chai'),
  assert = chai.assert,
  expect = chai.expect,
  nativeService = require('../index.js'),
  dsaUtil = require('../dsaUtil.js');

nativeService.dsaServer.on('nativeServerReady', function () {
  describe('dsaService tests', function () {

    /*
    describe('getScreenResolution', function () {
      var expected = {};
      expected.width = 1920;
      expected.height = 1080;

      it('should return screen resolution', function () {
        nativeService.getScreenResolution(function(actual){
          console.log('############### resolution: ' + JSON.stringify(actual));
          expect(actual).to.eql(expected);
        });
      });
    });*/

    describe('powerSavingDisable', function () {
      var expected = {};
      expected.cmd = 'SET_POWER_KEEP_ALIVE';
      expected.data = 'successful';

      it('should disable power saving mode', function () {
        nativeService.powerSavingDisable();
        nativeService.dsaServer.on('powerSavingDisable', function (actual) {
          expect(actual).to.eql(expected);
        });
      });
    });

    describe('powerSavingEnable', function () {
      it('should enable power saving mode', function () {
        var expected = {},
          actual = {};
        expected.cmd = 'SET_POWER_NORMAL';
        expected.data = 'successful';
        nativeService.powerSavingDisable();
        nativeService.dsaServer.on('powerSavingEnable', function (result) {
          actual = result;
          expect(actual).to.eql(expected);
        });
      });
    });

    if (process.platform === 'win32') {
      describe('aeroModeEnable', function () {
        var expected = {};
        expected.cmd = 'SET_POWER_KEEP_ALIVE';
        expected.data = 'successful';

        it('should disable power saving mode', function () {
          nativeService.powerSavingDisable();
          nativeService.dsaServer.on('aeroModeEnable', function (actual) {
            expect(actual).to.eql(expected);
          });
        });
      });

      describe('aeroModeDisable', function () {
        var expected = {};
        expected.cmd = 'SET_POWER_NORMAL';
        expected.data = 'successful';

        it('should enable power saving mode', function () {
          nativeService.powerSavingDisable();
          nativeService.dsaServer.on('aeroModeDisable', function (actual) {
            expect(actual).to.eql(expected);
          });
        });
      });
    }

  });
});
