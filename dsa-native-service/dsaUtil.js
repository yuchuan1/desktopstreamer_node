'use strict';
/**
 * Convert Integer to Big Endian Value
 * @param intValue
 * @returns {Int8Array}
 */
exports.convertIntToBytesBigEndian = function convertIntToBytesBigEndian(intValue) {
  var result = new Int8Array(4);
  result[3] = 0xff & intValue;
  result[2] = 0xff & (intValue >> 8);
  result[1] = 0xff & (intValue >> 16);
  result[0] = 0xff & (intValue >> 24);
  return result;
};

/**
 * Convert 4Bytes Big Endian arrays to Integer
 * @param byte1
 * @param byte2
 * @param byte3
 * @param byte4
 * @returns
 */
exports.convertBytesBigEndianToInt = function convertBytesBigEndianToInt(byte1, byte2, byte3, byte4) {
  var firstByte = 0xff & byte1;
  var secondByte = 0xff & byte2;
  var thirdByte = 0xff & byte3;
  var fourthByte = 0xff & byte4;
  var result = ((firstByte << 24) | (secondByte << 16) | (thirdByte << 8) | fourthByte) & 0xFFFFFFFF;
  return result;
};

function checkOffset(offset, ext, length) {
  if (offset + ext > length)
    throw new RangeError('index out of range');
}

exports.readUIntBE = function(data, offset, byteLength, noAssert) {
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;
  if (!noAssert) {
    checkOffset(offset, byteLength, data.length);
  }

  var val = data[offset + --byteLength];
  var mul = 1;
  while (byteLength > 0 && (mul *= 0x100))
    val += data[offset + --byteLength] * mul;

  return val;
};

exports.writeUIntBE = function(data, value, offset, byteLength) {
  value = +value;
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;
  //  if (!noAssert)
  //    checkInt(this, value, offset, byteLength, Math.pow(2, 8 * byteLength), 0);

  var i = byteLength - 1;
  var mul = 1;
  data[offset + i] = value;
  while (--i >= 0 && (mul *= 0x100))
    data[offset + i] = (value / mul) >>> 0;

  return data;
  //return offset + byteLength;
};

/**
 * Covert String to bytes arrays
 * @param str
 * @returns {Array}
 */
exports.convertStringToBytes = function convertStringToBytes(str) {
  var bytes = [];
  for (var i = 0; i < str.length; ++i) {
    bytes.push(str.charCodeAt(i));
  }

  return bytes;
};

/**
 * Covert String to bytes arrays
 * @param array
 * @returns {String}
 */
exports.convertByteArrayToString = function convertByteArrayToString(array) {
  var result = '';
  for (var i = 0; i < array.length; i++) {
    result += String.fromCharCode(array[i]);
  }

  return result;
};

/**
 * Conver String to bytes arrays with UTF-8 Decode
 * @param arrayBuffer
 * @returns {String}
 */
exports.convertByteArrayToStringUtf8 = function convertByteArrayToStringUtf8(arrayBuffer) {
  var result = '';
  var i = 0,
    c = 0,
    c2 = 0,
    c3 = 0;

  var data = new Uint8Array(arrayBuffer);

  // If we have a BOM skip it
  if (data.length >= 3 && data[0] === 0xef && data[1] === 0xbb && data[2] === 0xbf) {
    i = 3;
  }

  while (i < data.length) {
    c = data[i];

    if (c < 128) {
      result += String.fromCharCode(c);
      i++;
    } else if (c > 191 && c < 224) {
      if (i + 1 >= data.length) {
        throw 'UTF-8 Decode failed. Two byte character was truncated.';
      }
      c2 = data[i + 1];
      result += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
      i += 2;
    } else {
      if (i + 2 >= data.length) {
        throw 'UTF-8 Decode failed. Multi byte character was truncated.';
      }
      c2 = data[i + 1];
      c3 = data[i + 2];
      result += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      i += 3;
    }
  }
  return result;
};

exports.utf8ToByteArray = function(str) {
  var byteArray = [];
  for (var i = 0; i < str.length; i++)
    if (str.charCodeAt(i) <= 0x7F) {
      byteArray.push(str.charCodeAt(i));
    } else {
      var h = encodeURIComponent(str.charAt(i)).substr(1).split('%');
      for (var j = 0; j < h.length; j++)
        byteArray.push(parseInt(h[j], 16));
    }
  return byteArray;
};

exports.byteArrayToUtf8 = function(byteArray) {
  var str = '';
  for (var i = 0; i < byteArray.length; i++)
    str += byteArray[i] <= 0x7F ?
    byteArray[i] === 0x25 ? '%25' : // %
    String.fromCharCode(byteArray[i]) :
    '%' + byteArray[i].toString(16).toUpperCase();
  return decodeURIComponent(str);
};

/**
 * Covert array bytes to base64
 * @param bytes
 * @returns
 */
exports.covertArrayToBase64 = function covertArrayToBase64(bytes) {
  var binary = '';
  var len = bytes.byteLength;
  for (var i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
};

/**
 * Get Youtube URL
 * @param url
 * @returns
 */
exports.getYoutubeId = function getYoutubeId(url) {
  var result = url.replace('https://www.youtube.com/watch?v=', '').replace(/&.*/g, '');
  return result;
};

/**
 * Creates a new Uint8Array based on two different ArrayBuffers
 *
 * @private
 * @param {ArrayBuffers} buffer1 The first buffer.
 * @param {ArrayBuffers} buffer2 The second buffer.
 * @return {ArrayBuffers} The new ArrayBuffer created out of the two.
 */
exports.appendBuffer = function(buffer1, buffer2) {
  var tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
  tmp.set(new Uint8Array(buffer1), 0);
  tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
  return tmp.buffer;
};

exports.lengthInUtf8Bytes = function lengthInUtf8Bytes(str) {
  // Matches only the 10.. bytes that are non-initial characters in a multi-byte sequence.
  var m = encodeURIComponent(str).match(/%[89ABab]/g);
  return str.length + (m ? m.length : 0);
};
