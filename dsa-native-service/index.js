/* jshint -W030 */
/* jshint -W083 */
'use strict';
var settings = require('./settings.js'),
  dsaUtil = require('./dsaUtil.js'),
  spawn = require('child_process').spawn,
  exec = require('child_process').exec,
  conn,
  net = require('net'),
  dataFlag = '',
  previewImageBuffer,
  previewImageSize = 0,
  previewImageDataCount = 0,
  deviceDataBuffer,
  deviceDataSize = 0,
  expectedDataLength = 0,
  deviceDataCount = 0,
  licenseDataBuffer,
  //licenseDataSize = 0,
  licenseDataCount = 0,
  nativeService,
  initFileExist = false,
  licFileCorrect = false,
  handleBuffer = [],
  logger = settings.logger;

function checkNativeDataSize(rBuffer) {
  var result = -1;
  if (rBuffer.length < 9) {
    logger.debug('##### length < 9 ' + rBuffer[0]);
    logger.debug('##### length < 9 ' + rBuffer[1]);
    logger.debug('##### length < 9 ' + rBuffer[2]);
    logger.debug('##### length < 9 ' + rBuffer[3]);
  } else {
    var dataLength = dsaUtil.convertBytesBigEndianToInt(rBuffer[2], rBuffer[3], rBuffer[4], rBuffer[5]);
    var expDataLength = dataLength + 9;
    if (rBuffer.length >= expDataLength) {
      result = expDataLength;
    }
  }
  return result;
}

function parseNativeData(data, callback) {
  var response = {};
  // logger.debug('#NATIVE_SERVER# parse native data, data length: ' + data.length);
  if (data[0] === 0x5A && data[1] === 0x81) {
    logger.debug('#NATIVE_SERVER# data began with 0x5A and 0x81');
    var dataLength = dsaUtil.convertBytesBigEndianToInt(data[2], data[3], data[4], data[5]);
    var command = data[6] + '.' + data[7];
    var result = data[8]; //result success: 0x00 fail: 0x01

    logger.debug('#NATIVE_SERVER# data length: ' + dataLength);
    logger.debug('#NATIVE_SERVER# command: ' + command);
    if (command === '0.0') {
      logger.debug(JSON.stringify(data));
    }
    logger.debug('#NATIVE_SERVER# result: ' + result);

    switch (command) {
      case settings.CMD.SET_AUDIO_MUTE:
        logger.debug('#NATIVE_SERVER# SET_AUDIO_MUTE');
        response.cmd = 'SET_AUDIO_MUTE';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.RESTORE_AUDIO_MUTE:
        logger.debug('#NATIVE_SERVER# RESTORE_AUDIO_MUTE');
        response.cmd = 'RESTORE_AUDIO_MUTE';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.GET_CURRENT_MIRROR_SCREEN_STATE:
        logger.debug('#NATIVE_SERVER# GET_CURRENT_MIRROR_SCREEN_STATE');
        response.cmd = 'GET_CURRENT_MIRROR_SCREEN_STATE';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.GET_IS_SUPPORT_EXTEND_SCREEN_MODE:
        logger.debug('#NATIVE_SERVER# GET_IS_SUPPORT_EXTEND_SCREEN_MODE');
        response.cmd = 'GET_IS_SUPPORT_EXTEND_SCREEN_MODE';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.SET_EXTEND_SCREEN_MODE:
        logger.debug('#NATIVE_SERVER# SET_EXTEND_SCREEN_MODE');
        response.cmd = 'SET_EXTEND_SCREEN_MODE';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.SET_LED_ON:
        logger.debug('#NATIVE_SERVER# SET_LED_ON');
        response.cmd = 'SET_LED_ON';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.SET_LED_OFF:
        logger.debug('#NATIVE_SERVER# SET_LED_OFF');
        response.cmd = 'SET_LED_OFF';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.SET_LED_FLASH:
        logger.debug('#NATIVE_SERVER# SET_LED_FLASH');
        response.cmd = 'SET_LED_FLASH';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.SET_LED_GRADIENT_FLASH:
        logger.debug('#NATIVE_SERVER# SET_LED_GRADIENT_FLASH');
        response.cmd = 'SET_LED_GRADIENT_FLASH';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.SET_LED_CYCLE_FLASH:
        logger.debug('#NATIVE_SERVER# SET_LED_CYCLE_FLASH');
        response.cmd = 'SET_LED_CYCLE_FLASH';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.REPORT_CONNECT_STATE:
        logger.debug('#NATIVE_SERVER# REPORT_CONNECT_STATE');
        response.cmd = 'REPORT_CONNECT_STATE';
        response.data = true;
        callback(response);
        break;
      case settings.CMD.LAUNCHER_COMMAND:
        logger.debug('#NATIVE_SERVER# received LAUNCHER_COMMAND data length:' + data.length);
        dataFlag = 'LAUNCHER_COMMAND';
        var launcherCmd = '';
        expectedDataLength = 10;
        if (data.length === 10) {
          logger.debug('#NATIVE_SERVER# received LAUNCHER_COMMAND data length:' + data.length);
          /*
          0x00 = Full screen button
          0x01 = Split 1 button
          0x02 = Split 2 button
          0x03 = Split 3 button
          0x04 = Split 4 button
          0x10 = Disconnect button.
          0x20 = USB plug in.
          0x21 = USB plug out.
          */
          switch (parseInt(data[9])) {
            case 0x00:
              launcherCmd = 'Full screen button';
              break;
            case 0x01:
              launcherCmd = 'Split 1 button';
              break;
            case 0x02:
              launcherCmd = 'Split 2 button';
              break;
            case 0x03:
              launcherCmd = 'Split 3 button';
              break;
            case 0x04:
              launcherCmd = 'Split 4 button';
              break;
            case 0x10:
              launcherCmd = 'Disconnect button';
              break;
            case 0x20:
              launcherCmd = 'USB plug in';
              break;
            case 0x21:
              launcherCmd = 'USB plug out';
              break;

            default:
              launcherCmd = '';
          }
          logger.debug('#NATIVE_SERVER# LAUNCHER_COMMAND: 0x' + parseInt(data[9]).toString(16) + ' ' + launcherCmd);
          response.cmd = 'LAUNCHER_COMMAND';
          response.data = data[9];
          logger.debug('1 LAUNCHER_COMMAND buffer: ' + JSON.stringify(data));
          callback(response);
        } else {
          deviceDataBuffer = new Buffer(expectedDataLength);
          deviceDataCount = 0;
          for (var i = 0; i < data.length; i++) {
            deviceDataBuffer[i] = data[i];
            deviceDataCount++;
          }
        }

        break;
      case settings.CMD.GET_DEVICE_INFO:
        logger.debug('#NATIVE_SERVER# The command is GET_DEVICE_INFO, data length: ' + dataLength);
        logger.debug('#NATIVE_SERVER# The command is GET_DEVICE_INFO, data array length: ' + data.length);
        logger.debug('#NATIVE_SERVER# The command is GET_DEVICE_INFO, data array: ' + JSON.stringify(data));
        logger.debug('#NATIVE_SERVER# The command is GET_DEVICE_INFO, result is :' + JSON.stringify(result));
        dataFlag = 'deviceData';
        expectedDataLength = dataLength + 9;
        if (data.length === expectedDataLength) {
          if (result === 0x00) {
            logger.debug('#NATIVE_SERVER# The command is GET_DEVICE_INFO, data.length  == expectedDataLength');
            deviceDataSize = dataLength;
            deviceDataBuffer = new Buffer(data.length);
            if (data.length > dataLength) {
              var deviceType,
                vendorId = '',
                productId = '',
                serialNumber;

              if (data[9] === 0x01) {
                deviceType = settings.deviceType.USB;
              } else if (data[9] === 0x02) {
                deviceType = settings.deviceType.HDD;
              }

              var vidArr = [];
              for (var i = 0; i < 4; i++) {
                vidArr[i] = data[10 + i];
              }

              vendorId = dsaUtil.convertByteArrayToString(vidArr);
              logger.debug('1 vid: ' + vendorId);
              vendorId = vendorId.replace('\u0000', '').trim();
              logger.debug('2 vid: ' + vendorId);
              while (vendorId.length < 4) {
                vendorId = '0' + vendorId;
              }
              logger.debug('3 vid: ' + vendorId);

              for (var i = 0; i < 4; i++) {
                productId += String.fromCharCode(data[14 + i]);
              }
              var snArr = [];
              for (var i = 0; i < dataLength - 9; i++) {
                snArr[i] = data[18 + i];
              }
              serialNumber = dsaUtil.convertByteArrayToString(snArr);

              response.cmd = 'GET_DEVICE_INFO';
              response.data = {
                deviceType: deviceType,
                vendorId: vendorId,
                productId: productId,
                serialNumber: serialNumber
              };
              logger.debug('#NATIVE_SERVER# CMD.GET_DEVICE_INFO: ' + JSON.stringify(response.data));
              callback(response);

            }
          } else {
            logger.debug('#NATIVE_SERVER# GET_DEVICE_INFO failed');
            logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          }
        } else {
          logger.debug('#NATIVE_SERVER# The command is GET_DEVICE_INFO, size: ' + (data.length + expectedDataLength));
          deviceDataBuffer = new Buffer(data.length + expectedDataLength);
          deviceDataCount = data.length;
          for (var i = 0; i < data.length; i++) {
            deviceDataBuffer[i] = data[i];
          }
        }
        break;
      case settings.CMD.CHECK_INIT_FILE:
        logger.debug('#NATIVE_SERVER# The command is CHECK_INIT_FILE data.length: ' + data.length);
        dataFlag = 'CHECK_INIT_FILE';
        initFileExist = false,
          expectedDataLength = 10;
        if (data.length === expectedDataLength) {
          if (result === 0x00) {
            if (data[9] === 1) {
              initFileExist = true;
            } else {
              initFileExist = false;
            }
            response.cmd = 'CHECK_INIT_FILE';
            response.data = initFileExist;
            callback(response);
          } else {
            logger.debug('#NATIVE_SERVER# CHECK_INIT_FILE failed');
            logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          }
        } else {
          deviceDataBuffer = new Buffer(expectedDataLength);
          deviceDataCount = data.length;
          for (var i = 0; i < data.length; i++) {
            deviceDataBuffer[i] = data[i];
          }
        }

        break;
      case settings.CMD.CHECK_LICENSE_FILE:
        logger.debug('#NATIVE_SERVER# The command is CHECK_LICENSE_FILE, data length: ' + data.length);
        licFileCorrect = false,
          expectedDataLength = 10;
        dataFlag = 'CHECK_LICENSE_FILE';
        if (data.length === expectedDataLength) {
          if (result === 0x00) {
            if (data[9] === 1) {
              licFileCorrect = true;
            } else {
              licFileCorrect = false;
            }
            response.cmd = 'CHECK_LICENSE_FILE';
            response.data = licFileCorrect;
            logger.debug('#NATIVE_SERVER# The command is CHECK_LICENSE_FILE, data length is equal to expectedDataLength, response is ' + JSON.stringify(response));

            callback(response);
          } else {
            logger.debug('#NATIVE_SERVER# CHECK_LICENSE_FILE failed');
            logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          }
        } else {
          // logger.debug('#NATIVE_SERVER# The command is CHECK_LICENSE_FILE, data length: ' + data.length + ' is less than expectedDataLength: ' + expectedDataLength);
          licenseDataBuffer = new Buffer(expectedDataLength);
          licenseDataCount = data.length;
          for (var i = 0; i < data.length; i++) {
            licenseDataBuffer[i] = data[i];
          }
          // logger.debug('licenseData: ' + JSON.stringify(data));
          // logger.debug('licenseDataBuffer: ' + JSON.stringify(licenseDataBuffer));
        }
        break;
      case settings.CMD.SET_REMOTE_CONTROL_CONNECT:
        logger.debug('#NATIVE_SERVER# The command is SET_REMOTE_CONTROL_CONNECT, data.length:' + data.length);
        response.cmd = 'SET_REMOTE_CONTROL_CONNECT';
        if (result === 0x00) {
          response.data = true;
          callback(response);
        } else {
          response.data = false;
          callback(response);
          logger.debug('#NATIVE_SERVER# SET_REMOTE_CONTROL_CONNECT failed');
          logger.error('#NATIVE_SERVER# error code: ', data[9], data[10], data[11], data[12]);
        }
        break;
      case settings.CMD.DELETE_INIT_FILE:
        logger.debug('#NATIVE_SERVER# The command is DELETE_INIT_FILE');
        response.cmd = 'DELETE_INIT_FILE';
        if (result === 0x00) {
          response.data = 'successful';
          callback(response);
        } else {
          response.data = 'fail';
          callback(response);
          logger.debug('#NATIVE_SERVER# DELETE_INIT_FILE failed');
          logger.error('#NATIVE_SERVER# error code: ', data[9], data[10], data[11], data[12]);
        }
        break;
      case settings.CMD.GENERATE_LICENSE_FILE:
        logger.debug('#NATIVE_SERVER# The command is GENERATE_LICENSE_FILE');
        response.cmd = 'GENERATE_LICENSE_FILE';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# GENERATE_LICENSE_FILE successful');
          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# GENERATE_LICENSE_FILE failed');
          logger.error('#NATIVE_SERVER# error code: ', data[9], data[10], data[11], data[12]);
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.GET_IS_SOUNDFLOWER_OUTPUT:
        logger.debug('#NATIVE_SERVER# The command is GET_IS_SOUNDFLOWER_OUTPUT, data length: ' + data.length + ' result:' + data[9]);
        dataFlag = 'GET_IS_SOUNDFLOWER_OUTPUT';
        expectedDataLength = 10;
        if (data.length === expectedDataLength) {
          if (result === 0x00) {
            var soundflowerState = new Buffer(1),
              result = 'SOUNDFLOWER_OUTPUT_DISABLE';
            soundflowerState = data[9];
            if (soundflowerState === 1) {
              result = 'SOUNDFLOWER_OUTPUT_ENABLE';
            }
            response.cmd = 'GET_IS_SOUNDFLOWER_OUTPUT';
            response.data = result;
            callback(response);
          } else {
            response.cmd = 'GET_IS_SOUNDFLOWER_OUTPUT';
            response.data = 'fail';
            callback(response);
            logger.debug('#NATIVE_SERVER# GET_IS_SOUNDFLOWER_OUTPUT failed');
            logger.error('#NATIVE_SERVER# error code: ', data[9], data[10], data[11], data[12]);
          }
        } else {
          deviceDataBuffer = new Buffer(expectedDataLength);
          deviceDataCount = data.length;
          for (var i = 0; i < data.length; i++) {
            deviceDataBuffer[i] = data[i];
          }
        }

        break;
      case settings.CMD.SET_SOUNDFLOWER_OUTPUT_ENABLE:
        logger.debug('#NATIVE_SERVER# The command is SET_SOUNDFLOWER_OUTPUT_ENABLE');
        response.cmd = 'SET_SOUNDFLOWER_OUTPUT_ENABLE';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_SOUNDFLOWER_OUTPUT_ENABLE successful');
          response.data = true;
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_SOUNDFLOWER_OUTPUT_ENABLE failed');
          logger.error('#NATIVE_SERVER# error code: ', data[9], data[10], data[11], data[12]);
          response.data = false;
          callback(response);
        }
        break;
      case settings.CMD.SET_SOUNDFLOWER_OUTPUT_DISABLE:
        logger.debug('#NATIVE_SERVER# The command is SET_SOUNDFLOWER_OUTPUT_DISABLE');
        response.cmd = 'SET_SOUNDFLOWER_OUTPUT_DISABLE';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_SOUNDFLOWER_OUTPUT_DISABLE successful');
          response.data = true;
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_SOUNDFLOWER_OUTPUT_DISABLE failed');
          logger.error('#NATIVE_SERVER# error code: ', data[9], data[10], data[11], data[12]);
          response.data = false;
          callback(response);
        }
        break;
      case settings.CMD.GET_SCREEN_RESOLUTION:
        logger.debug('#NATIVE_SERVER# The command is GET_SCREEN_RESOLUTION');
        var widthBuffer = new Buffer(2);
        widthBuffer[0] = data[9];
        widthBuffer[1] = data[10];
        //logger.debug('width: ' + dsaUtil.readUIntBE(widthBuffer, 0, 2));
        var heightBuffer = new Buffer(2);
        heightBuffer[0] = data[11];
        heightBuffer[1] = data[12];
        //logger.debug('height: ' + dsaUtil.readUIntBE(heightBuffer, 0, 2));
        response.cmd = 'GET_SCREEN_RESOLUTION';
        response.data = {
          width: dsaUtil.readUIntBE(widthBuffer, 0, 2),
          height: dsaUtil.readUIntBE(heightBuffer, 0, 2)
        };
        callback(response);
        break;
      case settings.CMD.SET_SCREEN_RESOLUTION:
        logger.debug('#NATIVE_SERVER# The command is SET_SCREEN_RESOLUTION');
        response.cmd = 'SET_SCREEN_RESOLUTION';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_SCREEN_RESOLUTION successful');
          response.data = 'successful';
          callback(response);
        } else {
          response.data = 'fail';
          callback(response);
          logger.debug('#NATIVE_SERVER# SET_SCREEN_RESOLUTION failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
        }
        break;
      case settings.CMD.RESTORE_LAST_SCREEN_RESOLUTION:
        logger.debug('#NATIVE_SERVER# The command is RESTORE_LAST_SCREEN_RESOLUTION');
        response.cmd = 'RESTORE_LAST_SCREEN_RESOLUTION';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# RESTORE_LAST_SCREEN_RESOLUTION successful');
          response.data = 'successful';
          callback(response);
        } else {
          response.data = 'fail';
          callback(response);
          logger.debug('#NATIVE_SERVER# RESTORE_LAST_SCREEN_RESOLUTION failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
        }
        break;
      case settings.CMD.SET_AERO_MODE_ENABLE:
        logger.debug('#NATIVE_SERVER# The command is SET_AERO_MODE_ENABLE');
        response.cmd = 'SET_AERO_MODE_ENABLE';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_AERO_MODE_ENABLE successful');
          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_AERO_MODE_ENABLE failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.SET_AERO_MODE_DISABLE:
        logger.debug('#NATIVE_SERVER# The command is SET_AERO_MODE_DISABLE');
        response.cmd = 'SET_AERO_MODE_DISABLE';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_AERO_MODE_DISABLE successful');
          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_AERO_MODE_DISABLE failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.SET_REMOTE_CONTROL_CONNECT:
        logger.debug('#NATIVE_SERVER# The command is SET_REMOTE_CONTROL_CONNECT');
        response.cmd = 'SET_REMOTE_CONTROL_CONNECT';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_REMOTE_CONTROL_CONNECT successful');
          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_REMOTE_CONTROL_CONNECT failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.SET_REMOTE_CONTROL_DISCONNECT:
        logger.debug('#NATIVE_SERVER# The command is SET_REMOTE_CONTROL_DISCONNECT');
        response.cmd = 'SET_REMOTE_CONTROL_DISCONNECT';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_REMOTE_CONTROL_DISCONNECT successful');
          response.data = true;
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_REMOTE_CONTROL_DISCONNECT failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = false;
          callback(response);
        }
        break;
      case settings.CMD.GET_REMOTE_CONTROL_STATE:
        logger.debug('#NATIVE_SERVER# The command is GET_REMOTE_CONTROL_STATE');
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# GET_REMOTE_CONTROL_STATE successful');
          response.cmd = 'GET_REMOTE_CONTROL_STATE';
          response.data = data[9];
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# GET_REMOTE_CONTROL_STATE failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
        }
        break;
      case settings.CMD.SET_POWER_KEEP_ALIVE:
        logger.debug('#NATIVE_SERVER# The command is SET_POWER_KEEP_ALIVE');
        response.cmd = 'SET_POWER_KEEP_ALIVE';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_POWER_KEEP_ALIVE successful');
          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_POWER_KEEP_ALIVE failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.SET_POWER_NORMAL:
        logger.debug('#NATIVE_SERVER# The command is SET_POWER_NORMAL');
        response.cmd = 'SET_POWER_NORMAL';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_POWER_NORMAL successful');
          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_POWER_NORMAL failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.SET_VIDEO_FULL_MODE_START:
        logger.debug('#NATIVE_SERVER# The command is SET_VIDEO_FULL_MODE_START');
        response.cmd = 'SET_VIDEO_FULL_MODE_START';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_VIDEO_FULL_MODE_START successful');
          response.data = true;
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_VIDEO_FULL_MODE_START failed');
          logger.error('#NATIVE_SERVER# error code: ', data[9], data[10], data[11], data[12]);
          response.data = false;
          callback(response);
        }
        break;
      case settings.CMD.SET_VIDEO_FULL_MODE_STOP:
        logger.debug('#NATIVE_SERVER# The command is SET_VIDEO_FULL_MODE_STOP');
        response.cmd = 'SET_VIDEO_FULL_MODE_STOP';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_VIDEO_FULL_MODE_STOP successful');
          response.data = 'successful';
          callback(response);
        } else {
          response.data = 'fail';
          callback(response);
          logger.debug('#NATIVE_SERVER# SET_VIDEO_FULL_MODE_STOP failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
        }
        break;
      case settings.CMD.SET_IMAGE_SPLIT_MODE_START:
        logger.debug('#NATIVE_SERVER# The command is SET_IMAGE_SPLIT_MODE_START');
        response.cmd = 'SET_IMAGE_SPLIT_MODE_START';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_IMAGE_SPLIT_MODE_START successful');
          response.data = true;
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_IMAGE_SPLIT_MODE_START failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = false;
          callback(response);
        }
        break;
      case settings.CMD.SET_IMAGE_SPLIT_MODE_STOP:
        logger.debug('#NATIVE_SERVER# The command is SET_IMAGE_SPLIT_MODE_STOP');
        response.cmd = 'SET_IMAGE_SPLIT_MODE_STOP';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_IMAGE_SPLIT_MODE_STOP successful');
          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_IMAGE_SPLIT_MODE_STOP failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.SET_IMAGE_SPLIT_MODE_PAUSE:
        logger.debug('#NATIVE_SERVER# The command is SET_IMAGE_SPLIT_MODE_PAUSE');
        response.cmd = 'SET_IMAGE_SPLIT_MODE_PAUSE';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_IMAGE_SPLIT_MODE_PAUSE successful');

          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_IMAGE_SPLIT_MODE_PAUSE failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.SET_IMAGE_SPLIT_MODE_RESUME:
        logger.debug('#NATIVE_SERVER# The command is SET_IMAGE_SPLIT_MODE_RESUME');
        response.cmd = 'SET_IMAGE_SPLIT_MODE_RESUME';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# SET_IMAGE_SPLIT_MODE_RESUME successful');
          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# SET_IMAGE_SPLIT_MODE_RESUME failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.UPDATE_IMAGE_SPLIT_MODE_FRAME_RATE:
        logger.debug('#NATIVE_SERVER# The command is UPDATE_IMAGE_SPLIT_MODE_FRAME_RATE');
        response.cmd = 'UPDATE_IMAGE_SPLIT_MODE_FRAME_RATE';
        if (result === 0x00) {
          logger.debug('#NATIVE_SERVER# UPDATE_IMAGE_SPLIT_MODE_FRAME_RATE successful');
          response.data = 'successful';
          callback(response);
        } else {
          logger.debug('#NATIVE_SERVER# UPDATE_IMAGE_SPLIT_MODE_FRAME_RATE failed');
          logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          response.data = 'fail';
          callback(response);
        }
        break;
      case settings.CMD.GET_PREVIEW_IMAGE_DATA:
        logger.debug('#NATIVE_SERVER# The command is GET_PREVIEW_IMAGE_DATA');
        logger.debug('#NATIVE_SERVER# GET_PREVIEW_IMAGE_DATA successful, img data length: ' + dataLength + ' data received: ' + data.length);
        expectedDataLength = dataLength + 9;
        previewImageSize = dataLength;
        dataFlag = 'previewData';

        if (data.length === expectedDataLength) {
          if (result === 0x00) {
            logger.debug('#NATIVE_SERVER# GET_PREVIEW_IMAGE_DATA successful, img data length: ' + dataLength);
            previewImageBuffer = new Buffer(previewImageSize);
            for (var i = 9; i < data.length; i++) {
              previewImageBuffer[previewImageDataCount] = data[i];
              previewImageDataCount++;
            }
            response.cmd = 'GET_PREVIEW_IMAGE_DATA';
            response.data = previewImageBuffer;
            dataFlag = '';
            previewImageDataCount = 0;
            callback(response);
            logger.debug('#NATIVE_SERVER# GET_PREVIEW_IMAGE_DATA data length: ' + data.length);
            logger.debug('#NATIVE_SERVER# GET_PREVIEW_IMAGE_DATA previewImageDataCount: ' + previewImageDataCount);
          } else {
            logger.debug('#NATIVE_SERVER# GET_PREVIEW_IMAGE_DATA failed');
            logger.error('#NATIVE_SERVER# error code: x' + dsaUtil.convertBytesBigEndianToInt(data[9], data[10], data[11], data[12]).toString(16));
          }
        } else {
          logger.debug('#NATIVE_SERVER# GET_PREVIEW_IMAGE_DATA data.length + 9 is less than expectedDataLength: ' + expectedDataLength);
          previewImageBuffer = new Buffer(expectedDataLength);
          for (var i = 0; i < data.length; i++) {
            previewImageBuffer[i] = data[i];
            previewImageDataCount++;
          }
        }
        break;
      case settings.CMD.GET_IS_SUPPORT_MIRACAST:
        logger.debug('#NATIVE_SERVER# The command is GET_IS_SUPPORT_MIRACAST, data length: ' + data.length);
        dataFlag = 'GET_IS_SUPPORT_MIRACAST';
        expectedDataLength = 10;
        if (data.length === expectedDataLength) {
          if (result === 0x00) {
            var miracastState = new Buffer(1),
              result = false;
            miracastState = data[9];
            if (miracastState === 1) {
              result = true;
            }
            response.cmd = 'GET_IS_SUPPORT_MIRACAST';
            response.data = result;
            callback(response);
          } else {
            response.cmd = 'GET_IS_SUPPORT_MIRACAST';
            response.data = false;
            callback(response);
            logger.debug('#NATIVE_SERVER# GET_IS_SUPPORT_MIRACAST failed');
            logger.error('#NATIVE_SERVER# error code: ', data[9], data[10], data[11], data[12]);
          }
        } else {
          deviceDataBuffer = new Buffer(expectedDataLength);
          deviceDataCount = data.length;
          for (var i = 0; i < data.length; i++) {
            deviceDataBuffer[i] = data[i];
          }
        }
        break;
      case settings.CMD.GET_IS_SUPPORT_AIRPLAY:
        logger.debug('#NATIVE_SERVER# The command is GET_IS_SUPPORT_AIRPLAY, data length: ' + data.length);
        dataFlag = 'GET_IS_SUPPORT_AIRPLAY';
        expectedDataLength = 10;
        if (data.length === expectedDataLength) {
          if (result === 0x00) {
            var airplayState = new Buffer(1),
              result = false;
            airplayState = data[9];
            if (airplayState === 1) {
              result = true;
            }
            response.cmd = 'GET_IS_SUPPORT_AIRPLAY';
            response.data = result;
            callback(response);
          } else {
            response.cmd = 'GET_IS_SUPPORT_AIRPLAY';
            response.data = false;
            callback(response);
            logger.debug('#NATIVE_SERVER# GET_IS_SUPPORT_AIRPLAY failed');
            logger.error('#NATIVE_SERVER# error code: ', data[9], data[10], data[11], data[12]);
          }
        } else {
          deviceDataBuffer = new Buffer(expectedDataLength);
          deviceDataCount = data.length;
          for (var i = 0; i < data.length; i++) {
            deviceDataBuffer[i] = data[i];
          }
        }
        break;
      case settings.CMD.KS_RV_ERROR:
        logger.debug('#NATIVE_SERVER# KS_RV_ERROR');
        for (var j = 0; j < data.length; j++) {
          logger.debug('##### KS RV ERROR = ' + data[j]);
        }
        if (data[8] === 0x01) {
          if (data[9] === 0x80 && data[10] === 0x09 && data[11] === 0x03 && data[12] === 0x01) {
            //            response.data = 'KS_RV_VIDEO_ERROR';
            response.data = data;
          }
        }
        response.data = data;
        response.cmd = 'KS_RV_ERROR';
        callback(response);
        break;
    }
  } else {
    logger.debug('#NATIVE_SERVER# data header does not begin with 0x5A and 0x81, dataFlag: ' + dataFlag);
    for (var q = 0; q < data.length; q++) {
      logger.debug('unbegin with 0x5A 0x81 [' + q + '] = ' + data[q]);
    }
  }
}

var dsaServer = net.createServer(function(c) {
  console.time('#NATIVE_SERVER# startup');
  var self = this;
  c.on('error', function(error) {
    logger.error('#NATIVE_SERVER# client error!' + JSON.stringify(error));
    self.emit('serviceError', error);

    /*
    async.series([
      function (done) {
        logger.error('#NATIVE_SERVER# client error: trying to shut down native service');
        this.exit(done);
      },
      function(done){
        logger.error('#NATIVE_SERVER# client error: trying to re-start native service');
        nativeService = spawn(settings.params.program, settings.params.programArgs);
      }
    ]);
    */
  });

  c.on('data', function(data) {
    logger.debug('#NATIVE_SERVER# connection id: ' + c.id + ' Data received from ' + settings.params.program + ' through localhost:' + settings.params.serverPort);
    var receiveBuffer = new Uint8Array(data);
    handleBuffer.push.apply(handleBuffer, receiveBuffer);
    var sliceByte = 0;
    while (handleBuffer.length > 0) {
      sliceByte = checkNativeDataSize(handleBuffer);
      if (sliceByte > 0) {
        var handleData = handleBuffer.splice(0, sliceByte);
        //        logger.debug('handleData = '+handleData);
        parseNativeData(handleData, function(response) {
          logger.debug('response.cmd: ' + response.cmd);
          switch (response.cmd) {
            case 'SET_AUDIO_MUTE':
              self.emit('setAudioMute', response.data);
              break;
            case 'RESTORE_AUDIO_MUTE':
              self.emit('restoreAudioMute', response.data);
              break;
            case 'GET_CURRENT_MIRROR_SCREEN_STATE':
              //logger.debug('GET_CURRENT_MIRROR_SCREEN_STATE handleData = ' + handleData + ' response.data:' + response.data);
              self.emit('currentScreen', handleData[9]);
              break;
            case 'SAVE_USER_DEFAULT_EXTEND_MODE':
              self.emit('saveDefaultExtendMode');
              break;
            case 'GET_IS_SUPPORT_EXTEND_SCREEN_MODE':
              logger.debug('GET_IS_SUPPORT_EXTEND_SCREEN_MODE data:' + JSON.stringify(handleData));
              if (handleData[9] === 1) {
                self.emit('extendedScreenSupport', true);
              } else {
                self.emit('extendedScreenSupport', false);
              }
              break;
            case 'SET_EXTEND_SCREEN_MODE':
              self.emit('setExtendedScreenMode', response.data);
              break;
            case 'GET_IS_SUPPORT_AIRPLAY':
              self.emit('airplaySupport', response.data);
              break;
            case 'GET_IS_SUPPORT_MIRACAST':
              self.emit('miracastSupport', response.data);
              break;
            case 'SET_VIDEO_FULL_MODE_STOP':
              // logger.debug('desktopStreamingStop: ' + response.data);
              self.emit('desktopStreamingStop', response.data);
              break;
            case 'SET_VIDEO_FULL_MODE_START':
              self.emit('desktopStreamingStart', response.data);
              break;
            case 'GET_DEVICE_INFO':
              self.emit('getDeviceInfo', response.data);
              logger.debug('getDeviceInfo emit: ' + JSON.stringify(response.data));
              break;
            case 'CHECK_INIT_FILE':
              self.emit('checkInitFile', response.data);
              break;
            case 'DELETE_INIT_FILE':
              self.emit('deleteInitFile', response.data);
              break;
            case 'CHECK_LICENSE_FILE':
              logger.debug('CHECK_LICENSE_FILE response: ' + JSON.stringify(response));
              self.emit('checkLicenseFile', response.data);
              break;
            case 'GENERATE_LICENSE_FILE':
              self.emit('generateLicenseFile', response.data);
              break;
            case 'GET_IS_SOUNDFLOWER_OUTPUT':
              self.emit('getSoundflowerState', response.data);
              break;
            case 'SET_SOUNDFLOWER_OUTPUT_ENABLE':
              self.emit('soundflowerStateEnable', response.data);
              break;
            case 'SET_SOUNDFLOWER_OUTPUT_DISABLE':
              self.emit('soundflowerStateDisable', response.data);
              break;
            case 'GET_SCREEN_RESOLUTION':
              self.emit('getScreenResolution', response.data);
              break;
            case 'SET_SCREEN_RESOLUTION':
              self.emit('setScreenResolution', response.data);
              break;
            case 'RESTORE_LAST_SCREEN_RESOLUTION':
              self.emit('restoreResolution', response.data);
              break;
            case 'SET_AERO_MODE_ENABLE':
              logger.debug('#native aeroModeEnable: ' + response.data);
              self.emit('aeroModeEnable', response.data);
              break;
            case 'SET_AERO_MODE_DISABLE':
              self.emit('aeroModeDisable', response.data);
              break;
            case 'SET_POWER_KEEP_ALIVE':
              self.emit('powerSavingDisable', response.data);
              break;
            case 'SET_POWER_NORMAL':
              self.emit('powerSavingEnable', response.data);
              break;
            case 'GET_PREVIEW_IMAGE_DATA':
              self.emit('getPreviewImage', response.data);
              break;
            case 'UPDATE_IMAGE_SPLIT_MODE_FRAME_RATE':
              self.emit('setSplitFrameRate', response.data);
              break;
            case 'SET_IMAGE_SPLIT_MODE_START':
              self.emit('splitModeStart', response.data);
              break;
            case 'SET_IMAGE_SPLIT_MODE_STOP':
              self.emit('splitModeStop', response.data);
              break;
            case 'SET_IMAGE_SPLIT_MODE_PAUSE':
              self.emit('splitModePause', response.data);
              break;
            case 'SET_IMAGE_SPLIT_MODE_RESUME':
              self.emit('splitModeResume', response.data);
              break;
            case 'SET_REMOTE_CONTROL_CONNECT':
              self.emit('remoteControlStart', response.data);
              break;
            case 'SET_REMOTE_CONTROL_DISCONNECT':
              logger.debug('remoteControlStop: ' + response.data);
              self.emit('remoteControlStop', response.data);
              break;
            case 'GET_REMOTE_CONTROL_STATE':
              self.emit('getRemoteControlState', response.data);
              break;
            case 'LAUNCHER_COMMAND':
              dsaServer.emit('launcherCommand', response.data);
              break;
            case 'SET_LED_ON':
              self.emit('ledOn', response.data);
              break;
            case 'SET_LED_OFF':
              self.emit('ledOff', response.data);
              break;
            case 'SET_LED_FLASH':
              self.emit('ledFlash', response.data);
              break;
            case 'SET_LED_GRADIENT_FLASH':
              self.emit('ledGradientFlash', response.data);
              break;
            case 'SET_LED_CYCLE_FLASH':
              logger.debug('received SET_LED_CYCLE_FLASH, firing ledCycleFlash');
              self.emit('ledCycleFlash', response.data);
              break;
            case 'REPORT_CONNECT_STATE':
              self.emit('rvaConnectionState', response.data);
              break;
            case 'KS_RV_ERROR':
              logger.debug('KS_RV_ERROR');
              dsaServer.emit('ksrv_error', response.data);
              break;
            default:
              logger.debug('response:' + JSON.stringify(response));
              break;
          }

        });
      } else {
        logger.debug('#NATIVE_SERVER# NEED MORE DATA');
        break;
      }
    }
  });

  c.on('end', function() {
    logger.debug('#NATIVE_SERVER# native process disconnected');
    logger.debug(nativeService.connected);
    var err = new Error();
    err.code = 'nativeDisconnect';
    self.emit('serviceError', err);
  });

  conn = c;
});
dsaServer.on('close', function() {
  logger.debug('dsaServer closed!');
}).on('error', function(error) {
  if (error) {
    logger.debug('dsaServer error: ' + JSON.stringify(error));
  }
  logger.error('dsaServer error');
});
dsaServer.listen(settings.params.serverPort, function() {
  var self = this;
  logger.debug('#NATIVE_SERVER# Server started! Press Ctrl-C to exit');
  logger.debug('#NATIVE_SERVER# listening to port: ' + settings.params.serverPort);
  logger.debug('#NATIVE_SERVER# Trying to start program ' + settings.params.program + ' ' + settings.params.programArgs.join(' '));
  logger.debug('#NATIVE_SERVER# path: ' + process.cwd());
  logger.debug('#NATIVE_SERVER# exec path:' + process.execPath);
  logger.debug('#NATIVE_SERVER# settings.params.program: ' + settings.params.program);
  logger.debug('#NATIVE_SERVER# settings.params.programArgs' + settings.params.programArgs);

  nativeService = spawn(settings.params.program, settings.params.programArgs, {
    detached: true
  });
  logger.debug('#NATIVE_SERVER# pid: ' + nativeService.pid);

  exports.connected = nativeService.connected;

  nativeService.stdout.on('data', function(val) {
    var logToIgnore = 'wk_Trans_TcpRead() ret == WK_RV_TSP_RECVDATA_TIMEOUT in wk_trans_pp_win.c ->',
      logToIgnore1 = 'wk_Trans_TcpRead() ret ==0 in wk_trans_pp_mac.c ->';

    if (settings.fullNativeLog === true) {
      logger.debug('#NATIVE_SERVER# stdout data received from ' + settings.params.program + '\n' + val.toString());
    }

    if (val.toString().indexOf(logToIgnore) < 0 && val.toString().indexOf(logToIgnore1) < 0) {
      //  logger.debug('#NATIVE_SERVER# stdout data received from ' + settings.params.program + '\n' + val.toString());

      // TSP_AUTH log
      if (val.toString().indexOf('0x80090201') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUTH_CONNECTION_ERROR');
      }

      if (val.toString().indexOf('0x80090202') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUTH_REQUEST_ERROR');
      }

      if (val.toString().indexOf('0x80090203') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUTH_RESPONSE_ERROR');
      }

      if (val.toString().indexOf('0x80090204') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUTH_ERROR');
      }

      if (val.toString().indexOf('0x80090208') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUTH_INITIALIZE_ERROR');
      }
      // TSP_VIDEO
      if (val.toString().indexOf('0x80090301') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_VIDEO_CONNECTION_ERROR');
      }

      if (val.toString().indexOf('0x80090302') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_VIDEO_SEND_DATA_ERROR');
      }

      if (val.toString().indexOf('0x80090303') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_VIDEO_RECV_TOKEN_ERROR');
      }

      if (val.toString().indexOf('0x80090304') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_VIDEO_CODEC_ERROR');
      }

      if (val.toString().indexOf('0x80090305') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_VIDEO_CONNECTION_OK');
      }

      if (val.toString().indexOf('0x80090306') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_VIDEO_STOP_FAIL');
      }

      if (val.toString().indexOf('0x80090308') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_VIDEO_INITIALIZE_ERROR');
      }

      // TSP_AUDIO
      if (val.toString().indexOf('0x80090401') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUDIO_CONNECTION_ERROR');
      }

      if (val.toString().indexOf('0x80090402') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUDIO_SEND_DATA_ERROR');
      }

      if (val.toString().indexOf('0x80090403') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUDIO_RECV_TOKEN_ERROR');
      }

      if (val.toString().indexOf('0x80090404') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUDIO_CODEC_ERROR');
      }

      if (val.toString().indexOf('0x80090405') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUDIO_CONNECTION_OK');
      }

      if (val.toString().indexOf('0x80090406') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUDIO_STOP_FAIL');
      }

      if (val.toString().indexOf('0x80090408') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> KS_RV_AUDIO_INITIALIZE_ERROR');
      }


      if (val.toString().indexOf('0x80103303') >= 0) {
        logger.debug('#NATIVE_SERVER# <NativeService> split mode port disconnect');
      }
      // logger.debug(val.toString());
    }
  });

  nativeService.stderr.on('data', function(val) {
    if (settings.fullNativeLog === true) {
      logger.debug('#NATIVE_SERVER# stderr data received from ' + settings.params.program);
      logger.debug('#NATIVE_SERVER# stderr ' + val);
    }

  });

  nativeService.on('exit', function(code) {
    logger.debug('#NATIVE_SERVER# child process exited with code ' + code);
    self.emit('serviceError', {
      'code': 'nativeDisconnect'
    });
    //TODO: Handle client crash here...
  });
  nativeService.on('disconnect', function() {
    logger.debug('#NATIVE_SERVER# child process disconnected');
    self.emit('serviceError', {
      'code': 'nativeDisconnect'
    });
  });
  nativeService.on('close', function(code) {
    logger.debug('#NATIVE_SERVER# child process closed with code ' + code);
    self.emit('serviceError', {
      'code': 'nativeDisconnect'
    });
  });
  nativeService.on('error', function(error) {
    logger.error('#NATIVE_SERVER# error');
    logger.error(error);
  });

});

dsaServer.on('connection', function(conn) {
  logger.debug('client connected!!!');
  console.timeEnd('#NATIVE_SERVER# startup');
  conn.id = Math.floor(Math.random() * 1000);
  this.emit('nativeServerReady');
});

exports.setAudioMute = function setAudioMute(cb) {
  logger.debug('#NATIVE_SERVER# setAudioMute');
  var self = this;
  var buffer = new Buffer(settings.cmdLength + 1),
    dataLength = dsaUtil.convertIntToBytesBigEndian(1);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x05;
  buffer[7] = 0x05;
  buffer[8] = 0x01;

  dsaServer.once('setAudioMute', function(result) {
    logger.debug('#NATIVE_SERVER# audio is muted... ' + JSON.stringify(result));
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 5.5 SET_AUDIO_MUTE cmd data to app');
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 5.5 SET_AUDIO_MUTE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.restoreAudioMute = function restoreAudioMute(cb) {
  logger.debug('#NATIVE_SERVER# restoreAudioMute');
  var self = this;
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x05;
  buffer[7] = 0x06;

  dsaServer.once('restoreAudioMute', function(result) {
    logger.debug('#NATIVE_SERVER# audio mute state is restored... ' + JSON.stringify(result));
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 5.6 RESTORE_AUDIO_MUTE cmd data to app');
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 5.6 RESTORE_AUDIO_MUTE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.getCurrentScreen = function getCurrentScreen(cb) {
  logger.debug('#NATIVE_SERVER# getCurrentScreen');
  var self = this;
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x01;
  buffer[7] = 0x0a;

  dsaServer.once('currentScreen', function(result) {
    /*
      result:
      0 is no mirror screen.
      1 is mirror main screen.
      2 is mirror extend screen.
    */
    logger.debug('#NATIVE_SERVER# current screen is ' + JSON.stringify(result));
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.10 GET_CURRENT_MIRROR_SCREEN_STATE cmd data to app');
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.10 GET_CURRENT_MIRROR_SCREEN_STATE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.getDeviceInfo = function getDeviceInfo(cb) {
  logger.debug('#NATIVE_SERVER# getDeviceInfo');
  var self = this,
    buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x06;
  buffer[7] = 0x01;

  dsaServer.once('getDeviceInfo', function(getDeviceInfo) {
    if (cb) {
      cb(getDeviceInfo);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending GET_DEVICE_INFO cmd data to app');
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 6.1 GET_DEVICE_INFO command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.checkInitFile = function checkInitFile(cb) {
  logger.debug('#NATIVE_SERVER# checkInitFile');
  var self = this,
    buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x06;
  buffer[7] = 0x02;

  dsaServer.once('checkInitFile', function(checkInitFile) {
    if (cb) {
      cb(checkInitFile);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 6.2 CHECK_INIT_FILE cmd data to app');
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 6.2 CHECK_INIT_FILE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.deleteInitFile = function deleteInitFile(cb) {
  logger.debug('#NATIVE_SERVER# deleteInitFile');
  var self = this,
    buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x06;
  buffer[7] = 0x03;

  dsaServer.once('deleteInitFile', function(deleteInitFile) {
    if (cb) {
      cb(deleteInitFile);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 6.3 DELETE_INIT_FILE cmd data to app');
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 6.3 DELETE_INIT_FILE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.checkLicenseFile = function checkLicenseFile(cb) {
  logger.debug('#NATIVE_SERVER# checkLicenseFile');
  var self = this,
    buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x06;
  buffer[7] = 0x04;

  dsaServer.once('checkLicenseFile', function(checkLicenseFile) {
    if (cb) {
      cb(checkLicenseFile);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 6.4 CHECK_LICENSE_FILE cmd data to app');
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 6.4 CHECK_LICENSE_FILE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.generateLicenseFile = function generateLicenseFile(cb) {
  logger.debug('#NATIVE_SERVER# generateLicenseFile');
  var self = this,
    buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x06;
  buffer[7] = 0x05;

  dsaServer.once('generateLicenseFile', function(generateLicenseFile) {
    if (cb) {
      cb(generateLicenseFile);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 6.5 GENERATE_LICENSE_FILE cmd data to app');
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send  6.5 GENERATE_LICENSE_FILE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.getScreenResolution = function getScreenResolution(cb) {
  logger.debug('#NATIVE_SERVER# getScreenResolution');
  var self = this,
    buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x01;
  buffer[7] = 0x01;

  dsaServer.once('getScreenResolution', function(screenResolution) {
    if (cb) {
      cb(screenResolution);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.1 GET_SCREEN_RESOLUTION cmd data to app, buffer: ' + JSON.stringify(buffer));
    //logger.debug(conn);
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.1 GET_SCREEN_RESOLUTION command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.setScreenResolution = function setScreenResolution(width, height, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# setScreenResolution, set width: ' + width + ' height:' + height);
  var widthBuffer = new Buffer(2);
  widthBuffer = dsaUtil.writeUIntBE(widthBuffer, width, 0, 2);
  var heightBuffer = new Buffer(2);
  heightBuffer = dsaUtil.writeUIntBE(heightBuffer, height, 0, 2);

  var buffer = new Buffer(settings.cmdLength + 4),
    dataLength = dsaUtil.convertIntToBytesBigEndian(4);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  //command
  buffer[6] = 0x01;
  //function
  buffer[7] = 0x02;
  //data
  buffer[8] = widthBuffer[0];
  buffer[9] = widthBuffer[1];
  buffer[10] = heightBuffer[0];
  buffer[11] = heightBuffer[1];

  dsaServer.once('setScreenResolution', function(setScreenResolution) {
    if (cb) {
      cb(setScreenResolution);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.2 SET_SCREEN_RESOLUTION cmd data to app');
    logger.debug(JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.2 SET_SCREEN_RESOLUTION command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.restoreResolution = function restoreResolution(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# restoreResolution');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x01;
  buffer[7] = 0x03;

  dsaServer.once('restoreResolution', function(restoreResolution) {
    if (cb) {
      return cb(restoreResolution);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.3 RESTORE_LAST_SCREEN_RESOLUTION cmd to app');
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.3 RESTORE_LAST_SCREEN_RESOLUTION command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.aeroModeEnable = function aeroModeEnable(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# aeroModeEnable');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x01;
  buffer[7] = 0x04;

  dsaServer.once('aeroModeEnable', function(aeroModeEnable) {
    if (cb) {
      cb(aeroModeEnable);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.4 SET_AERO_MODE_ENABLE cmd to app');
    // logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.4 SET_AERO_MODE_ENABLE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.aeroModeDisable = function aeroModeDisable(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# aeroModeDisable');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x01;
  buffer[7] = 0x05;

  dsaServer.once('aeroModeDisable', function(aeroModeDisable) {
    if (cb) {
      cb(aeroModeDisable);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.5 SET_AERO_MODE_DISABLE cmd to app');
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.5 SET_AERO_MODE_DISABLE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.powerSavingDisable = function powerSavingDisable(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# powerSavingDisable');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x03;
  buffer[7] = 0x01;

  dsaServer.once('powerSavingDisable', function(powerSavingDisable) {
    if (cb) {
      cb(powerSavingDisable);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 3.1 SET_POWER_KEEP_ALIVE cmd to app');
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 3.1 SET_POWER_KEEP_ALIVE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.powerSavingEnable = function powerSavingEnable(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# powerSavingEnable');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x03;
  buffer[7] = 0x02;

  dsaServer.once('powerSavingEnable', function(powerSavingEnable) {
    if (cb) {
      cb(powerSavingEnable);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending cmd to app');
    // logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 3.2 SET_POWER_NORMAL command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.desktopStreamingStart = function desktopStreamingStart(rvaVersion, encrypt, rvaIP, port, framerate, bitrate, audio, key, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# desktopStreamingStart: rvaVersion: ' + rvaVersion + ' encrypt: ' + encrypt + ' rvaIP:' + rvaIP + ' port:' + port + ' framerate:' + framerate + ' bitrate:' + bitrate + ' audio:' + audio + ' key:' + JSON.stringify(key));
  // construct data portion and calculate data length
  var dataLength = 14;
  if (encrypt) {
    dataLength = 30;
  }

  var buffer = new Buffer(settings.cmdLength + dataLength);
  dataLength = dsaUtil.convertIntToBytesBigEndian(dataLength);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x04;
  buffer[7] = 0x01;
  rvaVersion = rvaVersion.replace('v', '').split('.');
  logger.debug('desktopStreamingStart rvaVersion: ' + JSON.stringify(rvaVersion));
  buffer[8] = rvaVersion[0];
  buffer[9] = rvaVersion[1];

  if (rvaVersion.length === 3) {
    buffer[10] = rvaVersion[2];
  } else if (rvaVersion.length < 3) {
    buffer[10] = 0;
  }

  var rvaIPArray = rvaIP.split('.');
  // rva IP
  buffer[11] = rvaIPArray[0];
  buffer[12] = rvaIPArray[1];
  buffer[13] = rvaIPArray[2];
  buffer[14] = rvaIPArray[3];
  var portBuffer = new Buffer(2);
  portBuffer = dsaUtil.writeUIntBE(portBuffer, port, 0, 2);
  // port
  buffer[15] = portBuffer[0];
  buffer[16] = portBuffer[1];
  // frame rate
  buffer[17] = framerate;
  var bitrateBuffer = new Buffer(2);
  bitrateBuffer = dsaUtil.writeUIntBE(bitrateBuffer, bitrate, 0, 2);
  // bit rate
  buffer[18] = bitrateBuffer[0];
  buffer[19] = bitrateBuffer[1];
  // audio
  buffer[20] = audio;

  if (encrypt) {
    buffer[21] = 0x01;
    for (var j = 0; j < 16; j++) {
      buffer[22 + j] = key[j];
    }
  } else {
    buffer[21] = 0x00;
  }

  dsaServer.once('desktopStreamingStart', function(result) {
    logger.debug('desktopStreamingStart: ' + result);
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# 4.1 SET_VIDEO_FULL_MODE_START data: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 4.1 SET_VIDEO_FULL_MODE_START command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }

};

exports.desktopStreamingStop = function desktopStreamingStop(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# desktopStreamingStop');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x04;
  buffer[7] = 0x02;

  dsaServer.once('desktopStreamingStop', function(result) {
    logger.debug('desktopStreamingStop: ' + result);
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 4.2 SET_VIDEO_FULL_MODE_STOP cmd to app');
    // logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 4.2 SET_VIDEO_FULL_MODE_STOP command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.splitModeStart = function splitModeStart(encrypt, rvaIP, port, framerate, key, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# splitModeStart, encrypt:' + encrypt + ' port:' + port + ' framerate:' + framerate + ' key:' + key);
  // construct data portion and calculate data length
  var dataLength = 8;
  if (encrypt) {
    dataLength = 24;
  }

  var buffer = new Buffer(settings.cmdLength + dataLength);
  dataLength = dsaUtil.convertIntToBytesBigEndian(dataLength);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x04;
  buffer[7] = 0x03;
  var rvaIPArray = rvaIP.split('.');
  // rva IP
  buffer[8] = rvaIPArray[0];
  buffer[9] = rvaIPArray[1];
  buffer[10] = rvaIPArray[2];
  buffer[11] = rvaIPArray[3];
  var portBuffer = new Buffer(2);
  portBuffer = dsaUtil.writeUIntBE(portBuffer, port, 0, 2);
  // port
  buffer[12] = portBuffer[0];
  buffer[13] = portBuffer[1];
  //framerate
  // var framerateBuffer = new Buffer(1);
  // framerateBuffer = framerate;
  buffer[14] = framerate;
  //logger.debug('framerateBuffer: ' + JSON.stringify(framerateBuffer));

  if (encrypt) {
    buffer[15] = 0x01;
    for (var q = 0; q < 16; q++) {
      buffer[16 + q] = key[q];
    }
  } else {
    buffer[15] = 0x00;
  }

  dsaServer.once('splitModeStart', function(splitModeStart) {
    if (cb) {
      cb(splitModeStart);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# 4.3 SET_IMAGE_SPLIT_MODE_START data: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 4.3 SET_IMAGE_SPLIT_MODE_START command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.splitModeStop = function splitModeStop(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# splitModeStop');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x04;
  buffer[7] = 0x04;

  dsaServer.once('splitModeStop', function(splitModeStop) {
    if (cb) {
      cb(splitModeStop);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 4.4 SET_IMAGE_SPLIT_MODE_STOP cmd to app');
    //logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 4.4 SET_IMAGE_SPLIT_MODE_STOP command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.splitModePause = function splitModePause(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# splitModePause');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x04;
  buffer[7] = 0x05;

  dsaServer.once('splitModePause', function(splitModePause) {
    if (cb) {
      cb(splitModePause);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 4.5 SET_IMAGE_SPLIT_MODE_PAUSE cmd to app');
    //  logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 4.5 SET_IMAGE_SPLIT_MODE_PAUSE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.splitModeResume = function splitModeResume(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# splitModeResume');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x04;
  buffer[7] = 0x06;

  dsaServer.once('splitModeResume', function(splitModeResume) {
    if (cb) {
      cb(splitModeResume);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 4.6 SET_IMAGE_SPLIT_MODE_RESUME cmd to app');
    // logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 4.6 SET_IMAGE_SPLIT_MODE_RESUME command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.getPreviewImage = function getPreviewImage(width, height, currentScreen, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# getPreviewImage');
  var widthBuffer = new Buffer(2);
  widthBuffer = dsaUtil.writeUIntBE(widthBuffer, width, 0, 2);
  var heightBuffer = new Buffer(2);
  heightBuffer = dsaUtil.writeUIntBE(heightBuffer, height, 0, 2);

  var buffer = new Buffer(settings.cmdLength + 5),
    dataLength = dsaUtil.convertIntToBytesBigEndian(5);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  //command
  buffer[6] = 0x04;
  //function
  buffer[7] = 0x07;
  //data
  buffer[8] = widthBuffer[0];
  buffer[9] = widthBuffer[1];
  buffer[10] = heightBuffer[0];
  buffer[11] = heightBuffer[1];

  if (currentScreen === settings.screenType.MAIN) {
    buffer[12] = 0;
  } else {
    buffer[12] = 1;
  }

  dsaServer.once('getPreviewImage', function(previewImage) {
    if (cb) {
      cb(previewImage);
    }
  });

  if (conn) {
    logger.debug('width: x' + widthBuffer[0].toString(16) + widthBuffer[1].toString(16));
    logger.debug('height: x' + heightBuffer[0].toString(16) + heightBuffer[1].toString(16));
    logger.debug('#NATIVE_SERVER# sending 4.7 GET_PREVIEW_IMAGE_DATA cmd data to app: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 4.7 GET_PREVIEW_IMAGE_DATA command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.setSplitFrameRate = function setSplitFrameRate(rate, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# getPreviewImage');
  var buffer = new Buffer(settings.cmdLength + 1),
    dataLength = dsaUtil.convertIntToBytesBigEndian(1);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  //command
  buffer[6] = 0x04;
  //function
  buffer[7] = 0x08;
  //data
  buffer[8] = rate;

  dsaServer.once('setSplitFrameRate', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 4.8 UPDATE_IMAGE_SPLIT_MODE_FRAME_RATE cmd data to app: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 4.8 UPDATE_IMAGE_SPLIT_MODE_FRAME_RATE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.remoteControlStart = function remoteControlStart(rvaIP, remoteControlPort, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# remoteControlStart');
  var dataLength = dsaUtil.convertIntToBytesBigEndian(6),
    buffer = new Buffer(settings.cmdLength + 6);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  //command
  buffer[6] = 0x02;
  //function
  buffer[7] = 0x01;

  var rvaIPArray = rvaIP.split('.');
  // rva IP
  buffer[8] = rvaIPArray[0];
  buffer[9] = rvaIPArray[1];
  buffer[10] = rvaIPArray[2];
  buffer[11] = rvaIPArray[3];
  var portBuffer = new Buffer(2);
  portBuffer = dsaUtil.writeUIntBE(portBuffer, remoteControlPort, 0, 2);
  // port
  buffer[12] = portBuffer[0];
  buffer[13] = portBuffer[1];

  dsaServer.once('remoteControlStart', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 2.1 SET_REMOTE_CONTROL_CONNECT cmd data to app: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 2.1 SET_REMOTE_CONTROL_CONNECT command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.remoteControlStop = function remoteControlStop(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# remoteControlStop');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x02;
  buffer[7] = 0x02;

  dsaServer.once('remoteControlStop', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending SET_REMOTE_CONTROL_DISCONNECT cmd to app');
    // logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 2.2 SET_REMOTE_CONTROL_DISCONNECT command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.getRemoteControlState = function getRemoteControlState() {
  var self = this;
  logger.debug('#NATIVE_SERVER# getRemoteControlState');

  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  //command
  buffer[6] = 0x02;
  //function
  buffer[7] = 0x03;

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending getRemoteControlState cmd data to app: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 2.3 GET_REMOTE_CONTROL_STATE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.exit = function exit(cb) {
  logger.debug('killing Dsa Native service...' + process.platform);
  // ps -ef | grep -v grep |grep Dsa| awk '{ print $2 }'n
  dsaServer.close(function() {
    logger.debug('server shut down');
  });
  if (process.platform === 'darwin') {
    logger.debug('nativeService.pid: ' + nativeService.pid);
    exec('kill -9 ' + nativeService.pid, function(error, stdout, stderr) {
      logger.debug(stdout);
      logger.debug(stderr);
      if (error) {
        logger.debug(error);
      }
      if (cb) {
        cb();
      }
    });
  } else if (process.platform === 'win32') {
    logger.debug('2 kill native, killing Dsa Native service...');
    nativeService.kill();
    if (cb) {
      cb();
    }
  }
};

exports.getSoundflowerState = function getSoundflowerState(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# getSoundflowerState');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x05;
  buffer[7] = 0x01;

  dsaServer.once('getSoundflowerState', function(getSoundflowerState) {
    if (cb) {
      cb(getSoundflowerState);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending GET_IS_SOUNDFLOWER_OUTPUT cmd to app');
    // logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 5.1 GET_IS_SOUNDFLOWER_OUTPUT command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.soundflowerStateEnable = function soundflowerStateEnable(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# soundflowerStateEnable');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x05;
  buffer[7] = 0x02;

  dsaServer.once('soundflowerStateEnable', function(soundflowerStateEnable) {
    if (cb) {
      cb(soundflowerStateEnable);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending SET_SOUNDFLOWER_OUTPUT_ENABLE cmd to app: ' + JSON.stringify(buffer));
    // logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 5.2 SET_SOUNDFLOWER_OUTPUT_ENABLE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.soundflowerStateDisable = function soundflowerStateDisable(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# soundflowerStateDisable');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x05;
  buffer[7] = 0x03;

  dsaServer.once('soundflowerStateDisable', function(soundflowerStateDisable) {
    if (cb) {
      cb(soundflowerStateDisable);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending SET_SOUNDFLOWER_OUTPUT_DISABLE cmd to app');
    // logger.debug(buffer);
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 5.3 SET_SOUNDFLOWER_OUTPUT_DISABLE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.getMiracastSupport = function getMiracastSupport(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# getMiracastSupport');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x01;
  buffer[7] = 0x06;

  dsaServer.once('miracastSupport', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.6 GET_IS_SUPPORT_MIRACAST cmd data to app, buffer: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.6 GET_IS_SUPPORT_MIRACAST command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.getAirplaySupport = function getAirplaySupport(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# getAirplaySupport');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x01;
  buffer[7] = 0x07;

  dsaServer.once('airplaySupport', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.7 GET_IS_SUPPORT_AIRPLAY cmd data to app');
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.7 GET_IS_SUPPORT_AIRPLAY command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.saveDefaultExtendMode = function saveDefaultExtendMode(cb){
  var self = this;
  logger.debug('#NATIVE_SERVER# saveDefaultExtendMode');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x01;
  buffer[7] = 0x0B;
  dsaServer.once('saveDefaultExtendMode', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.11 SAVE_USER_DEFAULT_EXTEND_MODE cmd data to app');
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.11 SAVE_USER_DEFAULT_EXTEND_MODE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};


exports.getExtendedScreenSupport = function getExtendedScreenSupport(cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# getExtendedScreenSupport');
  var buffer = new Buffer(settings.cmdLength),
    dataLength = dsaUtil.convertIntToBytesBigEndian(0);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }

  buffer[6] = 0x01;
  buffer[7] = 0x08;

  dsaServer.once('extendedScreenSupport', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.8 GET_IS_SUPPORT_EXTEND_SCREEN_MODE cmd data to app');
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.8 GET_IS_SUPPORT_EXTEND_SCREEN_MODE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

/***
SET_EXTEND_SCREEN_MODE
Enable extend command:
State = 1, Direction = 0, Width=1280, Height=720

Disable extend command:
State = 0, Direction = 0, Width=1280, Height=720

Restore extend command (use this for disconnect):
State = 0, Direction = 0, Width=0, Height=0
***/
exports.setExtendedScreenMode = function setExtendedScreenMode(enable, direction, width, height, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# setExtendedScreenMode, enable: ' + enable + ' direction ' + direction + ' w:' + width + ' h:' + height);
  var buffer = new Buffer(settings.cmdLength + 6);
  var widthBuffer = new Buffer(2);
  widthBuffer = dsaUtil.writeUIntBE(widthBuffer, width, 0, 2);
  var heightBuffer = new Buffer(2);
  heightBuffer = dsaUtil.writeUIntBE(heightBuffer, height, 0, 2);

  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];

  buffer[2] = 0;
  buffer[3] = 0;
  buffer[4] = 0;
  buffer[5] = 0x06;

  buffer[6] = 0x01;
  buffer[7] = 0x09;

  buffer[8] = enable;

  // direction Default Setting: 0
  // Right-side : 1
  // Left-side : 2
  // Up-side : 3
  // Down-side : 4

  buffer[9] = direction;

  buffer[10] = widthBuffer[0];
  buffer[11] = widthBuffer[1];

  buffer[12] = heightBuffer[0];
  buffer[13] = heightBuffer[1];

  dsaServer.once('setExtendedScreenMode', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 1.9 SET_EXTEND_SCREEN_MODE cmd data to app');
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 1.9 SET_EXTEND_SCREEN_MODE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

// Q-Launcher functions
exports.ledOn = function ledOn(led, duration, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# ledOn, led: ' + led + ' duration: ' + duration + ' * 100 ms');
  var dataLength = dsaUtil.convertIntToBytesBigEndian(2),
    buffer = new Buffer(settings.cmdLength + 2);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x07;
  buffer[7] = 0x01;
  buffer[8] = led;
  buffer[9] = duration;

  dsaServer.once('ledOn', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 7.1 SET_LED_ON cmd data to app, data: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 7.1 SET_LED_ON command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.ledOff = function ledOff(led, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# ledOff, led: ' + led);
  var dataLength = dsaUtil.convertIntToBytesBigEndian(1),
    buffer = new Buffer(settings.cmdLength + 1);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x07;
  buffer[7] = 0x02;
  buffer[8] = led;

  dsaServer.once('ledOff', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 7.2 SET_LED_OFF cmd data to app, data: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 7.2 SET_LED_OFF command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.ledFlash = function ledFlash(led, count, timer, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# ledFlash, led: ' + led + ' count: ' + count + ' timer: ' + timer + 'ms');
  var dataLength = dsaUtil.convertIntToBytesBigEndian(4),
    buffer = new Buffer(settings.cmdLength + 4);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x07;
  buffer[7] = 0x03;
  buffer[8] = led;
  buffer[9] = count;
  //var timerBuffer = dsaUtil.convertIntToBytesBigEndian(timer);
  var timerBuffer = new Buffer(2);
  dsaUtil.writeUIntBE(timerBuffer, timer, 0, 2);

  for (var i = 0; i < 2; i++) {
    buffer[10 + i] = timerBuffer[i];
  }

  dsaServer.once('ledFlash', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 7.3 SET_LED_FLASH cmd data to app, data: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 7.3 SET_LED_FLASH command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.ledGradientFlash = function ledGradientFlash(led, count, timer, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# ledGradientFlash, led: ' + led + ' count: ' + count + ' timer: ' + timer + 'ms');
  var dataLength = dsaUtil.convertIntToBytesBigEndian(4),
    buffer = new Buffer(settings.cmdLength + 4);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x07;
  buffer[7] = 0x04;
  buffer[8] = led;
  buffer[9] = count;
  var timerBuffer = new Buffer(2);
  dsaUtil.writeUIntBE(timerBuffer, timer, 0, 2);
  for (var i = 0; i < 2; i++) {
    buffer[10 + i] = timerBuffer[i];
  }

  dsaServer.once('ledGradientFlash', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 7.4 SET_LED_GRADIENT_FLASH cmd data to app, data: ' + JSON.stringify(buffer));
    conn.write(buffer);

  } else {
    logger.error('#NATIVE_SERVER# Failed to send 7.4 SET_LED_GRADIENT_FLASH, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

//7.5 SET_LED_CYCLE_FLASH
exports.ledCycleFlash = function ledCycleFlash(count, timer, cb) {
  var self = this,
    mode = 0x01;
  logger.debug('#NATIVE_SERVER# ledCycleFlash');
  var dataLength = dsaUtil.convertIntToBytesBigEndian(4),
    buffer = new Buffer(settings.cmdLength + 4);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x07;
  buffer[7] = 0x05;
  buffer[8] = mode;
  buffer[9] = count;

  var timerBuffer = new Buffer(2);
  dsaUtil.writeUIntBE(timerBuffer, timer, 0, 2);
  logger.debug('timerBuffer: ' + JSON.stringify(timerBuffer));
  for (var i = 0; i < 2; i++) {
    buffer[10 + i] = timerBuffer[i];
  }

  dsaServer.once('ledCycleFlash', function(result) {
    if (cb) {
      cb(result);
    }
  });

  if (conn) {
    logger.debug('#NATIVE_SERVER# sending 7.5 SET_LED_CYCLE_FLASH cmd data to app, data: ' + JSON.stringify(buffer));
    conn.write(buffer);
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 7.5 SET_LED_CYCLE_FLASH, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

//7.6 REPORT_CONNECT_STATE
exports.rvaConnectionState = function rvaConnectionState(rvaConnect, cb) {
  var self = this;
  logger.debug('#NATIVE_SERVER# rvaConnectionState, rvaConnect: ' + rvaConnect);
  var dataLength = dsaUtil.convertIntToBytesBigEndian(1),
    buffer = new Buffer(settings.cmdLength + 1);
  buffer[0] = settings.header[0];
  buffer[1] = settings.header[1];
  for (var i = 0; i < 4; i++) {
    buffer[i + settings.header.length] = dataLength[i];
  }
  buffer[6] = 0x07;
  buffer[7] = 0x06;
  buffer[8] = rvaConnect;

  dsaServer.once('rvaConnectionState', function(result) {
    if (cb) {
      cb(result);
    }
  });

  logger.debug('1 nativeService.connected: ' + nativeService.connected);
  if (conn) {
    //  if(nativeService.connected){
    logger.debug('#NATIVE_SERVER# sending 7.6 REPORT_CONNECT_STATE cmd data to app, data: ' + JSON.stringify(buffer));
    conn.write(buffer);
    //    } else {
    //      logger.debug('2 nativeService.connected: ' + nativeService.connected);
    //     cb(false);
    //   }
  } else {
    logger.error('#NATIVE_SERVER# Failed to send 7.6 REPORT_CONNECT_STATE command, native socket is not connected!');
    self.emit('error', 'native socket is not connected!');
  }
};

exports.dsaServer = dsaServer;
