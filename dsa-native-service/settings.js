var nativeFileName = '',
  dataPath = '',
  logLevel = '',
  novoServer = '',
  novotestExists,
  fs = require('fs'),
  winston = require('winston'),
  moment = require('moment'),
  logger,
  maxLogFileSize = 2000000,
  path = require('path');

if (process.platform === 'win32') {
  dataPath = process.env.PUBLIC + '/Novo';
  nativeFileName = 'DSAService.exe';
  try {
    novotestExists = fs.statSync(process.env.HOMEDRIVE + process.env.HOMEPATH + '/novotest');
    if (novotestExists.isFile()) {
      logLevel = 'debug';
      logger = new(winston.Logger)({
        transports: [
          new(winston.transports.File)({
            name: 'info-file',
            filename: path.join(dataPath, 'dsa.log'),
            json: false,
            level: logLevel,
            'timestamp': function() {
              return moment().format('MM-DD HH:mm:ss:SSS');
            },
            maxsize: maxLogFileSize
          }),
          new(winston.transports.Console)({
            level: logLevel,
            'timestamp': function() {
              return moment().format('MM-DD HH:mm:ss:SSS');
            }
          })
        ]
      });
    }
  } catch (err) {
    if (err.code === 'ENOENT') {
      logLevel = 'error';
      logger = new(winston.Logger)({
        transports: [
          new(winston.transports.File)({
            name: 'info-file',
            filename: path.join(dataPath, 'dsa.log'),
            json: false,
            level: logLevel,
            'timestamp': function() {
              return moment().format('MM-DD HH:mm:ss:SSS');
            },
            maxsize: maxLogFileSize
          }),
          new(winston.transports.Console)({
            level: logLevel,
            'timestamp': function() {
              return moment().format('MM-DD HH:mm:ss:SSS');
            }
          })
        ]
      });
    } else
      console.error(err);
  }
}

if (process.platform === 'darwin') {
  dataPath = process.env.HOME + '/Library/Logs/Novo';
  nativeFileName = 'DsaNativeService.app/Contents/MacOS/DsaNativeService';
  try {
    novotestExists = fs.statSync(process.env.HOME + '/novotest');
    if (novotestExists.isFile()) {
      logLevel = 'debug';
      logger = new(winston.Logger)({
        transports: [
          new(winston.transports.File)({
            name: 'info-file',
            filename: path.join(dataPath, 'dsa.log'),
            json: false,
            level: logLevel,
            'timestamp': function() {
              return moment().format('MM-DD HH:mm:ss:SSS');
            },
            maxsize: maxLogFileSize
          }),
          new(winston.transports.Console)({
            level: logLevel,
            'timestamp': function() {
              return moment().format('MM-DD HH:mm:ss:SSS');
            }
          })
        ]
      });
    }
  } catch (err) {
    if (err.code === 'ENOENT') {
      novotestExists = false;
      logLevel = 'error';
      logger = new(winston.Logger)({
        transports: [
          new(winston.transports.File)({
            name: 'info-file',
            filename: path.join(dataPath, 'dsa.log'),
            json: false,
            level: logLevel,
            'timestamp': function() {
              return moment().format('MM-DD HH:mm:ss:SSS');
            },
            maxsize: maxLogFileSize
          }),
          new(winston.transports.Console)({
            level: logLevel,
            'timestamp': function() {
              return moment().format('MM-DD HH:mm:ss:SSS');
            }
          })
        ]
      });
    } else
      console.error(err);
  }
}

exports.logLevel = logLevel;
exports.novoServer = novoServer;
exports.dataPath = dataPath;
exports.params = {
  program: process.cwd() + '/nativeApp/' + nativeFileName,
  programArgs: ['-service_port', '20567'], // this is the arguments passed to the program
  serverPort: '20567' // this is the tcp port that native code used to communicate with nodejs
};

exports.screenType = {
  MAIN: '1',
  EXT: '2'
};

exports.header = [0xA5, 0x18];
exports.cmdLength = 8;
exports.CMD = {
  GET_SCREEN_RESOLUTION: '1.1',
  SET_SCREEN_RESOLUTION: '1.2',
  RESTORE_LAST_SCREEN_RESOLUTION: '1.3',
  SET_AERO_MODE_ENABLE: '1.4',
  SET_AERO_MODE_DISABLE: '1.5',
  GET_IS_SUPPORT_MIRACAST: '1.6',
  GET_IS_SUPPORT_AIRPLAY: '1.7',
  GET_IS_SUPPORT_EXTEND_SCREEN_MODE: '1.8',
  SET_EXTEND_SCREEN_MODE: '1.9',
  GET_CURRENT_MIRROR_SCREEN_STATE: '1.10',
  SAVE_USER_DEFAULT_EXTEND_MODE: '1.11',
  SET_REMOTE_CONTROL_CONNECT: '2.1',
  SET_REMOTE_CONTROL_DISCONNECT: '2.2',
  GET_REMOTE_CONTROL_STATE: '2.3',
  SET_POWER_KEEP_ALIVE: '3.1',
  SET_POWER_NORMAL: '3.2',
  SET_VIDEO_FULL_MODE_START: '4.1',
  SET_VIDEO_FULL_MODE_STOP: '4.2',
  SET_IMAGE_SPLIT_MODE_START: '4.3',
  SET_IMAGE_SPLIT_MODE_STOP: '4.4',
  SET_IMAGE_SPLIT_MODE_PAUSE: '4.5',
  SET_IMAGE_SPLIT_MODE_RESUME: '4.6',
  GET_PREVIEW_IMAGE_DATA: '4.7',
  UPDATE_IMAGE_SPLIT_MODE_FRAME_RATE: '4.8',
  GET_IS_SOUNDFLOWER_OUTPUT: '5.1',
  SET_SOUNDFLOWER_OUTPUT_ENABLE: '5.2',
  SET_SOUNDFLOWER_OUTPUT_DISABLE: '5.3',
  SET_AUDIO_MUTE: '5.5',
  RESTORE_AUDIO_MUTE: '5.6',
  GET_DEVICE_INFO: '6.1',
  CHECK_INIT_FILE: '6.2',
  DELETE_INIT_FILE: '6.3',
  CHECK_LICENSE_FILE: '6.4',
  GENERATE_LICENSE_FILE: '6.5',
  SET_LED_ON: '7.1',
  SET_LED_OFF: '7.2',
  SET_LED_FLASH: '7.3',
  SET_LED_GRADIENT_FLASH: '7.4',
  SET_LED_CYCLE_FLASH: '7.5',
  REPORT_CONNECT_STATE: '7.6',
  LAUNCHER_COMMAND: '7.129',
  KS_RV_ERROR: '0.0'
};

exports.LED = {
  FULL_SCREEN_BLUE: 0x00,
  SPLIT_1: 0x01,
  SPLIT_2: 0x02,
  SPLIT_3: 0x03,
  SPLIT_4: 0x04,
  FULL_SCREEN_RED: 0x10,
  ALL: 0xFF
};

exports.LAUNCHER_CMD = {
  FULL_SCREEN: 0x00,
  SPLIT_1: 0x01,
  SPLIT_2: 0x02,
  SPLIT_3: 0x03,
  SPLIT_4: 0x04,
  DISCONNECT: 0x10,
  USB_PLUG_IN: 0x20,
  USB_PLUG_OUT: 0x21
};

exports.deviceType = {
  USB: '1',
  HDD: '2'
};
exports.maxLogFileSize = maxLogFileSize;
exports.logger = logger;
exports.fullNativeLog = true;
