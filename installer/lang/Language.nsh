
;PRODUCT_VERSION Get from parameter
;!define PRODUCT_VERSION "1.5.0.18852"

;------------------------------------------------------------------------
; English
;------------------------------------------------------------------------
LicenseLangString License ${LANG_ENGLISH} "license\Software License Statement- bundle with Delta HW__English.rtf"

LangString ProductName ${LANG_ENGLISH} "Novo"
LangString AppName ${LANG_ENGLISH} "Desktop Streamer"
LangString BrandText ${LANG_ENGLISH} "$(AppName) version ${PRODUCT_VERSION}"

LangString WelcomeTitle ${LANG_ENGLISH} "Welcome to $(AppName) Setup"
LangString WelcomeText ${LANG_ENGLISH} "\n\nSetup will guide you through the installation of $(AppName).\n\nIt is recommended that you close all other applications before proceeding further. This will make it possible to update relevant system files without having to reboot your computer.\n\nClick Next to continue."
LangString LicenseHeaderText ${LANG_ENGLISH} "License Agreement"
LangString LicenseHeaderSubText ${LANG_ENGLISH} "Please reviewe the license terms before installing $(AppName)."
LangString LicenseTextButton ${LANG_ENGLISH} "If you accept the terms of the agreement, select the first option below. You must accept the agreement to install $(AppName) version ${PRODUCT_VERSION}. Click Next to continue."
LangString LicenseRadioAccept ${LANG_ENGLISH} "Accept"
LangString LicenseRadioDeline ${LANG_ENGLISH} "Not accept"
LangString DirectoryHeaderText ${LANG_ENGLISH} "Installation Options"
LangString DirectoryHeaderSubText ${LANG_ENGLISH} "Select folder where $(AppName) files will be installed."
LangString InstFilesHeaderText ${LANG_ENGLISH} "Installing software componets"
LangString InstFilesHeaderSubText ${LANG_ENGLISH} "Please wait while Setup installs the software components. This may take several minutes."
LangString FinishTitle ${LANG_ENGLISH} "Congratulations!"
LangString FinishText ${LANG_ENGLISH} "$(AppName) version ${PRODUCT_VERSION} has been successfully installed.\n\nClick Finish to quit this installer."
LangString Uninstall ${LANG_ENGLISH} "Uninstall $(AppName)"
LangString UninstallerHeaderText ${LANG_ENGLISH} "Uninstalling"
LangString UninstallerHeaderSubText ${LANG_ENGLISH} "Please wait while $(AppName) is being uninstalled."
LangString UninstallInit ${LANG_ENGLISH} "Are you sure you want to completely remove $(AppName) and all of its components?"
LangString UninstallSuccess ${LANG_ENGLISH} "$(AppName) was successfully removed from your computer."
LangString V1_5_Uninstall ${LANG_ENGLISH} "Uninstall"

;------------------------------------------------------------------------
; Simplied Chinese
;------------------------------------------------------------------------
LicenseLangString License ${LANG_SIMPCHINESE} "license\Software License Statement- bundle with Delta HW__Simplified Chinese.rtf"

LangString ProductName ${LANG_SIMPCHINESE} "畅享汇"
LangString AppName ${LANG_SIMPCHINESE} "畅享汇"
LangString BrandText ${LANG_SIMPCHINESE} "$(ProductName) ${PRODUCT_VERSION}"

LangString WelcomeTitle ${LANG_SIMPCHINESE} "欢迎安装 $(ProductName)"
LangString WelcomeText ${LANG_SIMPCHINESE} "\n\n本程序将帮助引导您完成整个安装过程。\n\n建议您在安装之前关闭所有其它应用程序，以便自动更新相关的系统文件，从而无需重新启动计算机。\n\n按“下一步”键继续安装。"
LangString LicenseHeaderText ${LANG_SIMPCHINESE} "许可协议"
LangString LicenseHeaderSubText ${LANG_SIMPCHINESE} "安装 $(ProductName) 前请阅读许可条款"
LangString LicenseTextButton ${LANG_SIMPCHINESE} "如果您接受协议条款，请选择下面的第一个选项。您必须接受协议才能安装 $(ProductName) 版本 ${PRODUCT_VERSION}。单击“下一步”继续。"
LangString LicenseRadioAccept ${LANG_SIMPCHINESE} "同意"
LangString LicenseRadioDeline ${LANG_SIMPCHINESE} "不同意"
LangString DirectoryHeaderText ${LANG_SIMPCHINESE} "安装选项"
LangString DirectoryHeaderSubText ${LANG_SIMPCHINESE} "选择 $(ProductName) 文件安装的文件夹。"
LangString InstFilesHeaderText ${LANG_SIMPCHINESE} "安装软件组件"
LangString InstFilesHeaderSubText ${LANG_SIMPCHINESE} "整个安装过程可能需要几分钟的时间, 正在进行安装，请稍候…"
LangString FinishTitle ${LANG_SIMPCHINESE} "恭喜您！"
LangString FinishText ${LANG_SIMPCHINESE} "您已经成功安装 $(ProductName) 程序！\n\n请按“完成”键退出。"
LangString Uninstall ${LANG_SIMPCHINESE} "卸载$(ProductName)"
LangString UninstallerHeaderText ${LANG_SIMPCHINESE} "卸载中"
LangString UninstallerHeaderSubText ${LANG_SIMPCHINESE} "$(ProductName) 卸载中，请稍候…."
LangString UninstallInit ${LANG_SIMPCHINESE} "您确定要卸载软件 $(ProductName) 吗？"
LangString UninstallSuccess ${LANG_SIMPCHINESE} "已经成功卸载软件 $(ProductName)。"
LangString V1_5_Uninstall ${LANG_SIMPCHINESE} "卸载"

;------------------------------------------------------------------------
; Traditional Chinese
;------------------------------------------------------------------------
LicenseLangString License ${LANG_TRADCHINESE} "license\Software License Statement- bundle with Delta HW__Traditional Chinese.rtf"

LangString ProductName ${LANG_TRADCHINESE} "Novo"
LangString AppName ${LANG_TRADCHINESE} "Desktop Streamer"
LangString BrandText ${LANG_TRADCHINESE} "$(AppName) version ${PRODUCT_VERSION}"

LangString WelcomeTitle ${LANG_TRADCHINESE} "歡迎安裝 $(AppName)"
LangString WelcomeText ${LANG_TRADCHINESE} "\n\n本程式將説明引導您完成整個安裝過程。\n\n建議您在安裝之前關閉所有其它應用程式，以便自動更新相關的系統檔，從而無需重新開機電腦。\n\n按“下一步”鍵繼續安裝。"
LangString LicenseHeaderText ${LANG_TRADCHINESE} "授權合約"
LangString LicenseHeaderSubText ${LANG_TRADCHINESE} "請在安裝$(AppName)之前檢閱授權條款"
LangString LicenseTextButton ${LANG_TRADCHINESE} "若同意合約的條款，請選取以下的第一個選項。您必須接受合約，才可安裝$(AppName)版本 ${PRODUCT_VERSION}。按一下[下一步]繼續。"
LangString LicenseRadioAccept ${LANG_TRADCHINESE} "同意"
LangString LicenseRadioDeline ${LANG_TRADCHINESE} "不同意"
LangString DirectoryHeaderText ${LANG_TRADCHINESE} "安裝選項"
LangString DirectoryHeaderSubText ${LANG_TRADCHINESE} "選取將安裝$(AppName)檔案的資料夾。"
LangString InstFilesHeaderText ${LANG_TRADCHINESE} "安裝軟體元件"
LangString InstFilesHeaderSubText ${LANG_TRADCHINESE} "整個安裝過程可能需要幾分鐘的時間，正在進行安裝，請稍候…"
LangString FinishTitle ${LANG_TRADCHINESE} "恭喜您！"
LangString FinishText ${LANG_TRADCHINESE} "您已經成功安裝$(AppName)程式！\n\n請按“完成”鍵退出。"
LangString Uninstall ${LANG_TRADCHINESE} "卸載 $(AppName)"
LangString UninstallerHeaderText ${LANG_TRADCHINESE} "卸載中"
LangString UninstallerHeaderSubText ${LANG_TRADCHINESE} "$(AppName) 卸載中，請稍候…."
LangString UninstallInit ${LANG_TRADCHINESE} "您確認要解除安裝 $(AppName) 嗎？"
LangString UninstallSuccess ${LANG_TRADCHINESE} "已經完成解除安裝 $(AppName)"
LangString V1_5_Uninstall ${LANG_TRADCHINESE} "卸載"

;------------------------------------------------------------------------
; French
;------------------------------------------------------------------------
LicenseLangString License ${LANG_FRENCH} "license\Software License Statement- bundle with Delta HW__French.rtf"

LangString ProductName ${LANG_FRENCH} "Novo"
LangString AppName ${LANG_FRENCH} "Desktop Streamer"
LangString BrandText ${LANG_FRENCH} "$(AppName) version ${PRODUCT_VERSION}"

LangString WelcomeTitle ${LANG_FRENCH} "Bienvenue sur la configuration de $(AppName)"
LangString WelcomeText ${LANG_FRENCH} "\n\nLa configuration va vous guider à travers l'installation de $(AppName).\n\nIl est conseillé de fermer toutes les autres applications avant de poursuivre. Ceci permettra de mettre à jour les fichiers système nécessaires sans avoir à redémarrer votre ordinateur.\n\nCliquez sur Suivant pour continuer."
LangString LicenseHeaderText ${LANG_FRENCH} "Accord de licence"
LangString LicenseHeaderSubText ${LANG_FRENCH} "Veuillez lire les conditions de la licence avant d'installer $(AppName)"
LangString LicenseTextButton ${LANG_FRENCH} "Si vous acceptez les conditions de l'accord, sélectionnez la première option ci-dessous. Vous devez accepter l'accord pour installer $(AppName) version ${PRODUCT_VERSION}. Cliquez sur Suivant pour continuer."
LangString LicenseRadioAccept ${LANG_FRENCH} "Accepter"
LangString LicenseRadioDeline ${LANG_FRENCH} "Refuser"
LangString DirectoryHeaderText ${LANG_FRENCH} "Options d'installation"
LangString DirectoryHeaderSubText ${LANG_FRENCH} "Sélectionnez le dossier dans lequel les fichiers de $(AppName) seront installés."
LangString InstFilesHeaderText ${LANG_FRENCH} "Installation des composants logiciels"
LangString InstFilesHeaderSubText ${LANG_FRENCH} "Veuillez attendre pendant que le programme de configuration installe les composants logiciels. Ceci peut prendre quelques minutes."
LangString FinishTitle ${LANG_FRENCH} "Félicitations !"
LangString FinishText ${LANG_FRENCH} "$(AppName) version ${PRODUCT_VERSION} a été correctement installé.\n\nCliquez sur Terminer pour quitter ce programme d'installation."
LangString Uninstall ${LANG_FRENCH} "Désinstaller $(AppName)"
LangString UninstallerHeaderText ${LANG_FRENCH} "Désinstallation"
LangString UninstallerHeaderSubText ${LANG_FRENCH} "Veuillez patienter lors de la désinstallation de $(AppName)."
LangString UninstallInit ${LANG_FRENCH} "Êtes-vous sûr de vouloir totalement supprimer $(AppName) et tous ses composants ?"
LangString UninstallSuccess ${LANG_FRENCH} "$(AppName) a été supprimé de votre ordinateur."
LangString V1_5_Uninstall ${LANG_FRENCH} "Désinstaller"

;------------------------------------------------------------------------
; German
;------------------------------------------------------------------------
LicenseLangString License ${LANG_GERMAN} "license\Software License Statement- bundle with Delta HW__German.rtf"

LangString ProductName ${LANG_GERMAN} "Novo"
LangString AppName ${LANG_GERMAN} "Desktop Streamer"
LangString BrandText ${LANG_GERMAN} "$(AppName) version ${PRODUCT_VERSION}"

LangString WelcomeTitle ${LANG_GERMAN} "Willkommen zur Einrichtung von $(AppName)"
LangString WelcomeText ${LANG_GERMAN} "\n\nDer Einrichtungsvorgang führt Sie durch die Installation von $(AppName).\n\nEs wird empfohlen, vor dem Fortfahren alle anderen Anwendungen zu schließen. So können relevante Systemdateien aktualisiert werden, ohne dass der Computer neu gestartet werden muss.\n\nKlicken Sie auf Weiter, um fortzufahren."
LangString LicenseHeaderText ${LANG_GERMAN} "Lizenzvereinbarung"
LangString LicenseHeaderSubText ${LANG_GERMAN} "Bitte lesen Sie die Lizenzbedingungen, bevor Sie $(AppName) installieren."
LangString LicenseTextButton ${LANG_GERMAN} "Wenn Sie die Bedingungen der Vereinbarung akzeptieren, wählen Sie die erste Option unten. Sie müssen die Vereinbarung akzeptieren, um die $(AppName) Version ${PRODUCT_VERSION} zu installieren. Klicken Sie auf Weiter, um fortzufahren."
LangString LicenseRadioAccept ${LANG_GERMAN} "Akzeptieren"
LangString LicenseRadioDeline ${LANG_GERMAN} "Nicht akzeptieren"
LangString DirectoryHeaderText ${LANG_GERMAN} "Installationsoptionen"
LangString DirectoryHeaderSubText ${LANG_GERMAN} "Wählen Sie den Ordner aus, in dem $(AppName)-Dateien installiert werden sollen."
LangString InstFilesHeaderText ${LANG_GERMAN} "Softwarekomponenten werden installiert"
LangString InstFilesHeaderSubText ${LANG_GERMAN} "Bitte warten Sie, während die Softwarekomponenten installiert werden. Dies kann einige Minuten dauern."
LangString FinishTitle ${LANG_GERMAN} "Herzlichen Glückwunsch!"
LangString FinishText ${LANG_GERMAN} "$(AppName) Version ${PRODUCT_VERSION} wurde erfolgreich installiert.\n\nKlicken Sie auf Beenden, um das Installationsprogramm zu beenden."
LangString Uninstall ${LANG_GERMAN} "Deinstallieren $(AppName)"
LangString UninstallerHeaderText ${LANG_GERMAN} "Deinstalliere"
LangString UninstallerHeaderSubText ${LANG_GERMAN} "Bitte warten, während $(AppName) deinstalliert wird."
LangString UninstallInit ${LANG_GERMAN} "$(AppName) und sämtliche Komponenten wirklich vollständig entfernen?"
LangString UninstallSuccess ${LANG_GERMAN} "$(AppName) wurde erfolgreich von diesem Computer entfernt."
LangString V1_5_Uninstall ${LANG_GERMAN} "Deinstallieren"

;------------------------------------------------------------------------
; Russian
;------------------------------------------------------------------------
LicenseLangString License ${LANG_RUSSIAN} "license\Software License Statement- bundle with Delta HW__Russian.rtf"

LangString ProductName ${LANG_RUSSIAN} "Novo"
LangString AppName ${LANG_RUSSIAN} "Desktop Streamer"
LangString BrandText ${LANG_RUSSIAN} "$(AppName) version ${PRODUCT_VERSION}"

LangString WelcomeTitle ${LANG_RUSSIAN} "Вас приветствует программа установки $(AppName)"
LangString WelcomeText ${LANG_RUSSIAN} "\n\nПрограмма установки поможет вам установить приложение $(AppName). \n\nПеред продолжением установки рекомендуется закрыть все работающие приложения. Это позволит обновить важные системные файлы без перезагрузки компьютера.\n\nДля продолжения нажмите на кнопку Далее."
LangString LicenseHeaderText ${LANG_RUSSIAN} "Лицензионное соглашение"
LangString LicenseHeaderSubText ${LANG_RUSSIAN} "Перед установкой приложения $(AppName) ознакомьтесь с условиями лицензии"
LangString LicenseTextButton ${LANG_RUSSIAN} "Если вы согласны с условиями соглашения, выберите первый параметр, указанный ниже. Для установки версии ${PRODUCT_VERSION} приложения $(AppName) требуется принять условия соглашения. Для продолжения нажмите на кнопку Далее."
LangString LicenseRadioAccept ${LANG_RUSSIAN} "Принять"
LangString LicenseRadioDeline ${LANG_RUSSIAN} "Отклонить"
LangString DirectoryHeaderText ${LANG_RUSSIAN} "Параметры установки"
LangString DirectoryHeaderSubText ${LANG_RUSSIAN} "Выберите папку для установки файлов $(AppName)."
LangString InstFilesHeaderText ${LANG_RUSSIAN} "Установка программных компонентов"
LangString InstFilesHeaderSubText ${LANG_RUSSIAN} "Подождите завершения установки программных компонентов. Это может занять несколько минут."
LangString FinishTitle ${LANG_RUSSIAN} "Поздравляем!"
LangString FinishText ${LANG_RUSSIAN} "Установка приложения $(AppName) версии ${PRODUCT_VERSION} успешно завершена.\n\nНажмите на кнопку Завершить для выхода из программы установки."
LangString Uninstall ${LANG_RUSSIAN} "удалить $(AppName)"
LangString UninstallerHeaderText ${LANG_RUSSIAN} "Удаление"
LangString UninstallerHeaderSubText ${LANG_RUSSIAN} "Подождите, пока $(AppName) будет удален."
LangString UninstallInit ${LANG_RUSSIAN} "Вы уверены, что хотите полностью удалить программу $(AppName) и все ее компоненты?"
LangString UninstallSuccess ${LANG_RUSSIAN} "$(AppName) успешно удалена из вашего компьютера."
LangString V1_5_Uninstall ${LANG_RUSSIAN} "удалить"

;------------------------------------------------------------------------
; Japanese
;------------------------------------------------------------------------
LicenseLangString License ${LANG_JAPANESE} "license\Software License Statement- bundle with Delta HW__Japanese.rtf"

LangString ProductName ${LANG_JAPANESE} "Novo"
LangString AppName ${LANG_JAPANESE} "Desktop Streamer"
LangString BrandText ${LANG_JAPANESE} "$(AppName) version ${PRODUCT_VERSION}"

LangString WelcomeTitle ${LANG_JAPANESE} "デスクトップストリーマーのセットアップへようこそ"
LangString WelcomeText ${LANG_JAPANESE} "\n\nセットアップはデスクトップストリーマーのインストールをガイドします。\n\n先に進む前に他のすべてのアプリケーションを閉じることをお勧めします。 これにより、コンピュータを再起動することなく、関連するシステムファイルを更新できるようになります。\n\n次へをクリックして、続行します。"
LangString LicenseHeaderText ${LANG_JAPANESE} "ライセンス契約"
LangString LicenseHeaderSubText ${LANG_JAPANESE} "デスクトップストリーマーをインストールする前に、ライセンス条項を確認してください"
LangString LicenseTextButton ${LANG_JAPANESE} "契約の条項に同意する場合は、以下の最初のオプションを選択してください。 デスクトップストリーマーバージョン ${PRODUCT_VERSION} をインストールするには、契約に同意する必要があります。 次へをクリックして、続行します。"
LangString LicenseRadioAccept ${LANG_JAPANESE} "受入"
LangString LicenseRadioDeline ${LANG_JAPANESE} "拒否"
LangString DirectoryHeaderText ${LANG_JAPANESE} "インストールオプション"
LangString DirectoryHeaderSubText ${LANG_JAPANESE} "デスクトップストリーマーファイルをインストールするフォルダを選択します。"
LangString InstFilesHeaderText ${LANG_JAPANESE} "ソフトウェアコンポーネントのインストール"
LangString InstFilesHeaderSubText ${LANG_JAPANESE} "セットアップが、ソフトウェアコンポーネントをインストールしている間、お待ちください。 これには数分かかる場合があります。"
LangString FinishTitle ${LANG_JAPANESE} "おめでとうございます！"
LangString FinishText ${LANG_JAPANESE} "デスクトップストリーマーバージョン ${PRODUCT_VERSION} は、正常にインストールされました。\n\n完了をクリックして、このインストーラーを終了します。"
LangString Uninstall ${LANG_JAPANESE} "アンインストール $(AppName)"
LangString UninstallerHeaderText ${LANG_JAPANESE} "アンインストール中・・・"
LangString UninstallerHeaderSubText ${LANG_JAPANESE} "$(AppName)のアンインストール中… しばらくお待ち下さい。"
LangString UninstallInit ${LANG_JAPANESE} "$(AppName) とすべてのコンポーネントを完全に削除してもよろしいですか？"
LangString UninstallSuccess ${LANG_JAPANESE} "$(AppName) がコンピュータから正常に削除されました。"
LangString V1_5_Uninstall ${LANG_JAPANESE} "アンインストール"

;------------------------------------------------------------------------
; Spanish
;------------------------------------------------------------------------
LicenseLangString License ${LANG_SPANISH} "license\Software License Statement- bundle with Delta HW__Spanish.rtf"

LangString ProductName ${LANG_SPANISH} "Novo"
LangString AppName ${LANG_SPANISH} "Desktop Streamer"
LangString BrandText ${LANG_SPANISH} "$(AppName) version ${PRODUCT_VERSION}"

LangString WelcomeTitle ${LANG_SPANISH} "Bienvenido/a a la instalación de $(AppName)"
LangString WelcomeText ${LANG_SPANISH} "\n\nLa instalación lo guiará a través de la instalación de $(AppName).\n\nSe recomienda que cierre todas las aplicaciones antes de continuar. Esto permitirá actualizar los archivos del sistema necesarios sin tener que reiniciar el ordenador.\n\nHaga clic en Siguiente para continuar."
LangString LicenseHeaderText ${LANG_SPANISH} "Acuerdo de licencia"
LangString LicenseHeaderSubText ${LANG_SPANISH} "Examine los términos de la licencia antes de instalar $(AppName)."
LangString LicenseTextButton ${LANG_SPANISH} "Si acepta los términos del acuerdo, seleccione la primera opción que aparece a continuación. Deberá aceptar el acuerdo para instalar $(AppName) versión ${PRODUCT_VERSION}. Haga clic en Siguiente para continuar."
LangString LicenseRadioAccept ${LANG_SPANISH} "Aceptar"
LangString LicenseRadioDeline ${LANG_SPANISH} "No aceptar"
LangString DirectoryHeaderText ${LANG_SPANISH} "Opciones de instalación"
LangString DirectoryHeaderSubText ${LANG_SPANISH} "Seleccione la carpeta en la que se instalarán los archivos de $(AppName)."
LangString InstFilesHeaderText ${LANG_SPANISH} "Instalando componentes del software"
LangString InstFilesHeaderSubText ${LANG_SPANISH} "Espere mientras se realiza la instalación de los componentes del software. Este proceso podría demorarse varios minutos."
LangString FinishTitle ${LANG_SPANISH} "¡Felicitaciones!"
LangString FinishText ${LANG_SPANISH} "$(AppName) versión ${PRODUCT_VERSION} instalado de forma exitosa.\n\nHaga clic en Finalizar para salir del instalador."
LangString Uninstall ${LANG_SPANISH} "Desinstalar $(AppName)"
LangString UninstallerHeaderText ${LANG_SPANISH} "Desinstalando"
LangString UninstallerHeaderSubText ${LANG_SPANISH} "Espere mientras se desinstala el $(AppName)."
LangString UninstallInit ${LANG_SPANISH} "¿Seguro que desea eliminar por completo $(AppName) y todos sus componentes?"
LangString UninstallSuccess ${LANG_SPANISH} "$(AppName) se ha quitado con éxito de su ordenador."
LangString V1_5_Uninstall ${LANG_SPANISH} "Desinstalar"

;------------------------------------------------------------------------
; Polish
;------------------------------------------------------------------------
LicenseLangString License ${LANG_POLISH} "license\Software License Statement- bundle with Delta HW_Polish.rtf"

LangString ProductName ${LANG_POLISH} "Novo"
LangString AppName ${LANG_POLISH} "Desktop Streamer"
LangString BrandText ${LANG_POLISH} "$(AppName) version ${PRODUCT_VERSION}"

LangString WelcomeTitle ${LANG_POLISH} "Witamy w programie instalacyjnym aplikacji $(AppName)"
LangString WelcomeText ${LANG_POLISH} "\n\nProgram przeprowadzi Cię przez proces instalacji aplikacji $(AppName).\n\nZaleca się zamknięcie wszystkich innych aplikacji przed kontynuowaniem instalacji. Umożliwi to aktualizację wszystkich odpowiednich plików systemowych bez konieczności ponownego uruchamiania komputera.\n\nKliknij Dalej, aby kontynuować."
LangString LicenseHeaderText ${LANG_POLISH} "Umowa licencyjna"
LangString LicenseHeaderSubText ${LANG_POLISH} "Zapoznaj się z warunkami licencji przed zainstalowaniem aplikacji $(AppName)."
LangString LicenseTextButton ${LANG_POLISH} "Jeżeli zgadzasz się z postanowieniami umowy, wybierz pierwszą opcję poniżej. Aby zainstalować aplikację $(AppName) w wersji  ${PRODUCT_VERSION}, należy zaakceptować warunki licencji. Kliknij Dalej, aby kontynuować."
LangString LicenseRadioAccept ${LANG_POLISH} "Akceptuję"
LangString LicenseRadioDeline ${LANG_POLISH} "Nie akceptuję"
LangString DirectoryHeaderText ${LANG_POLISH} "Opcje instalacji"
LangString DirectoryHeaderSubText ${LANG_POLISH} "Wybierz folder, w którym zostaną zainstalowane pliki aplikacji $(AppName)."
LangString InstFilesHeaderText ${LANG_POLISH} "Instalowanie elementów oprogramowania"
LangString InstFilesHeaderSubText ${LANG_POLISH} "Zaczekaj, aż instalator zainstaluje elementy oprogramowania. Może to zająć klika minut."
LangString FinishTitle ${LANG_POLISH} "Gratulacje!"
LangString FinishText ${LANG_POLISH} "Aplikacja $(AppName) w wersji ${PRODUCT_VERSION} została poprawnie zainstalowana.\n\nKliknij Zakończ, aby opuścić program instalacyjny."
LangString Uninstall ${LANG_POLISH} "Odinstaluj aplikację $(AppName)"
LangString UninstallerHeaderText ${LANG_POLISH} "Odinstalowywanie"
LangString UninstallerHeaderSubText ${LANG_POLISH} "Poczekaj na zakończenie procesu usuwania aplikacji $(AppName)."
LangString UninstallInit ${LANG_POLISH} "Czy na pewno chcesz całkowicie usunąć aplikację $(AppName) i jej wszystkie elementy?"
LangString UninstallSuccess ${LANG_POLISH} "Aplikacja $(AppName) została poprawnie usunięta z komputera."
LangString V1_5_Uninstall ${LANG_POLISH} "Odinstaluj"
