SilentInstall silent
SilentUnInstall silent
# set the name of the installer
Outfile "..\build\win32\silent_setup.exe"
SetCompressor /SOLID lzma
!include x64.nsh
!include FileFunc.nsh
!define PRODUCT_NAME "$(ProductName)"
!define APP_NAME "$(AppName)"
!define PRODUCT_VERSION "version_number"
!define PRODUCT_PUBLISHER "Delta Electronics, Inc."
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\Novo Desktop Streamer.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

!define WINDOWS_INSTALLER_CHECK_MAINFOLDER "Software\Microsoft\Windows\CurrentVersion"
!define WINDOWS_INSTALLER_CHECK_KEY "SystemComponent"
!define WINDOWS_INSTALLER_CHECK_SUBFOLDER "Uninstall"
!define WINDOWS_INSTALLER_VERSION "KB893803"
!define WINDOWS_INSTALLER_PRODUCT_FOLDER "Software\Classes\Installer\Products"

!define FileCopy `!insertmacro FileCopy`
!macro FileCopy FilePath TargetDir
  CreateDirectory `${TargetDir}`
  CopyFiles `${FilePath}` `${TargetDir}`
!macroend

; MUI 1.67 compatible ------
!include "MUI.nsh"

; MUI Settings
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "images\Installation_S.bmp" ; optional
!define MUI_ABORTWARNING
!define MUI_WELCOMEFINISHPAGE_BITMAP "images\Installation_L.bmp"
!define MUI_WELCOMEPAGE_TITLE $(WelcomeTitle)
!define MUI_WELCOMEPAGE_TEXT $(WelcomeText)
!define MUI_ICON "icons\dsa_install.ico"
!define MUI_UNICON "icons\dsa_uninstall.ico"

; Language Selection Dialog Settings
!define MUI_LANGDLL_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_LANGDLL_REGISTRY_VALUENAME "NSIS:Language"

; Welcome page
!insertmacro MUI_PAGE_WELCOME
Var /GLOBAL lang
Var UninstallString
; License page
!define MUI_LICENSEPAGE_RADIOBUTTONS
!define MUI_PAGE_HEADER_TEXT $(LicenseHeaderText)
!define MUI_PAGE_HEADER_SUBTEXT $(LicenseHeaderSubText)
!define MUI_LICENSEPAGE_TEXT_BOTTOM $(LicenseTextButton)
!define MUI_LICENSEPAGE_RADIOBUTTONS_TEXT_ACCEPT $(LicenseRadioAccept)
!define MUI_LICENSEPAGE_RADIOBUTTONS_TEXT_DECLINE $(LicenseRadioDeline)
!insertmacro MUI_PAGE_LICENSE  $(license)

; Directory page
!define MUI_DIRECTORYPAGE_TEXT_TOP " "
!define MUI_PAGE_HEADER_TEXT $(DirectoryHeaderText)
!define MUI_PAGE_HEADER_SUBTEXT $(DirectoryHeaderSubText)
!define MUI_PAGE_CUSTOMFUNCTION_PRE "ChangeDefaultInstDir"
!insertmacro MUI_PAGE_DIRECTORY

; Instfiles page
!define MUI_PAGE_HEADER_TEXT $(InstFilesHeaderText)
!define MUI_PAGE_HEADER_SUBTEXT $(InstFilesHeaderSubText)
!insertmacro MUI_PAGE_INSTFILES

; Finish page
;!define MUI_FINISHPAGE_RUN "$INSTDIR\Novo Desktop Streamer.exe"
!define MUI_FINISHPAGE_TITLE $(FinishTitle)
!define MUI_FINISHPAGE_TEXT $(FinishText)
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!define MUI_UNPAGE_HEADER_TEXT $(UninstallerHeaderText)
!define MUI_UNPAGE_HEADER_SUBTEXT $(UninstallerHeaderSubText)
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English" ;first language is the default language
!verbose off
!include "lang\Language.nsh"
!verbose on
; MUI end ------

;Name "${APP_NAME} ${PRODUCT_VERSION}"
Name "$(BrandText)"
InstallDir "$PROGRAMFILES32\Novo"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""

Function .onInit
;nsExec::ExecToStack '"taskkill" /im DSAService.exe /im DesktopStreamer.exe'
ReadRegStr $0 HKLM "system\controlset001\control\nls\language" "Installlanguage"
${if} $0 == '0409'
${OrIf} $0 == '0809'
${OrIf} $0 == '0c09'
${OrIf} $0 == '2809'
${OrIf} $0 == '1009'
${OrIf} $0 == '2409'
${OrIf} $0 == '3c09'
${OrIf} $0 == '4009'
${OrIf} $0 == '3809'
${OrIf} $0 == '1809'
${OrIf} $0 == '2009'
${OrIf} $0 == '4409'
${OrIf} $0 == '1409'
${OrIf} $0 == '3409'
${OrIf} $0 == '4809'
${OrIf} $0 == '1c09'
${OrIf} $0 == '2c09'
${OrIf} $0 == '3009'
  ;MessageBox MB_ICONEXCLAMATION|MB_OK "$0 - English"
  StrCpy $UninstallString "Uninstall Desktop Streamer"
${EndIf}
${if} $0 == '0804'
${OrIf} $0 == '1404'
  ;MessageBox MB_ICONEXCLAMATION|MB_OK "$0 - 简体中文"
  StrCpy $UninstallString "卸载畅享汇"
${EndIf}

${if} $0 == '1004'
${OrIf} $0 == '0404'
${OrIf} $0 == '0c04'
  ;MessageBox MB_ICONEXCLAMATION|MB_OK "$0 - 繁體中文"
  StrCpy $UninstallString "卸载 Desktop Streamer"
${EndIf}

${if} $0 == '040c'
${OrIf} $0 == '080c'
${OrIf} $0 == '2c0c'
${OrIf} $0 == '0c0c'
${OrIf} $0 == '240c'
${OrIf} $0 == '300c'
${OrIf} $0 == '3c0c'
${OrIf} $0 == '140c'
${OrIf} $0 == '340c'
${OrIf} $0 == '180c'
${OrIf} $0 == '380c'
${OrIf} $0 == 'e40c'
${OrIf} $0 == '200c'
${OrIf} $0 == '280c'
${OrIf} $0 == '100c'
${OrIf} $0 == '1c0c'
  ;MessageBox MB_ICONEXCLAMATION|MB_OK "$0 - Français"
  StrCpy $UninstallString "Désinstaller Desktop Streamer"
${EndIf}

${if} $0 == '0407'
${OrIf} $0 == '0c07'
${OrIf} $0 == '1407'
${OrIf} $0 == '1007'
${OrIf} $0 == '0807'
  ;MessageBox MB_ICONEXCLAMATION|MB_OK "$0 - Deutsch"
  StrCpy $UninstallString "Deinstallieren Desktop Streamer"
${EndIf}

${if} $0 == '0419'
${OrIf} $0 == '0819'
  ;MessageBox MB_ICONEXCLAMATION|MB_OK "$0 - русский"
  StrCpy $UninstallString "удалить Desktop Streamer"
${EndIf}

${if} $0 == '0411'
  ;MessageBox MB_ICONEXCLAMATION|MB_OK "$0 - 日本語"
  StrCpy $UninstallString "アンインストール Desktop Streamer"
${EndIf}

${if} $0 == '0c0a'
${OrIf} $0 == '040a'
${OrIf} $0 == '2c0a'
${OrIf} $0 == '400a'
${OrIf} $0 == '340a'
${OrIf} $0 == '240a'
${OrIf} $0 == '140a'
${OrIf} $0 == '1c0a'
${OrIf} $0 == '300a'
${OrIf} $0 == '440a'
${OrIf} $0 == '100a'
${OrIf} $0 == '480a'
${OrIf} $0 == '580a'
${OrIf} $0 == '080a'
${OrIf} $0 == '4c0a'
${OrIf} $0 == '180a'
${OrIf} $0 == '3c0a'
${OrIf} $0 == '280a'
${OrIf} $0 == '500a'
${OrIf} $0 == '540a'
${OrIf} $0 == '380a'
${OrIf} $0 == '200a'
  ;MessageBox MB_ICONEXCLAMATION|MB_OK "$0 - Española"
  StrCpy $UninstallString "Desinstalar Desktop Streamer"
${EndIf}

${if} $0 == '0415'
  ;MessageBox MB_ICONEXCLAMATION|MB_OK "$0 - Polski"
  StrCpy $UninstallString "Odinstaluj aplikację Desktop Streamer"
${EndIf}
DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\畅享汇"
DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\Desktop Streamer"
DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\Novo"
InitPluginsDir
;File /oname=$PLUGINSDIR\splash.bmp "images\SplashForm.bmp"
;advsplash::show 1500 800 600 -1 $PLUGINSDIR\splash
;!insertmacro MUI_LANGDLL_DISPLAY
;Language selection dialog
;Push ""
;Push ${LANG_ENGLISH}
;Push "English"
;Push ${LANG_SIMPCHINESE}
;Push "简体中文"
;Push ${LANG_TRADCHINESE}
;Push "繁體中文"
;Push ${LANG_FRENCH}
;Push "Français"
;Push ${LANG_GERMAN}
;Push "Deutsch"
;Push ${LANG_RUSSIAN}
;Push "русский"
;Push ${LANG_JAPANESE}
;Push "日本語"
;Push ${LANG_SPANISH}
;Push "lengua española"
;Push A ; A means auto count languages
       ; for the auto count to work the first empty push (Push "") must remain
;LangDLL::LangDialog "Installer Language" "Please select the language of the installer"

Push ${LANG_ENGLISH}
Pop $LANGUAGE
StrCmp $LANGUAGE "cancel" 0 +2
  Abort
FunctionEnd

RequestExecutionLevel admin
# create a default section.
Section "MainSection" SEC01
  nsExec::ExecToStack '"taskkill" /im DSAService.exe /im DesktopStreamer.exe'
  ;MessageBox MB_OK "$INSTDIR\畅享汇"
  ;DetailPrint "$INSTDIR\Desktop Streamer"
  ;MessageBox MB_OK "$INSTDIR\Desktop Streamer"
  SetOutPath "$INSTDIR\Desktop Streamer"
  #Delete "..\build\win32\hdd\app\nativeApp\License.lic"
  RMDir /r "$INSTDIR\${PRODUCT_NAME}\Desktop Streamer"
  RMDir /r "$INSTDIR\Desktop Streamer"
  RMDir /r "$INSTDIR\畅享汇"
  RMDir /r "$INSTDIR\Novo Desktop Streamer.exe"
  RMDir /r "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*.*"
  SetShellVarContext all
  RMDir /r "$PROGRAMFILES\畅享汇"
  RMDir /r "$SMPROGRAMS\畅享汇"
  Delete "$DESKTOP\畅享汇.lnk"
  Delete "$DESKTOP\畅享汇 .lnk"

  SetShellVarContext all
  Delete "$DESKTOP\畅享汇.lnk"
  Delete "$DESKTOP\畅享汇 .lnk"
  RMDir /r "$INSTDIR\畅享汇"

  RMDir /r "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*.*"
  Delete "$SMPROGRAMS\畅享汇\畅享汇.lnk"
  Delete "$SMPROGRAMS\畅享汇\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\Novo\Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\Novo\Uninstall Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\*Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\卸载*.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\畅享汇.lnk"
  Delete "$SMPROGRAMS\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载*.lnk"
  SetShellVarContext all
  Delete "$DESKTOP\畅享汇.lnk"
  Delete "$DESKTOP\Novo Desktop Streamer.lnk"
  RMDir /r "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*.*"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\*Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\Desktop Streamer*.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\卸载*.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载*.lnk"
  SetShellVarContext current
  File /a /r "..\build\win32\hdd\*.*"

  ${Switch} $LANGUAGE
  ${Case} '1033'
    StrCpy $lang 'en-US'
    Push window.navigator.language #text to be replaced
    Push $\'en-US$\' #replace with
    Push all #replace all occurrences
    Push all #replace all occurrences


    ${Break}
  ${Case} '1028'
    StrCpy $lang 'zh-TW'
    Push window.navigator.language #text to be replaced
    Push $\'zh-TW$\' #replace with
    Push all #replace all occurrences
    Push all #replace all occurrences


    ${Break}
  ${Case} '1036'
    StrCpy $lang 'fr'
    Push window.navigator.language #text to be replaced
    Push $\'fr$\' #replace with
    Push all #replace all occurrences
    Push all #replace all occurrences


    ${Break}
  ${Case} '1031'
    StrCpy $lang 'ds'
    Push window.navigator.language #text to be replaced
    Push $\'de$\' #replace with
    Push all #replace all occurrences
    Push all #replace all occurrences


    ${Break}
  ${Case} '1049'
    StrCpy $lang 'ru'
    Push window.navigator.language #text to be replaced
    Push $\'ru$\' #replace with
    Push all #replace all occurrences
    Push all #replace all occurrences


    ${Break}
  ${Case} '1041'
    StrCpy $lang 'ja'
    Push window.navigator.language #text to be replaced
    Push $\'ja$\' #replace with
    Push all #replace all occurrences
    Push all #replace all occurrences


    ${Break}
  ${Case} '1034'
    StrCpy $lang 'es'
    Push window.navigator.language #text to be replaced
    Push $\'es$\' #replace with
    Push all #replace all occurrences
    Push all #replace all occurrences


    ${Break}
  ${Case} '2052'
    StrCpy $lang 'zh-CN'
    Push window.navigator.language #text to be replaced
    Push $\'zh-CN$\' #replace with
    Push all #replace all occurrences
    Push all #replace all occurrences


    ${Break}
  ${Default}
    ${Break}
  ${EndSwitch}

  SetShellVarContext all
  ${Switch} $LANGUAGE
  ${Case} '2052';zh-CN
    SetShellVarContext all
    CreateDirectory "$SMPROGRAMS\畅享汇"
    CreateShortCut "$SMPROGRAMS\畅享汇\畅享汇.lnk" "$INSTDIR\Desktop Streamer\Novo Desktop Streamer.exe"
    CreateShortCut "$DESKTOP\畅享汇.lnk" "$INSTDIR\Desktop Streamer\Novo Desktop Streamer.exe"
    ${Break}
  ${Default}
    SetShellVarContext all
    CreateDirectory "$SMPROGRAMS\${PRODUCT_NAME}"
    CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}.lnk" "$INSTDIR\Desktop Streamer\Novo Desktop Streamer.exe"
    CreateShortCut "$DESKTOP\${PRODUCT_NAME} ${APP_NAME}.lnk" "$INSTDIR\Desktop Streamer\Novo Desktop Streamer.exe"
    ${Break}
  ${EndSwitch}


  ${RefreshShellIcons}

  CreateDirectory C:\Users\Public\Novo
  nsExec::ExecToLog  'iCACLS "$INSTDIR\Desktop Streamer" /inheritance:r /grant :r "Administrators":(OI)(CI)F "Users":(OI)(CI)F "SYSTEM":(OI)(CI)F /t'
  nsExec::ExecToLog  'iCACLS "$INSTDIR\Desktop Streamer" /remove "BUILTIN"'
  nsExec::ExecToLog  'iCACLS "$INSTDIR\Desktop Streamer\*" /q /c /t /reset'
 ; DetailPrint "$INSTDIR\Desktop Streamer"
 ; Sleep 100000

  SetOutPath "$INSTDIR\Desktop Streamer\app\nativeApp\"
  ExecWait "$INSTDIR\Desktop Streamer\app\nativeApp\license.exe HDD"
  Delete "$INSTDIR\Desktop Streamer\app\nativeApp\license.exe"

  StrCpy $2 "\Novo\"
  SetShellVarContext all
  StrCpy $3 "$APPDATA$2"
  SetOutPath $3
  ${FileCopy} "$INSTDIR\Desktop Streamer\app\nativeApp\license.lic" $3
  Delete "$INSTDIR\Desktop Streamer\app\nativeApp\license.lic"

  #Remove old version
  RMDir /r "$PROGRAMFILES\NovoConnect"
  RMDir /r "$SMPROGRAMS\NovoConnect"

  Delete "$DESKTOP\NovoConnect Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\畅享汇.lnk"
  ;DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\畅享汇"
  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\NovoConnect"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\App Paths\NovoConnect Desktop Streamer.exe"
  ;DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\Novo"
  ;DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY_CN}"

SectionEnd

Section -AdditionalIcons
  SetOutPath "$INSTDIR\Desktop Streamer\app"
  File "icons\dsa_uninstall.ico"
  SetOutPath $INSTDIR

  ${Switch} $LANGUAGE
  ${Case} '2052'
    DetailPrint "Delete splashform"
    ; delete Engish splash image
    Delete "$INSTDIR\Desktop Streamer\app\SplashForm.bmp"
    ; rename CN splash image
    Rename "$INSTDIR\Desktop Streamer\app\SplashForm_CN.bmp" "$INSTDIR\Desktop Streamer\app\SplashForm.bmp"

    SetShellVarContext all
    CreateShortCut "$SMPROGRAMS\畅享汇\卸载畅享汇.lnk" "$INSTDIR\Desktop Streamer\uninstaller.exe"
     ${Break}
  ${Default}
    SetShellVarContext all
    CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\$UninstallString.lnk" "$INSTDIR\Desktop Streamer\uninstaller.exe"
    ${Break}
  ${EndSwitch}
  ${RefreshShellIcons}
SectionEnd

Section -Post
  ${Switch} $LANGUAGE
  ${Case} '2052'
    ; delete Engish splash image
    Delete "$INSTDIR\Desktop Streamer\SplashForm.bmp"
    ; rename CN splash image
    Rename "$INSTDIR\Desktop Streamer\app\SplashForm_CN.bmp" "$INSTDIR\Desktop Streamer\app\SplashForm.bmp"
    ${Break}
  ${EndSwitch}
  WriteUninstaller "$INSTDIR\Desktop Streamer\uninstaller.exe"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\Desktop Streamer\Novo Desktop Streamer.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\Desktop Streamer\uninstaller.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\Desktop Streamer\Novo Desktop Streamer.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
SectionEnd

Function un.onUninstSuccess
  HideWindow
  ;MessageBox MB_ICONINFORMATION|MB_OK $(UninstallSuccess)
FunctionEnd

Function un.onInit
;!insertmacro MUI_UNGETLANGUAGE
;  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 $(UninstallInit) IDYES +2
;  Abort
FunctionEnd

Section Uninstall
  nsExec::ExecToStack '"taskkill" /im DSAService.exe /im DesktopStreamer.exe'
  SetOutPath $TEMP
  DetailPrint "$INSTDIR"
  RMDir /r "$INSTDIR"

  StrCpy $0 "$INSTDIR\${PRODUCT_NAME}"
  Call un.DeleteDirIfEmpty

  StrCpy $0 "$PROGRAMFILES\${PRODUCT_NAME}"
  Call un.DeleteDirIfEmpty

  StrCpy $0 "$PROGRAMFILES\畅享汇"
  Call un.DeleteDirIfEmpty

  StrCpy $0 "$INSTDIR"
  Call un.DeleteDirIfEmpty

  RMDir /r "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*.*"
  SetShellVarContext all
  RMDir /r "$PROGRAMFILES\畅享汇"
  RMDir /r "$SMPROGRAMS\畅享汇"
  Delete "$DESKTOP\畅享汇.lnk"
  Delete "$DESKTOP\畅享汇 .lnk"

  SetShellVarContext all
  Delete "$DESKTOP\畅享汇.lnk"
  Delete "$DESKTOP\畅享汇 .lnk"
  RMDir /r "$INSTDIR\畅享汇"

  RMDir /r "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*.*"
  RMDir /r "$SMPROGRAMS\畅享汇"
  Delete "$SMPROGRAMS\畅享汇\畅享汇.lnk"
  Delete "$SMPROGRAMS\畅享汇\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\*Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\卸载*.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\畅享汇.lnk"
  Delete "$SMPROGRAMS\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载*.lnk"
  SetShellVarContext all
  Delete "$DESKTOP\畅享汇.lnk"
  Delete "$DESKTOP\Novo Desktop Streamer.lnk"
  RMDir /r "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*.*"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\*Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\Desktop Streamer*.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\卸载*.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\*Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载*.lnk"

  RMDir /r "$PROGRAMFILES\NovoConnect"
  RMDir /r "$SMPROGRAMS\NovoConnect"
  RMDir /r "$INSTDIR\畅享汇"
  Delete "$DESKTOP\NovoConnect Desktop Streamer.lnk"
  Delete "$SMPROGRAMS\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${APP_NAME}\卸载畅享汇.lnk"
  Delete "$SMPROGRAMS\畅享汇.lnk"

  StrCpy $0 "$SMPROGRAMS\${PRODUCT_NAME}"
  Call un.DeleteDirIfEmpty

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\NovoConnect"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\App Paths\NovoConnect Desktop Streamer.exe"
  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\畅享汇"
  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\Novo"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  SetAutoClose true
SectionEnd

Function "ChangeDefaultInstDir"
   StrCpy $INSTDIR "$PROGRAMFILES\Novo"
FunctionEnd

Function AdvReplaceInFile
Exch $0 ;file to replace in
Exch
Exch $1 ;number to replace after
Exch
Exch 2
Exch $2 ;replace and onwards
Exch 2
Exch 3
Exch $3 ;replace with
Exch 3
Exch 4
Exch $4 ;to replace
Exch 4
Push $5 ;minus count
Push $6 ;universal
Push $7 ;end string
Push $8 ;left string
Push $9 ;right string
Push $R0 ;file1
Push $R1 ;file2
Push $R2 ;read
Push $R3 ;universal
Push $R4 ;count (onwards)
Push $R5 ;count (after)
Push $R6 ;temp file name

  GetTempFileName $R6
  FileOpen $R1 $0 r ;file to search in
  FileOpen $R0 $R6 w ;temp file
   StrLen $R3 $4
   StrCpy $R4 -1
   StrCpy $R5 -1

loop_read:
 ClearErrors
 FileRead $R1 $R2 ;read line
 IfErrors exit

   StrCpy $5 0
   StrCpy $7 $R2

loop_filter:
   IntOp $5 $5 - 1
   StrCpy $6 $7 $R3 $5 ;search
   StrCmp $6 "" file_write1
   StrCmp $6 $4 0 loop_filter

StrCpy $8 $7 $5 ;left part
IntOp $6 $5 + $R3
IntCmp $6 0 is0 not0
is0:
StrCpy $9 ""
Goto done
not0:
StrCpy $9 $7 "" $6 ;right part
done:
StrCpy $7 $8$3$9 ;re-join

IntOp $R4 $R4 + 1
StrCmp $2 all loop_filter
StrCmp $R4 $2 0 file_write2
IntOp $R4 $R4 - 1

IntOp $R5 $R5 + 1
StrCmp $1 all loop_filter
StrCmp $R5 $1 0 file_write1
IntOp $R5 $R5 - 1
Goto file_write2

file_write1:
 FileWrite $R0 $7 ;write modified line
Goto loop_read

file_write2:
 FileWrite $R0 $R2 ;write unmodified line
Goto loop_read

exit:
  FileClose $R0
  FileClose $R1

   SetDetailsPrint none
  Delete $0
  Rename $R6 $0
  Delete $R6
   SetDetailsPrint lastused

Pop $R6
Pop $R5
Pop $R4
Pop $R3
Pop $R2
Pop $R1
Pop $R0
Pop $9
Pop $8
Pop $7
Pop $6
Pop $5
;These values are stored in the stack in the reverse order they were pushed
Pop $0
Pop $1
Pop $2
Pop $3
Pop $4
FunctionEnd

Function un.DeleteDirIfEmpty
  FindFirst $R0 $R1 "$0\*.*"
  strcmp $R1 "." 0 NoDelete
   FindNext $R0 $R1
   strcmp $R1 ".." 0 NoDelete
    ClearErrors
    FindNext $R0 $R1
    IfErrors 0 NoDelete
     FindClose $R0
     Sleep 1000
     RMDir "$0"
  NoDelete:
   FindClose $R0
FunctionEnd
