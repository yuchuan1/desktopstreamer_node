Download and install Node.js v5.3.0up from [https://nodejs.org/download/](https://nodejs.org/download/)

To implement and tranfer jsx to js
- Download and install [Ruby 1.9.3](http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-1.9.3-p550.exe)

If you need to see console for a production release, modify dsa.ini to the following and then open http://localhost:5858

```
#!javascript
  nwjs=nwjs/DesktopStreamer.exe --remote-debugging-port=5858
```
  
For PC only
Download and install [unsis](https://code.google.com/p/unsis/downloads/detail?name=nsis-2.46.5-Unicode-setup.exe&can=2&q=)

Add path

For PC only

    add C:\Program Files (x86)\NSIS\Unicode to path

Install gulp

    npm i gulp -g

Install gulp-compass & other modules

    npm i gulp-compass -g

Update ruby gem

```
#!javascript

gem update --system
```

Install compass

```
#!javascript

  gem install compass
```


Download dsa 2.0 source code

    svn co http://172.16.160.84/svn/trunk/NovoConnectPro/desktopstreamer_node

When commiting to SVN

    svn add * --auto-props --parents --depth infinity

Build project


```
#!javascript

  Type 'npm config set dsa.release development' to make loglevel always set to debug
  Otherwise set it to production mode, type 'npm config set dsa.release production'

 Now Jenkins build will always build the production release, meaning
The update/license server is set to download.novosync.com
Log level is set to "error"

If you want to enable log level to debug and change update/license server to download.staging.novosync.com,
please make a file named 'novotest' under the following folder according to your OS

For windows, save 'novotest' under %HOMEPATH%
For osx, save 'novotest' under ~

If you are the developer, please run the following command once in your development machine.
npm config set dsa.release development
This will make sure that when ever you type 'npm i' to build the app, the error level is always set to 'debug' regardless of the existence of 'novotest' file.

  npm i
```


Start DSA 2.0


```
#!javascript

  For PC HDD
  cd build\win32\
  setup.exe

  For PC USB
  copy build\win32\usb to a usb disk
  change dir to usb
  DesktopStreamerPro.exe

  For mac hdd
  cd build/darwin/hdd/DesktopStreamer.app/Contents/Resources/app.nw/nativeApp/
  ./license.app HDD
  double click on build/darwin/hdd/DesktopStreamer.app

  For mac usb
  copy build/darwin/usb to a usb disk
  change dir to usb
  double click on DesktopStreamer.app

```

App Data Path

```
#!javascript


PC "%CommonProgramFiles(x86)%\Novo"

mac ~/Library/Logs/Novo
```

License.lic path

```
#!javascript
   usb -> usb root ".Configure" folder
   os x hdd -> ~/Library/Preferences/
   win32 hdd -> %ALLUSERSPROFILE%\Novo\
```

To build native code

Download dsa 2.0 native source code

    svn co http://172.16.160.84/svn/trunk/NovoConnectPro/DSAService


For PC


```
#!javascript

  cd DSAService/compiler

  setup.bat

  cd /workspace/DSAService/src/

  make install
```

Win32 app launcher

```
#!javascript

initImg=xxx/init.bmp
initFlag=xxx/flag

init.bmp is the splash screen
initFlag, it will be deleted when program starts and recreated when DSA starts
When app launcher detects initFlag, it will stop displaying the init.bmp and kill itself
```


When upgrading nwjs
```
#!javascript
  run resources\win32Utils\ResourceHacker.exe, 
  open up resources\nwjs\*.*.*\win32\nw.exe
  replace the icon with \icons\nw.icns

  edit resources\nwjs\0.12.3\osx32\nwjs.app\Contents\Info.plist
  change line 49 <string>nwjs</string> to <string>Novo Desktop Streamer</string>
```

  To test native code


```
#!javascript

open 2 consoles
In console 1, start the server
DsaTestServer.exe 1234

In console 2, start the client
DsaService.exe -service_port 1234

In console 1, type the command you want and hit enter.
For example 6.1

```