var gui = require('nw.gui');
var win = gui.Window.get();
var video, rvaClient;

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function() {

  //var language = window.navigator.language; //'zh-TW';//en-US
  //var language = 'en-US'; //'zh-TW';//en-US
  var language = getParameterByName('setLng');
  translate(language);

  video = $("#video-page").videoPanel();
  //sendVideoResult(1, 'NovoConnect_EN_0618.mp4');  // playing
  //sendVideoResult(0);  // exit
  //sendVideoResult(-1, 'Error... Please send again.');  // error
  //sendVideoResult(2,'ArgoUml en Eclipse.mp4', {total : 600, percent: .7});
  //sendVideoResult(2,'test.mp4', {total : 700, percent: 0});

  checkResize();
  //win.setMaximumSize(354,400);
  win.setResizable(false);
  
  console.log($('#video-page'));
  
  var w = $(video).width(),
      h = $(video).height();
  var x = Math.round((screen.availWidth - w)/2),
      y = Math.round((screen.availHeight - h)/2);
  win.moveTo(x, y);
  //console.log('checkResize...', screen.availWidth, screen.availHeight, window.outerWidth, window.outerHeight, x, y);
  
  window.ondragover = function(e) {
    e.preventDefault();
    //console.log('ondragover...');
    return false;
  };
  
  var isWin = /^win/.test(process.platform);
  if (isWin) {
    $('.title').css('-webkit-user-select','none').css('-webkit-app-region','drag');
    $('#close-video-win').css('-webkit-app-region','no-drag');
  }
  else {
    $('body').css('-webkit-user-select','none').css('-webkit-app-region','drag');
    $('#video_points').css('-webkit-app-region','no-drag');
    $('input').css('-webkit-app-region','no-drag');
  }
  
  win.show(true);
});

win.on('closed', function() {
  //console.info('Destory Events...', rvaClient);
  rvaClient.removeListener('videoPlayStatus', videoHandler);
  rvaClient.removeListener('videoError', videoErrorHandler);
  rvaClient.removeListener('youTubeUrlError', youTubeUrlErrorHandler);
});

win.on('maximize', function(e) {
  win.restore();
  checkResize();
});

win.on('focus', function() {
  //console.log('focus event, start rvaDiscovery here');
  checkResize();
});

function checkResize() {
  //console.log('checkResize...');
  var w = $(video).width(),
      h = $(video).height();

  if (gui.App.manifest.window.frame) {
    w = w + 20;
    h = h + 80;
  }
  else {
    w = w + 4;
    h = h + 4;
  }
  win.resizeTo(w, h);
  
}

function init(rva){
  video.videoPanel("setRvaClient", rva);
  rvaClient = rva;

  rvaClient.on('videoPlayStatus', videoHandler);

  rvaClient.on('videoError', videoErrorHandler);

  rvaClient.on('youTubeUrlError', youTubeUrlErrorHandler);

  //console.log('rvaClient:', rvaClient);
};

/**
 * translation
 */
function translate(language){
  var opts = {
    lng: language,
    fallbackLng: 'en-US',
    debug: true,
    resGetPath: "../locales/__ns__-__lng__.json"
  };

  i18n.init(opts, function (t) {
    $(document).i18n();
  });
}

/*
rvaClient.on('connection', callback);
rvaClient.removeListener('connection', callback);
exports.PLAY_CMD = {
  START: 0,
  PLAY: 1,
  PAUSE: 2,
  RESUME: 3,
  FORWARD: 4,
  REVERSE: 5,
  EXIT: 6,
  FAST_FORWARD: 7,
  FAST_REVERSE: 8,
  UNKNOWN_ERROR: 9,
  READY_TO_SHOW: 10
};
*/
function videoHandler(videoPlayStatus) {
  console.log('videoPlayStatus:', videoPlayStatus);
  switch(videoPlayStatus.status){
    case 'playing':
      var data = {
        total: (videoPlayStatus.total)? videoPlayStatus.total : 0,
        percentage: (videoPlayStatus.percentage)? videoPlayStatus.percentage : 0
      };
      video.videoPanel("playing", videoPlayStatus.videoTitle, data);
      //console.log(formatSecond(videoPlayStatus.percentage * videoPlayStatus.total), '/', formatSecond(videoPlayStatus.total));
      break;
    case 'stopped':
      video.videoPanel("stopped");
      break;
  }
}

//videoErrorHandler('video.errorMsg');
//videoErrorHandler('Unknown error...');
function videoErrorHandler(err) {
  console.log('videoError:', err);
  //video.videoPanel("errorHandler", err.message);
  var str = (err === undefined || err === "" || err === 'errorMsg')? 'video.errorMsg' : err;
  video.videoPanel("errorHandler", $.t(str));
}

//youTubeUrlErrorHandler()
function youTubeUrlErrorHandler() {
  console.log('youTubeUrlErrorHandler:');
  video.videoPanel("errorHandler", $.t('video.youTubeUrlError'));
}

function disableProgressBar() {
  console.log('disableProgressBar...');
  video.videoPanel("disableProgressBar");
}
