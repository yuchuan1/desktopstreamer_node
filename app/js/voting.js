var gui = require('nw.gui'),
    win = gui.Window.get();
var voting, mainWin;

function init(obj){
  //console.info('init...', obj);
  mainWin = (obj)? obj : mainWin;
  return mainWin;
};

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function() {
  
  //var language = window.navigator.language; //'zh-TW';//en-US
  //var language = 'en-US'; //'zh-TW';//en-US
  var language = getParameterByName('setLng');
  translate(language);

  voting = $("#voting-page").votingPanel();

  $(".icon-minimize").off().click(function(){
    win.minimize();
  });
  
  $(".icon-fullscreen").off().click(function(){
    ($(this).hasClass('onMax'))? win.unmaximize() : win.maximize();
  });

  $(".icon-close").off().click(function() {
    win.close();
  });
  
  checkResize();
  
  console.log($('#q-title'));
  document.body.addEventListener('dragover', function(e){
    e.preventDefault();
    e.stopPropagation();
  }, false);
  
  var isWin = /^win/.test(process.platform);
  if (!isWin) {
    $('.icon-fullscreen').hide();
  }
});

win.setResizable(false);

win.on('maximize', function() {
  console.log('=========== maximize ==========');
  $(".icon-fullscreen").addClass('onMax').off().click(function(){
    win.unmaximize();
  });
  // "win32" for PC chrome app, the others for chrome book app
  //var h = (navigator.platform.toLowerCase()=="win32") ? Math.min(screen.availHeight, $(window).height()) : screen.availHeight;
  var h = Math.min(screen.availHeight, $(window).innerHeight());
  $("#voting-page").width("100%").height(h);
  //console.info('(#voting-page) height:', h);
  h = h - $("#taskbar").outerHeight(true);
  $("#main-content").height(h);
  //console.info('(#main-content) height:', h);
  tuningHeight();
});

win.on('unmaximize', function() {
  console.log('=========== unmaximize ==========');
  $(".icon-fullscreen").removeClass('onMax').off().click(function(){
    win.maximize();
  });
  //checkResize();
  var h = 620;
  var page = $("#voting-page").width(820).height(h);
  $(window).width(820).height(h);
  h = h - $("#taskbar").outerHeight(true);
  $("#main-content").height(h);
  tuningHeight();
});

function tuningHeight(){
  console.log('=========== tuningHeight ==========');
  // reset height
  $("#content").css("height","auto");
  $(".q-content").css("height","auto").css("max-height","");
  $("#q-content").css("height","auto").css("max-height","");

  var h = $("#main-content").innerHeight(),
      bottomHeight = 70,
      contentSetHeight = h - bottomHeight,
      contentHeight = $("#content").outerHeight(true);

  //console.log('h=' + h, "contentHeight="+contentHeight, contentSetHeight, bottomHeight);
  if (contentHeight+bottomHeight > h){
    var maxHeight = contentSetHeight - $("#q-title").outerHeight(true) - $("#answers").outerHeight(true);
    $(".q-content").css("min-height","410px").css("max-height", Math.floor(maxHeight - ($("#q-content").outerHeight(true) - $("#q-content").height())));
    //$("#q-content").css("max-height", Math.floor(maxHeight - ($("#q-content").outerHeight(true) - $("#q-content").height())));
    //console.log($("#content").height(), contentHeight, contentSetHeight, $("#q-content").outerHeight(true), $("#answers").outerHeight(true));
  }
}

function getMainWindow() {
  return mainWin;
}

function createQuestion(votingObject) {
  console.info('createQuestion...',votingObject);
  voting.votingPanel("createQuestion", votingObject.question);

  // Restore answer, result
  if (typeof votingObject.studentClickAnswer != "undefined") {
    $('*[value="'+ votingObject.studentClickAnswer +'"]').trigger("click").addClass("checked");
  }
  if (typeof votingObject.studentAnswerSubmited != "undefined") {
    if (votingObject.question.type != 6){
      var compare = (votingObject.studentClickAnswer == votingObject.studentSubmitAnswer);
      voting.votingPanel("submitAnswerResult", votingObject.studentAnswerSubmited, ((compare)? votingObject.studentAnswerSubmitedMsg : ''));
    }
    else {
      if (votingObject.studentAnswerSubmited){
        setUploading(votingObject.studentSubmitAnswer);
        voting.votingPanel("submitAnswerResult", votingObject.studentAnswerSubmited, votingObject.studentAnswerSubmitedMsg);
      }
    }
  }
  if (typeof votingObject.flag != "undefined") {
    voting.votingPanel("showAnswerResultDialog", votingObject.flag, votingObject.correctAnswer);
  }
  if (votingObject.stopVoting) {
    voting.votingPanel("stopAnswerQuestion");
  }
}

function submitAnswerResult(result, message) {
  voting.votingPanel("submitAnswerResult", result, message);
}

function stopAnswerQuestion() {
  voting.votingPanel("stopAnswerQuestion");
}

function showAnswerResultDialog(flag, correctAnswer) {
  voting.votingPanel("showAnswerResultDialog", flag, correctAnswer);
}

function closeAllDialogs() {
  voting.votingPanel("closeAllDialogs");
}

function checkResize() {
  //console.log('checkResize...');
  var w = $(voting).width(),
      h = $(voting).height();

  if (gui.App.manifest.window.frame) {
    w = w + 20;
    h = h + 80;
  }
  else {
    w = w + 4;
    h = h + 4;
  }
  win.resizeTo(w, h);
  
  var x = Math.round((screen.availWidth - w)/2),
      y = Math.round((screen.availHeight - h)/2);
  win.moveTo(x, y);
  //console.log('checkResize...', screen.availWidth, screen.availHeight, window.outerWidth, window.outerHeight, x, y);
}

/**
 * translation
 */
function translate(language){
  var opts = {
    lng: language,
    fallbackLng: 'en-US',
    debug: true,
    resGetPath: "../locales/__ns__-__lng__.json"
  };

  i18n.init(opts, function (t) {
    $(document).i18n();
  });
}
