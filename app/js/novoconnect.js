var fs = require('fs'),
    os = require('os'),
    path  = require('path'),
    async = require('async'),
    xml2js = require('xml2js'),
    _ = require('lodash');

var isWin = /^win/.test(process.platform),
    homePath = process.env[(isWin)? 'USERPROFILE' : 'HOME'],
    groupSharePath = path.join(homePath, 'GroupShare');

var presenterImgs = {
  "-1": '../images/screen_default.png',
   "0": '../images/screen_full.png',
   "1": '../images/screen_channel_01.png',
   "2": '../images/screen_channel_02.png',
   "3": '../images/screen_channel_03.png',
   "4": '../images/screen_channel_04.png',
};

$.widget("custom.novoconnect", {
  // Default options
  options: {
    rvaType: 'EDU',    // 'EDU': Education  'CORP': Corporation 'NOVOCAST': NovoCast
    rvaVersion: 2.2,
    sharedScreenNo: 1,  // 1:primary-display(default) 2:secondary-display
    connected: false,  // true/false
    //connectedTimer: 3, // 0~3 seconds
    isHost: false,     // true/false
    groupMode: false,  // true/false
    groupName: '',
    pinRequired: false,// true/false
    pinCode: '----',
    viewMode: 'full',
    projectionData: {grayOut: true},
    nowPage: '#icon-connect',  // #icon-connect/#icon-group/#icon-tool/#icon-offline-tool
    userList: [],      // display user list
    userListSortKey: 'name',  // name/time
    screenNo: -1,     // playing on screen number
    sendViewerRequest: {
      enable: false,
      millisecond: 0,      // default: 0 millisecond
      uid: null
    },
    viewModel: null,
    supportVideoClip: false,  // after v1.5
    votingMode: false, // after v1.5
    votingObject: null,// after v2.0
    sharingMode: true,// after v1.6
    sendingFile: {
      sending: false,  // true/false
      file: null,
      percentage: 0,
      finished: 0,
      total: 0
    },
    receivingFile: false,  // true/false
    childWindows: {
      qrWindow: null,
      videoWindow: null,
      votingWindow: null,
    },
    votingEditorWindow: null, //after v2.0
    votingEditorObject: null, //after v2.0
    deviceManagerWindow: null,
    tabletsLocked: false,
    notification: null,
  },
  _create: function _create() {
    var instance = this;
    ko.bindingProvider.instance = new ko.secureBindingsProvider();
    this.options.viewModel = new UserViewModel(this.options.userList);
    ko.cleanNode($("*[data-sbind]")[0]);
    ko.applyBindings(this.options.viewModel);

    // TaskBar: close
    $(".icon-close").off().click(function(){
      instance.askQuitDsa();
    });
    // TaskBar: minimize
    $(".icon-minimize").off().click(function(){
      //windows_minimize();
      var nw = require('nw.gui');
      var win = nw.Window.get();
      win.minimize();
    });

    $('#toggle-settings-block').off().click( function(){
      $("#setting").toggle();
    });

    $('#toggle-moderator-block').off().click( function(){
      $("#moderator-credential").toggle();
    });

    $('#toggle-rdm-block').off().click( function(){
      $('#rdm-block').toggle();
    });

    $('.sort-key').addClass((instance.options.userListSortKey=='time')? 'byTime' : '').off().click(function(){
      //instance.options.viewModel.sortPeople(instance.options.userListSortKey);
      instance.options.userListSortKey = $(this).hasClass('byTime')? 'name' : 'time';
      instance.refresh();
      $(this).toggleClass('byTime');
    });

    // Set Screenote icon
    instance._setScreenoteIcon();

    // support for MAC menu
    var gui = require('nw.gui');
    if (process.platform === "darwin") {
      var win = gui.Window.get();
      var mb = new gui.Menu({type: 'menubar'});
      mb.createMacBuiltin($.t("login.softwareName"));
      win.menu = mb;
    }
    
    $("#container").show();
  },
  _init: function _init() {
    //console.debug("_init() ", this.options);

    var instance = this;
    instance._hideAllpages();
    if (hasNewVersion) askUpgradeDialog();

    // Connected
    if (instance.options.connected) {
      //console.log("Init(connected)......");
      $(".taskbar .disconnect").hide();
      $(".taskbar .connected").show();
      $(".connected-part").show();
      instance._removeAllDialogs();
      $("#connect-btn").off();
      
      // Disconnect button
      $("#disconnect-btn").off().click(function(event){
        event.preventDefault();
        //console.log("disconnect...");
        instance._doLogout();
      });

      // QR button
      $('#qr-btn').off().click(function(event){
        event.preventDefault();
        //console.log("instance.options.childWindows.qrWindow==>", instance.options.childWindows.qrWindow);

        // already open, just focus it
        if (instance.options.childWindows.qrWindow) {
          gui.Window.get(instance.options.childWindows.qrWindow).show(true);
          return;
        }

        // not open, go to open it
        var qrWindow;
        instance.options.childWindows.qrWindow = window.open("../views/qrImage.html?setLng=" + i18n.lng(), "qrWindow", "width=650,height=440,*=no");
        qrWindow = gui.Window.get(instance.options.childWindows.qrWindow);
        qrWindow.setPosition("center");
        //qrWindow.showDevTools();
        qrWindow.on('loaded', function(){
          //console.log('Open QR page, set information');
          qrWindow.window.init(rvaClient, instance.options.rvaVersion);
          var hasPurview = (instance.options.isHost === true)? instance.options.isHost : (instance.options.rvaType == 'CORP' && !instance.options.viewModel.hasHost());
          qrWindow.window.displaySessionInfo({
            lanIP: instance.options.lanIP,
            pinRequired: instance.options.pinRequired,
            pinCode: instance.options.pinCode,
            wifi: instance.options.wifi,
            qrImg: instance.options.qrImg,
            hasPurview: hasPurview
          });
        });
        qrWindow.on('closed', function() {
          //console.log('Close QR page, set null');
          var hasPurview = (instance.options.isHost === true)? instance.options.isHost : (instance.options.rvaType == 'CORP' && !instance.options.viewModel.hasHost());
          if (instance.options.rvaVersion >= 2.2 && hasPurview) {
            //console.debug("Close session info dialog...");
            rvaClient.closeSessionInfo();
          }
          instance.options.childWindows.qrWindow = null;
          qrWindow = null;
        });
      });
      
      // Setting information
      $('#toggle-settings-info').off().click(function(){
        if ($(this).hasClass('up')) {
          $('#setting-info').hide();
          $(this).toggleClass('up').toggleClass('down');
          return;
        }
        $(this).toggleClass('up').toggleClass('down');
        $('#setting-info').show();
        var w = $('#setting-info').width() - 40 - $('#settings-info-label').width();
        $('#setting-info .set-line').width(w);
      });
      
      rvaClient.getSettings(function(settings){
        //console.log('settings:', settings);
        //$('#projectionMode').text((settings.projectionMode==1) ? 'settings.videoPlayback':'settings.presentation').i18n();
        $('#setting-info input:radio[name=projectionMode][value="' + settings.projectionMode + '"]').prop('checked', true);
        $('#videoQuality').text((settings.videoQuality==1)? 'settings.normal' :'settings.high').i18n();
        $('#refreshRate').text((settings.refreshRate==1)  ? 'settings.normal' :'settings.high').i18n();
        $('input:radio[name=remoteMouse][value='+ ( settings.remoteMouse || 1) +']').prop('checked', true);
      });

      $('#setting-info input:radio[name=projectionMode]').off().click(function(){
        var parent = $(this).closest('span'),
            radios = $('#setting-info input:radio[name=projectionMode]');
        if($(parent).hasClass('active')){
          return;
        }

        var field = {name: $(this).prop('name'), value: $(this).val()};
        //console.log("projectionMode-->", field);
        // Store
        rvaClient.set(field);
        //change projection mode -> turn on/off audio
        $(radios).prop('disabled',true);
        $(parent).addClass('active').powerTimer({
          delay: 10000,
          func: function() {
            $(parent).removeClass('active');
            $(radios).prop('disabled',false);
            //console.log('Stop timer of mirror quality...');
          },
        });
        
      });
      
      instance._enableTaskbar();
      instance.refresh();
      $(instance.options.nowPage).trigger("click");
      return;
    }

    // Disconnect
    if (!instance.options.connected) {
      //console.log("Init(disconnect)......");
      $('#container').removeClass("novocast");
      $(".taskbar .disconnect").show();
      $(".taskbar .connected").hide();
      $(".login").show();
      $("#disconnect-btn").off();
      $('#qr-btn').off();
      $('#setting-info input:radio[name=projectionMode]').off();

      // click "PIN required" to enable/disable "pin code" input
      $("#pin_required").off().click( function(){
        $(this).toggleClass('red');
        $("#pin_code").attr("disabled",!$(this).hasClass("red")).val("");
        $('span:last-child',this).text(($(this).hasClass("red"))? $.t("login.requiredPin") : $.t("login.notRequiredPin"));
        ($(this).hasClass("red"))? "" : $("#pin_code").focus();
      });

      // Connect button
      $("#connect-btn").off().click(function(event){
        event.preventDefault();
        instance._removeAllDialogs();
        instance._login();
      });

      // TaskBar: offline-tool
      $("#icon-offline-tool").show().off().click(function(){
        instance._removeAllDialogs();
        if ($(this).hasClass("focus")) {
          $(this).removeClass("focus");
          instance._hideAllpages();
          $(".login").show();
          return;
        }
        $(this).addClass("focus");
        instance._hideAllpages();
        $(".tool-list").show();
      });
      

      $('#toggle-drop-setting').off().click(function(){
        //console.log('Click #toggle-drop-setting...');
        $(this).toggleClass("focus");
        return false;
      });
      
      // Close if outside click
      $(document).off().on('click', function(){
        //console.log('Click document to close pop-up...');
        $('#toggle-drop-setting').removeClass("focus");
        $("#project").autocomplete('close');
        $('#drop-sharing').removeClass('open');
        $(".project-user").removeClass("clicked");
        $('#drop-mirroring').removeClass('open');
      });

      // toggle setting
      $('#toggle-setting').off().click(function(){
        //console.log('toggle-setting......');
        instance._resetBlocks();
        $('#setting').show();
        //$(this).addClass("active");
      });

      // toggle modertor credentials
      $('#toggle-modertor').off().click(function(){
        //console.log('toggle-modertor......');
        instance._resetBlocks();
        $('#moderator-credential').show();
        //$(this).addClass("active");
      });

      $('#moderator').off().change(function(){
        // Save checked/unchecked
        //console.log('Moderator Checked...', $(this).prop("checked"));
        rvaClient.setTeacherCredentialSetting($(this).prop("checked"));

        if ($(this).prop("checked")){
          // enable Teacher credentials
          $('.pwBlock').show();
          $('#toggleTeacherPw').focus();
          return;
        }
        // disable Teacher credentials
        $('.pwBlock').hide();
      });

      $('#toggleTeacherPw').focusout(function() {
        var pw = $(this).val();
        if (pw=="") {
          $('.pwBlock').addClass('error');
          $('.toggleTeacherPwMsg').text($.t('login.required'));
          $(this).focus();
          return;
        }
        if (pw.length < 4 || pw.length > 24) {
          $('.pwBlock').addClass('error');
          $('.toggleTeacherPwMsg').text($.t('login.moderatorPasswordLength'));
          $(this).focus();
          return;
        }
        $('.pwBlock').removeClass('error');
        $('.toggleTeacherPwMsg').empty();
        //console.log('Save pw...', pw);
        rvaClient.setTeacherCredentialPassword(pw);
      });

      // open group editor
      $('#open-group-editor').off().click(function(){
        //console.log('open-group-editor......');
        instance._resetBlocks();
        $('#group-panel').show();
        //$(this).addClass("active");
      });
      
      $('#group-done').off().click(function(){
        $('#group-panel').hide();
      });

      // open device manager
      $('#open-device-manager').off().click(function(){
        $('#toggle-drop-setting').removeClass("focus");
        instance._openDeviceManager();
        return;
      });

      // toggle RDM IP
      $('#toggle-rdm').off().click(function(){
        //console.log('toggle RDM IP......');
        instance._resetBlocks();
        $('#rdm-block').show();
        //$(this).addClass("active");
      });

      $('#rdm-ip').focusout(function() {
        var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        var ip = $(this).val();
        if (ip!="" && ip.match(ipformat) == null) {
          //console.log('Check IP error...', ip.match(ipformat));
          $('.rdmErrorMsg').text($.t('device.wrongIPMessage'));
          $(this).addClass('error').focus();
          return;
        }
        $(this).removeClass('error');
        $('.rdmErrorMsg').empty();
        // Save rdm ip
        //console.log('Save IP...', ip);
        rvaClient.saveLookupServerIP(ip);
      });

      $(".tool-list > div").not('#launch-screenote').addClass("disable");

      // Enable Tool: Voting
      $("#open-voting").removeClass("disable").off().click(function(){
        instance._openVotingEditor();
      });

      $('.online-edu').hide();

      instance._initSetting();
      instance._disableSharing();
    }

  },
  _initSetting: function _initSetting() {
    //console.log("_initSetting...");
    var instance = this;

    var save = function save(){
      var field = {name: $(this).prop('name'), value: $(this).val()};
      //console.log("save-->", field);
      // Store
      rvaClient.set(field);
    }

    $('#proj-mode-0').off().click(save).click(function(){
      //console.log("Click Video Playback...");
      $('.projItem').addClass('disabled');
    });
    $('#proj-mode-1').off().click(save).click(function(){
      //console.log("Click Presentation...");
      $('.projItem').removeClass('disabled');
    });
    $('[name="videoQuality"]').off().change(save);
    $('[name="refreshRate"]').off().change(save);
    $('input:radio[name=remoteMouse]').off().click(save).click(function(){
      //console.log("Click Remote Mouse...", $(this).val());
      ($(this).val()==0)? rvaClient.remoteMouseStart() : rvaClient.remoteMouseStop();
    });

    rvaClient.getSettings(function(settings){
      //console.info('rvaClient.getSettings:', settings);
      if (settings && settings.projectionMode != undefined){
        $('#setting input:radio[name=projectionMode][value="' + settings.projectionMode + '"]').prop('checked', true);
        $('[name="videoQuality"]').val(settings.videoQuality);
        $('[name="refreshRate"]').val(settings.refreshRate);
        if (settings.projectionMode=="1"){
          $('.projItem').addClass('disabled');
        }
        else {
          $('.projItem').removeClass('disabled');
        }
        $('#rdm-ip').val(settings.lookupServer);
        $('input:radio[name=remoteMouse][value='+ ( settings.remoteMouse || 1) +']').prop('checked', true);
      }
      else {
        $('#proj-mode-1').trigger('click');
        $('[name="videoQuality"]').val(1);
        $('[name="refreshRate"]').val(1);
        $('input:radio[name=remoteMouse][value="1"]').prop('checked', true);
      }
    });
    
    $('#checkNewVersionButton').off().click(function () {
      //console.log("Check new version...");
      var btn = $(this);
      $(btn).hide();
      $('#upgradeMsg > span').text($.t('upgrade.checkUpdate')).append($('<span>').addClass('small-loading'));
      rvaClient.checkNewVersion(function (error) {
          console.error(error);
          $('#upgradeMsg > span').text($.t('upgrade.serverNotFound'));
          $(btn).css('display','');
        },
        function (result) {
          if (result === true) {
            //console.log('new version detected');
            hasNewVersionAvailable();
          }
          else {
            //console.log('already up-to-date');
            $('#upgradeMsg > span').text($.t('device.upToDate'));
            $(btn).css('display','');
          }
        });
    });
    instance._initGroups();
  },
  _resetBlocks: function _resetBlocks() {
    this._removeAllDialogs();
    this._hideAllpages();
    $('.login').show();
    $("#icon-offline-tool").removeClass("focus");
    $('#toggle-drop-setting').removeClass("focus");
    $('#setting').hide();
    $('#moderator-credential').hide();
    $('#group-panel').hide();
    $('#rdm-block').hide();
  },
  _initGroups: function _initGroups() {
    var instance = this;

    $('#edit-group').removeClass('disable').off().click(function(){
      // open group panel
      $('#group-panel').show();
      $('.tool-list').hide();
    });

    // create group page
    $('#group-add').off().click(function(){
      instance._openGroupEditor();
    });

    // import button
    $('#group-import').off().click(function(){
      $('#xmlFileDialog').trigger('click');
    });

    $('#xmlFileDialog').on('change',function(){
      //console.log("Get xml file...");
      var files = $(this)[0].files;

      async.series([
        function(done){
          for (var i = 0; i < files.length; ++i){
            //console.log(files[i]);
            rvaClient.importXMLFile(files[i], done);
          };
        },
        function(done){
          instance.reloadGroupList();
          done();
        },
        function(err, results){
        }
      ]);


      $(this).val(null);
    });

    instance.reloadGroupList();

  },
  _askCancelGroupModePage: function _askCancelGroupModePage() {
    var instance = this;
    instance._removeAllDialogs();
    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t("group.confirm")))
                .append($('<p>').text($.t("group.confirmMsg")));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            // Remove group mode
            rvaClient.unloadGroup();
            $(this).dialog('destroy').remove();
          }
        },
      ]
    });
  },
  reloadGroupList: function reloadGroupList() {
    //var groups = (localStorage.groups)? JSON.parse(localStorage.groups) : {};
    var instance = this;
    var groups;

    async.series([
      function(done){
        rvaClient.reloadGroupList(function(groupList){
          groups = groupList;
          done();
        });
      },
      function(done){
        var panel = $('#existed-groups').empty(), div;
        _(groups).forEach(function(group){
          instance._createGroupRow(group.group);
        });
        done();
      },
      function(err, results){
      }
    ]);
    checkResize();

  },
  _createGroupRow: function _createGroupRow(group) {
    //console.log('_createGroupRow...', group);
    var instance = this;
    var span1 = $('<span class="edit"></span>').off().click(function(){
                  //console.log("edit...", group.class);
                  instance._openGroupEditor(group);
                });
    var span2 = $('<span class="export"></span>').off().click(function(){
                  //console.log("export...", group.class);
                  var elm = $('<input type="file" nwsaveas="'+ group.class + '.xml" />').on('change', function(){
                    var file = $(this)[0].files[0];
                    rvaClient.exportGroupXML(group, file);
                  });
                  $(elm).trigger('click');
                });
    var span3 = $('<span class="delete"></span>').off().click(function(){
                  //console.log("delete...", group.class);
                  instance._confirmRemoveGroupRow(group.class);
                });
    //console.warn(group.class, 'group.student=', group.student);
    var cnt = (group.student === undefined)? 0 : ((group.student.length > -1)? group.student.length : 1);
    var span4 = $('<span class="info"></span>').text("(" + cnt + ") " + group.teacher.name);
    var div = $('<div>').attr('id', group.class)
              .attr('title', group.class)
              .off().click(function(){
                $('#existed-groups > div').removeClass('selected');
                $(this).addClass('selected');
              })
              .append($('<span>').append($('<span>').addClass('group-name').text(group.class)))
              .append($(span1)).append($(span2)).append($(span3)).append($(span4));
    $('#existed-groups').append(div);
  },
  _confirmRemoveGroupRow: function _confirmRemoveGroupRow(groupName) {
    var instance = this;
    instance._removeAllDialogs();

    // Ask to be close page
    var page = $('<div class="message-page" id="close-page"></div>')
                .append($('<h4>').text($.t("group.confirmDelteTitle")))
                .append($('<p>').text($.t("group.confirmDelteMsg")));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      show: {
        effect: "blind",
        duration: 200
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            $(this).dialog('destroy').remove();
            rvaClient.deleteGroup(groupName);
            instance.reloadGroupList();
          }
        },
      ]
    });
  },
  _openGroupEditor: function _openGroupEditor(group){
    //console.log("Open editing group page...", group);
    var instance = this;
    var mainWin = gui.Window.get();
    childwin = gui.Window.get(window.open("../views/group-file.html?setLng=" + i18n.lng(), "groupWindow", "width=500,height=700,*=no"));
    childwin.hide();
    childwin.on('loaded', function() {
      var editor = (instance.options.connected)? $("#connected-username").text() : $('#client_name').val();
      this.window.init(rvaClient, editor);
    });
    childwin.on('closed', function() {
      //console.log('Close editing group page, set null');
      childwin = null;
      mainWin.show(true);
      mainWin.window.reloadGroupList();
      this.close(true);
    });
    if (group) {
      childwin.on('loaded', function() {
        this.window.editGroup(group, rvaClient);
      });
    }
    childwin.show(true);
    mainWin.hide();
    childwin = null;
  },
  _openDeviceManager: function _openDeviceManager(){
    //console.log("Open device manager... this.options.deviceManagerWindow=", this.options.deviceManagerWindow);
    
    if (this.options.deviceManagerWindow != null) {
      this.options.deviceManagerWindow.show(true);  // focus opened window
      return;
    }
    
    var instance = this,
        deviceWin;
    
    this.options.deviceManagerWindow = gui.Window.get(window.open("../views/device-manager.html?setLng=" + i18n.lng(), "deviceWindow", "width=655,height=395,*=no"));
    deviceWin = this.options.deviceManagerWindow;
    deviceWin.hide();
    deviceWin.on('loaded', function() {
      console.log('Load device manager', $('#rva_url').val());
      this.window.init($('#rva_url').val(), rvaClient);
    });
    deviceWin.on('closed', function() {
      //console.log('Close device manager');
      gui.Window.get().show(true);
      instance.options.deviceManagerWindow = null;
      instance = null;
      deviceWin = null;
    });
    deviceWin = null;
  },
  refresh: function refresh() {
    var instance = this;
    //console.log("[refresh] instance.options.votingEditorWindow=", instance.options.votingEditorWindow);
    //console.debug("[refresh] this.options.sharingMode=", instance.options.sharingMode);
    if (instance.options.connected){
      
      // for NovoCast
      if (instance.options.rvaType == "NOVOCAST") {
        $('#container').addClass("novocast"); // hide class: novocast-none
        instance._enableLogoButton()
        instance.enableCastButton4NovoCast();
        $('.remoteMouseDiv').hide();
        // TODO: hide sth.
        $('#qr-btn').off();
        $("#toggle-tablet").hide();
        $("#terminate-session").hide();
        instance.options.viewModel.refresh(instance.options.userList);
        instance._getFullMode();
        return;
      }
      
      // for NovoPRO, NovoConnect
      instance.options.viewModel.sortPeople(instance.options.userListSortKey);
      instance.options.viewModel.refresh(instance.options.userList);
      instance.options.isHost = instance.options.viewModel.iamHost();
      (instance.options.viewMode=="full") ? instance._getFullMode() : instance._getSplitMode();
      $("#pin-code").text((instance.options.pinRequired) ? instance.options.pinCode : "----");
      $("#qr-pin").text((instance.options.pinRequired) ? instance.options.pinCode : "----");
      ($("li.isMyself").hasClass("playing") && instance.options.childWindows.videoWindow == null) ? instance._enableLogoButton() : instance._disableLogoButton();
      (instance.options.votingMode)? instance._enableVoting() : instance._disableVoting();
      var opt = instance.options.projectionData;  //{grayOut: false, lightOn: true}
      if (!instance.options.supportVideoClip || (!opt.grayOut && opt.lightOn)) instance._disablePlayVideo();
      (instance.options.sharingMode && instance._receivers()) ? instance._enableSharing() : instance._disableSharing();
      //console.log('rvaVersion=', instance.options.rvaVersion);
      (instance.options.rvaVersion >= 2)? $('.remoteMouseDiv').hide() : $('.remoteMouseDiv').show();

      instance._setProjection();
      
      if (instance.options.childWindows.qrWindow) {
        var qrWindow = gui.Window.get(instance.options.childWindows.qrWindow);
        //console.info('Refresh QR page, set information', qrWindow);
        var hasPurview = (instance.options.isHost === true)? instance.options.isHost : (instance.options.rvaType == 'CORP' && !instance.options.viewModel.hasHost());
        qrWindow.window.displaySessionInfo({
          lanIP: instance.options.lanIP,
          pinRequired: instance.options.pinRequired,
          pinCode: instance.options.pinCode,
          wifi: instance.options.wifi,
          qrImg: instance.options.qrImg,
          hasPurview: hasPurview
        });
      }

      if (instance.options.rvaType == 'EDU') {
        // Education version
        $('.online-edu').show();
        $('.online-corp').hide();
        $('#white-cover').hide();
        instance.options.viewModel.updateGroup(instance.options.groupMode, instance.options.groupName);
        instance._setGroupMode(instance.options.groupMode);
        (instance.options.isHost) ? instance._enableHostControls() : instance._disableHostControls();
      }

      if (instance.options.rvaType == 'CORP') {
        // Corporation version
        $('.online-edu').hide();
        $('.online-corp').show();
        //console.log("refresh...", instance.options.viewModel.hasHost(), instance.options.isHost);

        if (instance.options.viewModel.hasHost() && !instance.options.isHost) { // i am not host
          // disable user list function
          instance._disableCorpPanelControls();
          // disable pin code function
          instance._removePinCodeControls();
          //$('#open-voting').addClass('disable');
          //$('#open-voting-text').text($.t('info.openVoting'));
          //console.warn('There is host but I am not host...');
        }
        else {  // no host or i am the host
          // enable panel function
          instance._enableCorpPanelControls();
          // enable pin code function
          instance._createPinCodeControls();
          //console.warn('There is no host or I am the host...');
        }
      }
      
      instance._refreshVotingStart();
      return;
    }

    // Return login page
    $(".taskbar .disconnect").show();
    $(".taskbar .connected").hide();
    $(".login").show();
  },
  refreshTabletsLocked: function refreshTabletsLocked(value) {
    //console.log("refreshTabletsLocked...");
    (value)? $("#toggle-tablet").addClass('locked') : $("#toggle-tablet").removeClass('locked');
    this.options.tabletsLocked = value;
  },
  _enableCorpPanelControls: function _enableCorpPanelControls() {
    //console.log('_enableCorpPanelControls...');
    var instance = this;
    $('#white-cover').hide();
    $('#corp-moderator-btn').off().click(function(){
      if (rvaClient.checkIsVoting()) {
        instance.messagePage($.t('voting.notAllowSetHost'));
        return;
      }
      if ($(this).hasClass('active')){
        //console.log("Already Clicked....", $(this));
        return;
      }
      //console.log("Get moderator role...", $(this));
      $(this).addClass('active');
      ($(this).hasClass('true'))? rvaClient.send_CMD_MODERATOR_RELEASE() : rvaClient.send_CMD_MODERATOR_REQUEST();
    });

    // Enable user list function
    $(".user-list > ul").addClass('has-right');
    
    //console.log('_enableCorpPanelControls....', $(".project-user").length);
    $(".project-user").off().click(function(){
      //console.log('[Click .project-user]...', $(this));
      $(".project-user").removeClass("clicked");
      $(this).addClass('clicked');
      var panel = $('.panel', this),
          layer = $('.panel-overlay', this);
      var top = $(this).closest('li').offset().top;
      $(panel).css('top', top - 67);
      $(layer).css('top', top - 78);
      //console.log('offset:', $(panel).offset(), $(panel), $(layer));
      return false;
    });
    
    //console.log('_enableCorpPanelControls....', $(".panel > img").length);
    if ($(".panel > img").length == 0){

      // create panel image
      var img = $('<img alt="Assign panel" width="186" height="108">'),
          map = $('<map class="panelMap"></map>'),
          areas = ['<area region="0" alt="0" shape="rect" coords="62,27,123,80" href="#">',
                   '<area region="1" alt="1" shape="poly" coords="0,0,93,0,93,27,62,27,62,54,0,54" href="#">',
                   '<area region="2" alt="2" shape="poly" coords="93,0,186,0,186,54,123,54,123,27,93,27" href="#">',
                   '<area region="3" alt="3" shape="poly" coords="0,54,62,54,62,80,93,80,93,108,0,108" href="#">',
                   '<area region="4" alt="4" shape="poly" coords="93,80,123,80,123,54,186,54,186,108,93,108" href="#">'];

      $.map(areas, function(area){ $(map).append(area); });

      $('.user.online').each(function(index) {
        var person = $(this),
            uid = $(person).attr("id"),
            panel = $(person).find('.panel');

        // create preview and panel
        $(panel).append(img.attr('src',$(panel).attr('src')).attr('usemap','#panel_'+uid).clone())
                .append(map.attr('name','panel_'+uid).clone());

        // li.user
        $(person).hover(
          function() {
            $(person).addClass("focused");
          },
          function() {
            $(person).removeClass("focused");
          }
        );

      });

      // map area
      if (instance.options.sendViewerRequest.enable && instance.options.sendViewerRequest.millisecond > 0) {
        //console.log("Disable panel", instance.options.sendViewerRequest);
        $(".user-control").empty();
        $("#" + instance.options.sendViewerRequest.uid +" .user-control").addClass("loading").append($('<div>'));
        return;
      }
      
      $(".panel area").hover(
        function() {
          var displayImg = $(this).closest('map').prev();
          var src= $(displayImg).attr('src');
          $(displayImg).attr('src',presenterImgs[$(this).attr('region')]);
        },
        function() {
          var displayImg = $(this).closest('map').prev();
          var src= $(displayImg).closest('td').attr('src');
          $(displayImg).attr('src',src);
        }
      ).click(function() {
        var pno = $(this).attr("region"),
            pnoNow = $(this).closest("div.panel").attr("no"),
            uid = $(this).closest("li").attr("id");
      
        //console.log("Click panel...", uid, pno, pnoNow);
        if (pno == pnoNow) {
          // click mirroring panel
          var isMyself = $(this).closest("li").hasClass('isMyself');
          //console.info("Click panel...", uid, pno, pnoNow, isMyself);
          var msg = (isMyself)?
              ((pno == 0)? $.t('info.currentPanel') : $.t('info.currentPanelSplit')) :
              ((pno == 0)? $.t('info.othersCurrentPanel') : $.t('info.othersCurrentPanelSplit'));

          (instance.options.isHost || isMyself)? instance._confirmStopProjection(msg, uid) : instance.messagePage(msg);
        }
        else {
          //console.log("Set someone's panel uid:" + uid + ' panel:' + pno);
          $(this).off().closest("td").addClass("loading").empty().append($('<div>'));
          if (!isWin) instance._clearNotification();
          rvaClient.send_CMD_VIEWER_REQUEST(uid, pno);
          if (instance.options.sendViewerRequest.enable) {
            var second = 4000;
            instance.options.sendViewerRequest = {enable: true, millisecond: second, uid: uid};
            $('.user-list').powerTimer({
              delay: second,
              func: function() {
                instance.options.sendViewerRequest = {enable: true, millisecond: 0, uid: null};
                instance.refresh();
                //console.log('==> sendViewerRequest...', uid, pno, instance.options.sendViewerRequest);
              },
            });
          }
        }
      });

    }

    //console.warn("(END)_enableCorpPanelControls...");
  },
  _disableCorpPanelControls: function _disableCorpPanelControls() {
    var h = $('#corp-moderator-func').outerHeight() + $('.user-list').outerHeight(),
        y = $('#corp-moderator-func').position().top;
    $('#white-cover').show().height(h + 'px').css('top', y + 'px');
    $('#corp-moderator-btn').off();
    $(".user-list > ul").removeClass('has-right');
    //console.log("_disableCorpPanelControls...");
    //console.log("_disableCorpPanelControls...", h, y, $('#white-cover'));
  },
  _confirmStopProjection: function _confirmStopProjection(msg, uid) {
    var instance = this;
    instance._removeAllDialogs();
    var page = $('<div class="message-page"></div>').text(msg);

    // confirm to No/Yes
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      show: {
        effect: "blind",
        duration: 500
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.stopProjection"),
          click: function() {
            $(this).dialog('close');
            //console.debug('ask RVA to stop projection ', uid);
            // stop presentation
            rvaClient.stopPresentation(uid);
          }
        },
        {
          text: $.t("button.ok"),
          click: function() {
            $(this).dialog('close');
          }
        },
      ]
    });
    
    this.displayOnTop();
  },
  corpModeStatus: function corpModeStatus(result) {
    $('#corp-moderator-btn').removeClass('active');
    //(result)? $('#corp-moderator-btn').addClass('true') : $('#corp-moderator-btn').removeClass('true');
    //console.log("corpModeStatus...");
    //console.log("corpModeStatus...", result, $('#corp-moderator-btn'));
  },
  _receivers: function _receivers(){
    //console.log('_isMoreThanTwoUser userList:', this.options.userList);
    if (this.options.userList.length < 2) {
      return false;
    }

    var arr = [],
        output = false;

    // Case 1: I am host and send to all
    if (this.options.isHost) {
      arr = $.grep(this.options.userList, function(n){
        return ( n.online && !n.isMyself);
      });
      output = (arr.length > 0)? true : false;
      //console.log('_isMoreThanTwoUser arr:', arr);
      return output;
    }

    // Case 2: [CORP,EDU] I am not host and send file to the host
    arr = $.grep(this.options.userList, function(n){
      return (n.online && !n.isMyself && n.isHost); // get the host
    });
    if (arr.length > 0) {
      return true;
    }

    // Case 3: [CORP-No host] I am not host and send file to all when NO host is online.
    if (this.options.rvaType == 'CORP') {
      arr = $.grep(this.options.userList, function(n){
        return (n.online && !n.isMyself); // get the others
      });
      output = (arr.length > 0)? true : false;
      //console.log('[CORP] _isMoreThanTwoUser arr:', arr);
      return output;
    }

    //Case 4: [EDU-No host] I am not host and do nothing when NO host is online.
    return false;
  },
  _login: function _login() {
    //console.log("_login......");
    var instance = this;
    instance.openWaitingPageNoTimer($.t('login.connecting'));

    var rvaIp = $.trim($('#rva_url').val());
    if (rvaIp == "" || !validateIPaddress(rvaIp)) {
      instance.messagePage($.t('login.ipInvalid')); // i18n
      return;
    }

    var needPin = $('#pin_required').hasClass("red");
    var pinCode = (needPin)? $.trim($('#pin_code').val()) : null;
    if (needPin && ( pinCode.length !=4 || isNaN(pinCode) )) {
      instance.messagePage($.t('login.pinInvalid'));
      return;
    }

    var clientName = $.trim($('#client_name').val());
    if (clientName == "") {
      clientName = os.hostname();
    }

    var info = {rvaIp: rvaIp, clientName: clientName, pinCode: pinCode};
    connectToRVA(info); // Connect To RVA
    
  },
  doLogin: function doLogin(result) {
    // Real Login
    if (result) {
      this.options.connected = true;  // set connected
    }
    this._init();
  },
  _doLogout: function _doLogout() {
    //console.log('_doLogout...');
    this.openWaitingPageNoTimer($.t('login.disconnecting'));
    rvaClient.disconnect();
  },
  resetAll: function resetAll() {
    //console.log('resetAll...');
    this._removeAllDialogs();
    
    // Close all child windows
    this._closeAllChildWindow();

    // Stop Voting
    if (this.options.votingEditorWindow != null && this.options.votingEditorObject != null) {
      var votingWin = gui.Window.get(this.options.votingEditorWindow);
      votingWin.window.stopVoting();
    }
    
    // Clear options...
    this._resetInfo();
    
    win.show(true);
    
    this._init();
  },
  _resetInfo: function _resetInfo() {
    //console.log('_resetInfo...');
    this.options.rvaType = 'EDU';
    this.options.rvaVersion = 2.1;
    this.options.sharedScreenNo = 1;
    this.options.connected = false;
    this.options.isHost = false;
    this.options.groupMode = false;
    this.options.groupName = "Not selected";
    this.options.pinRequired = false;
    this.options.pinCode = "----";
    this.options.viewMode = "full";
    this.options.nowPage = '#icon-connect';
    this.options.supportVideoClip = false;
    this.options.votingMode = false;
    this.options.votingObject = null;
    this.options.sharingMode = true;
    this.options.sendingFile = {
      sending: false,  // true/false
      file: null,
      percentage: 0,
      finished: 0,
      total: 0
    };
    this.options.receivingFile = false;
    
    this.options.rvaIP = null;
    this.options.ssid = null;
    this.options.lanIP = null;
    this.options.wifi = null;
    this.options.qrImg = null;
    this.options.screenNo = -1;

    $("#connected-username").text("");
    $("#connected-message").text("");
    $(".connected_rvaurl").text("");

    this.updateUserList([]);  // with refresh();
    
    this.options.votingEditorObject == null;
    this.options.tabletsLocked = false;
    this.options.projectionData = {grayOut: true};
    this._refreshVotingStart();
    this._disablePlayVideo();
    this._disableSharing();
    this._clearNotification();

  },
  updateInfo: function updateInfo(info) {
    //console.debug('updateInfo...', info.rvaType, info.rvaVersion, info);
    //console.log(JSON.stringify(info));

    this.options.rvaType = info.rvaType;
    if (this.options.rvaVersion != info.rvaVersion){
      if (info.rvaVersion < 2 && $('input[name=remoteMouse]:checked').val()=="0"){
        //console.log('updateInfo --> enable remote mouse', info.rvaVersion, $('input[name=remoteMouse]:checked').val());
        rvaClient.remoteMouseStart();
      }
    }
    
    this.options.rvaVersion = info.rvaVersion;
    //this.options.rvaVersion = "2.3";
    this.options.pinRequired = info.pinRequired;
    this.options.pinCode = info.pinCode;
    this.options.rvaIP = info.rvaIP;
    this.options.ssid = info.ssid;
    this.options.lanIP = info.lanIP;
    this.options.wifi = info.wifi;
    this.options.qrImg = info.qrImg;
    this.options.groupMode = info.groupMode;
    this.options.groupName = (this.options.groupMode)? info.groupName : $.t('group.notSelected');

    $("#connected-username").text(info.clientName);
    $("#connected-message").text($.t('login.youConnected'));
    $(".connected_rvaurl").text(info.rvaIP);

    this.options.supportVideoClip = (this.options.rvaVersion >= 1.5 || this.options.rvaType == "NOVOCAST") ? true : false;
    this.options.votingMode = (this.options.rvaVersion < 1.5 || this.options.rvaType == "NOVOCAST")? false : true;
    this.options.sharingMode = (this.options.rvaVersion < 1.6 || this.options.rvaType == "NOVOCAST")? false : true;

    this.refresh();
    $(this.options.nowPage).trigger("click");
  },
  updatePinCode: function updatePinCode(pin) {
    if (pin) {
      this.options.pinRequired = true;
      this.options.pinCode = pin;
      this.refresh();
      return;
    }

    this.options.pinRequired = false;
    this.options.pinCode = '----';
    this.refresh();
  },
  updateUserList: function updateUserList(data) {
    //console.log('[updateUserList]... connected=', this.options.connected, data);
    var instance = this;
    if (instance.options.connected) {
      instance.options.userList = data;  // update user list
      instance.refresh();
      var preScreen = instance.options.screenNo;
      $.each(data, function( i, obj ) {
        if (obj['isMyself']) instance.options.screenNo = obj['panel'];
      });
      
      if (instance.options.screenNo != preScreen) {
        //console.log('updateUserList...change screen', preScreen, instance.options.screenNo);
        if (instance.options.screenNo > -1) {
          instance._sendNotification();
        }
        else {
          instance._clearNotification();
        }
      }
    }
  },
  _sendNotification: function _sendNotification(){
    //console.log('-----_sendNotification...');
    var instance = this;
    instance._clearNotification();
    var screenNo = instance.options.screenNo;
    var iconFile = (isWin)? "../images/icon.png" : path.resolve("../images/icon.png");
    var options = {
      icon: iconFile,
      body: $.t("login.softwareName")
    };
    var msg = (screenNo === 0)? $.t("info.showOnScreen") : $.t("info.showOnSubScreen", {no: screenNo});
    instance.options.notification = new Notification(msg, options);
    //console.log('-----_sendNotification done...', instance.options.notification.title);
  },
  _clearNotification: function _clearNotification(){
    //console.log('-----_clearNotification...');
    var instance = this;
    if (instance.options.notification != null && typeof instance.options.notification === "object") {
      var title = instance.options.notification.title;
      instance.options.notification.close();
      instance.options.notification = null;
    }
  },
  toBeHost: function toBeHost(flag) {
    //console.warn('toBeHost...', flag);
    this.options.isHost = flag;  // set as host
    this.refresh();
  },
  setViewMode: function setViewMode(mode) {
    this.options.viewMode = mode;  // full/split
    this.refresh();
  },
  previewImage: function previewImage(data) {
    $('#'+data.uid).powerTimer('stop').removeClass('waiting');
    //var obj = $("#" + data.uid).find('.preview img').prop("src", '../' + data.src);
    var obj = $("#" + data.uid).find('.preview').empty().append('<img src="data:image/png;base64,'+ data.src +'">');
    //.empty().append('<img src="data:image/png;base64,'+ data.src +'">');
  },
  previewTimeout: function previewTimeout(uid) {
    //console.log('previewTimeout...uid=', uid, $("#" + uid).find('.preview'));
    $('#' + uid).powerTimer('stop').removeClass('waiting');
    var obj = $("#" + uid).find('.preview').empty().append('<img src="../images/preveiw-bg.jpg" style="width: 100%; height: 100%;">');
  },
  _setGroupMode: function _setGroupMode(groupMode) {
    var instance = this;
    if (groupMode) {
      $("#group-tabs > li").off().click(function(){
        //console.log($(this).attr("display"));
        $("#group-tabs > li").removeClass("active");
        $(this).addClass("active");
        instance.options.viewModel.display($(this).attr("display"));
        instance.refresh();
      });

      var tab = $('#group-tabs'),
          w = tab.parent().width() - tab.outerWidth() - 40;
      $('#group-select').width(w);
      $('#group-select span:last-child').width(w - 23);
      return;
    }

    // Not group mode
    $("#group-tabs > li").off().removeClass("active");
    $("#group-tabs > li").first().addClass("active");
    instance.options.viewModel.display("0");
    var div = $('#group-select'),
        w = div.width() - 23;
    $(div).width('auto');
    $('#group-select span:last-child').width('auto');
  },
  _enableLogoButton: function _enableLogoButton() {
    //console.debug("instance._enableLogoButton...", $("#logo"));
    var instance = this;
    $('#drop-mirroring').remove();
    
    //console.debug("instance._enableLogoButton...", instance.options.childWindows.videoWindow);
    if (instance.options.childWindows.videoWindow != null) {
      console.log("Disable projection button because of video window is open.");
      instance._disableLogoButton();
      return;
    }
    
    var popup = $('<ul id="drop-mirroring" class="f-dropdown drop-left" style="position: absolute; left: 20px; top: 20px; width: auto; min-width: initial; white-space: nowrap;"></ul>');
    var logo = $("#logo").append(popup);
    
    if (logo.hasClass("pause")){
      // resume presentation
      $('<li>').append($('<a href="#" id="resumeMirroring"></a>')
                       .text($.t('info.resumeMirroring'))
                       .prepend('<span class="play"></span>'))
        .click(function(){
          //console.log("Resume Screen");
          logo.removeClass().addClass("loading").off();
          popup.removeClass('open');
          rvaClient.resumeScreen();
        }).appendTo(popup);
    }
    else {
      // pause presentation
      $('<li>').append($('<a href="#" id="pauseMirroring"></a>')
                       .text($.t('info.pauseMirroring'))
                       .prepend('<span class="pause"></span>'))
        .click(function(){
          //console.log("Pause Screen");
          logo.removeClass().addClass("loading").off();
          popup.removeClass('open');
          rvaClient.pauseScreen();
        }).appendTo(popup);
    }
    
    // withdraw presentation
    if (instance.options.rvaVersion >= 2.2) {
      $('<li>').addClass("novocast-none")
        .append($('<a href="#" id="stopMirroring"></a>')
                 .text($.t('info.stopMirroring'))
                 .prepend('<span class="stop"></span>'))
        .click(function(){
          //console.log("Stop stopPresentation");
          logo.removeClass().addClass("loading").off();
          popup.removeClass('open');
          // stop myself presentation
          rvaClient.stopPresentation();
        }).appendTo(popup);
    }
    
    logo.off().click(function(){
      //console.log('Click logo...');
      if (instance.options.childWindows.videoWindow != null) {
        console.log("Disable projection button because of video window is open.");
        instance._disableLogoButton();
        return;
      }
      
      $("li.extendItem").remove();
      if (gui.Screen.screens.length < 2) {
        instance.options.sharedScreenNo = 1;
      }
      rvaClient.extendedScreenSupport(function(result){
        if (result) {
          // result == true: support
          rvaClient.getCurrentScreen(function(result){
            /*
              result: 
              0 is no mirror screen.
              1 is mirror main screen.
              2 is mirror extended screen.
            */
            //if (instance.options.sharedScreenNo === 1) {
            if (result == 2) {
              // go to duplicate/primary screen
              $('<li>').addClass("extendItem")
              .append($('<a href="#"></a>')
                      .text($.t('info.extendScreen1'))
                      .prepend('<span class="extendScreen1"></span>'))
              .click(function(){
                //console.log("back to duplicate/primary screen");
                logo.removeClass().addClass("loading").off();
                popup.removeClass('open');
                rvaClient.extendScreen(0);
                instance.options.sharedScreenNo = 1;
                return false;
              }).appendTo(popup);
            }
            else {
              // go to extended/secondary screen
              $('<li>').addClass("extendItem")
              .append($('<a href="#"></a>')
                      .text($.t('info.extendScreen2'))
                      .prepend('<span class="extendScreen2"></span>'))
              .click(function(){
                //console.log("extended/secondary screen");
                logo.removeClass().addClass("loading").off();
                popup.removeClass('open');
                rvaClient.extendScreen(1, 0);
                instance.options.sharedScreenNo = 2;
                return false;
              }).appendTo(popup);
            }
          });
        }
        else {
          // result == false: NOT support
          instance.options.sharedScreenNo = 1;
        }
      });
      // show menu
      popup.addClass('open');
      //$(this).addClass("hover");
      return false;
    }).hover(
      function(){
        //console.log('Hover logo...');
        $(this).addClass("hover");
        return false;
      },
      function(){
        //console.log('Leave logo...');
        $(this).removeClass("hover");
        return false;
      }
    );

  },
  _disableLogoButton: function _disableLogoButton() {
    //console.log("instance._disableLogoButton...");
    $("#logo").removeClass().off();
    $('#drop-mirroring').remove();
  },
  receiveScreenPause: function receiveScreenPause(flag) {
    //console.debug("instance.receiveScreenPause...", flag);
    var logo = $("#logo").removeClass("loading");
    if (flag){
      logo.addClass("pause");
    }
    else {
      logo.removeClass("pause");
    }
    this._enableLogoButton();
  },
  _enableTaskbar: function _enableTaskbar() {
    var instance = this;
 
    // TaskBar: connect
    $("#icon-connect").off().click(function(){
      instance.options.nowPage = "#icon-connect";
      instance.refresh();
      $(".topBtn").removeClass("focus");
      $(this).addClass("focus");
      instance._hideAllpages();
      $(".connected-part").show();
    });

    // TaskBar: group
    $("#icon-group").off().click(function(){
      instance.options.nowPage = "#icon-group";
      $(".topBtn").removeClass("focus");
      $(this).addClass("focus");
      instance._hideAllpages();
      $(".connected-list").show();
      instance.refresh();
      ($('#white-cover').css('display')=='block')? instance._disableCorpPanelControls() : null;
    });

    // TaskBar: tool
    $("#icon-tool").off().click(function(){
      instance.options.nowPage = "#icon-tool";
      instance.refresh();
      $(".topBtn").removeClass("focus");
      $(this).addClass("focus");
      instance._hideAllpages();
      $(".tool-list").show();
    });

  },
  _getFullMode:function _getFullMode() {
    $(".full-view").show();
    $(".split-view").hide();

    var instance = this;
    (instance.options.supportVideoClip && instance.options.screenNo === 0)? instance._enablePlayVideo() : instance._disablePlayVideo();
  },
  _getSplitMode: function _getSplitMode() {
    $(".full-view").hide();
    $(".split-view").show();
    this._disablePlayVideo();
  },
  setProjectionData: function setProjection(data) {
    //console.info("[setProjectionData]...", data);
    var instance = this;
    instance.options.projectionData = data;
    instance.refresh();
  },
  _setProjection: function _setProjection() {
    var instance = this;
    var rvaVersion = instance.options.rvaVersion;
    var opt = instance.options.projectionData;
    //opt = {grayOut: true/false, lightOn: true/false, isHotspot: true/false, WiFiOn: true/false} // next time
    //opt = {grayOut: true/false, lightOn: true/false}
    //opt = {grayOut: true}
    //opt = {grayOut: false, lightOn: false}
    //opt = {grayOut: false, lightOn: true}
    //console.debug('[_setProjection]...', rvaVersion, opt);

    if (rvaVersion < 2.1 || !opt || opt.grayOut){
      instance._disableProjection();
      //console.log('grayOutAll...', opt);
      rvaVersion = null;
      opt = null;
      return;
    }
    
    var hasProjectionPrevilege = false;
    if (instance.options.rvaType == 'EDU') {
      hasProjectionPrevilege = instance.options.isHost;
    }
    if (instance.options.rvaType == 'CORP') {
      if (instance.options.viewModel.hasHost()) {
        hasProjectionPrevilege = instance.options.isHost;
      }
      else {
        hasProjectionPrevilege = true;
      }
    }

    // reset and display right icon
    var btn = $("#icon-projection").removeClass().attr('title', $.t('info.switchMode')).off(); // reset icon
    btn.click(function(){
      if (hasProjectionPrevilege) {
        instance._confirmProjection();
      }
      else {
        ($(this).hasClass("focus"))? rvaClient.send_CMD_REMOTE_CONTROL(50) : rvaClient.send_CMD_REMOTE_CONTROL(40);
        $(this).toggleClass("focus").off();
      }
    });
    (!hasProjectionPrevilege && opt.lightOn)? $("#icon-projection").addClass("focus") : $("#icon-projection").removeClass("focus");
    (!hasProjectionPrevilege && opt.lightOn)? instance._disableLogoButton() : null;

  },
  _confirmProjection: function _confirmProjection() {
    var instance = this;
    instance._removeAllDialogs();

    // confirm to Ask someone to be host page
    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t('info.switchMode')))
                .append($('<p>').html($.t('info.switchModeMsg')));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            $("#icon-projection").off();
            //console.log('Click #icon-projection to send ', 40);
            rvaClient.send_CMD_REMOTE_CONTROL(40);
            $(this).dialog('destroy').remove();
          }
        },
      ]
    });
  },
  _disableProjection: function _disableProjection() {
    var instance = this;
    var rvaVersion = instance.options.rvaVersion;
    //console.log('_disableProjection...', rvaVersion);
    var btn = $("#icon-projection").removeClass().attr('title', '').off(); // reset icon
    
    if (rvaVersion < 2) {
      btn.addClass('hide');
      return false;
    }
    
    if (rvaVersion < 2.1) {
      btn.addClass('disable').addClass('warning').attr('title', $.t('info.needToGradeMsg', {verNo: "2.1"}));
      return false;
    }
    
    // the others
    btn.addClass('disable');
  },
  enableCastButton4NovoCast: function enableCastButton4NovoCast() {
    var instance = this;
    var opt = instance.options.projectionData;
    var btn = $("#icon-projection").removeClass().attr('title', '').off(); // reset icon
    console.warn("[enableCastButton4NovoCast]...", opt);
    
    // not supprot miracast/airplay
    if (opt.grayOut === undefined || opt.grayOut){
      btn.addClass('disable');
      opt = null;
      return;
    }
    
    // could toggle button
    btn.attr('title', $.t('info.switchMode')).off().click(function(){
      ($(this).hasClass("focus"))? rvaClient.send_CMD_REMOTE_CONTROL(50) : rvaClient.send_CMD_REMOTE_CONTROL(40);
      $(this).toggleClass("focus").off();
    });
    (opt.lightOn)? $("#icon-projection").addClass("focus") : $("#icon-projection").removeClass("focus");
    (opt.lightOn)? instance._disableLogoButton() : null;
  },
  _enablePlayVideo: function _enablePlayVideo() {
    var instance = this;
    $("#play-video").removeClass("disable").attr('title','').off().click(function() {
      //console.log("Open video player...", instance.options.childWindows.videoWindow);
      var mainWin = gui.Window.get();

      var win;
      if (instance.options.childWindows.videoWindow != null) {
        mainWin.minimize();
        win = gui.Window.get(instance.options.childWindows.videoWindow);
        win.hide();
        win.show(true);
        win = null;
        return;
      }

      instance.options.childWindows.videoWindow = window.open("../views/video.html?setLng=" + i18n.lng(), "videoWindow");
      win = gui.Window.get(instance.options.childWindows.videoWindow);
      win.hide();

      win.on('loaded', function() {
        this.window.init(rvaClient);
        instance.refresh();
      });

      win.on('closed', function() {
        //console.log('Close video player page, set null');
        win = instance.options.childWindows.videoWindow = null;
        rvaClient.stopYouTubeVideo();
        instance.refresh();
        mainWin.show(true);
      });
      win = null;
      mainWin.minimize();
    });
  },
  _disablePlayVideo: function _disablePlayVideo() {
    var instance = this;
    var obj = $("#play-video").addClass("disable").attr('title','').off();

    // close video window
    var win;
    if (instance.options.childWindows.videoWindow != null) {
      win = gui.Window.get(instance.options.childWindows.videoWindow);
      win.close(true);
      instance.options.childWindows.videoWindow = null;
      win = null;
      //console.log("Close video window");
      rvaClient.stopYouTubeVideo();
      //close_video_panel();
    }

    // Enable video big icon, then display tooltip
    // console.info('_disablePlayVideo...', instance.options.supportVideoClip);
    if (instance.options.supportVideoClip && instance.options.screenNo === 0) {
      $("#play-video").attr('title',$.t('video.needFullScreen'));
      return;
    }
    
  },
  _enableSharing: function _enableSharing() {
    //console.debug("[_enableSharing] this.options.sharingMode=", this.options.sharingMode, this.options.rvaVersion);
    var instance = this;

    // sharing button
    $('#open-sharing').removeClass("disable").attr('title', '').off().click(function() {
      //console.log("Click sharing...", this);
      $('#drop-sharing').toggleClass('open');
      return false;
    });
    
    $('#drop-sharing').find('.online').removeClass('hide').end();

    // open GroupShare folder
    $('#openGroupShare').off().click(function(e){
      e.preventDefault();
      instance._openGroupShareFolder();
    });

    // choose file to share
    $('#chooseFile').off().click(function(e){
      e.preventDefault();
      //console.log("Click chooseFile...", this);
      $('#sendingFile').val(null).trigger('click');
      return false;
    });

    $('#sendingFile').off().change(function(e) {
      //console.warn("Change sendingFile...", this);
      var files = $(this)[0].files;
      var file = files[0];
      //console.log("file=", file, file.size);

      // File size should be less than 10MB/3MB.
      var maxFileSize = (instance.options.rvaVersion >= 2)? 10485760 : 3145728;
      if (file.size >= maxFileSize) {
        instance._fileTooLargePage();
        return;
      }

      //console.log("Send this file...", file);
      async.series([
        function(done){
          // Step1: check client is receiving file?
          if (rvaClient.isFileDownloading()) {
            instance.systemBusyPage();
          }
          else {
            done();
          }
        },
        function(done){
          if (!rvaClient.isFileReadyToSend()) {
            // Step2: check last sending finish?
            rvaClient.askSendAnotherFilePage();
          }
          else {
            rvaClient.initialFileSharing(file);
            // Step3: check RVA busy?
            done();
          }
        },
        function(err, results){
          $(this).val(null);
        }]);
      });

    // choose file to share
    $('#shareScreenShot').off().click(function(e){
      e.preventDefault();
      // Minimize the window
      gui.Window.get().minimize();
      // To capture screen
      //console.log('Click to capture screen and share...');
      async.series([
        function(done){
          // Step1: check client is receiving file?
          if (rvaClient.isFileDownloading()) {
            instance.systemBusyPage();
          }
          else {
            done();
          }
        },
        function(done){
          if (!rvaClient.isFileReadyToSend()) {
            // Step2: check last sending finish?
            rvaClient.askSendAnotherFilePage();
          }
          else {
            rvaClient.initialFileSharing(null);
            // Step3: check RVA busy?
            done();
          }
        },
        function(err, results){
          $(this).val(null);
        }
      ]);
    });

    $("#callUploading").off().click(function(){
      // To get upload_report
      rvaClient.setBackGroundFileSending(false);
      // To open this dialog
      rvaClient.openSendingFilePage();
    });
    
    // by RVA version
    if ( this.options.rvaVersion >= 2.2) {
      // share a URL
      $('#shareLink').off().click(function(e){
        e.preventDefault();
        //console.log("Click to share a URL...");
        instance._enterLinkDialog();
      });
    }
    else {
      $('#shareLink').off().closest('li').addClass('hide').end();
      //$('#receivedLinks').off().closest('li').addClass('hide').end();
    }

    // open links dialog
    $('#receivedLinks').off().click(function(e){
      e.preventDefault();
      instance.linkHistory();
    });
  },
  _disableSharing: function _disableSharing() {
    //console.debug("[_disableSharing] this.options.sharingMode=", this.options.sharingMode);
    var instance = this;
    
    if (this.options.sharingMode) {
      // No Receiver
      // open pop-up
      $('#open-sharing').removeClass("disable").attr('title','').off().click(function() {
        //console.log("Click sharing...", this);
        $('#drop-sharing').toggleClass('open');
        return false;
      });
      // hide on-line function
      $('#drop-sharing').find('.online').addClass('hide').end();
      
      // open GroupShare folder
      $('#openGroupShare').off().click(function(e){
        e.preventDefault();
        instance._openGroupShareFolder();
      });
      
      // open links history
      $('#receivedLinks').off().click(function(e){
        e.preventDefault();
        instance.linkHistory();
      });
    
    }
    else {
      // too low version and need to upgrade, disable all function
      $("#open-sharing").off().removeClass('active').addClass("disable").attr('title', $.t('info.needToGradeMsg', {verNo: "1.6"}));
      $('#openGroupShare').off();
      $('#receivedLinks').off()
    }
    
    $('#chooseFile').off();
    $('#shareScreenShot').off();
    $("#callUploading").removeClass("active").off();
  },
  _enterLinkDialog: function _enterLinkDialog(){
    var instance = this;
    instance._removeAllDialogs();
    var input = $('<input id="inputLink" type="text">').attr('placeholder',$.t('sharing.urlInputPlaceholder'));
    var error = $('<div>').addClass('error');
    var inputArea = $('<div>').addClass('inputURLArea').append(input).append(error);
    
    if(document.queryCommandSupported('paste')) {
      $('<span>').addClass('inputIcon').addClass('paste').click(function(){
        $(input).focus();
        document.execCommand('paste');
      }).appendTo(inputArea);
    }
    
    $('<span>').addClass('inputIcon').addClass('clear').click(function(){
      $(input).val('');
    }).appendTo(inputArea);
                                           
    // Display message
    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t('sharing.shareLink')))
                .append($('<p>').text($.t('sharing.urlInput')))
                .append(inputArea);
 
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page no-titlebar",
      create: function( event, ui ) {
        $(input).focus();
        supportRightClick(input);
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.ok"),
          click: function() {
            if (input.val() == ""){
              error.text($.t('sharing.urlNone'));
              input.focus();
              return;
            }
            if (input.val().length > 504 ){
              error.text($.t('sharing.urlOverLength'));
              input.focus();
              return;
            }
            $(this).dialog("close");
            instance._confirmSendUrlDialog($(input).val());
          }
        },
        {
          text: $.t("button.cancel"),
          click: function() {
            $(this).dialog("close");
          }
        },
      ]
    });
  },
  _confirmSendUrlDialog: function _confirmSendUrlDialog(url, orgPage){
    var instance = this;
    //instance._removeAllDialogs();

    var receivers = (instance.options.isHost)?
                      $.t('sharing.allParticipants') :
                      ((instance.options.viewModel.hasHost())?
                          instance.options.viewModel.moderatorName() :
                          $.t('sharing.allParticipants'));
    
    var obj = $('<span class="sendLinkMsg">')
              .html($.t("sharing.sendURLMsg", {link: '<a>URL</a>', receiver: receivers}));
    
    $(obj).find('a').text(url).click(function(e){
      e.preventDefault();
      //console.log("Click to open URL:", url);
      gui.Shell.openExternal(url); // open brower
    }).end();
    
    //console.debug('description=', obj);    
    var msg = {
      title: $.t('sharing.shareLink'),
      description: obj
    };
    
    var cmd = function(){
      //console.debug('Click Yes... Send', url);
      var obj = $('<span class="sendLinkMsg">').html($.t("sharing.sentLinkMsg", {link: '<a>URL</a>'}));
      $(obj).find('a').text(url).click(function(e){
                    e.preventDefault();
                    //console.log("Click to open URL:", url);
                    gui.Shell.openExternal(url); // open brower
                  }).end();
      var msg2 = {
        title: $.t('sharing.sentLinkTitle'),
        description: obj,
        btn: 'button.close'
      };
      rvaClient.sendURLSharing(url);
      instance.messagePage(msg2, orgPage);
    }
    
    instance.confirmPage(msg, cmd, orgPage);

  },
  urlReceived: function urlReceived(data) {
    //console.log("[urlReceived] data=", data);
    var instance = this;
    instance._removeAllDialogs();
    
    var link = $('<a>').text(data.url);
    var p = $('<p>').append($.t('sharing.receiveLinkMsg', {sender: '<b>'+ data.sender +'</b>'}));
                                                             
    // Display message
    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t('sharing.receiveLinkTitle')))
                .append(p)
                .append(link);

    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page no-titlebar",
      create: function( event, ui ) {
        $(link).click(function(e){
          e.preventDefault();
          //console.log("Click to open URL:", data.url);
          gui.Shell.openExternal(data.url); // open brower
          $(page).dialog('close');
        });                                             
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("voting.open"),
          click: function() {
            //console.log("Click to open URL:", data.url);
            gui.Shell.openExternal(data.url); // open brower
            $(this).dialog("close");
          }
        },
        {
          text: $.t("button.close"),
          click: function() {
            $(this).dialog("close");
          }
        },
      ]
    });
  },
  linkHistory: function linkHistory() {
    var instance = this;
    var links = rvaClient.getURLHistory();
    //console.log("[linkHistory] links=", links);
    
    // Sort by timestamp
    links.sort(function(a, b) {
      return a.timestamp < b.timestamp;
    });
                        
    instance._removeAllDialogs();
    
    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t('sharing.receiveLinkTitle')));
    var div = $('<div>').addClass('remove-cover').appendTo(page);
    var removeAll = $('<span>').text($.t('sharing.removeAll')).appendTo(div);
    var list = $('<ul>').addClass('left').addClass('linkHistory').appendTo(page);
    
    $.each(links, function(index, row) {
      //console.log(index, ": ", row.pid, row.url, row.timestamp);
      var link = $('<a>').text(row.url).click(function(e){
                    e.preventDefault();
                    //console.log("Click to open URL:", row.url);
                    gui.Shell.openExternal(row.url); // open brower
                  });
      var li = $('<li>').append(link).append('<br>')
                .append($("<span>").addClass('senderName').text(row.sender))
                .appendTo(list);
      // remove icon
      $('<span>').addClass('remove').appendTo(li).click(function(){
        //console.debug("Remove pid=", row.pid, links.length, links);
        rvaClient.removeURLHistory(row.pid);
        $(li).remove();
        if (links.length == 0) {
          $('<li>').text($.t('sharing.noLink')).appendTo(list);
          removeAll.hide();
        }
      });
      // send url icon
      if (instance.options.connected && instance.options.sharingMode && instance._receivers()) {
        $('<span>').addClass('send-url').appendTo(li).click(function(){
          //console.debug("Send url=", row.url);
          instance._confirmSendUrlDialog(
            row.url,
            function(){ $('#receivedLinks').trigger('click'); }
          );
        });
      }
    });
    // remove all icon
    removeAll.click(function(){
      //console.debug("Remove all links...");
      instance.removeAllLinksDialog();  // dialog
    });
    
    if (links.length == 0) {
      $('<li>').text($.t('sharing.noLink')).appendTo(list);
      removeAll.hide();
    }
    
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page no-titlebar",
      create: function( event, ui ) {
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.close"),
          click: function() {
            $(this).dialog("close");
          }
        },
      ]
    });
  },
  removeAllLinksDialog: function removeAllLinksDialog() {
    var instance = this;
    instance._removeAllDialogs();
    
    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t('sharing.removeAllLinksTitle')))
                .append($('<p>').text($.t('sharing.removeAllLinksMsg')));
    
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page no-titlebar",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
        instance.linkHistory();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            $(this).dialog("close");
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            rvaClient.removeURLHistory(-1); // remove from DB
            $(this).dialog("close");
          }
        },
      ]
    });

  },
  goVoting: function goVoting(data){
    var instance = this;
    //console.log('goVoting...', data);
    instance.options.votingObject = {isReady: data.isReady, question: data.question};
    // the window existed.
    if (instance.options.childWindows.votingWindow) {
      childwin = gui.Window.get(instance.options.childWindows.votingWindow);
      //console.log("New question....");
      childwin.window.createQuestion(instance.options.votingObject);
      childwin.show(true);
      return;
    }
    $('#open-voting').trigger('click');
  },
  votingHandler: function votingHandler(evt, data) {
    var instance = this,
        mainWin = gui.Window.get();
    //console.log('votingHandler...', evt, data);

    //votingHandler('closeVotingWindow');
    if (evt === 'closeVotingWindow') {
      if (instance.options.childWindows.votingWindow) {
        votingWin = gui.Window.get(instance.options.childWindows.votingWindow);
        votingWin.close();  // Close it when open
      }
      mainWin.show(true);
      return;
    }

    // no preious question
    if (!instance.options.votingObject) {
      return;
    }

    //votingHandler('clickVotingAnswer', {answer: 'A'});
    if (evt === 'clickVotingAnswer') {
      //console.log('clickVotingAnswer...', data, instance.options.votingObject);
      instance.options.votingObject.studentClickAnswer = data.answer;
      return;
    }

    //votingHandler('submitVotingAnswer', {answer: 'A'});
    if (evt === 'submitVotingAnswer') {
      //console.log('submitVotingAnswer...', typeof data.answer, instance.options.votingObject);
      instance.options.votingObject.studentSubmitAnswer = data.answer;
      // Send answer to RVA
      getDownscaleImageBase64String(data.answer, function(result) {
        rvaClient.sendVotingAnswer(instance.options.votingObject.question.num, result);
      });
      return;
    }

    var votingWin = instance.options.childWindows.votingWindow;

    //votingHandler('submitAnswerResult', {result: true, message: 'Your answer is submitted successfully'});
    if (evt === 'submitAnswerResult') {
      //console.log('submitAnswerResult...', data);
      instance.options.votingObject.studentAnswerSubmited = data.result;
      instance.options.votingObject.studentAnswerSubmitedMsg = data.message;
      (votingWin)? votingWin.window.submitAnswerResult(data.result, data.message) : $('#open-voting').trigger('click');
      return;
    }

    //votingHandler('showAnswerResultDialog', {flag: 1/2/3, correctAnswer: '...'});
    //votingHandler('showAnswerResultDialog', {flag: 1, correctAnswer: '3'});
    //votingHandler('showAnswerResultDialog', {flag: 2, correctAnswer: '3'});
    //votingHandler('showAnswerResultDialog', {flag: 3, correctAnswer: '3'});
    if (evt === 'showAnswerResultDialog') {
      //console.log('showAnswerResultDialog...', data);
      instance.options.votingObject.flag = data.flag;
      instance.options.votingObject.correctAnswer = data.correctAnswer;
      (votingWin)? votingWin.window.showAnswerResultDialog(data.flag, data.correctAnswer) : $('#open-voting').trigger('click');
      return;
    }

    //votingHandler('removeVotingDialog');
    if (evt === 'removeVotingDialog') {
      //console.log('removeVotingDialog...', instance.options.votingObject);
      delete instance.options.votingObject['flag'];
      delete instance.options.votingObject['correctAnswer'];
      (votingWin)? votingWin.window.closeAllDialogs() : null;
      return;
    }

    //votingHandler('stopAnswerQuestion');
    if (evt === 'stopAnswerQuestion') {
      //console.log('stopAnswerQuestion...');
      instance.options.votingObject.stopVoting = true;
      instance.options.votingObject.isReady = false;
      (votingWin)? votingWin.window.stopAnswerQuestion() : null;
      // after stop voting, user can't open the voting window.
      instance.options.votingObject = {isReady:false, question: null};
      return;
    }

  },
  _openVotingEditor: function _openVotingEditor(){
    //console.info('_openVotingEditor...');

    // focus window when it's already open
    if (this.options.votingEditorWindow != null) {
      //console.info('Open existed voting editor window...');
      var voteWin = gui.Window.get(this.options.votingEditorWindow);
      voteWin.show(true);
      voteWin = null;
      return;
    }

    // open new window
    //console.info('Open NEW voting editor window...');
    var instance = this;
    instance.options.votingEditorWindow = window.open("../views/voting-editor.html?setLng=" + i18n.lng(), "votingEditorWindow", "width=830,height=630,*=no");
    var voteWin = gui.Window.get(instance.options.votingEditorWindow);
    voteWin.hide();
    
    voteWin.on('loaded', function(){
      //console.info('Open voting page for teacher...');
      this.window.init(gui.Window.get());
    });
    voteWin.on('closed', function() {
      //console.info('Close voting editor page, set null');
      instance.options.votingEditorWindow = null;
      instance.options.votingEditorObject = null;
      instance = null;
    });
    gui.Window.get().minimize();

    voteWin = null;
  },
  votingEditorHandler: function votingEditorHandler(evt, data) {
    var instance = this,
        mainWin = gui.Window.get();
    //console.log('votingEditorHandler...', evt, data);

    // votingEditorHandler('startVoting', data);
    if (evt === 'startVoting') {
      instance.options.votingEditorObject = data;
      // Start Voting
      //rvaClient.sendVotingPollingStart(data);
      rvaClient.callVotingStart(data);
      //console.log('startVoting...', data);
      return;
    }
    
    // getUserList
    if (evt === 'getUserList') {
      //console.log('getUserList...');
      return instance.options.userList;
    }    
    
    // votingEditorHandler('stopVoting');
    if (evt === 'stopVoting') {
      //console.log('stopVoting...');
      // Stop Voting
      instance.options.votingEditorObject = null;
      rvaClient.callVotingStop();
      return;
    }

    // votingEditorHandler('votingStartSuccess');
    if (evt === 'votingStartSuccess') {
      //console.log('votingStartSuccess...');
      if (instance.options.votingEditorWindow != null) {
        var votingWin = gui.Window.get(instance.options.votingEditorWindow);
        votingWin.show(true);
      }
      //mainWin.show(true);
      return;
    }
    
    // votingEditorHandler('votingStartFail');
    if (evt === 'votingStartFail') {
      console.error('votingStartFail...', data);
      instance.options.votingEditorObject = null;
      if (instance.options.votingEditorWindow != null) {
        var votingWin = gui.Window.get(instance.options.votingEditorWindow);
        votingWin.show(true);
        // Stop Voting
        votingWin.window.stopVoting();
      }
      return;
    }
    
    // votingEditorHandler('receiveAnswer', data);
    if (evt === 'receiveAnswer') {
      //console.log('receiveAnswer...', data);
      if (instance.options.votingEditorWindow != null) {
        var votingWin = gui.Window.get(instance.options.votingEditorWindow);
        votingWin.show(true);
        // Receive Answer
        votingWin.window.receiveAnswer(data);
      }
      return;
    }

  },
  _refreshVotingStart: function _refreshVotingStart(){
    //console.log("[_refreshVotingStart] this.options.isHost=", this.options.isHost);
    //console.log("[_refreshVotingStart] this.options.votingEditorWindow=", this.options.votingEditorWindow);
    if (this.options.votingEditorWindow != null) {
      gui.Window.get(this.options.votingEditorWindow).window.setModerator();
    }
  },
  _enableVoting: function _enableVoting() {
    var instance = this, openVoting = $('#open-voting');
    //console.warn('_enableVoting...', instance.options.viewModel.hasHost(), instance.options.viewModel.iamHost(), instance.options.isHost);
    $(openVoting).removeClass('disable').attr('title','').off();

    // [EDU] teacher role / [CORP] i am the host
    if (instance.options.viewModel.iamHost()) {
      $('#open-voting-text').text($.t('info.editVoting'));
    }
    else {
      $('#open-voting-text').text($.t('info.openVoting'));
      if (!instance.options.viewModel.hasHost()) {  // no host
        $(openVoting).addClass('disable');
      }
    }

    // Voting button
    $(openVoting).click(function(){
      var childwin, mainWin = gui.Window.get();

      // [EDU] teacher role / [CORP] i am the host
      // votingEditorWindow
      if (instance.options.viewModel.iamHost()) {
        // console.log("Open votingEditorWindow...");
        // close student voting page
        if (instance.options.childWindows.votingWindow != null) {
          win = gui.Window.get(instance.options.childWindows.votingWindow);
          win.close(true);
          win = instance.options.childWindows.votingWindow = null;
        }
        instance._openVotingEditor();
        return;
      }

      // [CORP] no host
      if (!instance.options.viewModel.hasHost()) {
        instance.messagePage($.t('voting.toBeHostFirst'));
        return;
      }

      // [EDU]student role / [CORP] not host but there is host
      //console.log("Open votingWindow...", instance.options.votingObject);
      if (!instance.options.votingObject || !instance.options.votingObject.isReady) {
        instance.messagePage($.t('voting.notStarted'));
        return;
      }

      // focus window when it's already open
      if (instance.options.childWindows.votingWindow != null) {
        childwin = gui.Window.get(instance.options.childWindows.votingWindow);
        childwin.show(true);
        childwin = null;
        return;
      }

      // open new window
      newpopup = instance.options.childWindows.votingWindow = window.open("../views/voting.html?setLng=" + i18n.lng(), "votingWindow", "width=820,height=620,*=no");
      childwin = gui.Window.get(newpopup);
      childwin.on('loaded', function(){
        //console.log('Open voting page for student', instance.options.votingObject);
        this.window.init(mainWin);
        this.window.createQuestion(instance.options.votingObject);
        //this.window.init(rvaClient, editor);
        //this.window.getMainWindow(mainWin);
      });
      childwin.on('closed', function() {
        //console.log('Close voting page, set null');
        childwin = instance.options.childWindows.votingWindow = null;
      });
      childwin = null;
    });
  },
  _disableVoting: function _disableVoting() {
    var instance = this, win;
    var obj = $("#open-voting").off().addClass("disable");
    // Close voting window when it is open
    if (instance.options.childWindows.votingWindow != null) {
      win = gui.Window.get(instance.options.childWindows.votingWindow);
      win.close(true);
      instance.options.childWindows.votingWindow = null;
      win = null;
    }
  },
  _enableHostControls: function _enableHostControls() {
    //console.log("_enableHostControls...");
    var instance = this;

    // enable pin code function
    instance._createPinCodeControls();

    // enable group function
    instance._enableGroupFunction();

    // enable function area
    instance._enableFunctionArea();

    // Enable user list function
    $(".user-list > ul").addClass('has-right');

    //console.log('_enableHostControls....', $(".panel > img").length);
    $(".project-user").off().click(function(){
      $(".project-user").removeClass("clicked");
      $(this).addClass('clicked');
      var panel = $('.panel', this),
          layer = $('.panel-overlay', this);
      var top = $(this).closest('li').offset().top;
      $(panel).css('top', top - 67);
      $(layer).css('top', top - 78);
      //console.log('offset:', $(panel).offset(), $(panel), $(layer));
      return false;
    });
    
    if ($(".panel > img").length == 0){

      // create panel image
      var img = $('<img alt="Assign panel" width="186" height="108">'),
          map = $('<map class="panelMap"></map>'),
          areas = ['<area region="0" alt="0" shape="rect" coords="62,27,123,80" href="#">',
                   '<area region="1" alt="1" shape="poly" coords="0,0,93,0,93,27,62,27,62,54,0,54" href="#">',
                   '<area region="2" alt="2" shape="poly" coords="93,0,186,0,186,54,123,54,123,27,93,27" href="#">',
                   '<area region="3" alt="3" shape="poly" coords="0,54,62,54,62,80,93,80,93,108,0,108" href="#">',
                   '<area region="4" alt="4" shape="poly" coords="93,80,123,80,123,54,186,54,186,108,93,108" href="#">'];

      $.map(areas, function(area){ $(map).append(area); });

      $('.user.online').each(function(index) {
        var person = $(this),
            uid = $(person).attr("id"),
            iconUser = $(person).find('.icon-user'),
            middle = [$(person).find('.user-name'), $(person).find('.user-device')],
            userName = $(person).find('.user-name').text(),
            panel = $(person).find('.panel');

        // create preview and panel
        if ($(person).hasClass("isMyself") || $(panel).attr("no") > -1){
          middle = null;
        }
        else {
          var preview = $('<div class="preview"></div>');
          $(person).append(preview);
        }

        $(panel).append(img.attr('src',$(panel).attr('src')).attr('usemap','#panel_'+uid).clone())
                .append(map.attr('name','panel_'+uid).clone());

        // li.user
        $(person).hover(
          function() {
            $(person).addClass("focused");
          },
          function() {
            $(person).removeClass("focused");
          }
        );

        if (!$(iconUser).hasClass("ishost")){
          $(iconUser).off().click(function(){
            if (rvaClient.checkIsVoting()) {
              instance.messagePage($.t('voting.notAllowTransferHost'));
              return;
            }
            instance._confirmAskHost(userName, uid);
          });
        }

        // click to preview and load image
        if (middle){
          $.map(middle, function(element) {
            $(element).off().click(function() {
              if ($(person).hasClass('waiting')) {
                return;
              }
              if ($(preview).css('display')!="none") {
                $(".user-list").css("max-height", "");
                $(preview).hide().empty();
                //console.warn('Hide preview...');
              }
              else {
                $(person).addClass('waiting');
                $(preview).fadeIn(1000);
                $(".user-list").css("max-height", 450);
                $('.preview').not($(preview)).hide().empty(); // hide the other preview images
                $('.user.online').not($(person)).removeClass('waiting');  // reset the others
                // Ask preview image and display
                //console.log("Ask preview image and display for uid:" + uid);
                // FUTURE: Update to support v1.6 RVA PREVIEW_DATA
                rvaClient.send_CMD_SCREENSHOT_REQUEST(uid);
              }
            })
          });
        }

      });

      // map area
      if (instance.options.sendViewerRequest.enable && instance.options.sendViewerRequest.millisecond > 0) {
        //console.log("Disable panel", instance.options.sendViewerRequest);
        $(".user-control").empty();
        $("#" + instance.options.sendViewerRequest.uid +" .user-control").addClass("loading").append($('<div>'));
        return;
      }
      
      $(".panel area").hover(
        function() {
          var displayImg = $(this).closest('map').prev();
          var src= $(displayImg).attr('src');
          $(displayImg).attr('src',presenterImgs[$(this).attr('region')]);
        },
        function() {
          var displayImg = $(this).closest('map').prev();
          var src= $(displayImg).closest('td').attr('src');
          $(displayImg).attr('src',src);
        }
      ).click(function() {
        var pno = $(this).attr("region"),
            pnoNow = $(this).closest("div.panel").attr("no"),
            uid = $(this).closest("li").attr("id");
        
        //console.log("Click panel...", uid, pno, pnoNow);
        if (pno == pnoNow) {
          // click mirroring panel
          var isMyself = $(this).closest("li").hasClass('isMyself');
          //console.info("Click panel...", uid, pno, pnoNow, isMyself);
          var msg = (isMyself)?
              ((pno == 0)? $.t('info.currentPanel') : $.t('info.currentPanelSplit')) :
              ((pno == 0)? $.t('info.othersCurrentPanel') : $.t('info.othersCurrentPanelSplit'));
          instance._confirmStopProjection(msg, uid);
        }
        else {
          //console.log("Set someone's panel uid:" + uid + ' panel:' + pno);
          $(this).off().closest("td").addClass("loading").empty().append($('<div>'));
          if (!isWin) instance._clearNotification();
          rvaClient.send_CMD_VIEWER_REQUEST(uid, pno);
          if (instance.options.sendViewerRequest.enable) {
            var second = 4000;
            instance.options.sendViewerRequest = {enable: true, millisecond: second, uid: uid};
            $('.user-list').powerTimer({
              delay: second,
              func: function() {
                instance.options.sendViewerRequest = {enable: true, millisecond: 0, uid: null};
                instance.refresh();
                //console.log('==> sendViewerRequest...', uid, pno, instance.options.sendViewerRequest);
              },
            });
          }
        }
      });

    }

    //console.log("(END)_enableHostControls...");
  },
  _disableHostControls: function _disableHostControls() {
    //console.debug("_disableHostControls...");

    // disable pin code function
    this._removePinCodeControls();
    // disable group function
    this._disableGroupFunction();
    // disable function area
    this._disableFunctionArea();

    $(".user-list > ul").removeClass('has-right');

    // disable click function
    $(".user").off();
    $(".icon-user").off();
    $(".user-name").off();
    $(".panel").off().empty();
    $(".user-device").off();

    // remove preview
    $(".preview").remove();

    // disable hover function
    $(".user").off("mouseenter mouseleave");

    // add hover css
    $(".user.online").hover(
      // Display host function
      function() {
        //$(this).addClass("hover");
      },
      // Hide host function
      function() {
        //$(this).removeClass("hover");
      }
    );

    //console.log("_disableHostControls...");
  },
  _createPinCodeControls: function _createPinCodeControls() {
    var instance = this;
    var pincode = $("#pin-code");
    if ($(".pin").length==0){
      // Pin on button
      var pinOn = $('<input type="button" class="pin" id="pin-on-btn">').click(function(event){
                    event.preventDefault();
                    //console.log("Disable Pin Code");
                    rvaClient.send_CMD_PIN_NOT_REQUIRED();
                    instance.options.pinRequired = false;
                    instance.refresh();
                  });
      $(pincode).after(pinOn);

      // Pin off button
      var pinOff = $('<input type="button" class="pin" id="pin-off-btn">').click(function(event){
                    event.preventDefault();
                    //console.log("Enable pin code");
                    rvaClient.send_CMD_PIN_REQUIRED();
                    instance.options.pinRequired = true;
                    instance.refresh();
                   });
      $(pinOn).after(pinOff);
    }
    // current on/off
    instance._setPinBtn(instance.options.pinRequired);
  },
  _removePinCodeControls: function _removePinCodeControls(){
    $(".pin").remove();
  },
  _setPinBtn: function _setPinBtn(status) {  // true/false
    // true
    if (status){
      $("#pin-on-btn").show();
      $("#pin-off-btn").hide();
    }
    else {  // false
      $("#pin-on-btn").hide();
      $("#pin-off-btn").show();
    }
  },
  _enableGroupFunction: function _enableGroupFunction(){
    var instance = this;
    
    $('#group-select .triangleRight').show();
    
    $('#group-select').removeClass('disabled').off().click(function(){
      //console.log('Open group...');
      if (rvaClient.checkIsVoting()) {
        instance.messagePage($.t('voting.notAllowLoadGroup'));
        return;
      }
      if (rvaClient.checkIsFileSharing()) {
        instance.messagePage($.t('sharing.notAllowLoadGroup'));
        return;
      }

      $("#dialog-group-list").dialog({
        autoOpen: true,
        resizable: false,
        modal: true,
        width: '90%',
        height: 190,
        minHeight: 190,
        maxHeight: 500,
        dialogClass: "titlbar-only-close no-buttonpane",
        show: {
          effect: "blind",
          duration: 300
        },
        close: function( event, ui ) {
          $(this).dialog('destroy');
        }
      });
    });

    // Generate group list to choose
    var groups;
    async.series([
      function(done){
        rvaClient.getGroups(function(data){
          groups = data;
          done();
        });
      },
      function(done){
        var panel = $('#g-list').empty(), div;
        
        _(groups).forEach(function(item){
          //console.info("item=", item, item.group.class);
          //console.info("item=", item, item.group.student.length, item.group.teacher.name);
          if (item.group.student === undefined || item.group.student.length == 0) {
            return;
          }
          //console.warn(item.group.class, 'group.student=', item.group.student);
          var cnt = (item.group.student === undefined)? 0 : ((item.group.student.length > -1)? item.group.student.length : 1);
          div = $('<div></div>')
                .append($('<div></div>').text(item.group.class + " (" + cnt + ")"))
                .append($("<div></div>").text(item.group.teacher.name))
                .off().click(function(){
                  $("#dialog-group-list").dialog('destroy');
                  if (rvaClient.checkIsVoting()) {
                    instance.messagePage($.t('voting.notAllowLoadGroup'));
                    return;
                  }
                  if (instance.options.groupName == item.group.class) {
                    return;
                  }
                  rvaClient.setGroup(item.group.class);
                });
          $(panel).append(div);
        });
        
        if (instance.options.groupMode) {
          // Add remove option to TOP
          div = $('<div></div>')
                .append($('<div></div>').text($.t('group.deselected')))
                .off().click(function(){
                  //console.log("Leave group mode...");
                  $("#dialog-group-list").dialog('destroy');
                  if (rvaClient.checkIsVoting()) {
                    instance.messagePage($.t('voting.notAllowRemoveGroup'));
                    return;
                  }
                  instance._askCancelGroupModePage();
                });
          $(panel).prepend($('<hr>')).prepend(div);
        }
        
        if ($('#g-list > div').length == 0) {
          // display: No group to select
          $(panel).append($('<div>'+$.t('group.noGroupItem')+'</div>'));
        }
        done();
      },
      function(err, results){
      }
    ]);

  },
  _disableGroupFunction: function _disableGroupFunction(){
    $('#group-select').off().addClass('disabled');
    $('#group-select .triangleRight').hide();
    $('#g-list').empty();
  },
  _enableFunctionArea: function _enableFunctionArea(){
    //console.debug("_enableFunctionArea...");
    var instance = this;

    // enable Student Tablets
    var btn = $('#toggle-tablet');
    (instance.options.tabletsLocked)? $(btn).addClass('locked') : $(btn).removeClass('locked');
    $(btn).removeClass('disable').off().click(function(){
      if ($(this).hasClass('loading')){
        return;
      }
      $(this).addClass('loading');
      if ($(this).hasClass('locked')){
        $(this).removeClass('locked');
        //console.debug('Unlock all student Tablet');
        //Unlock all student Tablet
        rvaClient.sendLockUnlock();
        $(btn).powerTimer({
          name: 'sendLockTablet',
          sleep: 5000,
          func: function() {
            $(btn).powerTimer('stop').removeClass('loading');
          },
        });
        return;
      }
      $(this).addClass('locked');
      //console.debug('Lock all student Tablet');
      //Lock all student Tablet
      rvaClient.sendLockUnlock();
      $(btn).powerTimer({
        name: 'sendLockTablet',
        sleep: 5000,
        func: function() {
          $(btn).powerTimer('stop').removeClass('loading');
        },
      });
      return;
    });

    // enable Terminate Session function
    $('#terminate-session').removeClass('disable').off().click(function(){
      instance._confirmTerminateSession();
      return;
    });

  },
  _disableFunctionArea: function _disableFunctionArea(){
    //console.debug("_disableFunctionArea...");
    
    // disable Tablets
    $('#toggle-tablet').removeClass('locked').removeClass('loading').addClass('disable').off();

    // disable Terminate Session function
    $('#terminate-session').addClass('disable').off();

  },
  _hideAllpages: function _hideAllpages(){
    //console.log('_hideAllpages...');
    $(this.element).children().not('.taskbar').hide();
    $("#projection-tab").hide();
  },
  _removeAllDialogs: function _removeAllDialogs(){
    //console.log('_removeAllDialogs...');
    $('.message-page').not('.prevent-close-outside').dialog('destroy').remove();
    if ($('.prevent-close-outside').length == 0) $(".ui-effects-wrapper").remove();
  },
  messagePage: function messagePage(msg, orgPage) {
    var instance = this;
    instance._removeAllDialogs();
    
    var page = $('<div class="message-page"></div>');

    //console.debug(msg, typeof(msg));
    if (typeof(msg) == "string") {
      $(page).text(msg);
    }
    else {
      if (msg.title != undefined && msg.title != '') {
        $('<h4>').text(msg.title).prependTo(page);
      }
      //console.debug(msg.description, typeof(msg.description));
      if (typeof(msg.description) == "string") {
        $('<p>').text(msg.description).appendTo(page);
      }
      else {
        $('<p>').append($(msg.description)).appendTo(page);
      }
    }
    
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      show: {
        effect: "blind",
        duration: 500
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t((msg.btn != undefined)? msg.btn : "button.ok"),
          click: function() {
            $(this).dialog('destroy').remove();
            (orgPage != undefined)? orgPage() : null;
         }
        }
      ]
    });
    
    this.displayOnTop();

  },
  confirmPage: function confirmPage(msg, cmd, orgPage) {
    var instance = this;
    instance._removeAllDialogs();
    var page = $('<div class="message-page"></div>');

    //console.debug(msg, typeof(msg));
    if (typeof(msg) == "string") {
      $(page).text(msg);
    }
    else {
      if (msg.title != undefined && msg.title != '') {
        $('<h4>').text(msg.title).prependTo(page);
      }
      //console.debug(msg.description, typeof(msg.description));
      if (typeof(msg.description) == "string") {
        $('<p>').text(msg.description).appendTo(page);
      }
      else {
        $('<p>').append($(msg.description)).appendTo(page);
      }
    }
    
    // confirm to No/Yes
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      show: {
        effect: "blind",
        duration: 500
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            $(this).dialog('close');
            (orgPage != undefined)? orgPage() : null;
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            $(this).dialog('close');
            cmd();
          }
        },
      ]
    });
    
    this.displayOnTop();
  },
  connectionFailPage: function connectionFailPage(msg){
    var instance = this;
    instance._removeAllDialogs();

    // Display message
    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t('login.connectFail')))
                .append($('<p>').text(msg));

    $(page).dialog({
      autoOpen: true,
      resizable: false,
      closeOnEscape: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("login.tryAgain"),
          click: function() {
            $(this).dialog('destroy').remove();
            //console.log('Reconnect RVA');
            $("#connect-btn").trigger("click");
          }
        },
        {
          text: $.t("button.cancel"),
          click: function() {
            $(this).dialog('destroy').remove();
            instance.refresh();
          }
        },
      ]
    });

    this.displayOnTop();

  },
  openModeratorPage: function openModeratorPage(data){
    //console.debug("[openModeratorPage] data=", data);
    var instance = this;
    instance._removeAllDialogs();
    
    var err = (data==0 || data==1)? 'error' : '';
    var errMsg = (data==0)? $.t('login.required') : ((data==1)? $.t('login.wrongPassword') : '');

    // Display message
    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t('login.credentialFail')))
                .append($('<p>').text($.t('login.credentialFailMsg')))
                .append($('<p>').addClass('left').addClass('error').css('margin','auto 1rem')
                          .append($('<input id="moderatorPw" type="password">').attr('placeholder',$.t('info.enterPasswordHere')).addClass(err))
                          .append('<span class="toggleTeacherPwMsg">'+ errMsg +'</span>'));

    $(page).dialog({
      autoOpen: true,
      resizable: false,
      closeOnEscape: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page no-titlebar",
      create: function( event, ui ) {
        $('#moderatorPw').focus();
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.ok"),
          click: function() {
            var pw = $('#moderatorPw').val();
            if (pw=="") {
              $('#moderatorPw').addClass('error');
              $('.toggleTeacherPwMsg').text($.t('login.required'));
              $(this).focus();
              return;
            }
            if (pw.length < 4 || pw.length > 24) {
              $('#moderatorPw').addClass('error');
              $('.toggleTeacherPwMsg').text($.t('login.moderatorPasswordLength'));
              $(this).focus();
              return;
            }
            $('#pwBlock').removeClass('error');
            $('#toggleTeacherPw').val(pw);
            $('.toggleTeacherPwMsg').empty();
            $(this).dialog('destroy').remove();
            $('#icon-connect').trigger('click');
            // Check and save modified password
            rvaClient.revalidateTeacherCredentialPassword(pw);
          }
        },
        {
          text: $.t("button.cancel"),
          click: function() {
            $(this).dialog('destroy').remove();
            instance.refresh();
            $('#icon-connect').trigger('click');
          }
        },
      ]
    });
  },
  openWaitingPageNoTimer: function openWaitingPageNoTimer(msg){
    //console.log('openWaitingPageNoTimer...');
    var instance = this;
    instance._removeAllDialogs();

    // Waiting with loading image and message
    page = $('<div class="message-page"></div>')
            .append($('<div>').addClass('loading-circle'))
            .append($('<p>').append($('<span class="msg"></span>').text(msg)));
  
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      closeOnEscape: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      dialogClass: "msg-page no-close",
    });
  },
  openConnectedTimer: function openConnectedTimer(msg){
    //console.log('openConnectedTimer...');
    var instance = this;
    instance._removeAllDialogs();

    // Waiting with loading image and message
    page = $('<div id="connectedTimerPage"></div>')
            .append($('<div>').addClass('loading-circle'))
            .append($('<p>').append($('<span class="msg"></span>').text(msg)));
  
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      closeOnEscape: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      dialogClass: "msg-page no-close connectedTimerPage",
    });
    
    $('.connectedTimerPage').css('top',50);
    
    window.setTimeout(function(){
      $(page).dialog('destroy').remove();
      //console.log('connectedTimer Timeout...');
    }, 3000);
  },
  _openWaitingPageWithTimer: function _openWaitingPageWithTimer(msg){
    //console.log('_openWaitingPageWithTimer...', msg);
    var instance = this;
    instance._removeAllDialogs();
    instance.options.second = 25;

    // Waiting page with Timer
    var page = $('<div class="message-page"></div>')
              .append($('<div>').addClass('loading-circle'))
              .append($('<p>').append($('<span class="msg"></span>').text(msg)))
              .append($('<p>').text(instance.options.second).attr("id","second"));
    
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      closeOnEscape: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page no-titlebar",
      close: function( event, ui ) {
        $(this).dialog('destroy').remove();
      },
    });
    
    $('#second').powerTimer({
      interval: 1000,
      func: function(params) {
        instance.options.second--;
        $('#second').text(instance.options.second);
        //console.log('interval...', instance.options.second, params);
      },
    });

    instance.options.timer = window.setTimeout(function(){
      $('#second').powerTimer('stop');
      instance.options.second = 0;
      $(page).dialog('destroy').remove();
      //console.log('setTimeout...');
    }, instance.options.second * 1000);

  },
  closeWaitingPageWithTimer: function closeWaitingPageWithTimer(msg) {
    //console.log('closeWaitingPageWithTimer...', msg);
    var instance = this;
    instance.stopWaitingPageWithTimer();
    instance.messagePage(msg);
  },
  stopWaitingPageWithTimer: function stopWaitingPageWithTimer() {
    //console.log('stopWaitingPageWithTimer...');
    var instance = this;
    $('#second').powerTimer('stop');
    clearTimeout(instance.options.timer);
    instance.options.second = 0;
    instance._removeAllDialogs();
    instance.refresh();
  },
  _confirmAskHost: function _confirmAskHost(askName, uid) {
    var instance = this;
    instance._removeAllDialogs();

    // confirm to Ask someone to be host page
    var page = $('<div class="message-page"></div>')
                .append($('<p>').html($.t('login.transfterHost', {username: '<b>'+ askName +'</b>'})));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            $(this).dialog('destroy').remove();
            instance._openWaitingPageWithTimer($.t('login.waiting'));
            // Ask someone to be host
            rvaClient.send_CMD_MODERATOR_REQUEST(uid);
          }
        },
      ]
    });
    
  },
  beAskedHostPage: function beAskedHostPage(msg) {
    var instance = this;
    instance._removeAllDialogs();
    instance.options.second = 23;

    // Ask to be host page
    var page = $('<div class="message-page"></div>')
                .append($('<p>').text(msg))
                .append($('<p>').text(instance.options.second).attr("id","second"));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      closeOnEscape: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page no-titlebar",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            instance.options.second = 0;
            $('#second').powerTimer('stop');
            rvaClient.send_CMD_MODERATOR_DENIED();
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            instance.options.second = 0;
            $('#second').powerTimer('stop');
            rvaClient.send_CMD_MODERATOR_APPROVED();
            $(this).dialog('destroy').remove();
          }
        },
      ]
    });

    $('#second').powerTimer({
      name: 'mytimer',
      sleep: 1000,
      func: function() {
        $('#second').text(instance.options.second);
        if(instance.options.second <= 0){
          instance.options.second = 0;
          instance._removeAllDialogs();
          $('#second').powerTimer('stop');
          //console.log('End beAskedHostPage Timer...');
        }
        else {
          instance.options.second--;
          //console.log('beAskedHostPage Timer --');
        }
      },
    });
    
    this.displayOnTop();

  },
  beAskedPresenterPage: function beAskedPresenterPage(msg) {
    var instance = this;
    instance._removeAllDialogs();
    instance.options.second = 23;

    // be Asked to be presenter dialog
    var page = $('<div class="message-page"></div>')
                .append($('<p>').text(msg))
                .append($('<p>').text(instance.options.second).attr("id","beAskedPresenterPage"));
    
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      closeOnEscape: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page no-titlebar",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            instance.options.second = 0;
            $('#beAskedPresenterPage').powerTimer('stop');
            rvaClient.send_CMD_VIEWER_DENIED();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            rvaClient.send_CMD_VIEWER_APPROVED();
          }
        },
      ]
    });

    $('#beAskedPresenterPage').powerTimer({
      name: 'mytimer',
      sleep: 1000,
      func: function() {
        $('#second').text(instance.options.second);
        if (instance.options.second <= 0) {
          instance.options.second = 0;
          $('#beAskedPresenterPage').powerTimer('stop');
          instance._removeAllDialogs();
        }
        else {
          $('#beAskedPresenterPage').text(instance.options.second--);
        }
      },
    });

    this.displayOnTop();
  },
  _openGroupShareFolder: function _openGroupShareFolder(){
    //console.log('_openGroupShareFolder...');
    mkdirSync(groupSharePath);  // Check "GroupShare" folder exist or create it
    gui.Shell.openItem(groupSharePath); // open GroupShare folder
  },
  askSendAnotherFilePage: function askSendAnotherFilePage(data) {
    var instance = this;
    instance._removeAllDialogs();

    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t('sharing.shareInProgress')))
                .append($('<p>').text($.t('sharing.shareUploading')+' : '+ data.finished +'/'+ data.total +'.'))
                .append($('<p>').text($.t('sharing.shareGiveUp')));
    // No, Yes
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            instance.refresh();
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            instance.refresh();
            $(this).dialog('destroy').remove();
            rvaClient.send_CMD_FILE_SHARING_DATA_CANCEL();
          }
        },
      ]
    });
    
    this.displayOnTop();
  },
  _fileTooLargePage: function _fileTooLargePage() {
    var instance = this;
    instance._removeAllDialogs();
    
    var maxFileSize = (instance.options.rvaVersion >= 2)? "10" : "3";
    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text($.t('sharing.failTitle')))
                .append($('<p>').text($.t('sharing.sizeError', { postProcess: "sprintf", sprintf: [maxFileSize]})));

    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.ok"),
          click: function() {
            $(this).dialog('destroy').remove();
            instance.refresh();
          }
        },
      ]
    });

    this.displayOnTop();
  },
  askSendFilePage: function askSendFilePage(data) {
    var instance = this;
    //instance.options.viewModel.hasHost() && !instance.options.isHost
    //console.log('askSendFilePage...', data, instance.options.isHost, instance.options.viewModel.hasHost());
    var mainWin = gui.Window.get();
    mainWin.restore();
    mainWin.show(true);
    instance._removeAllDialogs();
    
    var title, img, description,
        receivers = (instance.options.isHost)?
                      $.t('sharing.allParticipants') :
                      ((instance.options.viewModel.hasHost())?
                          instance.options.viewModel.moderatorName() :
                          $.t('sharing.allParticipants')
                      );

    if (data.image && data.image!='') {
      // Send Screen
      title = $.t('sharing.sendScreen');
      img = '<p><img src="data:image/png;base64,'+ data.image +'"></p>';
      description = $.t('sharing.sendScreenMsg', {receiver: receivers});
    }
    else {
      // Send File
      title = $.t('sharing.sendFile');
      img = null;
      description = $.t('sharing.sendFileMsg', {filename: data.filename, receiver: receivers});
    }

    var page = $('<div class="message-page"></div>')
                .append($('<h4>').text(title))
                .append(img)
                .append($('<p>').text(description));
    // No, Yes
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      draggable: false,
      modal: true,
      dialogClass: "msg-page",
      width: '95%',
      open: function( event, ui ) {
        var obj = $('.ui-dialog');
        if ($(obj).length > 0 && $(obj).height() > $(novo).height()){
          $(novo).height(450);
        }
        gui.Window.get().requestAttention(true);
      },
      close: function( event, ui ) {
        $(novo).css('height','');
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            $(novo).css('height','');
            $(this).dialog('destroy').remove();
            instance.refresh();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            $(novo).css('height','');
            $(this).dialog('destroy').remove();
            //console.warn('instance._receivers()=', instance._receivers());
            if (!instance._receivers()) {  // no receivers
              instance.messagePage($.t('sharing.noReceiverMsg'));
              return;
            }
            instance.waitingforSendingPage();
            rvaClient.confirmToSharingFile();
          }
        },
      ]
    });
    
    this.displayOnTop();
  },
  waitingforSendingPage: function waitingforSendingPage() {
    //console.log('waitingforSendingPage...');
    var instance = this;
    instance._removeAllDialogs();
    
    var page = $('<div class="message-page flexbox" style="display: flex;"></div>')
                .append($('<div>').addClass('loading-circle'));

    $(page).dialog({
      autoOpen: true,
      resizable: false,
      closeOnEscape: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      dialogClass: "msg-page no-titlebar no-buttonpane"
    });
  },
  systemBusyPage: function systemBusyPage() {
    var mainWin = gui.Window.get();
    mainWin.restore();
    mainWin.show(true);
    var instance = this;
    instance._removeAllDialogs();

    var page = $('<div class="message-page"></div>')
                .append($('<b>').text($.t('sharing.systemBusy')))
                .append($('<p>').text($.t('sharing.systemBusyMsg')));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.ok"),
          click: function() {
            $(this).dialog('destroy').remove();
            instance.refresh();
          }
        },
      ]
    });
    
    this.displayOnTop();
  },
  sendingFilePage: function sendingFilePage(data) {
    var instance = this;
    instance._removeAllDialogs();
    $("#callUploading").addClass("active");

    // Sending file
    var page = $('<div class="message-page" id="sendingFilePage"></div>')
                .append($('<h4>').text($.t('sharing.sendingFile')))
                .append($('<div class="sending-image"></div>'))
                .append($('<p>').text(data.finished +'/'+ data.total + ' '+$.t('sharing.haveReceived')));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false,
      modal: true,
      width: '95%',
      dialogClass: "msg-page no-titlebar",
      open: function( event, ui ) {
        $(novo).height(400);
        gui.Window.get().requestAttention(true);
      },
      close: function( event, ui ) {
        $(novo).css('height','');
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.cancel"),
          click: function() {
            $("#callUploading").removeClass("active");
            // Ask RVA to stop sending file
            rvaClient.send_CMD_FILE_SHARING_DATA_CANCEL();
            $(novo).css('height','');
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.close"),
          click: function() {
            rvaClient.setBackGroundFileSending(true);
            $(novo).css('height','');
            $(this).dialog('destroy').remove();
          }
        },
      ]
    });  
  },
  closeSendingFilePage: function closeSendingFilePage() {
    var instance = this;
    $("#callUploading").removeClass("active");
    $("#sendingFilePage").dialog('close');
  },
  fileReceived: function fileReceived(data) {
    var instance = this;
    instance._removeAllDialogs();

    // Sending file
    var page = $('<div class="message-page" id="sendingFilePage"></div>')
                .append($('<h4>').text($.t('sharing.receivedTitle')))
                .append($('<p>').text($.t('sharing.receivedMsg', {filename: data.filename, sender: data.sender})));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("sharing.openFolder"),
          click: function() {
            instance._openGroupShareFolder(); // open GroupShare folder
            instance.refresh();
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.ok"),
          click: function() {
            instance.refresh();
            $(this).dialog('destroy').remove();
          }
        },
      ]
    });
    
    this.displayOnTop();
  },
  _confirmTerminateSession: function _confirmTerminateSession() {
    var instance = this;
    instance._removeAllDialogs();

    // confirm to Ask someone to be host page
    var page = $('<div class="message-page"></div>')
                .append($('<b>').text($.t('info.confirmExitTitle')))
                .append($('<p>').text($.t('info.confirmTerminateMsg')));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      show: {
        effect: "blind",
        duration: 500
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            instance.refresh();
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            //console.log('Terminate all session');
            rvaClient.terminateSession();
            $(this).dialog('destroy').remove();
          }
        },
      ]
    });
  },
  _setScreenoteIcon: function _setScreenoteIcon() {
    //console.debug("_setScreenoteIcon...");
    var instance = this;
    var paths = {
      win: ['C:\\Program Files\\Novo Interactive Tools\\NovoScreenote\\Screenote.exe',
            'C:\\Program Files\\畅享白板\\畅享白板.exe'],
      mac: ['/Applications/Novo Interactive Tools/NovoScreenote.app',
            '/Applications/畅享白板.app'],
    };
    var screenotePath = (isWin)? paths.win: paths.mac;
    
    //console.debug('Launch Screenote...',screenotePath);
    
    if (fs.existsSync(screenotePath[0])) {
      $('#launch-screenote').removeClass("disable").off().dblclick(function(){
        gui.Shell.openItem(screenotePath[0]);
      });
    }
    else if (fs.existsSync(screenotePath[1])) {
      $('#launch-screenote').removeClass("disable").off().dblclick(function(){
        gui.Shell.openItem(screenotePath[1]);
      });
    }
    else {
      $('#launch-screenote').addClass("disable").off().click(function(){
        //console.log('Download ScreenNote...');
        instance._askDownloadScreenote();
      });
    }

  },
  _askDownloadScreenote: function _askDownloadScreenote() {
    var instance = this;
    instance._removeAllDialogs();

    // confirm to Ask someone to be host page
    var page = $('<div class="message-page"></div>')
                .append($('<b>').text($.t('info.confirmation')))
                .append($('<p>').text($.t('info.confirmDownloadMsg')));
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      show: {
        effect: "blind",
        duration: 500
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            $(this).dialog('destroy').remove();
            //console.log('Download NovoScreenote', process.platform, language, isWin);
            var url = (isWin)? 'http://vivitekusa.com/novoconnect/downloads/NovoScreenote_setup_Win.exe' : 'http://vivitekusa.com/novoconnect/downloads/NovoScreenote_setup_Mac.mpkg.zip';
            if (language.toLowerCase() == "zh-cn") {
              // download from baidu
              url = (isWin)? 'http://pan.baidu.com/s/1kTtAZtX' : 'http://pan.baidu.com/s/1mgoPobI';  
            }
            // Open URL with default browser.
            gui.Shell.openExternal(url);
          }
        },
      ]
    });
    
    this.displayOnTop();
  },
  askInstallSoundFlower: function askInstallSoundFlower() {
    var instance = this;
    instance._removeAllDialogs();

    // ask user for sound flower permission
    var page = $('<div class="message-page"></div>')
                .append($('<b>').text($.t('info.confirmation')))
                .append($('<p>').text($.t('info.confirmInstallSoundFlower')))
                .append($('<p>').addClass('left').text($.t('info.askSoundFlowerYes')))
                .append($('<p>').addClass('left').text($.t('info.askSoundFlowerNo')))
                .append($('<p>').addClass('left')
                          .append($('<input id="neverSoundFlower" type="checkbox">'))
                          .append($('<label for="neverSoundFlower"></label>').attr('data-i18n-options', '{"type":"checkbox"}')
                                    .append('<span></span>')
                                    .append('<code>' + $.t("info.neverSoundFlower") + '</code>'))
                       );
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      show: {
        effect: "blind",
        duration: 500
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            //console.log('NO...', $('#neverSoundFlower').prop('checked'));
            //if($('#neverSoundFlower').prop('checked'))
            //  rvaClient.soundFlowerReminderOff();
            //rvaClient.enableSoundFlowerState(false);
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            //console.log('OK-install it.', $('#neverSoundFlower').prop('checked'));
            //if($('#neverSoundFlower').prop('checked'))
            //  rvaClient.soundFlowerReminderOff();
            //rvaClient.enableSoundFlowerState(true);
            $(this).dialog('destroy').remove();
          }
        },
      ]
    });
    
    this.displayOnTop();
  },
  displayOnTop: function displayOnTop() {
    // force to show main window on top
    win.restore();
    win.setAlwaysOnTop(true);
    win.setAlwaysOnTop(false);
  },
  askEnableSoundFlower: function askEnableSoundFlower(connectionRequest) {
    //console.log("askEnableSoundFlower", connectionRequest);
    var instance = this;

    // ask user for sound flower permission
    var page = $('<div class="message-page prevent-close-outside"></div>')
                .append($('<b>').text($.t('info.confirmEnableSFTitle')))
                .append($('<p>').addClass('left').text($.t('info.askEnableSoundFlower')))
                .append($('<p>').addClass('left').text($.t('info.askEnableSoundFlower2')));
    
    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      closeOnEscape: false,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page no-titlebar",
      show: {
        effect: "blind",
        duration: 500
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.cancel"),
          click: function() {
            //console.log('Do Not output sound.');
            rvaClient.enableSoundFlowerState(false, function(result){
              rvaClient.enableSFReminder();
              rvaClient.SFNotAllowed();
            }, connectionRequest);
            instance.refresh();
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.ok"),
          click: function() {
            //console.log('Enable sound flower.');
            rvaClient.enableSoundFlowerState(true, function(result){
              rvaClient.enableSFReminder();
              rvaClient.SFAllowed();
            }, connectionRequest);
            instance.refresh();
            $(this).dialog('destroy').remove();
          }
        },
      ]
    });
    
    this.displayOnTop();
  },
  askQuitDsa: function askQuitDsa() {
    var instance = this;
    instance._removeAllDialogs();

    // Ask to be close page
    var page = $('<div class="message-page" id="close-page"></div>')
                .append($('<h4>').text($.t('info.confirmExitTitle')))
                .append($('<p>').text($.t('info.confirmExitMsg')));

    $(page).dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      width: '95%',
      minheight: '200',
      maxHeight: $('#container').height(),
      dialogClass: "msg-page",
      show: {
        effect: "blind",
        duration: 500
      },
      close: function(event, ui) {
        $(this).dialog('destroy').remove();
      },
      buttons: [
        {
          text: $.t("button.no"),
          click: function() {
            $(this).dialog('destroy').remove();
          }
        },
        {
          text: $.t("button.yes"),
          click: function() {
            $(this).dialog('destroy').remove();
            instance.exitSystem();
          }
        },
      ]
    });
  },
  _closeAllChildWindow: function _closeAllChildWindow() {
    var windows = this.options.childWindows;
    
    //console.log('this.options.childWindows=', windows);
    $.map(windows, function(win, index) {
      //console.log('win=', win);
      if (win != null) win.close(true);
      win = null;
      return;
    });
    
    this.options.childWindows = {
      qrWindow: null,
      videoWindow: null,
      votingWindow: null,
    };
    
    windows = null;
    //console.log('Finish closing all child windows.');
  },
  exitSystem: function exitSystem(){
    var win = gui.Window.get();
    // Hide the window to give user the feeling of closing immediately
    win.hide();
    this._clearNotification();
    rvaClient.disconnect('appEnd');

    var instance = this;
    if (instance.options.connected) {
      instance._closeAllChildWindow();
      return;
    }
  },
  _destroy: function _destroy() {
    this.element.remove();
  },
});

function mkdirSync(folder) {
  try {
    fs.mkdirSync(folder);
  } catch(e) {
    if ( e.code != 'EEXIST' ) throw e;
  }
}

function supportRightClick(obj) {
    
  if(document.queryCommandSupported('paste')) {
    // Create an empty menu
    var menu = new gui.Menu();
    // Add Paste item
    menu.append(new gui.MenuItem({
      label: 'Cut',
      click: function() {
        //console.log('Cut is clicked',this);
        document.execCommand('cut');
      }
    }));
    menu.append(new gui.MenuItem({
      label: 'Copy',
      click: function() {
        //console.log('Copy is clicked',this);
        document.execCommand('copy');
      }
    }));
    menu.append(new gui.MenuItem({
      label: 'Paste',
      click: function() {
        //console.log('Paste is clicked',this);
        document.execCommand('paste');
      }
    }));
    menu.append(new gui.MenuItem({
      label: 'Delete',
      click: function() {
        //console.log('Delete is clicked',this);
        document.execCommand('delete');
      }
    }));
    menu.append(new gui.MenuItem({ type: 'separator' }));
    menu.append(new gui.MenuItem({
      label: 'Select all',
      click: function() {
        //console.log('selectAll is clicked',this);
        document.execCommand('selectAll');
      }
    }));

    function isTextSelected(input){
      var startPos = input.selectionStart;
      var endPos = input.selectionEnd;
      var doc = document.selection;
      if(doc && doc.createRange().text.length != 0){
        return true;
      }
      else if (!doc && input.value.substring(startPos,endPos).length != 0){
        return true;
      }
      return false;
    }

    $(obj).mousedown(function(ev){ 
      $(this).focus();
      if( ev.button == 2 ) {
        for (var i = 0; i < menu.items.length; ++i) {
          menu.items[i].enabled = false;
        }
        if ($(obj).val()==''){
          //console.log('No input.');
          menu.items[2].enabled = true;
        }
        else {
          menu.items[2].enabled = true;
          menu.items[5].enabled = true;
          if (isTextSelected(this)){
            menu.items[0].enabled = true;
            menu.items[1].enabled = true;
            menu.items[3].enabled = true;
          }
        }
        menu.popup(ev.clientX, ev.clientY);
        return false; 
      } 
      return true; 
    }); 
  }

}
