function UserModel(item) {
  var self = this;
  self.uid = ko.observable(item.uid);
  self.data = ko.observable(item);
  self.ishost = ko.computed(function() {
    return (self.data().isHost) ? "ishost" : "";
  });
  self.isMyself = ko.computed(function() {
    return (self.data().isMyself) ? "isMyself" : "";
  });
  self.label = ko.computed(function() {
    return self.data().label;
  });
  self.username = ko.computed(function() {
    return (self.data().isMyself)? $.t('login.myself', {usrname: self.data().label}) : self.data().label;
    //return (self.data().isMyself)? 'Myself ('+ self.data().label +')' : self.data().label;
  });
  self.panelcss = ko.computed(function() {
    var cssArray = {"-1": "", "0": "full-on", "1": "no1", "2": "no2", "3": "no3", "4": "no4"};
    return cssArray[self.data().panel];
  });
  self.panelimgsrc = ko.computed(function() {
    return presenterImgs[self.data().panel];
  });
  self.playing = ko.computed(function() {
    return (self.data().panel > -1) ? "playing" : "stop-play";
  });
  self.online = ko.computed(function() {
    return (self.data().online)? "online" : "offline";
  });
  self.loginTime = ko.observable(item.loginTime);
}

function UserViewModel(userList) {
  var self = this;
  var noModerator = $.t("info.noModerator");

  // Data
  self.allusers = ko.observableArray($.map(userList, function(user) { return new UserModel(user) }));
  self.refresh = function(userList) {
    //console.warn('refresh user...', userList);
    self.allusers($.map(userList, function(user) { return new UserModel(user) }));
    self.iamHost(false);
    self.hasHost(false);
    self.moderatorName(noModerator);
  }
  
  // Sort Key
  self.sortKey = ko.observable('name');  // name/time, default: name
  self.sortPeople = function(key) {
    //console.warn('sortPeople...');
    self.sortKey(key);
  }

  // Display
  self.display = ko.observable(0);
  self.displayUsers = ko.computed(function() {
    var output = [];
    ko.utils.arrayForEach(self.allusers(), function(user) {
      //console.warn(user.data().label, self.moderatorName(), self.hasHost());
      if (user.data().isHost){
        self.moderatorName(user.data().label);
        self.hasHost(true);
        (user.data().isMyself)? self.iamHost(true) : null;
      }
      self.iamHost();
      self.moderatorName();
      if (self.display()==0) output.push(user);
      if (self.display()==1 && user.data().online) output.push(user);
      if (self.display()==2 && !user.data().online) output.push(user);
    });    
    
    return output.sort(function(left, right) {
      if (left.ishost() == right.ishost()) {
        var key = self.sortKey();
        if (key=='name')
          return (left.label().toUpperCase() == right.label().toUpperCase()) ? 0 : (left.label().toUpperCase() < right.label().toUpperCase() ? -1 : 1); 
        if (key=='time')
          return (left.loginTime() == right.loginTime())? 0 : (left.loginTime() < right.loginTime() ? -1 : 1);
      }
      else {
        return (left.ishost() > right.ishost() ? -1 : 1); 
      }
    });
  }, this);
  
  self.fullViewName = ko.computed(function() {
    var output = "";
    ko.utils.arrayForEach(self.allusers(), function(user) {
      if (user.data().panel===0)
        return output = user.data().label;
    });
    return output;
  }, this);
  self.splitViews = ko.computed(function() {
    var output = ["","","",""];
    ko.utils.arrayForEach(self.allusers(), function(user) {
      if (user.data().panel > 0)  output[user.data().panel-1]=user.data().label;
    });    
    return output;
  }, this);
  self.onlineCount = ko.computed(function(flag) {
    var output = 0;
    ko.utils.arrayForEach(self.allusers(), function(user) {
      if (user.data().online) output++;
    });
    return output;
  }, this);
  self.offlneCount = ko.computed(function(flag) {
    var output = 0;
    ko.utils.arrayForEach(self.allusers(), function(user) {
      if (!user.data().online) output++;
    });
    return output;
  }, this);

  self.groupMode = ko.observable(false);
  self.groupName = ko.observable($.t("group.notSelected"));
  self.updateGroup = function(gMode, gName) {
    self.groupMode(gMode);
    self.groupName(gName);
  }
  
  self.iamHost = ko.observable(false);
  self.iamHostClass = ko.computed(function() {
    return (self.iamHost()? 'iamhost ' : ' ');
  }, this);
  
  self.hasHost = ko.observable(false);
  self.hasHostClass = ko.computed(function() {
    return (self.hasHost()? 'has-host' : 'no-host');
  }, this);
  
  self.moderatorName = ko.observable(noModerator);
};
