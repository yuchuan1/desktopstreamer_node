var gui = require('nw.gui'),
    win = gui.Window.get(),
    rvaClient,
    groupName='',
    orgJson={};

function init(rva, opener){
  rvaClient = rva;
  $('#teacher-name').val(opener);
};

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function() {

  var language = getParameterByName('setLng');
  //var language = window.navigator.language;
  //var language = 'ru'; //zh-TW zh-CN en-US en-GB ja fr es ru
  translate(language);

  checkResize();
  win.setResizable(false);
  $(".icon-minimize").off().click(function(){
    win.minimize();
  });
  
  $(".icon-fullscreen").off().click(function(){
    win.maximize();
  });

  $(".icon-close").off().click(function() {
    $('#close').trigger('click');
  });

  $('#add-item').click(function(e){
    e.preventDefault();
    createItem();
    $('.student-name').last().focus();
    $("section > div.border").scrollTop(1000);
  });

  $('#add-item').closest('li')
    .on('dragover',dragoverHandler)
    .on('dragleave',dragleaveHandler)
    .on('drop',dropHandler);

  $('#done').click(function(e){
    e.preventDefault();
    $('#save').trigger('click');
    var result = saveJsonObj();
    if (result){
      win.close();  
    }
  });

  $('#save').click(function(e){
    e.preventDefault();
    var result = saveJsonObj();
  });

  $('#close').click(function(e){
    e.preventDefault();
    // Check changes
    var change = false;
    if (groupName == $('#group-name').val()){
      var nowJson = JSON.stringify(getJsonObj());
      delete orgJson['documentType'];
      delete orgJson['_id'];
      if (JSON.stringify(orgJson) == nowJson){
        change = false;
      }
      else {
        change = true;
      }
    }
    else {
      change = true;
    }
    //console.info("orgJson->", JSON.stringify(orgJson));
    //console.info("nowJson->", nowJson);
    $("#discard-change").dialog({
      autoOpen: false,
      closeOnEscape: false,
      resizable: false,
      modal: true,
      dialogClass: "msg-page titlbar-only-close",
      buttons: [
        {
          text: $.t('button.no'),
          click: function() {
            $(this).dialog("close");
          }
        },
        {
          text: $.t('button.yes'),
          click: function() {
            win.close();  // close this window
          }
        },
      ]
    });

    (change)? $("#discard-change").dialog('open') : window.close();
  });
  
  $('input[type="text"]').change(trimInput);
  
  $('#group-name').focusout(function() {
    $(this).removeClass('error');
    $(this).next().removeClass('show');
  });
  
  $('#teacher-name').focusout(function() {
    $(this).removeClass('error');
  });
  
  orgJson = getJsonObj();
  
  console.log($('#teacher-name'));
  
  document.body.addEventListener('dragover', function(e){
    e.preventDefault();
    e.stopPropagation();
  }, false);
  document.body.addEventListener('drop', function(e){
    e.preventDefault();
    e.stopPropagation();
  }, false);
  
  var label = $('#main-content header > div > div.border label'),
      input = $('#main-content header input[type="text"]');
  if (language == 'es') {
    label.css('width','80px');
    input.css('width','145px');
  }
  if (language == 'fr') {
    label.css('width','63px');
    input.css('width','162px');
  }
  if (language == 'ru') {
    label.css('width','80px');
    input.css('width','145px');
  }
  if (language == 'ja') {
    label.css('width','70px');
    input.css('width','150px');
  }

  var isWin = /^win/.test(process.platform);
  if (isWin) {
    //$('#taskbar').css('-webkit-user-select','none').css('-webkit-app-region','drag');
    $('#taskbar .icons > span').css('-webkit-app-region','no-drag');
    $('#main-content').css('-webkit-app-region','no-drag');
    //$('.ui-dialog').css('-webkit-app-region','no-drag');
  }
  else {
    $('input').css('-webkit-app-region','no-drag');
  }
});

win.on('maximize', function() {
  win.restore();
  /*
  //console.log('=========== maximize ==========');
  $(".icon-fullscreen").addClass('onMax').off().click(function(){
    win.unmaximize();
  });
  //var h = screen.availHeight;
  var h = $(window).height();
  $("#group-editor").width("100%").css('height',$(window).height());
  h = h - 60 - $('.header').height() - $('#main-content > header').height() - $('#main-content > footer').height();
  $("#main-content section > div.border").height(h+"px");
  //console.info(screen.availHeight, 'h=', h, $('#group-editor').height());
  */
});

win.on('unmaximize', function() {
  //console.log('=========== unmaximize ==========');
  $(".icon-fullscreen").removeClass('onMax').off().click(function(){
    win.maximize();
  });
  checkResize();
});

function trimInput() {
  var txt = $(this).val();
  $(this).val($.trim(txt));
};

function removeItem() {
  $(this).closest('li').prev().trigger('click');
  $(this).closest('li').remove();
  //drawSerial();
};

function createItem(student) {
  //console.log( "createItem...", student );
  var selectLi = function(){
                   $('li').removeClass('selected');
                   $(this).addClass('selected');
                   //console.log('selectLi...', $(this));
                 };
  var focusInput = function(){
                     $(this).closest('li').trigger('click');
                     //console.log('focusInput...', $(this));
                   };

  var li = $('<li>').uniqueId().attr('draggable',true)
            .on('dragstart',dragstartHandler)
            .on('dragover',dragoverHandler)
            .on('dragleave',dragleaveHandler)
            .on('drop',dropHandler)
            .append(
              $('<div class="border"></div>')
              .append($('<div>').append($('<span>').addClass('person-icon')))
              .append($('<div>')
                .append($('<div>').append($('<input type="text" class="student-name">').attr('placeholder',$.t('group.studentName')).focusin(focusInput).change(trimInput)))
                .append($('<div>').append($('<input type="text" class="student-device" name="student-device">').attr('placeholder',$.t('group.studentDevice')).focusin(focusInput).change(trimInput)))
              )
              .append($('<div>').append($('<span class="delete"></span>').click(removeItem)))
            ).click(selectLi)
            .appendTo($('#members'));

  if (student) {
    //console.log( "student:", student, $(li).find('.student-name'));
    $($(li).find('.student-name')[0]).val(student.name);
    $($(li).find('.student-device')[0]).val(student.device);
  }
  //drawSerial();
  $(li).trigger('click');
}

function dragstartHandler(event) {
  //console.log('dragstartHandler...', event, $(this));
  event.originalEvent.dataTransfer.setData("li", event.target.id);
}

function dragoverHandler(event) {
  event.preventDefault();
  //console.log('dragoverHandler...', event, $(this));
  $(this).addClass('dragover');
  return false;
}

function dragleaveHandler(event) {
  event.preventDefault();
  //console.log('dragleaveHandler...');
  $(this).removeClass('dragover');
  return false;
}

function dropHandler(event) {
  event.preventDefault();
  //console.log('dropHandler...', event, $(this));
  var did = event.originalEvent.dataTransfer.getData('li');
  //console.log('did...', did);
  $(this).removeClass('dragover');
  $('#'+did).insertBefore($(this));
  //drawSerial();
  return false;
}

function drawSerial() {
  $(".num").each(function( index ) {
    $(this).text((index+1) + '.')
  });
};

function saveJsonObj() {
  var group = getJsonObj();

  var gname = group.group.class;
  if (gname==""){
    $('#group-name').addClass('error').focus();
    $('#group-name').next().addClass('show');
    return false;
  }
  
  var tname = group.group.teacher.name.toUpperCase().replace(/\s/g,'');
  if (tname==""){
    $('#teacher-name').addClass('error').focus().attr('placeholder',$.t('group.required'));
    return false;
  }
  
  // Check duplicate device or user name
  var arr = [], Map = {}, duplicate = false;
  var checkList = $.merge($.merge($.merge($('#teacher-name'), $('#teacher-device')), $('.student-name')), $('.student-device'));
  // console.info("checkList...", checkList);
  $(checkList).each(function(index) {
    var obj = $(this),
        n = $(obj).val().toUpperCase().replace(/\s/g,'');
    if (n != "" && Map[n]){
      //console.info('duplicate...', $(obj));
      $(obj).addClass('error').focus().off().focusout(function() {
        $(this).removeClass('error');
      });
      $(obj).closest('li').trigger('click');
      $('#msg').text($.t('group.duplicateMsg')).addClass('error').powerTimer({
        name: 'errMsg',
        sleep: 3000,
        func: function() {
          $(this).empty().removeClass('error');
        },
      });
      duplicate = true;
    }
    arr.push(n);
    Map[n] = true;
  });
  if (duplicate) return false;

  //console.info("saveJsonObj:", group, gname, tname);
  rvaClient.deleteGroup(groupName);
  rvaClient.updateGroup(group);
  groupName = gname;
  orgJson = group;
  
  $('#msg').text($.t('group.saveMsg')).powerTimer({
    name: 'saveMsg',
    sleep: 3000,
    func: function() {
      $(this).empty();
    },
  });
  
  return true;

};

function getJsonObj() {
  var txt;
  $.each($('input[type="text"]'), function( key, input ) {
    txt = $(input).val();
    $(input).val($.trim(txt));
  });

  var group = {
    'group': {
      'class': $('#group-name').val(),
      'teacher': {
        'name': $('#teacher-name').val(),
        'device': $('#teacher-device').val()
      }
    }
  };

  var device = $(".student-device");
  if (device) {
    group.group.student = [];
    $(".student-name").each(function( index ) {
      if ( $(this).val() != "" || $(device[index]).val() != "" ) {
        group.group.student.push({"name": $(this).val(), "device": $(device[index]).val()});
      }
    });
  }

  return group;
};

function editGroup(group, rvaClient){
  console.log("group=", group);
  rva = rvaClient;
  $('#group-name').val(group.class);
  $('#teacher-name').val(group.teacher.name);
  $('#teacher-device').val(group.teacher.device);

  if (group.student && group.student.length >= 0) {
    $.each(group.student, function(key, student) {
      createItem(student);
    });
  }
  else {
    console.log("group.student=", group.student);
    createItem(group.student);
  }
  
  //console.info("editGroup:", group);
  groupName = group.class;
  orgJson = getJsonObj();
  //console.info("orgJson:", orgJson);
}

function checkResize() {
  var w = $('#group-file').width(),
      h = $('#group-file').height();

  if (gui.App.manifest.window.frame) {
    w = 540;
    h = 790;
  }
  else {
    w = w + 4;
    h = h + 4;
  }
  win.resizeTo(w, h);
  
  var isWin = /^win/.test(process.platform);
  var x = Math.round((screen.availWidth - w)/2),
      y = Math.round((screen.availHeight - h)/2) + ((isWin)? 0 : 22);
  win.moveTo(x,y);
  //$('#wh').text(isWin +':' + screen.availWidth + 'x' + screen.availHeight + ',' + w + 'x' + h + ',' + x + 'x' + y);
  //console.log('checkResize...', screen.availWidth, screen.availHeight, w, h, x, y);
}

/**
 * translation
 */
function translate(language){
  var opts = {
    lng: language,
    fallbackLng: 'en-US',
    debug: true,
    resGetPath: "../locales/__ns__-__lng__.json"
  };

  i18n.init(opts, function (t) {
    $(document).i18n();
  });
}