'use strict';
var gui = require('nw.gui');
var win = gui.Window.get();
var rvaClient, rvaVersion, hasPurview;

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

win.on('maximize', function() {
  //console.log('maximize', e);
  //e.preventDefault();
  win.restore();
});

win.on('focus', function() {
  //console.log('focus event, start rvaDiscovery here');
  checkResize();
});

$(document).ready(function() {

  //var language = window.navigator.language; //'zh-TW';//en-US
  //var language = 'en-US'; //'zh-TW';//en-US
  var language = getParameterByName('setLng');
  translate(language);

  win.setResizable(false);

  $("#resizable").resizable({
    maxHeight: 400,
    maxWidth: 400,
    minHeight: 245,
    minWidth: 245,
    aspectRatio: 1 / 1,
    helper: "ui-resizable-helper",
    stop: function(event, ui) {
      checkResize();
    }
  });

  $('#qr-close').click(function() {
    if (win != null) win.close(true);
    win = null;
    gui = null;
  });

  /*
  // Usage
  var qrObject1 = {
    lanIP: "172.108.169.109",
    pinRequired: true,
    pinCode: 2191,
    wifi: {
      IP: "192.168.111.103",
      ssid: "Delta-Guest",
    },
    hasPurview: true
  }
  displaySessionInfo(qrObject1);
	*/

  checkResize();
  //var x = Math.round((screen.availWidth - window.outerWidth)/2),
      //y = Math.round((screen.availHeight - window.outerHeight)/2);
  var x = Math.round((screen.availWidth - 617)/2),
      y = Math.round((screen.availHeight - 440)/2);
  win.moveTo(x, y);
  //console.log('moveTO...', screen.availWidth, screen.availHeight, window.outerWidth, window.outerHeight, x, y);

  document.body.addEventListener('dragover', function(e){
    e.preventDefault();
    e.stopPropagation();
  }, false);
  document.body.addEventListener('drop', function(e){
    e.preventDefault();
    e.stopPropagation();
  }, false);

  var isWin = /^win/.test(process.platform);
  if (isWin) {
    $('#qr-close').css('-webkit-app-region','no-drag');
    $('#qr-rva-btn').css('-webkit-app-region','no-drag');
  }
  isWin = null;

  //win.showDevTools();
});

function init(rva, rVersion){
  console.log("init...", rva, rVersion, $('#qr-page'));
  rvaClient = rva;
  rvaVersion = rVersion;
};

function displaySessionInfo(obj) {
  console.log("displaySessionInfo...", obj, obj.hasPurview);
  hasPurview = obj.hasPurview;
  var ul = $('#info-list');
  $('#qr-img').attr("src","data:image/png;base64,"+obj.qrImg);

  $(ul).empty().append($('<li><label>PIN</label><span>'+ ((obj.pinRequired) ? obj.pinCode : "----") +'</span></li>'));

  if (obj.lanIP){
    $(ul).append($('<li><label>LAN</label><span>'+ obj.lanIP +'</span></li>'));
  }

  if (obj.wifi && obj.wifi.IP!="0.0.0.0"){
    $(ul).append($('<li></li>')
                 .append($('<label class="blue">WiFi</label>'))
                 .append($('<span></span>')
                          .append('<div>'+ obj.wifi.IP +'</div>')
                          .append('<div id="ssid">'+ obj.wifi.ssid +'</div>')));
  }

  $('#qr-rva-btn').hide();
  if (rvaVersion >= 2.2) {
    var clearText;
    function toggleRvaSessionInfo(){
      console.log("call RVA to togrvaClient.gle session information...");
      rvaClient.sendCmdSessionInfo();
      $('#qr-rva-btn').addClass('loading');
      $('#msg').empty();
      clearTimeout(clearText);

      window.setTimeout(function(){
        $('#qr-rva-btn').removeClass().one("click",toggleRvaSessionInfo);
        $('#msg').text($.t("info.sendRequest"));
        //console.log('connectedTimer Timeout...');
        clearText = window.setTimeout(function(){
          $('#msg').empty();
        }, 5000);
      }, 2000);

    }

    (obj.hasPurview)? $('#qr-rva-btn').show().off().one("click",toggleRvaSessionInfo) : $('#qr-rva-btn').hide().off();
  }

  ul = null;
  checkResize();
}

function checkResize() {
  $('#qr-page').width($('section').width()+5+$('#info').width());
  var w = $('#qr-page').width(),
      h = $('#qr-page').height();

  $('#info').height(h);
  $('section').height(h);

  if (gui.App.manifest.window.frame) {
    w = w + 20;
    h = h + 80;
  }
  win.resizeTo(w, h);
  //console.log('checkResize...', w, h, window.outerWidth, window.outerHeight);
  w = null;
  h = null;
}

/**
 * translation
 */
function translate(language){
  var opts = {
    lng: language,
    fallbackLng: 'en-US',
    debug: true,
    resGetPath: "../locales/__ns__-__lng__.json"
  };

  i18n.init(opts, function (t) {
    $(document).i18n();
  });
}
