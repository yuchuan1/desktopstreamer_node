var gui = require('nw.gui');
var win = gui.Window.get();
gui.Screen.Init();

var language = window.navigator.language;
//var language = "en-US";
function translate(language){
  var opts = {
    lng: language,
    fallbackLng: 'en-US',
    debug: true,
    resGetPath: "../locales/__ns__-__lng__.json"
  };

  i18n.init(opts, function (t) {
    $(document).i18n();
  });
  //console.log("language=", language);
}

var rva = require('desktop_streamer_node');
var rvaClient = new rva.RVAClient(gui.Screen.screens, language);
var rvaDiscovery = rva.RVADiscovery;
var nativeService = rva.nativeService;
var novo,
    hasNewVersion = false;

// do nothing loop, fix https://github.com/nwjs/nw.js/issues/1169
setInterval(function(){}, 30);

// Listen to `open` event
console.log('argv:');
console.log(gui.App.argv);
gui.App.on('open', function(cmdline) {
  console.log('command line: ' + cmdline);
});

/**
*
* RVA license event handlers
*
*/
nativeService.dsaServer.once('LICENSE_APPROVED', function(){
  console.log('LICENSE_APPROVED');
  createInstance();
});

nativeService.dsaServer.once('LICENSE_INSTALL_SUCCESS', function(){
  console.log('LICENSE_INSTALL_SUCCESS');
  createInstance();
});

nativeService.dsaServer.once('NO_LICENSE', function(){
  console.log('NO_LICENSE');
  showLicenseMessage('noLicense');
});

nativeService.dsaServer.once('SERVER_BUSY', function(){
  console.log('SERVER_BUSY');
  showLicenseMessage('serverBusy');
});

nativeService.dsaServer.once('LICENSE_COUNT_OVER', function(){
  console.log('LICENSE_COUNT_OVER');
  showLicenseMessage('licenseCountOver');
});

nativeService.dsaServer.once('SET_DATA_FAILED', function(){
  console.log('SET_DATA_FAILED');
  showLicenseMessage('setDataFailed');
});

nativeService.dsaServer.once('CONNECT_FAILED', function(){
  console.log('CONNECT_FAILED');
  showLicenseMessage('connectFailed');
});

function createInstance() {
  /**
  * Below is initial status
  */
  console.time('main.js createInstance');
  $(document).ready(function () {

    console.log('dataPath=', rva.dataPath);

    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    var observer = new MutationObserver(function (mutations, observer) {
      checkResize();
    });
    observer.observe(document, {
      subtree: true,
      attributes: true
    });

    $('#startLoading').remove();
    novo = $("#container").novoconnect();
    $(".input-toggle").hide();

    // to get last session's IP and user name
    rvaDiscovery.lastSession(function (err) {
      console.error(err);
    }, function (data) {
      console.log('[rvaDiscovery.lastSession]...', data);
      $("#rva_url").val(data[0].ip);
      $("#client_name").val(data[0].userName);
    });

    rvaClient.startLookupServerChecking();

    win.on('maximize', function() {
      win.restore();
    });

    win.on('focus', function() {
      //console.log('window focus event, start rvaDiscovery here');
      //rvaDiscovery.resume();
    });

    win.on('blur', function() {
      //console.log('window blur event, stop rvaDiscovery here');
      //rvaDiscovery.stop();
    });

    win.setResizable(false);
    checkResize();

    console.log('start rvaDiscovery.findAllRvas here');
    rvaDiscovery.findAllRvas();

    var isWin = /^win/.test(process.platform);
    if (isWin) {
      $('.taskbar').css('-webkit-app-region','drag');
      $('.taskbar > div > div').css('-webkit-app-region','no-drag');
      $('.taskbar > div > span').css('-webkit-app-region','no-drag');
      $('#drop-setting').css('-webkit-app-region','no-drag');
      $('body').append($('<style>\
                          .ui-dialog, .ui-front {-webkit-app-region: no-drag;}\
                          .ui-widget-overlay:before { -webkit-app-region: drag;}\
                          </style>'));
      win.once('close', function() {
        this.close(false);  // stop closing window
        console.log('window once close event...');
        novo.novoconnect("exitSystem");
      });
      win.on('close', function() {
        this.close(false);  // stop closing window
        console.log('window on close event...');
      });
    }
    else {
      $('body').css('-webkit-app-region','drag');
      $('input').css('-webkit-app-region','no-drag');
      // for MAC
      win.on('close', function(ev) {
        //console.log("You tried to close the app from the " , ev);
        //novo.novoconnect("askQuitDsa");
        rvaClient.disconnect('appEnd');
      });
    }

    if (language=='ru') {
      $('#connect-btn').css('font-size','.65rem');
    }

    //console.log($('#updateConfirmPage'));

    //printMemory();
    win.show(true);
    win.setAlwaysOnTop(true);
    //console.error('I am from main.js.');
    win.setAlwaysOnTop(false);
    //win.showDevTools();
    console.timeEnd('main.js createInstance');
  });
}

function showLicenseMessage(msg) {
  console.log('showLicenseMessage...', msg);
  win.once('close', function() {
    this.close(false);  // stop closing window
    rvaClient.disconnect('appEnd');
    gui.App.quit();
  });

  win.on('maximize', function() {
    win.restore();
  });

  $(document).ready(function () {
    var div = $('<div>')
                .append('<div><img src="../images/icon_warning.png"></div>')
                .append($('<div data-i18n="license.' + msg + '"></div>').text($.t('license.' + msg)));
    var p = $('<p>').appendTo(div);
    $('<input type="button" data-i18n="[value]button.close">').val($.t("button.close")).appendTo(p).click(function(event){
      event.preventDefault();
      win.close();
    });
    novo = $("#container").empty().addClass('license').append(div).show();
    $('#startLoading').remove();
    win.show(true);
    win.setAlwaysOnTop(true);
    win.setAlwaysOnTop(false);
    checkResize();
  });

}

function checkResize() {
  var w = $(novo).width(),
      h = $(novo).height();

  if (gui.App.manifest.window.frame) {
    w = w + 10;
    h = h + 69;
  }
  else {
    w = w + 4;
    h = h + 4;
  }

  win.resizeTo(w, h);
}

function printMemory(){
  function printMem(){
    var mem = process.memoryUsage();  // Initial usage
    //Object {rss: 140832768, heapTotal: 45956736, heapUsed: 32527024}
    //console.debug("Memory-rss: " + (mem.rss/1024) + ' KB');
    console.debug("Memory-rss: " + Math.round(mem.rss/1024/1024) + ' MB');
    mem = null;
  }
  window.setInterval(printMem, 5000);
  //window.clearInterval(printMem);
}

function reloadGroupList(){
  console.log('[reloadGroupList]...');
  novo.novoconnect('reloadGroupList');
}

function refreshIPList(ipList) {
  //console.log('[refreshIPList] ipList=', ipList);
  var control = $("#project").val("");

  // remove the autocomplete behaviour
  if ($(control).data('autocomplete')) {
    $(control).autocomplete("destroy");
    $(control).removeData('autocomplete');
  }

  $(control).autocomplete({
    minLength: 0,
    delay: 50,
    source: ipList,
    focus: function (event, ui) {
      //$( "#project" ).val( ui.item.ip );
      $("#rva_url").val(ui.item.ip);
      return false;
    },
    select: function (event, ui) {
      //$( "#project" ).val( ui.item.ip );
      $("#rva_url").val(ui.item.ip);
      $("#client_name").val(ui.item.userName);
      return false;
    },
    change: function (event, ui) {
      return false;
    },
  })
  .autocomplete("option", "position", {
    my: "right top",
    at: "right bottom",
    of: "#rva_url"
  })
  .autocomplete("instance")._renderItem = function (ul, item) {
    return $("<li>")
      .addClass(item.broadcast ? "broadcast" : "")
      .append("<a>" + item.label + "</a>") // + "<br>" + item.userName
      .appendTo(ul);
  };

  $("#launch_ip_list").off().show().click(function () {
    $("#rva_url").focus();
    if (!$(this).data('toggle')) {
      $("#project").trigger("keydown");
    }
    else {
      $("#ui-id-1").hide();
    }
    $(this).data('toggle', !$(this).data('toggle'));
  });

  $("#rva_url").off().blur(function () {
    $("#ui-id-1").hide();
  }).keydown(function (e) {
    var code = e.keyCode || e.which;
    //console.log("keydown.....", code, $(".ui-state-focus").index());

    // arrow down
    if (code == 40) {
      if ($("#ui-id-1").css("display") == "none") {
        //console.log('trigger("keydown")');
        $("#project").trigger("keydown");
      }
      if ($(".ui-state-focus").index() > -1) {
        $(".ui-state-focus").next().trigger("mouseover");
      }
      else {
        //console.log("enter.....", code, $(".ui-state-focus").index(), $(".ui-menu-item"));
        $(".ui-menu-item").first().trigger("mouseover");
      }
    }

    // arrow up
    if (code == 38) {
      $(".ui-state-focus").prev().trigger("mouseover");
    }

    // enter
    if (code == 13) {
      $(".ui-state-focus").trigger("click");
    }

  });

  //console.log('refreshIPList...', $('.ui-autocomplete'));

}

function sortIpList(array, key, key2) {
  return array.sort(function(a, b) {
      var key1 = 'status', key2 = 'deviceName', key3 = 'ip';
      var x  = a[key1]; var y  = b[key1];
      var x2 = a[key2]; var y2 = b[key2];
      var x3 = a[key3]; var y3 = b[key3];
      return ((x > y) ? -1 : ((x < y) ? 1 : ((x2 < y2) ? -1 : ((x2 > y2) ? 1 : ((x3 < y3) ? -1 : ((x3 > y3) ? 1 : 0))))));
  });
}

function getLookupServer(ipList) {
  //console.log('[getLookupServer]...', ipList);
  var newIpList = sortIpList(ipList);

  var control = $("#dialog-ip-list ul").empty();
  $.each(newIpList, function(i, obj) {
    var ip = obj.ip;
    $('<li class="ui-menu-item" tabindex="-1" ip="'+ ip +'"></li>')
      .append($('<a></a>').text(obj.deviceName + ' ('+ ip +')'))
      .addClass((obj.status=="Online")? 'broadcast' : '')
      .off().click(function() {
        $('#rva_url').val(ip);
        $("#dialog-ip-list").dialog('close');
      })
      .appendTo($(control));
  });

  if (newIpList && newIpList.length > 0) {
    $("#launch_lookup_ip").off().show().click(function () {
      $("#dialog-ip-list").dialog({
        autoOpen: true,
        resizable: false,
        modal: true,
        draggable: false,
        width: '90%',
        height: '200',
        dialogClass: "titlbar-only-close no-buttonpane",
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        show: {
          effect: "blind",
          duration: 800
        }
      });
    });
  }
}

/**
*
* RVA discovery event handlers
*
*/
rvaDiscovery.on('RvaDiscovery', function(ipList){
  //console.log('RvaDiscovery', ipList);
  refreshIPList(ipList);
});

rvaDiscovery.on('error', function(err){
  console.error('rvaDiscovery Error detected:', err);
  console.error(err.message);
  console.error(err.stack);

  switch (err.message) {
  case "EPIPE":
    //console.log(err.message);
    messagePage(err.message);
    //messagePage("Connection failed...");
    break;
  default:
    console.error(err);
    //messagePage(err.message);
  }
});



/**  RVA Client event handlers **/
rvaClient.on('SoundFlowerWarning', function(connectionRequest){
  console.log('SoundFlowerWarning...');
  novo.novoconnect("askEnableSoundFlower", connectionRequest);
});

rvaClient.on('getSoundFlowerPermission', function(){
  console.log('getSoundFlowerPermission...');
  novo.novoconnect("askInstallSoundFlower");
});

rvaClient.on('connected', function(settings){
  console.log('[connected]...', settings);
  novo.novoconnect("doLogin", true);
  novo.novoconnect("updateInfo", settings);
  novo.novoconnect("openConnectedTimer", $.t('login.connecting'));  // 3 seconds timer
});

rvaClient.on('hostRequested', function(value){
  console.log('[hostRequested]...', value);
  novo.novoconnect('toBeHost', value);
});

rvaClient.on('moderatorRequest', function(value){
  console.log('[moderatorRequest]...', value);
  novo.novoconnect('beAskedHostPage',$.t('login.wannaBeHost'));
  win.requestAttention(true); // highlight system toolbar icon
});

rvaClient.on('settingsUpdated', function(settings){
  console.log('[settingsUpdated]...', settings);
  novo.novoconnect("updateInfo", settings);
});

rvaClient.on('teacherCredentialPassword', function(password){
  //console.log('[teacherCredentialPassword]...', password);
  $('#toggleTeacherPw').val(password);
  if (password == undefined || password == null || password=="") {
    $('.pwBlock').addClass('error');
    $('.toggleTeacherPwMsg').text($.t('login.required'));
    return;
  }
});

rvaClient.on('teacherCredentialSetting', function(value){
  //console.log('[teacherCredentialSetting]...', value);
  $('#moderator').prop('checked', value);
  (value)? $('.pwBlock').show() : $('.pwBlock').hide();
});

rvaClient.on('teacherCredentialPasswordError', function(value){
  console.log('teacherCredentialPasswordError:', value);
  novo.novoconnect("openModeratorPage", value);
});

rvaClient.on('teacherCredentialNoPermission', function(value){
  console.log('teacherCredentialNoPermission event', value);
  novo.novoconnect('messagePage', $.t('login.teacherCredentialNoPermission'));
});

rvaClient.on('teacherCredentialNoPermissionVotingOn', function(value){
  console.log('teacherCredentialNoPermissionVotingOn event', value);
  novo.novoconnect('messagePage', $.t('login.teacherCredentialNoPermissionVotingOn'));
});

rvaClient.on('teacherCredentialNoPermissionTeacherOn', function(value){
  console.log('teacherCredentialNoPermissionTeacherOn event', value);
  novo.novoconnect('messagePage', $.t('login.teacherCredentialNoPermissionTeacherOn'));
});

rvaClient.on('viewModeChanged', function(mode){
  console.log('viewModeChanged:', mode);
  novo.novoconnect("setViewMode", mode);
});

rvaClient.on('receivePreviewImage', function (previewImage) {
  console.log('receivePreviewImage event:', previewImage);
  novo.novoconnect("previewImage", previewImage);
});

rvaClient.on('previewTimeout', function (userId) {
  console.log('[previewTimeout]... uid:', userId);
  novo.novoconnect("previewTimeout", userId);
  //novo.novoconnect("previewTimeout", 3);
});

rvaClient.on('changeScreenModeTimeout', function (userId) {
  console.log('changeScreenModeTimeout event, uid:', userId);
  novo.novoconnect("refresh");
});

rvaClient.on('modeSwitch', function(mode) {
  var pno = $('.user.isMyself .panel').attr('no');
  console.log('[modeSwitch]...', mode, ' from ', pno);
  if (typeof pno === 'undefined') {
    console.log("TODO: NO Send Command for undefined");
  } else {
    if (mode == pno) {
      // stop myself presentation
      console.log("TODO: withdraw projection", mode);
      $('.user.isMyself .panel area[region="' + mode + '"]').off().closest("td").addClass("loading").empty().append($('<div>'));
      rvaClient.stopPresentation();
    } else {
      // mirror myself
      console.log("TODO: project", mode);
      $('#icon-group').trigger("click");
      $('.user.isMyself .panel area[region="' + mode + '"]').trigger('click')
    }
  }
});

rvaClient.on('moderatorRequestApproved', function () {
  console.log('moderatorRequestApproved event');
  novo.novoconnect('closeWaitingPageWithTimer', $.t('login.requestAccepted'));
});

rvaClient.on('moderatorRequestDenied', function () {
  console.log('moderatorRequestDenied event');
  novo.novoconnect('closeWaitingPageWithTimer', $.t('login.requestDeclined'));
});

rvaClient.on('userListUpdated', function(userList){
  console.log('userListUpdated:', userList);
  novo.novoconnect("updateUserList", userList);
});

rvaClient.on('uploadGroupFileSuccess', function(group){
  console.log('uploadGroupFileSuccess...', group);
});

rvaClient.on('removeGroupSuccess', function(){
  console.log('removeGroupSuccess...');
});

rvaClient.on('notGroupTeacher', function(){
  console.log('[notGroupTeacher]...');
  novo.novoconnect('messagePage', $.t('group.notGroupTeacher'));
});

rvaClient.on('pinRequired', function(value){
  // value is the pin number, if not pin required, value would be false
  console.log('pinRequired: ', value);
  novo.novoconnect("updatePinCode", value);
});

rvaClient.on('viewerState', function(value){
  console.log('viewerState:', value);
  switch (value) {
    case 'pause':
      novo.novoconnect("receiveScreenPause", true);
      break;
    case 'resume':
      novo.novoconnect("receiveScreenPause", false);
      break;
    default:
      console.error('Error viewerState:', value);
  }
});

rvaClient.on('tabletsLocked', function(value){
  console.debug('tabletsLocked...', value);
  novo.novoconnect("refreshTabletsLocked", value);
});

nativeService.dsaServer.once('newVersionAvailable', function(){
  console.log('newVersionAvailable...');
  hasNewVersionAvailable();
});

nativeService.dsaServer.once('incorrectDevice', function(){
  console.log('incorrectDevice...');
  showLicenseMessage('noLicense');
});

function hasNewVersionAvailable() {
  //console.log('hasNewVersionAvailable...', novo, $(novo).novoconnect('instance'));
  var downloadButton = $('<button id="downloadButton" class="button" data-i18n>upgrade.upgrade</button>').text(i18n.t('upgrade.upgrade'))
                      .button()
                      .on("click", function() {
                        $(this).button("option", {
                          disabled: true,
                          label: $.t('upgrade.downloading')
                        });
                        initProgressBar();
                      });

  $('#upgradeMsg').text($.t('upgrade.newVersion')).append(downloadButton);

  if (novo && $(novo).novoconnect('instance')) {
    //console.log('newVersionAvailable... delay to call dialog');
    askUpgradeDialog();
    return;
  }

  hasNewVersion = true;
};

function askUpgradeDialog() {
  //console.log('askUpgradeDialog...', hasNewVersion);
  var page = $('<div id="updateConfirmPage"></div>')
              .append($('<p></p>').append($.t('upgrade.newVersion'))
                        .append($.t('upgrade.yesToInstall1'))
                        .append($.t('upgrade.yesToInstall2')))
              .appendTo($('#setting'));
  $(page).dialog({
    autoOpen: true,
    resizable: false,
    draggable: false,
    closeOnEscape: false,
    modal: true,
    width: '95%',
    dialogClass: "msg-page no-titlebar upgrade-msg",
    open: function( event, ui ) {
      $('.upgrade-msg').css('top','15px');
    },
    close: function(event, ui) {
      $(this).dialog('destroy').remove();
    },
    buttons: [
      {
        text: i18n.t('upgrade.continue'),
        click: function() {
          $(this).dialog('destroy').remove();
          initProgressBar();
        }
      },
      {
        text: i18n.t('upgrade.remind'),
        click: function() {
          $(this).dialog('destroy').remove();
        }
      }
    ]
  });

  hasNewVersion = false;
  win.requestAttention(true); // highlight system toolbar icon
};

function initProgressBar() {
  //console.log('initProgressBar...');
  if ($(novo).novoconnect('instance').options.connected) {
    // disconnect from RVA if connected
    $('#disconnect-btn').trigger('click');
  }

  var progressDialog = $('<div id="progressbar-dialog" class="message-page prevent-close-outside"></div>').attr('title',$.t('upgrade.downloading')).appendTo($('#setting')),
      progressLabel = $('<div class="progress-label"></div>').text($.t('upgrade.donwloadInProgress')).appendTo(progressDialog),
      progressbar = $('<div id="progressbar"></div>').appendTo(progressDialog),
      dialogButtons = [{
        text: $.t('upgrade.downloadCancelButton'),
        click: closeDownload
      }];

  $(progressDialog).dialog({
    autoOpen: true,
    closeOnEscape: false,
    resizable: false,
    draggable: false,
    modal: true,
    width: '95%',
    dialogClass: "msg-page upgrade-progress no-close",
    buttons: dialogButtons,
    open: function () {
      $(progressbar).show();
      console.log('Start to download...');
      rvaClient.beginDownloadUpdate(function (err) {
        console.error(err);
      },
      function (result) {
        if (result)
          console.log('download complete');
        else
          console.log('download failed');
      });
      $('.upgrade-progress').css('top','15px');
      $('.ui-dialog-buttonpane').css('visibility', 'hidden');
    },
    close: function(event, ui) {
      $(this).dialog('destroy').remove();
    },
    beforeClose: function () {
      $('#downloadButton').button("option", {
        disabled: false,
        label: $.t('upgrade.upgrade')
      });
    }
  });

  progressbar.progressbar({
    value: false,
    complete: function() {
      progressDialog.dialog("option","title",$.t('upgrade.complete'))
        .dialog("option","dialogClass","msg-page no-close no-buttonpane");
      $(progressbar).hide();
      progressLabel.text($.t('upgrade.startInstall'));
      $('<div>').addClass('loading-circle').appendTo(progressLabel);
      // Starting setup after download, dsa will exit after setup runs.
      //$("#progressbar").progressbar("value", 100);
    }
  });

  function closeDownload() {
    console.log('Cancel Download...');
    rvaClient.cancelDownloadUpdate();
    progressDialog.dialog("close");
  }
}

rvaClient.on('newVersionDownloadProgress', function(progress){
  console.log('newVersionDownloadProgress', progress);
  //Speed unit: kBps, time: seconds
  var val = progress.downloadPercentage*100 || 0;
  $("#progressbar").progressbar("value", val);
  $("#progressbar").on("progressbarchange",function(event,ui){
    $('.ui-dialog-buttonpane').css('visibility', 'visible');
    $(".progress-label")
     .html($.t('upgrade.currentProgress') +" <span class='deepskyblue'>" + Math.round(val*10)/10 + "</span> %, <span class='deepskyblue'>" + formatBytes(progress.downloadedBytes) + "</span>" + $.t('upgrade.percentOf') + formatBytes(progress.totalBytes) + "<br />")
     .append($.t('upgrade.remainTime')).append(' ')
     .append($("<span class='deepskyblue'>" + toTimeString(Math.round(progress.remainingTime)) + "</span>"));
  });
});

function formatBytes(bytes, decimals) {
  if(bytes == 0) return '0 Byte';
  var k = 1024;
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  var i = Math.floor(Math.log(bytes) / Math.log(k));
  var num = (bytes / Math.pow(k, i));
  var dm = ((num >= 1000)? 4 : (decimals + 1 || 3));
  return num.toPrecision(dm) + " " + sizes[i];
}

function toTimeString(seconds) {
  var str = (new Date(seconds * 1000)).toUTCString().match(/(\d\d:\d\d:\d\d)/)[0];
  return (seconds >= 3600)? str : str.substr(3,5);
}

rvaClient.once('shutDownDSA', function(){
  console.log('[shutDownDSA]...');
  killMyself();
});

function killMyself(){
  //TODO: disconnect from RVA and quit DSA
  novo.novoconnect('resetAll');
  gui.App.closeAllWindows();
  gui.App.quit();
}

rvaClient.on('disconnected', function(data){
  console.log('disconnected events:', data);
  switch(data.event){
    case 'disconnected':
      console.log('disconnected - disconnected');
      novo.novoconnect('resetAll');
      break;
    case 'failedToConnect':
      console.log('disconnected - failedToConnect');
      novo.novoconnect("connectionFailPage", $.t('login.connectFailMsg'));
      break;
    case 'reconnect':
      console.log('disconnected - reconnect');
      novo.novoconnect("openWaitingPageNoTimer", $.t('login.reconnecting'));
      break;
    case 'reconnectFailed':
      console.log('disconnected - reconnectFailed');
      novo.novoconnect("connectionFailPage", $.t('login.connectFailMsg'));
      break;
    case 'invalidRVAVersion':
      console.log('disconnected - invalidRVAVersion');
      novo.novoconnect('messagePage', $.t('login.incompatible'));
      break;
    case 'applicationEnd':
      console.log('disconnected - applicationEnd');
      gui.App.closeAllWindows();
      gui.App.quit();
      break;
    case 'PIN_INVALID':
      console.log('disconnected - PIN_INVALID');
      novo.novoconnect('messagePage', $.t('login.pinInvalid'));
      break;
    case 'NAME_NOT_IN_GROUP':
      console.log('disconnected - NAME_NOT_IN_GROUP');
      novo.novoconnect('messagePage', $.t('group.nameNotInGroup'));
      break;
    case 'DUPLICATED_NAME_IN_GROUP':
      console.log('disconnected - DUPLICATED_NAME_IN_GROUP');
      novo.novoconnect('messagePage', $.t('group.duplicateNameInGroup'));
      break;
    case 'REACH_MAX_NUMBER_USERS':
      console.log('disconnected - REACH MAX NUMBER USERS');
      novo.novoconnect("connectionFailPage", $.t('login.connectionOverMsg'));
      break;
    case 'NOVOCAST_REACH_MAX_NUMBER_USERS':
      console.log('disconnected - REACH MAX NUMBER USERS');
      novo.novoconnect("connectionFailPage", $.t('login.connectionOver2Msg'));
      break;
    case 'connectReset':
      console.log('disconnected - connectReset');
      novo.novoconnect('resetAll');
      novo.novoconnect("messagePage", {
        title: $.t('login.connectFail'),
        description: $.t('login.unstableNetwork'),
      });
      break;
    default:
      console.error(data);
      novo.novoconnect('resetAll');
      break;
  }
});

function connectToRVA(info){
  rvaClient.connect(info.rvaIp, info.clientName, info.pinCode, gui.Screen.screens.length);
}

rvaClient.on('goVoting', function(data) {
  console.log('[goVoting]... data=', data);
  novo.novoconnect("goVoting", data); // send quetion to student
});

rvaClient.on('votingHandler', function(evt, data) {
  console.log('[votingHandler]... ', evt, data);
  novo.novoconnect('votingHandler', evt, data);
});

function votingHandler(evt, data) {
  novo.novoconnect('votingHandler', evt, data);
}

function votingEditorHandler(evt, data) {
  var output = novo.novoconnect('votingEditorHandler', evt, data);
  return output;
}

function messagePage(msg) {
  //novo.novoconnect("messagePage", msg);
  $(".message-page").dialog('destroy').remove();
  var title, description;
  title = (typeof(msg)=="string")? '' : msg.title;
  description = (typeof(msg)=="string")? msg : msg.description;

  // Display message
  var page = $('<div class="message-page"></div>')
              .append($('<p></p>').text(description));
  if (title!='') {
    $('<h4></h4>').text(title).prependTo(page);
  }

  $(page).dialog({
    autoOpen: true,
    title: "Error Message",
    resizable: false,
    modal: true,
    width: '95%',
    //height: '200',
    //dialogClass: "no-close",
    show: {
      effect: "blind",
      duration: 800
    },
    close: function(event, ui) {
      $(this).dialog('destroy').remove();
    },
    buttons: [
      {
        text: 'Close',
        click: function() {
          $(this).dialog("close");
        }
      }
    ]
  });
}

rvaClient.on('connnectRVA', function() {
  console.log('[connnectRVA]... ');
  $('#connect-btn').trigger('click');
});

rvaClient.on('disconnnectRVA', function() {
  console.log('[disconnnectRVA]... ');
  $('#disconnect-btn').trigger('click');
});

rvaClient.on('askSendFilePage', function(data) {
  console.log('[askSendFilePage]...', data);
  novo.novoconnect("askSendFilePage", data);
});

rvaClient.on('systemBusyPage', function() {
  console.log('[systemBusyPage]...');
  novo.novoconnect("systemBusyPage");
});

rvaClient.on('sendingFilePage', function(data) {
  console.log('[sendingFilePage]...', data);
  novo.novoconnect("sendingFilePage", data);
});

rvaClient.on('closeSendingFilePage', function() {
  console.log('[closeSendingFilePage]...');
  novo.novoconnect("closeSendingFilePage");
});

rvaClient.on('askPresentPage', function() {
  console.log('askPresentPage...');
  novo.novoconnect('beAskedPresenterPage',$.t('info.wannaPresenter'));
});

rvaClient.on('corpModeStatus', function(status) {
  console.log('corpModeStatus...', status);
  novo.novoconnect('corpModeStatus', status);
});

rvaClient.on('fileReceived', function(data) {
  console.log('[fileReceived]...', data);
  novo.novoconnect("fileReceived", data);
  win.requestAttention(true); // highlight system toolbar icon
});

rvaClient.on('urlReceived', function(data) {
  console.log('[urlReceived]...', data);
  //data = {url: "https://html5test.com/", sender: "Molly"};
  novo.novoconnect("urlReceived", data);
  //novo.novoconnect("urlReceived", {url: "https://html5test.com/", sender: "Molly"});
  win.requestAttention(true); // highlight system toolbar icon
});

rvaClient.on('requestDenied', function() {
  console.log('[requestDenied]...');
  novo.novoconnect("messagePage", $.t('info.requestDeclined'));
});

rvaClient.on('askSendAnotherFilePage', function(data) {
  console.log('[askSendAnotherFilePage]...', data);
  novo.novoconnect("askSendAnotherFilePage", data);
});

rvaClient.on('votingStartSuccess', function() {
  console.log('[votingStartSuccess]...');
  novo.novoconnect("votingEditorHandler", 'votingStartSuccess');
});

rvaClient.on('votingStartFail', function() {
  console.log('[votingStartFail]...');
  novo.novoconnect("votingEditorHandler", 'votingStartFail');
});

rvaClient.on('votingReceiveAnswer', function(data) {
  console.log('[votingReceiveAnswer]...', data);
  novo.novoconnect("votingEditorHandler", 'receiveAnswer', data);
});

rvaClient.on('stopWaitingPageWithTimer', function() {
  console.log('[stopWaitingPageWithTimer]...');
  novo.novoconnect("stopWaitingPageWithTimer");
});

rvaClient.on('votingStartingException', function(evt) {
  console.error('[votingStartingException]...', evt);
  switch(evt){
    case 'setgroup':
      novo.novoconnect('messagePage', $.t('group.votingLoadError'));
      break;
    case 'removegroup':
      novo.novoconnect('messagePage', $.t('group.votingRemoveError'));
      break;
  }
});

rvaClient.on('duplicateNameInGroup', function() {
  console.log('[duplicateNameInGroup]...');
  novo.novoconnect('messagePage', $.t('group.duplicateNameFile'));
});

rvaClient.on('getLookupServer', function(ipList) {
  console.log('[getLookupServer]...');
  getLookupServer(ipList);
});

rvaClient.on('minimizedAttention', function() {
  console.log('[minimizedAttention]...');
  win.requestAttention(true); // highlight system toolbar icon
});

rvaClient.on('remoteStatusUpdate', function(data) {
  console.log('[remoteStatusUpdate]...', data);
  novo.novoconnect('setProjectionData', data);
});

rvaClient.on('dsaQuit', function() {
  console.log('[dsaQuit]...');
  win.close(true);
  gui.App.closeAllWindows(); // will close all windows
  gui.App.quit();
});
/**  End - RVA Client event handlers **/

function validateIPaddress(ipaddress) {
  if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {
    return (true)
  }
  console.error("Invalid IP address: ", ipaddress);
  return (false)
}

function testLogin(){
  console.debug("[testLogin]");

  //novo.novoconnect("setInitValue", {"imageQuality":0.6,"moderator":false,"moderatorPassword":""});
  novo.novoconnect("doLogin", true);
  novo.novoconnect("updateInfo", {
    clientName: 'Molly',
    message: 'You are connected.',
    rvaIP: "192.168.8.105",
    rvaType: "NOVOCAST",
    rvaVersion: 1.0,
    pinRequired: false,
    pinCode: "6746",
    groupMode: false,
    dataPath: "",
  });
  novo.novoconnect("stopWaitingPageWithTimer");

  novo.novoconnect("receiveScreenPause", false);
  novo.novoconnect("setViewMode", "full");

/*
  novo.novoconnect("toBeHost", true);
  var userList = [{"uid":1,"label":"Vince","isHost":true,"device":"Chrome on Win","panel":0,"isMyself":true,"online":true,"display_name":"Vince","os_type":10}];
  novo.novoconnect("updateUserList", userList);
  $("#rva_url").val("192.168.8.103");
  $("#client_name").val("Vince");
  $("#connect-btn").trigger("click");
*/

}
