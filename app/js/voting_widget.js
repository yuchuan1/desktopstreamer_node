var gui = require('nw.gui'),
    win = gui.Window.get();

var mainWindow,
    fileEntry,
    questionType = {
      1: "Thumb Up/Down",
      2: "True/False",
      3: "Multiple-choice: 3",
      4: "Multiple-choice: 4",
      5: "Multiple-choice: 5",
      6: "Open-ended",
    },
    questionTypeTransfer = {
      1: "voting.thumbsUpDown",
      2: "voting.TrueFalse",
      3: "voting.3Choices",
      4: "voting.4Choices",
      5: "voting.5Choices",
      6: "voting.openEnded",
    };

$.widget("custom.votingPanel", {
  // Default options
  options: {
    currentQuestion: null,
    dropFile: null,
  },
  _create: function _create() {
    mainWindow = getMainWindow();
    //document.title = $.t("voting.title") + ' | Novo';
    //console.log('_create...', document.title, $.t("voting.title"));
  },
  _init: function _init() {
    // init status
    fileEntry = null;
    this.closeAllDialogs();
    window.ondrop = noDrop;
    $("#sidebar").empty();
    $("#q-title").empty();
    $("#q-content").empty().append('<div id="q-image" style="position: absolute; top: 38%; left: 38%;"><img src="../images/logo_watermark.png" /></div>');
    $("#answers").empty().removeClass();
    $("#bottom-btn").css("visibility", "hidden");
    $("#send-btn").off();
    $("#hint").empty();
  },
  createQuestion: function createQuestion(question){
    var instance = this;
    instance._init();  // reset all objects
    instance.options.currentQuestion = question;
    mainWindow = getMainWindow();
    //console.log("createQuestion...", question, mainWindow);
    
    $("#sidebar").append($("<div>").addClass("active").append($('<b>Q' + (question.num+1) + '. </b>')).append(question.subject));
    $("#q-title").text($.t("voting.newTitle") + (question.num+1))
      .append($('<span class="small"> ('+ $.t(questionTypeTransfer[question.type]) + ')</span>'));
    $("#q-content").show().html(question.richSubject);
    
    if (question.image){
      var img = $('<img src="data:image/png;base64,'+ question.image +'">').click(function(){
                  instance.closeAllDialogs();
                  $('#dialog-preview').empty().append($('<img>').attr('src','data:image/png;base64,'+ question.image).addClass('only-img'));
                  $("#dialog-preview").dialog({
                    autoOpen: true,
                    modal: true,
                    resizable: false,
                    draggable: false,
                    width: '90%',
                    dialogClass: "msg-page titlbar-only-close",
                    show: {
                      effect: "blind",
                      duration: 300
                    }
                  });
                });
      $("#q-content").append($('<div id="q-image"></div>').append(img));
    }
    $("#send-btn").click(function(){
      instance._submitAnswerEvent(this);
    });
    
    var answer = $('<button class="answer"></button>').click(instance._answerEvent);
    var qType = parseFloat(question.type);
    switch(qType) {
      case 1:  // QuestionTypeThumbsUpDown = 1
        answer = $('<span class="answer"></span>').click(instance._answerEvent);
        $("#answers").addClass("commend")
          .append($(answer).clone(true).addClass('thumbs-down').attr('value',4))
          .append($(answer).clone(true).addClass('thumbs-up').attr('value',3));
        break;
      case 2:  // QuestionTypeTrueFalse = 2
        $("#answers").addClass("boolean")
          .append($(answer).clone(true).val(1).text($.t('voting.true')))
          .append($(answer).clone(true).val(2).text($.t('voting.false')));
        break;
      case 3:  // QuestionType3Choices = 3
        $("#answers").addClass("choice-3")
          .append($(answer).clone(true).val(5).text('A'))
          .append($(answer).clone(true).val(6).text('B'))
          .append($(answer).clone(true).val(7).text('C'));
        break;
      case 4:  // QuestionType4Choices = 4
        $("#answers").addClass("choice-4")
          .append($(answer).clone(true).val(5).text('A'))
          .append($(answer).clone(true).val(6).text('B'))
          .append($(answer).clone(true).val(7).text('C'))
          .append($(answer).clone(true).val(8).text('D'));
        break;
      case 5:  // QuestionType5Choices = 5
        $("#answers").addClass("choice-5")
          .append($(answer).clone(true).val(5).text('A'))
          .append($(answer).clone(true).val(6).text('B'))
          .append($(answer).clone(true).val(7).text('C'))
          .append($(answer).clone(true).val(8).text('D'))
          .append($(answer).clone(true).val(9).text('E'));
        break;
      case 6:  // QuestionType4Choices = 6
        fileEntry = $('<input id="fileDialog" type="file" accept=".jpg,.jpeg,.png,image/*" style="display: none;">').change(function(evt) {
                      instance.options.dropFile = null;
                      var file = $(this)[0].files[0];
                      var accepts = ['jpg','jpeg','png'];
                      var ext = file.name.substr((~-file.name.lastIndexOf(".") >>> 0) + 2).toLowerCase();
                      if ($.inArray(ext, accepts) > -1){
                        setUploading(file);
                        return;
                      }
                      console.error('Error file type...');
                    });
        window.ondrop = function dropImage(e) {
                          e.preventDefault();
                          e.stopPropagation();
                          var files = e.dataTransfer.files,
                              file = files[0],
                              accepts = ['jpg','jpeg','png'],
                              extension = file.name.slice((file.name.lastIndexOf(".") - 1 >>> 0) + 2).toLowerCase();
                          //console.info('dropImage...', file, extension);
                          if (accepts.indexOf(extension) > -1) {
                            $('#fileDialog').val(null);
                            setUploading(file);
                            instance.options.dropFile = file;
                          }
                          else {
                            console.error('Error file type...', file);
                          }
                          return false;
                        };
        answer = $('<span id="choose_img">...</span>').click(function() {
                    $('#fileDialog').trigger('click');
                 });
        $("#answers").addClass("upload")
          .append($('<input id="img_path" type="text" />')
                    .attr('placeholder',$.t('voting.drogImage'))
                    .attr('readonly','readonly')
                    .click(function(){$("#choose_img").trigger('click');}))
          .append($(answer))
          .append($(fileEntry));
        break;
      default: // QuestionTypeOpenEnded = ?
        console.error("Question Type does NOT exist.", question);
        //video.videoPanel("[createQuestion] question=", question);
    }

    //(chrome.app.window.current().isMaximized()) ? winOnMax() : winOnRestore();
  },
  submitAnswerResult: function submitAnswerResult(result, message) {
    console.log("submitAnswerResult...", result, message, this.options.currentQuestion.answerSubmited);
    this.options.currentQuestion.answerSubmited = result;
    if (message && message!='') {
      $("#send-btn").hide();
      $("#hint").show().text($.t(message));
      $("#bottom-btn").css("visibility", "visible");
    }
  },
  stopAnswerQuestion: function stopAnswerQuestion() {
    var instance = this;
    //console.log("stopAnswerQuestion......", instance.options.currentQuestion);
    if (instance.options.currentQuestion != null){
      // disable all buttons
      instance._disableAnswer();
      // show message
      $("#hint").show().text($.t('voting.stoppedMsg'));
      $("#bottom-btn").css("visibility", "visible");
      // close all dialogs
      instance.closeAllDialogs();
    }
    window.ondrop = noDrop;
  },
  showAnswerResultDialog: function showAnswerResultDialog(flag, correctAnswer) {
    this._disableAnswer();
    this.closeAllDialogs();

    var answerDialog = {
      1: "#dialog-correct",
      2: "#dialog-incorrect",
      3: "#dialog-timeout",
    };

    console.log("correctAnswer=", correctAnswer);
    if (correctAnswer) {
      var str = correctAnswer.toLowerCase().trim();
      $("#correctAnswer").text((str.length==1)? correctAnswer : $.t('voting.' + str));
    }

    $(answerDialog[flag]).dialog({
      closeOnEscape: true,
      resizable: false,
      width: 400,
      modal: true,
      dialogClass: "msg-page",
      buttons: [
        {
          text: "OK",
          click: function() {
            mainWindow.window.votingHandler('removeVotingDialog');
            //$(this).dialog('destroy').remove();
            $(this).dialog("close");
          }
        }
      ]
    });
    return;
  },
  _answerEvent: function _answerEvent() {
    if ($(this).hasClass("checked")){
      return false;
    }
    $(this).siblings().removeClass("checked");
    $(this).addClass("checked");
    
    var data = {
      answer: $(this).attr('value')
    };
    //console.info('_answerEvent...', data);
    mainWindow.window.votingHandler('clickVotingAnswer', data);

    // show button
    $("#send-btn").removeClass("active").show();
    $("#hint").empty();
    $("#bottom-btn").css("visibility", "visible");
  },
  _submitAnswerEvent: function _submitAnswerEvent(btn) {

    if ($(btn).hasClass('active')){
      return false;
    }

    var instance = this;

    // Double confirm when the user send duplicate answer.
    if (instance.options.currentQuestion.answerSubmited){
      $("#dialog-confirm").dialog({
        resizable: false,
        width: 400,
        modal: true,
        dialogClass: "msg-page alert",
        buttons: [
          {
            text: $.t('button.cancel'),
            click: function() {
              $(this).dialog("close");
              $("#img_path").focus();
            }
          },
          {
            text: $.t('button.ok'),
            click: function() {
              instance._sendAnswer();
              $(this).dialog("close");
              $("#img_path").focus();
            }
          }
        ]
      });
      return false;
    }

    instance._sendAnswer();
  },
  _sendAnswer: function _sendAnswer() {
    var instance = this;
    $("#send-btn").addClass('active');

    // answer type 6
    if ($("#answers").hasClass("upload")){
      console.log(instance.options.dropFile, $('#fileDialog')[0].files[0]);
      var file = instance.options.dropFile || $('#fileDialog')[0].files[0];
      //var file = $('#fileDialog')[0].files[0];
      console.log('_sendAnswer...', file);
      if (file != null){
        mainWindow.window.votingHandler('submitVotingAnswer', {answer: file});
      }
      return false;
    }

    console.info("_sendAnswer...", {answer: $('.answer.checked').attr('value')});
    mainWindow.window.votingHandler('submitVotingAnswer', {answer: $('.answer.checked').attr('value')});
    return false;
  },
  _disableAnswer: function _disableAnswer() {
    //console.log("_disableAnswer......");
    // disable all answer buttons, input
    $(".answer").off().removeClass("answer");
    $("#answers").addClass("stopped");
    $("#answers input").off();
    $("#answers span").off();
    $("#img_path").off().attr("disabled", true);
    $("#choose_img").off().addClass("disabled");
    $("#send-btn").hide();
  },
  closeAllDialogs: function closeAllDialogs() {
    //console.log("closeAllDialogs......");
    var answerDialogs = ["#dialog-correct","#dialog-incorrect","#dialog-timeout","#dialog-confirm","#dialog-preview"];
    $.map(answerDialogs, function( val, idx ) {
      // Check exist and open, then close it.
      if ($(val) && $(val).hasClass("ui-dialog-content") && $(val).dialog("isOpen"))
        $(val).dialog("close");
      return;
    });
  },
  _destroy: function _destroy() {
    // events bound via _on are removed automatically
    // revert other modifications here
    // remove generated elements
    this.remove();
  }
});

function noDrop(e) {
  e.preventDefault();
  e.stopPropagation();
  //console.info('noDrop...');
  return false;
}

function setUploading(file){
  $('#img_path').val(file.path).addClass("disable").attr('readonly','readonly');
  // show button
  $("#send-btn").removeClass("active").show();
  $("#hint").empty();
  $("#bottom-btn").css("visibility", "visible");
}