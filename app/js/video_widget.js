var gui = require('nw.gui'),
    win = gui.Window.get(),
    _ = require('lodash'),
    rvaClient;

$.widget("custom.videoPanel", {
  // Default options
  options: {
    videoTotalSeconds: 0,  // video total seconds
    videoPlayPercentage: 0,
    playFile: null,
    isPlaying: false,
  },
  _create: function _create() {
    var instance = this;

    $("#close-video-win").off().click(function() {
      console.log('call rvaClient.stopYouTubeVideo();');
      rvaClient.stopYouTubeVideo();
      if (win != null) win.close(true);
      win = null;
    });

    $("#choose_file").off().click(function() {
      $('#fileDialog').trigger('click');
    });

    instance._enableVideoPath();

    $('#fileDialog').off().change(function(evt) {
      instance.removeErrorMsg();
      var file = $(this)[0].files[0];
      var ext = file.name.slice((file.name.lastIndexOf(".") - 1 >>> 0) + 2).toLowerCase();
      var accepts = ['mp4','mov','mpg','avi','wmv','3gpp','3gp','3g2','mkv','mp3','wma','aac'];
      var audios = ['mp3','wma','aac'];
      //console.log("ext=", ext, $.inArray(ext.toLowerCase(), accepts));
      if ($.inArray(ext, accepts) > -1){
        instance.options.playFile = file;
        var title = ($.inArray(ext, audios) > -1)? $.t('video.audioTitle') : $.t('video.title');
        $('#video_path').val(file.path).trigger('change').addClass("disable").attr('readonly','readonly');
        $('.span2').text(title);
        $('title').text(title + ' | Novo');
        $('#clear').addClass("enable");
        return;
      }
      console.error('Error file type...');
      instance.errorHandler($.t('video.wrongFileType'));
    });

    $('#clear').off().click(function(){
      $('#clear').removeClass("enable");
      $('#video_path').val("").off().removeAttr('readonly').removeClass("disable").focus();
      $('#fileDialog').val("").empty();
      var title = $.t('video.title');
      $('.span2').text(title);
      $('title').text(title + ' | Novo');
      instance.options.playFile = null;
      instance._enableVideoPath();
      instance._disablePlayback();
    });
    
    window.ondrop = function(e) {
      e.preventDefault();
      console.log('window.ondrop...');
      if ($('.path').css('display')=='none') return false;
      
      instance.removeErrorMsg();
      var file = e.dataTransfer.files[0];
      var ext = file.name.slice((file.name.lastIndexOf(".") - 1 >>> 0) + 2).toLowerCase();
      var accepts = ['mp4','mov','mpg','avi','wmv','3gpp','3gp','3g2','mkv','mp3','wma','aac'];
      var audios = ['mp3','wma','aac'];
      //console.log(file, ext);
      if ($.inArray(ext, accepts) > -1){
        instance.options.playFile = file;
        var title = ($.inArray(ext, audios) > -1)? $.t('video.audioTitle') : $.t('video.title');
        $('#video_path').val(file.path).trigger('change').addClass("disable").attr('readonly','readonly');
        $('.span2').text(title);
        $('title').text(title + ' | Novo');
        $('#clear').addClass("enable");
        $('#fileDialog').val(null);
      }
      else {
        console.error('Error file type...', e);
        $("#video_path").val(file.name);
        instance.options.playFile = null;
        instance.errorHandler($.t('video.wrongFileType'));
      }
      
      return false;
    };

    // initialize prograss bar
    var moveTime = function() {
      if ($(this).hasClass("active")) {
        console.log("No Change point...");
        instance._setProgressBar();
        return false;
      }
      $(this).addClass("active");
      var red = $(this).slider("value");
      console.log("Change point to ", red);
      var percentage = red / 100;
      //instance.options.videoPlayPercentage = percentage;
      instance._setProgressBar(percentage);
      rvaClient.sendStreamingPercentToRVA(percentage);
    }
    
    var prograssBar = $('#points').slider({
      orientation: "horizontal",
      range: "min",
      min: 0,
      max: 100,
      value: 0,
      stop: moveTime
    }).slider("disable");

    checkResize();
  },
  setRvaClient: function setRvaClient(rva){
    //console.log('setRvaClient=', rva);
    rvaClient = rva;
  },
  playing: function playing(message, data){
    //console.log('playing:', message, data);
    var instance = this;
    if (instance.options.isPlaying) {
      instance._enablePlayback();
      $("#video_play").addClass("pause");
      instance._enableFileName(message);
      instance.options.videoTotalSeconds = data.total;  // set video total time;
      instance.options.videoPlayPercentage = data.percentage;
      instance._setProgressBar(data.percentage);
      $("#points").removeClass("active");
      (instance.options.videoPlayPercentage >= 1)? instance.stopPlaying() : ''; // stop playing when 100%
    }
  },
  stopPlaying: function stopPlaying(){
    console.log('stopPlaying...');
    $("#video_stop").addClass("active");
    this.options.isPlaying = false;
    this._enableLoading();
    this._disablePlayback();
    console.log('call rvaClient.stopYouTubeVideo();');
    rvaClient.stopYouTubeVideo();
  },
  stopped: function stopped(){
    console.log('stopped...');
    this.options.isPlaying = false;
    this._disableFileName();
    this._disablePlayback();
    this._enableSendVideo();
  },
  errorHandler: function errorHandler(message){
    console.log('errorHandler...', message);
    var instance = this;
    instance.options.isPlaying = false;
    instance._disableFileName();
    instance._disablePlayback();
    $("#errorMsg").text(message);
    $("#video_path").focus();
    $(".path").addClass("error");
  },
  removeErrorMsg: function removeErrorMsg(){
    $("#errorMsg").text('');
    $(".path").removeClass("error");
  },
  disableProgressBar: function disableProgressBar(){
    var instance = this;
    instance.options.videoPlayPercentage = 0;
    instance.options.videoTotalSeconds = 0;
    //instance._setProgressBar(0);
    $("#points").slider("value", 0).slider("disable");
    $("#video_points").off().removeClass();
    $('#nowTime').val(formatSecond(0));
    $('#totalTime').val(formatSecond(0));
  },
  _enableVideoPath: function _enableVideoPath(){
    var instance = this;

    $("#video_path").off().change(function() {
      // enable play button
      instance.removeErrorMsg();
      ($(this).val().trim()!="")? instance._enableSendVideo() : instance._disablePlayback();
    })
    .bind('input propertychange', function() {
      instance.removeErrorMsg();
      ($(this).val().trim()!="")? instance._enableSendVideo() : instance._disablePlayback();
    });
    
    instance._supportPassword('#video_path');    

  },
  _supportPassword: function _supportPassword(obj) {
    
    //var pasteSupported = false;
    var pasteSupported = document.queryCommandSupported('paste'); // It works!
    if(pasteSupported) {
      // Create an empty menu
      var menu = new gui.Menu();
      // Add Paste item
      menu.append(new gui.MenuItem({
        label: 'Cut',
        click: function() {
          console.log('Cut is clicked',this);
          document.execCommand('cut');
        }
      }));
      menu.append(new gui.MenuItem({
        label: 'Copy',
        click: function() {
          console.log('Copy is clicked',this);
          document.execCommand('copy');
        }
      }));
      menu.append(new gui.MenuItem({
        label: 'Paste',
        click: function() {
          console.log('Paste is clicked',this);
          document.execCommand('paste');
        }
      }));
      menu.append(new gui.MenuItem({
        label: 'Delete',
        click: function() {
          console.log('Delete is clicked',this);
          document.execCommand('delete');
        }
      }));
      menu.append(new gui.MenuItem({ type: 'separator' }));
      menu.append(new gui.MenuItem({
        label: 'Select all',
        click: function() {
          console.log('selectAll is clicked',this);
          document.execCommand('selectAll');
        }
      }));

      function isTextSelected(input){
        var startPos = input.selectionStart;
        var endPos = input.selectionEnd;
        var doc = document.selection;
        if(doc && doc.createRange().text.length != 0){
          return true;
        }
        else if (!doc && input.value.substring(startPos,endPos).length != 0){
          return true;
        }
        return false;
      }
      
      $(obj).mousedown(function(ev){ 
        $(this).focus();
        if( ev.button == 2 ) {
          for (var i = 0; i < menu.items.length; ++i) {
            menu.items[i].enabled = false;
          }
          if ($(obj).val()==''){
            console.log('No input.');
            menu.items[2].enabled = true;
          }
          else {
            menu.items[2].enabled = true;
            menu.items[5].enabled = true;
            if (isTextSelected(this)){
              menu.items[0].enabled = true;
              menu.items[1].enabled = true;
              menu.items[3].enabled = true;
            }
          }
          menu.popup(ev.clientX, ev.clientY);
          return false; 
        } 
        return true; 
      }); 
    }
    
  },
  _enableSendVideo: function _enableSendVideo() {
    var instance = this;
    $("#video_play").addClass("enable").off().click(function() {
      //console.log('send video...');
      var pathString = $("#video_path").val();
      if (pathString == "") {
        $("#get_file").trigger("click");
        return false;
      }
      instance.options.isPlaying = true;
      $(".path").removeClass("error");
      instance._enableLoading();
      instance._disablePlayback();

      // Send youtube link
      if(_.includes(pathString, 'www.youtube.com')){
        console.log("TODO: Send youtube path", pathString);
        console.log('this is a youtube link');
        var title = $.t('video.title');
        $('.span2').text(title);
        $('title').text(title + ' | Novo');
        rvaClient.playYouTubeVideo(pathString);
        instance.options.playFile = null;
        return;
      }
      
      // Send weburl link
      if(pathString.startsWith('https://') || pathString.startsWith('http://')){
        console.log("TODO: Send web url path", pathString);
        console.log('this is a web link');
        var title = $.t('video.title');
        $('.span2').text(title);
        $('title').text(title + ' | Novo');
        rvaClient.setLocalStreamingType('web');
        rvaClient.playLocalVideo(pathString);
        instance.options.playFile = null;
        return;
      }
      
      //console.log(instance.options.playFile, $('#fileDialog'))      
      if (instance.options.playFile != null) {
        console.log("TODO: Send video/audio", pathString);
        console.log('this is a local video');
        rvaClient.setLocalStreamingType('local');
        rvaClient.playLocalVideo(pathString);
        return;
      }

      // Send video
      var files = $('#fileDialog')[0].files;
      if (files.length > 0) {
        var file = files[0];
        console.log("TODO: Send video/audio", file.path);
        console.log('this is a local video');
        rvaClient.setLocalStreamingType('local');
        rvaClient.playLocalVideo(pathString);
        return;
      }
      
      // No file send
      console.error("No file sent......");
      instance.errorHandler($.t('video.wrongFileType'));

    });
  },
  _enableLoading: function _enableLoading() {
    console.log("_enableLoading...");
    this._hideBlock();
    $("#loading").show();
  },
  _enablePlayButton: function _enablePlayButton() {
    $("#video_play").addClass("enable").off().click(function() {
      //console.log("Click play/pause...", $(this).hasClass("pause"));
      if ($(this).hasClass("pause")) {
        $(this).removeClass("pause");
        // TODO: Pause when playing video
        console.log("TODO: pauseYouTubeVideo()");
        rvaClient.pauseYouTubeVideo();
      }
      else {
        $(this).addClass("pause");
        // TODO: Continue playing video
        console.log("TODO: resumeYouTubeVideo()");
        rvaClient.resumeYouTubeVideo();
      }
    });
  },
  _enablePlayback: function _enablePlayback() {
    var instance = this;

    $("#playback > span").off().addClass("enable");
    this._enablePlayButton();

    $("#video_stop").off().click(function() {
      console.log("Click #video_stop...");
      if ($(this).hasClass("active")) {
        return false;
      }
      instance.stopPlaying();
    });

    $("#points").slider("value", 0).slider("enable");

  },
  _disablePlayback: function _disablePlayback() {
    $("#playback > span").off().removeClass();
    var instance = this;
    instance.disableProgressBar();
  },
  _setProgressBar: function _setProgressBar(percentage){
    //console.log("_setProgressBar ... percentage=", percentage);
    var instance = this;
    var percent = (percentage) ? percentage : instance.options.videoPlayPercentage;
    percent = Math.max(0, percent);
    percent = Math.min(1, percent);
    //if (percentage) $("#points").slider("value", percent * 100);
    $("#points").slider("value", percent * 100);
    console.log("_setProgressBar...", percentage, percent);
    // Update Time
    var totalTime = instance.options.videoTotalSeconds;  // total Seconds
    var nowTime = Math.floor(totalTime * percent);
    $('#nowTime').val(formatSecond(nowTime));
    $('#totalTime').val(formatSecond(totalTime));
    //console.log("_setProgressBar ... percentage=", percentage, formatSecond(nowTime));
  },
  _enableFileName: function _enableFileName(title) {
    this._hideBlock();
    $(".file_name").show();
    $("#file_name").empty().append(title);
  },
  _disableFileName: function _disableFileName() {
    this._hideBlock();
    $(".path").show();
    $("#file_name").empty();
  },
  _hideBlock: function _hideBlock() {
    $(".content > div:not(#playback)").hide();
  },
  _destroy: function _destroy() {
    // events bound via _on are removed automatically
    // revert other modifications here
    // remove generated elements
    this.remove();
    //this.element.enableSelection();
  },
});


function formatSecond(secs) {         
  var hr = Math.floor(secs / 3600);
  var min = Math.floor((secs - (hr * 3600)) / 60).toString();
  var sec = parseInt( secs % 60).toString();
 
  if (min.length < 2 && hr > 0) { min = '0' + min; }
  if (sec.length < 2) { sec = '0' + sec; }
  hr = (hr > 0)?  hr+':' : '';
  return hr + min + ':' + sec;
}

//console.log('menu=', menu);
