var gui = require('nw.gui');
var win = gui.Window.get();
var mainWin;

function init(obj){
  console.log('init...', obj);
  mainWin = (obj)? obj : mainWin;
};

function getMainWindow() {
  console.log('getMainWindow...');
  return mainWin;
}

function setModerator() {
  console.log('setModerator...');
  $('#refreshStart').trigger('click');
}

function stopVoting() {
  console.log('stopVoting...');
  $('#votingStartFail').trigger('click');
}

$(document).ready(function() {
  
  $('#newFileDialog').change(function() {
    var fullPath = $(this).val();
    if (fullPath) {
      $('#newFileName').val(fullPath).trigger('click');
      $(this).val("");
    }
  });

  $('#openFileDialog').change(function() {
    var fullPath = $(this).val();
    //var filename = fullPath.replace(/^.*[\\\/]/, '');
    $('#openFileName').val(fullPath).trigger('click');
    $(this).val('');
  });
  
  $('#csvFileDialog').change(function() {
    var fullPath = $(this).val();
    if (fullPath) {
      $('#csvFileName').val(fullPath).trigger('click');
      $(this).val("");
    }    
  });
  
  $('#pdfFileDialog').change(function() {
    var fullPath = $(this).val();
    if (fullPath) {
      $('#pdfFileName').val(fullPath).trigger('click');
      $(this).val("");
    }    
  });
  
  console.info($('#page'));
  checkResize();
  
});

function checkResize() {
  //console.log('checkResize...');
  var w = $('#page').width(),
      h = $('#page').height();

  if (gui.App.manifest.window.frame) {
    w = w + 20;
    h = h + 80;
  }
  else {
    w = w + 4;
    h = h + 4;
  }
  win.resizeTo(w, h);
  
  var x = Math.round((screen.availWidth - w)/2),
      y = Math.round((screen.availHeight - h)/2);
  win.moveTo(x, y);
  win.show(true);
  
  //console.log('checkResize...', screen.availWidth, screen.availHeight, w, h, x, y);
  w = null;
  h = null;
  x = null;
  y = null;
}

win.setResizable(false);

document.body.addEventListener('dragover', function(e){
  e.preventDefault();
  e.stopPropagation();
}, false);
document.body.addEventListener('drop', function(e){
  e.preventDefault();
  e.stopPropagation();
}, false);