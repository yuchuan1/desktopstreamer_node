/** Utility **/
var fs = require("fs"),
    //PDF = require("pdfkit"),
    sql = window.SQL;

var mainWindow,
    userAnswer = {};

function getModerator() {
  var isHost = (mainWindow)? mainWindow.window.novo.novoconnect('instance').options.isHost : false;
  //console.debug('getModerator...', isHost);
  return isHost;
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
//var language = window.navigator.language; //'zh-TW';//en-US
//var language = getLanguage(); //'zh-TW';//en-US
var language = getParameterByName('setLng');
var opts = {
  lng: language,
  fallbackLng: 'en-US',
  debug: true,
  resGetPath: "../locales/__ns__-__lng__.json"
};
i18n.init(opts, function(t) {
  $(document).i18n();
  //console.log('i18n.init...', i18next.lng());
  console.log('i18n.init...', language);
});

function receiveAnswer(data) {
  //console.debug('receiveAnswer...', data);
  userAnswer = data;
  $('#receiveAnswer').trigger('click');
}

function Uint8ToString(u8a){
  var CHUNK_SZ = 0x8000;
  var c = [];
  for (var i=0; i < u8a.length; i+=CHUNK_SZ) {
    c.push(String.fromCharCode.apply(null, u8a.subarray(i, i+CHUNK_SZ)));
  }
  return c.join("");
}

function convertDataURIToBinary(dataURI) {
  var BASE64_MARKER = ';base64,';
  var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
  var base64 = dataURI.substring(base64Index);
  var raw = window.atob(base64);
  var rawLength = raw.length;
  var array = new Uint8Array(new ArrayBuffer(rawLength));
  for(i = 0; i < rawLength; i++) {
    array[i] = raw.charCodeAt(i);
  }
  return array;
}

// Return an Object sorted by it's Key
function sortObjectByKey(obj){
  var keys = [];
  var sorted_obj = {};

  for(var key in obj){
    if(obj.hasOwnProperty(key)){
      keys.push(key);
    }
  }

  // sort keys
  keys.sort();

  // create new array based on Sorted Keys
  $.each(keys, function(i, key){
    sorted_obj[key] = obj[key];
    sorted_obj[key].index = i;
    //console.log("sorted_obj[key]=", sorted_obj[key]);
  });

  return sorted_obj;
};

function convertNameToCompare(name) {
  return name.toUpperCase().replace(/\s/g,'');
}

var questionLibary = [
      {value: 1, "type": "voting.thumbsUpDown", "answers": [{value: 0, label: "voting.notSelected"}, {value: 3, label: "voting.thumbUp"}, {value: 4, label: "voting.thumbDown"}] },
      {value: 2, "type": "voting.TrueFalse", "answers": [{value: 0, label: "voting.notSelected"}, {value: 1, label: "voting.true"}, {value: 2, label: "voting.false"}] },
      {value: 3, "type": "voting.3Choices", "answers": [{value: 0, label: "voting.notSelected"}, {value: 5, label: "voting.A"}, {value: 6, label: "voting.B"}, {value: 7, label: "voting.C"}] },
      {value: 4, "type": "voting.4Choices", "answers": [{value: 0, label: "voting.notSelected"}, {value: 5, label: "voting.A"}, {value: 6, label: "voting.B"}, {value: 7, label: "voting.C"}, {value: 8, label: "voting.D"}] },
      {value: 5, "type": "voting.5Choices", "answers": [{value: 0, label: "voting.notSelected"}, {value: 5, label: "voting.A"}, {value: 6, label: "voting.B"}, {value: 7, label: "voting.C"}, {value: 8, label: "voting.D"}, {value: 9, label: "voting.E"}] },
      {value: 6, "type": "voting.openEnded", "answers": []},
    ],
    summaryLibary = {
      1: {"type": "voting.summaryThumbsUpDown", "answers": [{value: 0, label: "voting.notSelected"}, {value: 3, label: "voting.thumbUp"}, {value: 4, label: "voting.thumbDown"}] },
      2: {"type": "voting.summaryTrueFalse", "answers": [{value: 0, label: "voting.notSelected"}, {value: 1, label: "voting.true"}, {value: 2, label: "voting.false"}] },
      5: {"type": "voting.summaryMultiChoices", "answers": [{value: 0, label: "voting.notSelected"}, {value: 5, label: "voting.A"}, {value: 6, label: "voting.B"}, {value: 7, label: "voting.C"}, {value: 8, label: "voting.D"}, {value: 9, label: "voting.E"}] },
      6: {"type": "voting.summaryOpenEnded", "answers": []},
    };

/** Draw page */
var PieChart = React.createClass({displayName: "PieChart",
  render: function() {
    var percent = this.props.percent;
    //console.warn("percent=", percent);
    var percent2 = 1 - percent;
    
    var startAngle = -(2 * Math.PI * percent) * ((percent == 1)? .9999 : 1);
    //var startAngle = -(2 * Math.PI * percent);
    var cx = 75;
    var cy = 75;
    var r  = 70;
    var deg1 = 0;
    var deg2 = ((2 * Math.PI * percent2) * ((percent2 == 1)? .9999 : 1)) + deg1;
    var largeArcFlag  = (percent > 0.5)? 1 : 0;
    var largeArcFlag2 = (percent2 > 0.5)? 1 : 0;
    //console.warn("startAngle=", startAngle, "largeArcFlag=", largeArcFlag);
    
    var x0 = cx + r * Math.cos(startAngle);
    var y0 = cy - r * Math.sin(startAngle);
    var x1 = cx + r * Math.cos(deg1);
    var y1 = cy - r * Math.sin(deg1);
    var x2 = cx + r * Math.cos(deg2);
    var y2 = cy - r * Math.sin(deg2);
    
    var angle  = "M "+cx+","+cy+" L "+x0+","+y0+" A "+r+","+r+" 0 "+ largeArcFlag  +",0 "+x1+","+y1+" Z";
    var angle2 = "M "+cx+","+cy+" L "+x1+","+y1+" A "+r+","+r+" 0 "+ largeArcFlag2 +",0 "+x2+","+y2+" Z";
    
    return (React.createElement("svg", null, 
        React.createElement("path", {fill: this.props.fill, d: angle}), 
        React.createElement("path", {fill: "#58595B", d: angle2}), 
        React.createElement("circle", {cx: cx, cy: cy, r: "60", fill: "#FFF"})
      )
    );
  }
});

var PieChartSet = React.createClass({displayName: "PieChartSet",
  render: function() {
    var percent1 = this.props.set[0].percent;
    var percent2 = this.props.set[1].percent;
    var percent3 = this.props.set[2].percent;
    
    var startAngle = -(2 * Math.PI * percent1) * ((percent1 == 1)? .9999 : 1);
    var cx = 75;
    var cy = 75;
    var r  = 70;
    var deg1 = 0;
    var deg2 = (2 * Math.PI * percent2) + deg1;
    var deg3 = (2 * Math.PI * percent3) + deg2;
    
    var largeArcFlag1 = (percent1 > 0.5)? 1 : 0;
    var largeArcFlag2 = (percent2 > 0.5)? 1 : 0;
    var largeArcFlag3 = (percent3 > 0.5)? 1 : 0;
    /*
    console.warn("startAngle=", startAngle);
    console.warn("percent1=", percent1, "largeArcFlag1=", largeArcFlag1);
    console.warn("percent2=", percent2, "largeArcFlag2=", largeArcFlag2);
    console.warn("percent3=", percent3, "largeArcFlag3=", largeArcFlag3);
    */
    var x0 = cx + r * Math.cos(startAngle);
    var y0 = cy - r * Math.sin(startAngle);
    var x1 = cx + r * Math.cos(deg1);
    var y1 = cy - r * Math.sin(deg1);
    var x2 = cx + r * Math.cos(deg2);
    var y2 = cy - r * Math.sin(deg2);
    var x3 = cx + r * Math.cos(deg3);
    var y3 = cy - r * Math.sin(deg3);
    
    var angle1 = "M "+cx+","+cy+" L "+x0+","+y0+" A "+r+","+r+" 0 "+ largeArcFlag1 +",0 "+x1+","+y1+" Z";
    var angle2 = "M "+cx+","+cy+" L "+x1+","+y1+" A "+r+","+r+" 0 "+ largeArcFlag2 +",0 "+x2+","+y2+" Z";
    var angle3 = "M "+cx+","+cy+" L "+x2+","+y2+" A "+r+","+r+" 0 "+ largeArcFlag3 +",0 "+x3+","+y3+" Z";
    
    return (
      React.createElement("svg", null, 
        React.createElement("path", {fill: this.props.set[0].fill, d: angle1}), 
        React.createElement("path", {fill: this.props.set[1].fill, d: angle2}), 
        React.createElement("path", {fill: this.props.set[2].fill, d: angle3}), 
        React.createElement("circle", {cx: cx, cy: cy, r: "60", fill: "#FFF"})
      )
    );
  }
});

var Summary = React.createClass({displayName: "Summary",
  getInitialState: function() {
    return {
      questions: this.props.data.questions,
      answers: this.props.data.answers,
      focusColumn: "-1"
    };
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({
      questions: nextProps.data.questions,
      answers: nextProps.data.answers,
      focusColumn: "-1"
    });
  },
  handleClick: function(i) {
    //console.log("i=", i, this.state.focusColumn);
    //console.log(e.target, e.target["data-col"], this.state.focusColumn);
    this.setState({focusColumn: i});
  },
  render: function() {
    var questions = this.state.questions;
    var answers = this.state.answers;
    var focusColumn = this.state.focusColumn;
    //console.log("Summary...", questions, answers);
    var getType = function(val){
      return (val==3 || val==4)? 5 : val;
    }
    var qType = {}, qq = {}, totalType = 0, hasAnswer = 0;

    questions.map(function(question, i){
      var flag = getType(question.type);
      //console.log("question=", flag, summaryLibary[flag], opt.qid, opt);
      qq[question.qid] = question;
      if (qType.hasOwnProperty(flag)) {
        qType[flag]["total"]++;
      } else {
        qType[flag] = {"total": 1};
        totalType++;
      }
      // question with answer
      if (question.type != 1 && question.type != 6 && question.correct > 0){
        //console.warn("question=", question.type, question.correct);
        hasAnswer++;
      }
    }, this);
    qType = sortObjectByKey(qType);

    var instance = this, n = 0;
    var row1 = $.map(qType, function(value, key) {
      n++;
      //console.log(n + ".", key, value);
      return React.createElement("span", {className: (focusColumn==n)? "focused" : "", onClick: instance.handleClick.bind(null, n)}, $.t(summaryLibary[key].type));
    });
/*    
    console.log("qType=", qType);
    console.log("qq=", qq);
    console.log("answers.length=", answers.length);
*/
    var answered = 0, correct = 0, wrong = 0,
        rows = {}, users = [];
/*
    answers.map(function(answer, i){
      console.log("answer.qid=", answer.qid, answer.answer);
    });
*/
    answers.map(function(answer, i){
      var question = qq[answer.qid],
          questionType = getType(question.type);
      
      //(answer.imageBLOB != null)? console.log(i, answer.imageBLOB) : null;
      //console.log("answer.qid=", answer.qid);
      
      (answer.answer > 0 || answer.imageBLOB != null)? answered++ : null;
      //console.log("question.type=", question.type, "question.correct=", question.correct, "answer.answer=", answer.answer);
      
      // Count answered
      if (questionType != 1 && questionType != 6 && question.correct > 0 && answer.answer > 0){
        // Count answered correctly
        (question.correct == answer.answer)? correct++ : wrong++;
        //console.warn("question.type=", question.type, "question.correct=", question.correct, "answer.answer=", answer.answer);
      }

      var row = [];
      if (rows.hasOwnProperty(answer.senderName)) {
        row = rows[answer.senderName];
      } else {
        users.push(answer.senderName);
        row = [], i = 0;
        while (++i <= totalType) row.push(0);
      }
      row[qType[questionType].index] = ((answer.answer > 0 || answer.imageBLOB != null)? 1 : 0);
      rows[answer.senderName] = row;
    });
    //console.warn("rows=", rows);
    //console.warn("answered=", answered, "correct=", correct);

    var sortedType = [];
    $.each(qType, function(key, obj) {
      sortedType.push(obj.total);
    });
    //console.warn("sortedType=", sortedType);

    var members = users.map(function(user, i){
      var cell  = rows[user].map(function(opt, j){
                    return (React.createElement("span", {className: (focusColumn == (j+1))? "focused" : ""}, opt, " / ", sortedType[j]));
                  });
      return (React.createElement("div", {className: "td"}, 
                React.createElement("span", {className: (focusColumn == "0")? "focused" : ""}, user), 
                cell
              ));
    });

    var total  = questions.length * members.length;
    var total2 = hasAnswer * members.length;

    var answeredPercent = (total > 0) ? Math.round(answered / total * 100) : 0;
    var correctPercent = (total2 > 0)? Math.round(correct / total2 * 100) : 0;
    var wrongPercent = (total2 > 0)? Math.round(wrong / total2 * 100) : 0;
    var noAnswerPercent = Math.round(100 - correctPercent - wrongPercent);

    //console.log(answeredPercent, "--> answered / total = ", answered, total);
    //console.log(correctPercent, "--> correct / total2 = ", correct, total2);

    var set = [{fill: "#8BC53F", percent: correctPercent/100},
               {fill: "#58595B", percent: noAnswerPercent/100},
               {fill: "#F16522", percent: wrongPercent/100}];

    var h2 = $('#main-content').height() - 185;
    var maxHeight = {maxHeight: h2 + 'px'};
    //console.log('[CheckResult.render] this.props.onMax=', this.props.onMax, $('#main-content').height(), "height=", h2, $('.summaryTable'));
    
    return (
      React.createElement("div", {className: "summary"}, 
        React.createElement("div", null, 
          React.createElement("div", null, 
            React.createElement("div", {className: "chart"}, 
              React.createElement(PieChart, {fill: "#26A9E0", percent: answeredPercent/100}), 
              React.createElement("div", {className: "pieText"}, 
                React.createElement("span", null, $.t('voting.participation')), React.createElement("br", null), 
                React.createElement("span", {className: "number"}, answeredPercent), 
                React.createElement("span", {className: "percent"}, "%")
              )
            )
          ), 
          React.createElement("div", null, 
            React.createElement("div", {className: "chart correct"}, 
              React.createElement(PieChartSet, {set: set}), 
              React.createElement("div", {className: "pieText"}, 
                React.createElement("span", null, $.t('voting.correctRatio')), React.createElement("br", null), 
                React.createElement("span", {className: "number"}, correctPercent), 
                React.createElement("span", {className: "percent"}, "%")
              )
            ), 
            React.createElement("div", {className: "textInfo"}, 
              React.createElement("div", {className: "wrong"}, React.createElement("span", {className: "square"}, "■"), "  ", $.t('voting.wrongStut'), " ", (total2 == 0) ? 0 : wrongPercent, "%"), 
              React.createElement("div", {className: "notSubmitted"}, React.createElement("span", {className: "square"}, "■"), " ", $.t('voting.unsubmitStus'), " ", (total == 0) ? 0 : noAnswerPercent, "%")
            )
          )
        ), 
        React.createElement("div", {className: "summaryTable", style: maxHeight}, 
          React.createElement("div", {className: "table"}, 
            React.createElement("div", {className: "th"}, 
              React.createElement("span", {className: (focusColumn==0)? "focused" : "", onClick: this.handleClick.bind(null, 0)}, $.t('voting.name')), 
              row1
            ), 
            members
          )
        )
      )
    );
  }
});

var Taskbar = React.createClass({displayName: "Taskbar",
  handleMinimize: function(event) {
    win.minimize();
  },
  handleMaximize: function(event) {
    win.maximize();
  },
  handleRestore: function(event) {
    win.restore();
  },
  handleClose: function(event) {
    //window.close();
    this.props.onClose();
  },
  render: function() {
    var isWin = /^win/.test(process.platform);
    return (
      React.createElement("div", {className: "taskbar"}, 
        React.createElement("span", {className: "icons clearfix"}, 
          React.createElement("span", {onClick: this.handleMinimize, className: "icon-minimize"}), 
          React.createElement("span", {onClick: this.handleMaximize, className: "icon-fullscreen" + ((isWin)? "" : " hide")}), 
          React.createElement("span", {onClick: this.handleRestore, className: "icon-restore hide"}), 
          React.createElement("span", {onClick: this.handleClose, className: "icon-close"})
        )
      )
    );
  }
});

var Toolbar = React.createClass({displayName: "Toolbar",
  handleCreate: function(event) {
    this.props.data.onCreate();
  },
  handleOpenFile: function(event) {
    this.props.data.onOpen();
  },
  handleSave: function(event) {
    this.props.data.onSave();
  },
  handleClearHistory: function(event) {
    var p = this;
    $('#dialog-message em').text($.t('voting.confirmClearHistory'));
    $('#dialog-message p').css("white-space", "normal")
    $("#dialog-message").dialog({
      resizable: false,
      draggable: false,
      modal: true,
      width: 550,
      dialogClass: "titlbar-only-close",
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: $.t('button.cancel'),
          click: function() {
            $(this).dialog("close");
          }
        },
        {
          text: $.t('button.ok'),
          click: function() {
            p.props.data.onClearHistory();
            $(this).dialog("close");
          }
        }
      ]
    });
  },
  handleExportPDF: function(event) {
    var p = this;
    p.props.data.onExportPDF();
  },
  handleExportCSV: function(event) {
    var p = this;
    p.props.data.onExportCSV();
  },
  render: function() {
    var toggle = this.props.data.enableAllBtns;
    var filename = this.props.currentFile.replace(/^.*[\\\/]/, '');
    var onAirQnum = (this.props.data.onAirIndex > -1)? 'Q'+this.props.data.onAirIndex : '';
    var onAirCircle = (this.props.data.onAirIndex > -1)? (React.createElement("span", {className: "onAirCircle"})) : null;
    var dropdown = (React.createElement("ul", {className: "f-dropdown"}, 
                      React.createElement("li", null, React.createElement("a", {href: "#", onClick: this.handleExportPDF}, $.t("voting.exportPDF"))), 
                      React.createElement("li", null, React.createElement("a", {href: "#", onClick: this.handleExportCSV}, $.t("voting.exportCSV")))
                    ));
    
    return (
      React.createElement("div", {className: "toolbar"}, 
        React.createElement("div", {title: $.t("voting.title"), className: "logo"}, 
          React.createElement("span", {"data-i18n": "voting.title"})
        ), 
        React.createElement("div", {title: $.t("voting.new"), className: "voting-tool-create " + ((toggle[0])? "" : "disable"), onClick: (toggle[0])? this.handleCreate : null}, 
          React.createElement("span", {"data-i18n": "voting.new"})
        ), 
        React.createElement("div", {title: $.t("voting.open"), className: "voting-tool-open " + ((toggle[1])? "" : "disable"), onClick: (toggle[1])? this.handleOpenFile : null}, 
          React.createElement("span", {"data-i18n": "voting.open"})
        ), 
        React.createElement("div", {title: $.t("voting.save"), className: "voting-tool-save " + ((toggle[2])? "" : "disable"), onClick: (toggle[2])? this.handleSave : null}, 
          React.createElement("span", {"data-i18n": "voting.save"})
        ), 
        React.createElement("div", {title: $.t("voting.clearHistory"), className: "voting-tool-clear " + ((toggle[3])? "" : "disable"), onClick: (toggle[3])? this.handleClearHistory : null, id: "clearHistory"}, 
          React.createElement("span", {"data-i18n": "voting.clearHistory"})
        ), 
        React.createElement("div", {title: $.t("voting.exportCSV"), className: "voting-tool-export " + ((toggle[4])? "" : "disable"), onClick: (toggle[4])? this.handleExportCSV : null, id: "export"}, 
          React.createElement("span", {"data-i18n": "voting.exportCSV"})
        ), 
        React.createElement("div", {title: filename, className: "filename"}, 
          React.createElement("div", null, filename), 
          React.createElement("div", null, onAirQnum, " ", onAirCircle)
        )
      )
    );
  }
});

var SideLink = React.createClass({displayName: "SideLink",
  handleClick: function handleClick(e) {
    e.preventDefault();
    //console.error("SideLink.handleClick...", $(this.getDOMNode()).index());
    //console.log("SideLink.handleClick...", e.target, this.props.active);
    if (this.props.active == 'active') return;
    this.props.onChange($(this.getDOMNode()).index()+1);
  },
  render: function() {
    var question = this.props.question;
        //<span className="subject"> ({question.qid}) {question.subject.substring(0, 20)}</span>
    return (
      React.createElement("li", {onClick: this.handleClick, className: this.props.active}, 
        React.createElement("span", {className: (question.onAir)? "on-air" : ""}, "Q", question.num, "."), 
        React.createElement("span", {className: "subject"}, " ", question.subject)
      )
    );
  }
});

var Side = React.createClass({displayName: "Side",
  getInitialState: function() {
    return {
      questions: this.props.data.questions,
      idx: this.props.data.idx
    };
  },
  switchQuestion: function(idx) {
    this.props.data.onSwitch(idx);
  },
  handleClickSummary: function() {
    this.props.data.onSummary();
  },
  handleAddQuestion: function() {
    this.props.data.onAdd();
    this.scrollElement();
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({
      questions: nextProps.data.questions,
      idx: nextProps.data.idx
    });
  },
  scrollElement: function() {
    //wait for a paint to do scrolly stuff
    window.requestAnimationFrame(function() {
      var node = $('.side li.active');
      var ul = $(".side ul");
      var myPosX = $(node).position().top - $(ul).position().top;
      $(ul).scrollTop(myPosX);
    });
  },
  componentDidUpdate: function() {
    //console.log('[Side.componentDidUpdate]...', this.state);
  },
  render: function() {
    var questions = this.state.questions,
        idx = this.state.idx;
    var liList = questions.map(function(question, i){
                    question.num = i+1;
                    //console.log('Side.liList...', idx, i, question.num);
                    return React.createElement(SideLink, {question: question, onChange: this.switchQuestion, active: (idx == question.num)? "active" : ""});
                 }, this);

    var summaryLi = (React.createElement("div", {id: "summary", onClick: this.handleClickSummary, className: ((idx == 0)? "active" : "") + ((this.props.data.mode=='add')? " disabled" : " ")}, 
                       React.createElement("span", {className: "voting-tool-summary", "data-i18n": "voting.summary"}, $.t('voting.summary'))
                     ));
    
    var addItem = (this.props.data.onAirIndex > -1 || this.props.data.mode=='add')?
                    null : 
                    (React.createElement("div", {id: "add-item", className: "black-btn", onClick: this.handleAddQuestion}, 
                       "＋", React.createElement("span", {"data-i18n": "voting.addQuestion"}, $.t('voting.addQuestion'))
                     ));
    
    var h2 = (this.props.data.onMax)? ($('#main-content').height() - 60) : 510;
    var maxHeight = {maxHeight: h2 + 'px'};
    //console.log('[Side.render] this.props.data.onMax=', this.props.data.onMax, maxHeight);
  
    return (
      React.createElement("div", {className: "side"}, 
        summaryLi, 
        React.createElement("ul", {style: maxHeight}, 
          liList
        ), 
        addItem
      )
    );
  }
});

var QuestionType = React.createClass({displayName: "QuestionType",
  getInitialState: function() {
    return {type: this.props.type};
  },
  componentWillReceiveProps: function(nextProps) {
    //console.log("QuestionType.nextProps--", nextProps);
    this.setState({type: nextProps.type});
  },
  handleChange: function(event) {
    this.setState({type: event.target.value});
    this.props.onChange({type: event.target.value, answer: "0"});
  },
  render: function() {
    var value = this.state.type;
    var options = questionLibary.map(function(opt, i){
                    //console.log(opt, i, value);
                    return React.createElement("option", {value: opt.value, selected: value == opt.value, "data-i18n": opt.type}, $.t(opt.type));
                  }, this);
    return (
      React.createElement("select", {className: "black-btn type", multiple: false, value: value, defaultValue: value, onChange: this.handleChange}, 
        options
      )
    );    
  }
});

var QuestionProperty = React.createClass({displayName: "QuestionProperty",
  getInitialState: function() {
    return {
      type: this.props.type,
      answer: this.props.answer
    };
  },
  componentWillReceiveProps: function(nextProps) {
    //console.debug("QuestionProperty.nextProps--", nextProps);
    this.setState({type: nextProps.type, answer: nextProps.answer});
  },
  handleChange: function(property) {
    //console.log("QuestionProperty.handleChange...", property);
    this.setState(property);
    this.props.onChange(property);
  },
  handleUpdateAnswer: function(e) {
    var property = {type: this.props.type, answer: e.target.value};
    //console.log("QuestionProperty.handleUpdateAnswer...", property, e.target.value);
    this.setState(property);
    this.props.onChange(property);
  },
  render: function() {
    var type = this.state.type;
    var answer = this.state.answer;
    var answers = questionLibary[type-1].answers.map(function(opt, i){
                    //console.log(opt, i, value);
                    return React.createElement("option", {value: opt.value, selected: answer == opt.value, "data-i18n": opt.label}, $.t(opt.label));
                  }, this);
    //console.debug("QuestionProperty... type=", type);
    return (
      React.createElement("div", {className: "property"}, 
        React.createElement("div", null, 
          React.createElement(QuestionType, {type: type, onChange: this.handleChange}), 
          React.createElement("span", {className: "arrowDown"})
        ), 
        React.createElement("div", {className: (type == 1 || type == 6)? "hide" : ""}, 
          React.createElement("select", {className: "black-btn answer", onChange: this.handleUpdateAnswer, multiple: false, defaultValue: answer}, 
            answers
          ), 
          React.createElement("span", {className: "arrowDown"})
        )
      )
    );
  }
});

var Question = React.createClass({displayName: "Question",
  getInitialState: function() {
    return {question: this.props.question};
  },
  handleUpdateContent: function(e) {
    //console.log("[Question.handleUpdateContent]", e.target, $(e.target).html());
    //console.log("[Question.handleUpdateContent1]", $(e.target).text());
    var question = this.state.question;
    question.richSubject = $(e.target).html();
    
    var subject = $('<div>').append(question.richSubject);
    //console.log("[Question.handleUpdateContent]", $('style',subject));
    $('title',subject).remove();
    $('meta',subject).remove();
    $('style',subject).remove();
    $('script',subject).remove();
    $('noscript',subject).remove();
    //console.log("[Question.handleUpdateContent2]", $(subject).text());    
    question.subject = $(subject).text();
    
    question.title = $.t('voting.newTitle') + question.num;
    this.props.onUpdate(question);
    //console.log("Question.handleChangeContent...", question);
  },
  handleChangeProperty: function(propery) {
    var question = this.state.question;
    question.type = propery.type;
    question.correct = propery.answer;
    //console.log("Question.handleChangeProperty1...", question);
    //this.setState({question: question});
    this.props.onUpdate(question);
    //console.log("Question.handleChangeProperty2...", question);
  },
  handleFile: function(e) {
    var q = this;
    var question = this.state.question;
    var file = e.target.files[0];
    //console.debug("Question.handleFile", file, file.type);
    if (file) {
      getDownscaleImageBase64String(file, function(result) {
        question.image = "data:"+ file.type +";base64,"+result;
        q.props.onUpdate(question);
      });
    } else {
      question.image = null;
      q.props.onUpdate(question);
    }
    e.target.value = null;
    //console.log("Question.handleFile...", question.image);
  },
  handleImageClick: function(e) {
    e.preventDefault();
    $('#imageFile').trigger('click');
    //console.log("Question.handleImageClick...");
  },
  handleImageRemove: function(e) {
    e.preventDefault();
    var question = this.state.question;
    $("#imagefile").val("");
    question.image = null;
    this.props.onUpdate(question);
    //console.log("Question.handleImageRemove...", question);
  },
  componentWillReceiveProps: function(nextProps) {
    //console.log("Question.componentWillReceiveProps.nextProps--", nextProps.question.image);
    this.setState({question: nextProps.question});
  },
  componentDidMount: function() {
    //console.log("Question.componentDidMount...");
    setModerator(getModerator());
  },
  render: function() {
    var question = this.state.question;
    //console.debug("[Question] question=", question);
    //console.log("[Question] question.image=", question.image);
    
    var previewImage = (question.image == null || question.image == "")? (
          React.createElement("div", {id: "previewImage", className: "image", onClick: this.handleImageClick}, 
            $.t('voting.enterImage')
          )
        ) : (
          React.createElement("div", {id: "previewImage", className: "image active"}, 
            React.createElement("div", {className: "imageTool"}, 
              React.createElement("span", {className: "changeImage", onClick: this.handleImageClick, "data-i18n": "button.change"}, $.t('button.change')), 
              React.createElement("span", {className: "removeImage", onClick: this.handleImageRemove, "data-i18n": "button.remove"}, $.t('button.remove'))
            ), 
            React.createElement("img", {src: question.image})
          ));
    
    var h2 = $('#main-content').height() - 337;
    var height = {height: h2 + 'px'};
    //console.log('[Question.render] this.props.onMax=', this.props.onMax, $('#main-content').height(), "height=", h2);

    return (
      React.createElement("div", {className: "question"}, 
        React.createElement("div", {className: "subject"}, 
          React.createElement("span", {"data-i18n": "voting.question"}, $.t('voting.question')), " ", question.num
        ), 
        React.createElement("div", null, 
          React.createElement("div", {onBlur: this.handleUpdateContent, id: "richSubject", className: "content", style: height, contentEditable: "true", "data-i18n": "[placeholder]voting.enterText", dangerouslySetInnerHTML: {__html: question.richSubject}})
        ), 
        previewImage, 
        React.createElement("input", {id: "imageFile", onChange: this.handleFile, type: "file", accept: ".png,.jpg,.jpeg;image/*", className: "hide"}), 
        React.createElement(QuestionProperty, {type: question.type, answer: question.correct, onChange: this.handleChangeProperty})
      )
    );
  }
});
  
var PreviewQuestion = React.createClass({displayName: "PreviewQuestion",
  handleClickImage: function() {
    var question = this.props.question;
    //console.log("[PreviewQuestion.handleClickImage]...", this);
    $('#dialog-preview').empty().append($('<img>').attr('src',question.image).addClass('only-img'));
    $("#dialog-preview").dialog({
      autoOpen: true,
      modal: true,
      resizable: false,
      draggable: false,
      width: '90%',
      dialogClass: "titlbar-only-close no-buttonpane",
      show: {
        effect: "blind",
        duration: 300
      },
      close: function(event, ui) {
        $(this).dialog('destroy');
      }
    });
  },
  render: function() {
    var question = this.props.question;
    var previewImage = (question.image)? React.createElement("img", {src: question.image, className: "previewImage", onClick: this.handleClickImage}) : null;
    var qType = $.t(questionLibary[question.type-1].type);

    var h2 = $('#main-content').height() - 90;
    var maxHeight = {height: h2 + 'px'};
    //console.log('[PreviewQuestion.render] this.props.onMax=', this.props.onMax, "previewContent=", maxHeight);

    return (
      React.createElement("div", {className: "question preview"}, 
        React.createElement("div", {className: "subject"}, 
          React.createElement("span", {"data-i18n": "voting.question"}, $.t('voting.question')), " ", question.num, 
          React.createElement("span", {className: "small"}, "( ", React.createElement("span", {"data-i18n": questionLibary[question.type-1].type}, qType), " )")
        ), 
        React.createElement("div", {className: "previewContent", style: maxHeight}, 
          React.createElement("div", {dangerouslySetInnerHTML: {__html: question.richSubject}}), 
          previewImage
        )
      )
    );
  }
});

var AnswerDetail= React.createClass({displayName: "AnswerDetail",
  getInitialState: function() {
    return {
      question: this.props.question,
      focusColumn: -1
    };
  },
  componentWillReceiveProps: function(nextProps) {
    var col = -1;
    if (nextProps.question.correctOpen){
      var qTypes = questionLibary[nextProps.question.type-1].answers;
      //console.log('[AnswerDetail.componentWillReceiveProps] qTypes=', qTypes, nextProps.question);
      qTypes.map(function(opt, i){
        if(opt.value == nextProps.question.correct){
          col = i+1;
        }
      });
      //console.log('[AnswerDetail.componentWillReceiveProps] col=', col);
    }
    
    this.setState({
      question: nextProps.question,
      focusColumn: col
    });
  },
  handleClickColumn: function(i) {
    //console.log("i=", i, this.state.focusColumn);
    //console.log(e.target, e.target["data-col"], this.state.focusColumn);
    this.setState({focusColumn: i});
  },
  render: function() {
    var question = this.state.question;
    var answers = this.props.answers;
    var focusColumn = this.state.focusColumn;
    var hideAnswer = this.props.hideAnswer;
    //console.log('[AnswerDetail] answers=', answers);
    var qType = questionLibary[question.type-1].answers;
    var total = [0], idx = 1;
    var row1  = qType.map(function(opt, i){
                  total.push(0);
                  answers.map(function(answer, j){
                    var answerValue = (answer.answer == null)? 0 : answer.answer;
                    if(opt.value == answerValue){
                      total[0]++;
                      total[i+1]++;
                      //console.log(i, "opt.value=", opt.value, "answer.answer=", answer.answer, total);
                    }
                  }, opt);
                  if(opt.value != 0){
                    idx++;
                    return (React.createElement("span", {className: "answer", onClick: this.handleClickColumn.bind(null, idx)}, $.t(opt.label)));
                  }
                }, this);

    var row2  = total.map(function(opt, i){
                  //console.log("row2=", i, this.state.hideAnswer);
                  return (React.createElement("span", {className: ((i == 0)? "senderName" : "") + " " + ((focusColumn == i)? "focused" : "") + " " + ((hideAnswer === true && i > 1)? "hide" : "")}, (i==0)? $.t('info.total') : "", opt));
                });

    var row3  = answers.map(function(obj, j){
                  //console.log("row3=", j, obj.answer);
                  var output = qType.map(function(opt, i){
                    //console.log("qType=", i, opt.value, obj.answer);
                    //{(opt.value==0)? status : record} {(opt.value==obj.answer)? 'on' : 'off'}
                    var answer1 = (obj.answer == null)? 0 : obj.answer;
                    var a = (opt.value == 0)? 'answered' : 'record',
                        b = (opt.value == answer1)? 'on' : 'off',
                        c = (hideAnswer === true && i > 0)? 'hide' : '';
                    return React.createElement("span", {className: a + " " + b + " " + c + " " + ((focusColumn==(i+1))? "focused" : "")});
                  }, obj);
                  return (React.createElement("div", {className: "td"}, 
                            React.createElement("span", {className: "senderName " + ((focusColumn==0)? "focused" : "")}, obj.senderName), 
                            output
                          ));
                });

    var h2 = $('#main-content').height() - 283;
    var maxHeight = {maxHeight: h2 + 'px'};
    //console.log('[AnswerDetail.render] this.props.onMax=', this.props.onMax, $('#main-content').height(), "height=", h2, $('.clickView'));
    
    return (
      React.createElement("div", {className: "clickView", style: maxHeight}, 
        React.createElement("div", {className: "table"}, 
          React.createElement("div", {className: "th"}, 
            React.createElement("span", {onClick: this.handleClickColumn.bind(null, 0)}, $.t('voting.name')), 
            React.createElement("span", {onClick: this.handleClickColumn.bind(null, 1)}, $.t('voting.status')), 
            row1
          ), 
          React.createElement("div", {className: "td total"}, 
            row2
          ), 
          row3
        )
      )
    );
  }
});

var ResultStat = React.createClass({displayName: "ResultStat",
  getInitialState: function() {
    return {
      question: this.props.question
    };
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({
      question: nextProps.question
    });
  },
  handlePublishAnswer: function() {
    //console.log("[ResultStat.handlePublishAnswer]", question.qid, question.correctOpen, question.correct, question);
    if (this.state.question.correctOpen == true) {
      return;
    }
    
    var question = this.state.question;
    if (question.correct == "0" || question.correct == "" || question.correct == null) {
      var instance = this;
      $(".answer option").clone().appendTo($("#dialog-select-answer select").empty());
      $("#dialog-select-answer").dialog({
        resizable: false,
        draggable: false,
        modal: true,
        width: 500,
        dialogClass: "no-titlebar",
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        buttons: [
          {
            text: $.t('button.confirm'),
            click: function() {
              question.correct = $("#dialog-select-answer select").val();
              instance.setState({question: question});
              instance.handlePublishAnswer();
              $(this).dialog("close");
            }
          },
          {
            text: $.t('button.cancel'),
            click: function() {
              $(this).dialog("close");
            }
          }
        ]
      });
      return;
    }
    
    //console.log("[ResultStat.handlePublishAnswer2]", question.qid, question.correctOpen, question.correct, question);
    //question.correctOpen = (question.correctOpen)? false : true;
    question.correctOpen = true;
    this.setState({question: question});
    this.props.onUpdate(question);

    if (question.onAir) {
      // Publish answer to members
      var sendingJson = {
            correct: question.correct,
            image: question.image,
            num: question.num,
            richSubject: question.richSubject,
            subject: question.subject,
            title: $.t('voting.newTitle') + question.num,
            type: question.type
          };
      mainWindow.window.votingEditorHandler('startVoting', sendingJson);
      //console.log("[ResultStat.handlePublishAnswer3] sendingJson=", sendingJson);
      sendingJson = null;
    }
  },
  render: function() {
    var question = this.state.question;
    var answers = this.props.answers;
    //hideAnswer={this.props.hideAnswer}
    //console.log('answers=', answers);
    var qType = questionLibary[question.type-1].answers;
    var output = [], total = 0;
    answers.map(function(answer, j){
      //console.log("answer.qid=", obj.qid);
      if (answer.answer > 0){
        total++;
      }
    });
    qType.map(function(opt, i){
      var counting = 0;
      answers.map(function(obj, j){
        if (obj.answer != 0){
          if(opt.value == obj.answer){
            counting++;
          }
        }
      });
      if (opt.value != 0){
        output.push({
          type: opt.label.replace('voting.',''),
          cnt: counting,
          percent: ((total > 0)? Math.round(counting/total*10000) / 100 : 0)
        });
      }
    }, this);
    var arr = output.map(function(opt, i){
                //console.log("output=", i, opt);
                return (React.createElement("div", null, 
                          React.createElement("span", {className: opt.type}), 
                          React.createElement("span", {className: "percent " + ((opt.percent==100)? "notZero" : "")}, opt.percent, "%"), 
                          React.createElement("span", {className: "person"}), 
                          React.createElement("span", {className: "cnt"}, opt.cnt)
                        ));
              }, this);
    
    var statClass = "stat " + ((this.props.hideAnswer)? "hide" : "");
    
    return (
      React.createElement("div", {className: "resultStat"}, 
        React.createElement("div", {className: "correctOpen " + ((question.type == "1" || question.onAir == false)? "hide" : "")}, 
          $.t('voting.answer'), 
          React.createElement("span", {onClick: this.handlePublishAnswer, className: (question.correctOpen)? "answerOn" : "answerOff"})
        ), 
        React.createElement("div", null, 
          React.createElement("div", {className: "chart"}, 
            React.createElement(PieChart, {fill: "#F17D22", percent: (answers.length==0)? 0 : total/answers.length}), 
            React.createElement("div", null, 
              React.createElement("span", {"data-i18n": "voting.submitted"}, $.t('voting.submitted')), 
              React.createElement("br", null), 
              React.createElement("span", {className: "number"}, total), 
              React.createElement("span", {className: "total"}, "/ ", answers.length)
            )
          )
        ), 
        React.createElement("div", {className: statClass}, arr)
      )
    );
  }
});

var DisplayAnswerButton = React.createClass({displayName: "DisplayAnswerButton",
  render: function() {
    var questionType = parseInt(this.props.questionType);
    //console.log('[DisplayAnswerButton] questionType=', questionType);
    
    var answersClass, btns = (React.createElement("div", {id: "answers"}));
    switch(questionType) {
      case 1:  // QuestionTypeThumbsUpDown = 1
        btns = (
          React.createElement("div", {id: "answers", className: "commend"}, 
            React.createElement("span", {className: "answer thumbs-up"}), 
            React.createElement("span", {className: "answer thumbs-down"})
          )
        );
        break;
      case 2:  // QuestionTypeTrueFalse = 2
        btns = (
          React.createElement("div", {id: "answers", className: "boolean"}, 
            React.createElement("span", {className: "answer true"}, "True"), 
            React.createElement("span", {className: "answer false"}, "False")
          )
        );
        break;
      case 3:  // QuestionTypeTrueFalse = 3
        btns = (
          React.createElement("div", {id: "answers", className: "choice-3"}, 
            React.createElement("span", {className: "answer"}, "A"), 
            React.createElement("span", {className: "answer"}, "B"), 
            React.createElement("span", {className: "answer"}, "C")
          )
        );
        break;
      case 4:  // QuestionTypeTrueFalse = 4
        btns = (
          React.createElement("div", {id: "answers", className: "choice-4"}, 
            React.createElement("span", {className: "answer"}, "A"), 
            React.createElement("span", {className: "answer"}, "B"), 
            React.createElement("span", {className: "answer"}, "C"), 
            React.createElement("span", {className: "answer"}, "D")
          )
        );
        break;
      case 5:  // QuestionTypeTrueFalse = 5
        btns = (
          React.createElement("div", {id: "answers", className: "choice-5"}, 
            React.createElement("span", {className: "answer"}, "A"), 
            React.createElement("span", {className: "answer"}, "B"), 
            React.createElement("span", {className: "answer"}, "C"), 
            React.createElement("span", {className: "answer"}, "D"), 
            React.createElement("span", {className: "answer"}, "E")
          )
        );
        break;
      case 6:  // QuestionType4Choices = 6
        btns = (
          React.createElement("div", {id: "answers", className: "upload"}, 
            React.createElement("input", {id: "img_path", type: "text", placeholder: "Select an image", readOnly: true}), 
            React.createElement("span", {id: "choose_img"}, "...")
          )
        );
        break;
      default: // QuestionTypeOpenEnded = ?
        console.error("Question Type does NOT exist.");
    }
    
    return btns;
  }
});

var CheckResult = React.createClass({displayName: "CheckResult",
  handleUpdateQuestion: function(question) {
    this.props.onUpdate(question);
  },
  handleClickSubject: function(e) {
    var question = this.props.question;
    var previewImage = (question.image)? React.createElement("img", {src: question.image, className: "previewImage"}) : null;
    var qType = $.t(questionLibary[question.type-1].type);
    var previewDialog = React.renderToStaticMarkup(
      React.createElement("div", null, 
        React.createElement("div", {className: "subject"}, 
          React.createElement("span", {"data-i18n": "voting.question"}, $.t('voting.question')), " ", question.num, 
          React.createElement("span", {className: "small"}, "( ", React.createElement("span", {"data-i18n": questionLibary[question.type-1].type}, qType), " )")
        ), 
        React.createElement("div", {className: "previewContent"}, 
          React.createElement("div", {dangerouslySetInnerHTML: {__html: question.richSubject}}), 
          previewImage
        ), 
        React.createElement(DisplayAnswerButton, {questionType: question.type})
      ));

    $('#dialog-preview').empty().append($(previewDialog));

    $("#dialog-preview").dialog({
      autoOpen: true,
      modal: true,
      resizable: false,
      draggable: false,
      width: '90%',
      dialogClass: "titlbar-only-close no-buttonpane",
      show: {
        effect: "blind",
        duration: 300
      },
      close: function(event, ui) {
        $(this).dialog('destroy');
      }
    });
  },
  handleClick: function(e) {
    var h = $('#page').height() * .9;
    //console.log(e.target, e.target.src, h);
    $("#dialog-pic img").attr('src', e.target.src).css('max-height', h-90);
    $("#dialog-pic").dialog({
      autoOpen: true,
      title: e.target.title,
      modal: true,
      width: '90%',
      maxHeight: h,
      dialogClass: "no-buttonpane",
      resizable: false,
      draggable: false,
      show: {
        effect: "blind",
        duration: 500
      },
      close: function(event, ui) {
        $(this).dialog('destroy');
      }
    });
  },
  render: function() {
    var question = this.props.question;
    var answers = this.props.answers;
    //console.log('[CheckResult] question=', question);
    //console.log('[CheckResult] answers=', answers);
    var previewImage = (question.image)? React.createElement("img", {src: question.image}) : null;
    var qType = $.t(questionLibary[question.type-1].type);
    var total = 0;

    answers.map(function(obj, j){
      (obj.imageBLOB)? total++ : null;
    });
    
    var showPic = answers.map(function(obj, j){
      //console.log("answers=", obj.senderName, obj.imageBLOB, obj);
      if (obj.imageBLOB) {
        return (React.createElement("div", {className: (this.props.hideAnswer)? "hide" : ""}, React.createElement("img", {onClick: this.handleClick, src: window.URL.createObjectURL(new Blob([obj.imageBLOB])), title: obj.senderName}), obj.senderName));
      } else {
        return (React.createElement("div", null, React.createElement("span", {className: "emptyAnswer", title: obj.senderName}), obj.senderName));
      }
    }, this);
  
    var h2 = $('#main-content').height() - 120;
    var height = {height: h2 + 'px'};
    var showPicHeight = {height: (h2-16) + 'px'};
    //console.log('[CheckResult.render] this.props.onMax=', this.props.onMax, $('#main-content').height(), "height=", h2, $('.question.result'));

    var output = (question.type==6)?
        (React.createElement("div", null, 
           React.createElement("div", {className: "submittedPicInfo"}, 
             React.createElement("span", {"data-i18n": "voting.submitted"}, $.t('voting.submitted'), "："), 
             React.createElement("span", null, " ", total, " / ", answers.length)
           ), 
           React.createElement("div", {className: "showStudentPic", style: showPicHeight}, showPic)
         )
        ) :
        (React.createElement("div", {className: "checkAnswer", style: height}, 
           React.createElement(ResultStat, {question: question, answers: answers, hideAnswer: this.props.hideAnswer, onUpdate: this.handleUpdateQuestion}), 
           React.createElement(AnswerDetail, {question: question, answers: answers, hideAnswer: this.props.hideAnswer, onMax: this.props.onMax})
         ));

    return (
      React.createElement("div", {className: "question result"}, 
        React.createElement("div", {className: "subject"}, 
          React.createElement("span", {"data-i18n": "voting.question"}, $.t('voting.question')), " ", question.num, 
          React.createElement("span", {className: "small"}, "( ", React.createElement("span", {"data-i18n": questionLibary[question.type-1].type}, qType), " )")
        ), 
        React.createElement("div", {className: "contentNoImg", onClick: this.handleClickSubject}, question.subject), 
        output
      )
    );
  }
});

var Section = React.createClass({displayName: "Section",
  getInitialState: function() {
    return {
      question: this.props.data.question,
      answers: this.props.data.answers,
      display: this.props.data.display,
      onAirIndex: this.props.data.onAirIndex,
      cancel: false,
      hideAnswer: true
    };
  },
  handleUpdateQuestionWithoutDone: function(question) {
    //console.log("this.state.question=", this.state.question);
    //console.log("[Section.handleUpdateQuestionWithoutDone] question=", question);
    var cancelBtn = (this.props.data.mode=='add')? false : true;
    this.setState({
      question: question,
      cancel: cancelBtn
    });
    //console.log("Section.handleUpdateQuestion...", this.props.data.question.image);
    //console.log("this.props.data.question=", this.props.data.question);
  },
  handleUpdateQuestionWithDone: function(question) {
    //console.log("this.state.question=", this.state.question);
    //console.log("[Section.handleUpdateQuestionWithDone] question.correct=", question.correct);
    this.setState({question: question});
    this.props.data.onUpdate(question); // really update
    //console.log("Section.handleUpdateQuestion...", this.props.data.question.image);
    //console.log("this.props.data.question=", this.props.data.question);
  },
  handleClickDone: function(e) {
    e.preventDefault();
    var question = this.state.question;
    question.title = $.t('voting.newTitle') + question.num;
    this.props.data.onUpdate(question); // really update
    this.setState({display: 2});
    this.props.data.onChangeDisplay(2);
    //console.log("Section.handleClickDone...");
  },
  handleClickDelete: function(e) {
    e.preventDefault();
    this.props.data.onDelete();
    //console.log("Section.handleClickDelete...");
  },
  handleClickCancel: function(e) {
    e.preventDefault();
    // TODO: Cancel Change...
    this.setState({display: 2, cancel: false});
    this.props.data.onCancel();
    //console.log("Section.handleClickCancel...");
    //console.log("this.props.data.question=", this.props.data.question);
    //console.log("this.state.question=", this.state.question);
  },
  handleClickEdit: function(e) {
    e.preventDefault();
    //console.log("Section.handleClickEdit...");
    this.setState({display: 1, cancel: true});
    this.props.data.onChangeDisplay(1);
  },
  handleClickStart: function(e) {
    e.preventDefault();
    //console.log("Section.handleClickStart...");
    if (this.props.data.moderator) {
      var question = this.state.question;
      question.onAir = true;
      this.props.data.onAir();
    }
  },
  handleClickStop: function(e) {
    e.preventDefault();
    //console.log("Section.handleClickStop...");
    var question = this.state.question;
    question.onAir = false;
    //this.props.data.onChangeDisplay(2);
    this.props.data.onStop();
  },
  handleClickViewResult: function(e) {
    e.preventDefault();
    //console.log("Section.handleViewResult...");
    this.setState({display: 3});
    this.props.data.onChangeDisplay(3);
  },
  handleClickViewQuestion: function(e) {
    e.preventDefault();
    //console.log("Section.handleViewQuestion...");
    this.setState({display: 2});
    this.props.data.onChangeDisplay(2);
  },
  handleClickHideAnswer: function() {
    var hideAnswer = this.state.hideAnswer;
    console.log("handleDisplayBtn...", hideAnswer);
    this.setState({hideAnswer: !(hideAnswer)});
  },
  componentWillReceiveProps: function(nextProps) {
    //console.log(nextProps.data.question);
    this.setState({
      question: nextProps.data.question,
      display: nextProps.data.display,
      cancel: false
    });
  },
  render: function() {
    var question = this.state.question;
    var currentAnswers = [];
    this.props.data.answers.map(function(answer, i){
      if (question.qid == answer.qid){
        currentAnswers.push(answer);
      }
    });
    //console.log('[Section] currentAnswers=', currentAnswers);
    var display = this.state.display;
    var moderator = this.props.data.moderator;
    var onAirIndex= this.props.data.onAirIndex;
    //console.log("Section--", this.props.data.question);
    //console.log("Section--", this.props.data.mode);

    var cancelButton = (this.props.data.mode=='update')? 
      React.createElement("button", {onClick: this.handleClickCancel, "data-i18n": "button.cancel"}, $.t('button.cancel')) : 
      "";

    var btns = (question.onAir)?
      (React.createElement("div", null, 
         React.createElement("button", {onClick: this.handleClickStop, "data-i18n": "button.stop"}, $.t('button.stop')), 
         React.createElement("button", {onClick: this.handleClickViewResult, "data-i18n": "button.viewResult"}, $.t('button.viewResult'))
       )) :
      ((onAirIndex > -1)?
        React.createElement("div", null) :
        (React.createElement("div", null, 
          React.createElement("button", {onClick: this.handleClickEdit, "data-i18n": "button.edit"}, $.t('button.edit')), 
          React.createElement("button", {onClick: this.handleClickStart, className: (moderator)? null : "disable", "data-i18n": "button.start"}, $.t('button.start')), 
          React.createElement("button", {onClick: this.handleClickViewResult, "data-i18n": "button.viewResult"}, $.t('button.viewResult'))
        )));

    var hideViewText = (this.state.hideAnswer)? $.t('voting.showDetails') : $.t('voting.hideDetails');
    //<span className="hideView" onClick={this.handleDisplayBtn}>{hideViewText}</span>

    var stopButton = (question.onAir)?
      React.createElement("button", {onClick: this.handleClickStop, "data-i18n": "button.stop"}, $.t('button.stop')) : 
      "";

    return (
      React.createElement("div", {className: "section"}, 
        React.createElement("div", {className: (display==1)? "" : "hide"}, 
          React.createElement(Question, {question: question, onUpdate: this.handleUpdateQuestionWithoutDone, onMax: this.props.data.onMax}), 
          React.createElement("div", {className: "bottom-btn"}, 
            React.createElement("div", null, 
              React.createElement("button", {onClick: this.handleClickDone, "data-i18n": "button.done"}, $.t('button.done')), 
              React.createElement("button", {onClick: this.handleClickDelete, "data-i18n": "button.delete"}, $.t('button.delete')), 
              cancelButton
            )
          )
        ), 
        React.createElement("div", {className: (display==2)? '' : 'hide'}, 
          React.createElement(PreviewQuestion, {question: question, onMax: this.props.data.onMax}), 
          React.createElement("div", {className: "bottom-btn"}, 
            btns
          )
        ), 
        React.createElement("div", {className: (display==3)? '' : 'hide'}, 
          React.createElement(CheckResult, {question: question, answers: currentAnswers, hideAnswer: this.state.hideAnswer, onUpdate: this.handleUpdateQuestionWithDone, onMax: this.props.data.onMax}), 
          React.createElement("div", {className: "bottom-btn"}, 
            React.createElement("div", null, 
              React.createElement("button", {onClick: this.handleClickViewQuestion, "data-i18n": "button.viewQuestion"}, $.t('button.viewQuestion')), 
              React.createElement("button", {onClick: this.handleClickHideAnswer}, hideViewText), 
              stopButton
            )
          )
        )
      )
    );
  }
});

var Page = React.createClass({displayName: "Page",
  getInitialState: function() {
    //console.log('1. Page.getInitialState...');
    mainWindow = getMainWindow();
    return {
      currentFile: "",
      questions: [],
      preQuestions: [],
      answers: [],
      idx: 1,
      currentQuestion: null,
      currentSummary: false,
      enableAllBtns: [1,1,0,0,0],
      display: 2,
      mode: 'update',
      moderator: getModerator(),
      onAirIndex: -1,
      maxWindow: false,
      goCloseWin: false,
      goCreateFile: false,
      goOpenFile: false
    };
  },
  checkFileChange: function() {
    if (this.state.questions.length > 0) {
      // TODO: more checking
      //console.log("[Page.checkFileChange]", true);
      return true;
    }
    //console.log("[Page.checkFileChange]", false);
    return false;
  },
  handleNewFile: function() {
    var p = this;
    if (this.checkFileChange()) {
      $('#dialog-message em').text($.t('voting.quitConfirmMsg'));
      $('#dialog-message p').css("white-space", "normal")
      $("#dialog-message").dialog({
        resizable: false,
        draggable: false,
        modal: true,
        minWidth: 400,
        dialogClass: "titlbar-only-close",
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        buttons: [
          {
            text: $.t('button.yes'),
            click: function() {
              $(this).dialog("close");
              p.setState({goCreateFile: true});
              p.handleSaveFile();
            }
          },
          {
            text: $.t('button.no'),
            click: function() {
              $(this).dialog("close");
              p.handleCreateFile();
            }
          },
          {
            text: $.t('button.cancel'),
            click: function() {
              $(this).dialog("close");
            }
          },
        ]
      });
      return;
    }
    p.handleCreateFile();
    //console.log("[Page.handleNewFile]...");
  },
  handleCreateFile: function() {
    var questions = [{
          qid: 0,
          num: 1,
          subject: "",
          richSubject: "",
          title: "",
          type: "1",
          image: null,
          correct: null,
          correctOpen: false,
          onAir: false
        }];
    var options = this.getInitialState();
    //console.log("getInitialState=", options);
    options.questions = questions;
    options.currentQuestion = options.questions[0];
    options.display = 1;
    options.mode = 'add';
    this.setState(options);
    //console.log("[Page.handleCreateFile] options=", options);
  },
  handleOpenFile: function() {
    //console.log("Page.handleOpenFile...");
    var p = this;
    if (this.checkFileChange()) {
      $('#dialog-message em').text($.t('voting.quitConfirmMsg'));
      $('#dialog-message p').css("white-space", "normal")
      $("#dialog-message").dialog({
        resizable: false,
        draggable: false,
        modal: true,
        minWidth: 400,
        dialogClass: "titlbar-only-close",
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        buttons: [
          {
            text: $.t('button.yes'),
            click: function() {
              $(this).dialog("close");
              p.setState({goOpenFile: true});
              p.handleSaveFile();
            }
          },
          {
            text: $.t('button.no'),
            click: function() {
              $(this).dialog("close");
              $('#openFileDialog').trigger('click');
            }
          },
          {
            text: $.t('button.cancel'),
            click: function() {
              $(this).dialog("close");
            }
          },
        ]
      });
      return;
    }
    $('#openFileDialog').trigger('click');
    //p.handleOpenExistedFile();
    //console.log("[Page.handleOpenFile]...");
  },
  handleOpenExistedFile: function() {
    //e.preventDefault();
    var filePath = $('#openFileName').val(), options = {};
    var done = this.loadQuestionsFromFile(filePath);
    //console.log("Page.handleOpenExistedFile...", filePath);
  },
  loadQuestionsFromFile: function(filePath) {
    //console.log("Page.loadQuestionsFromFile===> ", filePath);
    var options = this.getInitialState();
    //this.setState(options);

    try {
      var filebuffer = fs.readFileSync(filePath);
      //console.log("filebuffer=", filebuffer);
      if (filebuffer == undefined || filebuffer.length == 0) {
        throw "Wrong file format.";
      }

      var db = new sql.Database(filebuffer);
      //console.debug("db= ", db);
      var res = db.exec("SELECT * FROM questions ORDER BY id; SELECT * FROM answers ORDER BY qid;");
      //console.log("res= ", res);
      if (res.length == 0) {
        $('#dialog-message em').text($.t('voting.openFileError'));
        $('#dialog-message p').css("white-space", "normal")
        $("#dialog-message").dialog({
          modal: true,
          resizable: false,
          draggable: false,
          dialogClass: "titlbar-only-close",
          close: function(event, ui) {
            $(this).dialog('destroy');
          },
          buttons: [
            {
              text: $.t("button.ok"),
              click: function() {
                $(this).dialog("close");
              }
            }
          ]
        });
        this.setState(options);
        return false;
      }
      //console.log("res= ", res[0], res[1]);

      var questions = [], question, records = res[0].values, record, objurl, qq = {};
      //console.log("records= ", records);
      for(var key in records){ 
        record = records[key];
        //console.log(key, record);
        //console.log(record['5']);
        //var objurl = (record['5'])? window.URL.createObjectURL(new Blob([record['5']])) : ""; // UInt8Array containing the bytes of the image(record['5'])
        objurl = (record['5'])? "data:image/"+record[6]+";base64,"+btoa(Uint8ToString(record['5'])) : ""; // UInt8Array containing the bytes of the image(record['5'])
        //console.log(key, record, objurl);
        question = {
          qid: record[0],
          num: key,
          subject: record[2],
          richSubject: record[3],
          title: record[1],
          type: record[4],
          image: objurl,
          correct: record[7],
          correctOpen: false,
          onAir: false
        };
        questions.push(question);
        qq[question.qid] = question;  // all questions
      }
      //console.log("questions===>", questions);
      //console.log("qq===>", qq);

      var answers = [], answer;
      if (res[1] && res[1].values){
        var records = res[1].values;
        //console.log("records===>", records);
        for(var key in records){ 
          record = records[key];
          //console.log(key, record);
          if (qq.hasOwnProperty(record[1])) {
            objurl = (record['4'])? window.URL.createObjectURL(new Blob([record['4']])) : ""; // UInt8Array containing the bytes of the image(record['5'])
            answer = {id: record[0],
                      qid: record[1],
                      senderName: record[2],
                      answer: record[3],
                      imageBLOB: record[4],
                      image: objurl
                     };
            answers.push(answer);
          }
        }
      }
      //console.log("answers===>", answers);

      // Update state
      //var i = this.state.idx;
      //var i = 1;
      options = {
        currentFile: filePath,
        questions: questions,
        preQuestions: jQuery.extend(true, [], questions),
        answers: answers || [],
        idx: 1,
        currentQuestion: questions[0],
        enableAllBtns: [1,1,1,1,1],
        display: 2,
        mode: 'update',
        currentSummary: false
      };
      this.setState(options);
      $('#openFileName').val("");
      //console.log("Page.loadQuestionsFromFile...", filePath, options);
      return true;
    }
    catch(err) {
      console.error(err);
      $('#dialog-message em').text($.t('voting.openFileError'));
      $('#dialog-message p').css("white-space", "normal")
      $("#dialog-message").dialog({
        modal: true,
        resizable: false,
        draggable: false,
        dialogClass: "titlbar-only-close",
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        buttons: [
          {
            text: $.t("button.ok"),
            click: function() {
              $(this).dialog("close");
            }
          }
        ]
      });
      this.setState(options);
      return false;
    }
  },
  handleClickSummary: function() {
    if (this.state.mode=='add') return;
    //console.log("[Page.handleClickSummary]...");
    // TODO: go summary page
    this.setState({
      idx: 0,
      currentSummary: true,
      currentQuestion: null,
    });
    //console.log("[Page.handleClickSummary] this.state=", this.state);
    //console.log("[Page.handleClickSummary] currentFile=", this.state.currentFile);
    //console.log("[Page.handleClickSummary] questions=", this.state.questions);
    //console.log("[Page.handleClickSummary] currentQuestion=", this.state.currentQuestion);
  },
  switchQestion: function(i) {
    //console.error("Page.switchQestion...", this.state.idx, i, this.state.display);
    //console.error("[Page.switchQestion]... this.state.idx=", this.state.idx +"-->"+ i);
    if (this.state.mode=='add'){
      var p = this;
      $('#dialog-message em').text($.t('voting.discardChange'));
      $('#dialog-message p').css("white-space", "normal")
      $("#dialog-message").dialog({
        modal: true,
        resizable: false,
        draggable: false,
        dialogClass: "titlbar-only-close",
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        buttons: [
          {
            text: $.t("button.yes"),
            click: function() {
              //this.setState({mode: 'update'});
              p.deleteQuestion(i);
              //p.changeQuestion(i);
              $(this).dialog("close");
            }
          },
          {
            text: $.t("button.no"),
            click: function() {
              $(this).dialog("close");
            }
          }
        ]
      });
      return;
    }
    
    var questions = this.state.questions;
    if (this.state.idx > 0) {
      //var originalQuestion = questions[this.state.idx];
      //console.log("currentQuestion=", this.state.currentQuestion.qid, this.state.currentQuestion);
      questions[this.state.idx-1] = jQuery.extend(true, {}, this.state.preQuestions[this.state.idx-1]);  // reset changes
      //questions[this.state.idx-1] = jQuery.extend(true, {}, this.state.preQuestions[this.state.idx-1]);  // reset changes
      //this.state.questions = jQuery.extend(true, {}, this.state.preQuestions);  // reset changes
      //var questions = jQuery.extend(true, [], this.state.preQuestions);
    }
    
    var currentQuestion = questions[i-1];
    currentQuestion.correctOpen = false;

    this.setState({
      idx: i,
      currentQuestion: currentQuestion,
      currentSummary: false,
      display: 2
    });
    //console.log("currentFile=", this.state.currentFile);
    //console.log("questions=", this.state.questions);
    //console.log("currentQuestion=", this.state.currentQuestion);
    //console.log("preQuestions=", this.state.preQuestions);
    //this.forceUpdate();
  },
  handleAddQuestion: function() {
    //console.log("Page.handleAddQuestion...");
    if (this.state.mode=='add') return;
    
    // Start creating...
    var questions = this.state.questions;
    var cnt = questions.length;
    var qid = (cnt==0)? 0 : (questions[cnt-1].qid + 1);
    cnt++;
    var newQuestion = {
          num: cnt,
          qid: qid,
          subject: null,
          richSubject: "",
          title: $.t('voting.newTitle') + cnt,
          type: "1",
          image: null,
          correct: null
        };
    questions.push(newQuestion);
    this.setState({
      questions: questions,
      idx: cnt,
      currentQuestion: questions[cnt-1],
      enableAllBtns: [1,1,0,1,1],
      display: 1,
      mode: 'add',
      currentSummary: false
    });
  
    //console.info("Page.handleAddQuestion...", newQuestion);
    //console.debug("Page.handleAddQuestion...", this.state.idx, this.state.currentQuestion, this.state.questions);
  },
  handleUpdateQuestion: function(question) {
    var questions = this.state.questions;
    questions[this.state.idx-1] = question;
    var btns = (this.state.onAirIndex > -1)? [0,0,0,0,0] : [1,1,1,1,1];
    this.setState({
      questions: questions,
      enableAllBtns: btns,
      preQuestions: jQuery.extend(true, [], questions),
      mode: 'update'
    });
    //console.log("Page.handleUpdateQuestion...", question);
  },
  handleSaveFile: function() {
    if (this.state.currentFile) {
      this.saveFile(this.state.currentFile);
      this.setState({enableAllBtns: [1,1,0,1,1]});
      return;
    }
    // no file and go new file dialog
    $('#newFileDialog').trigger('click');
    //console.log("[Page.handleSaveFile]...");
  },
  handleSaveNewFile: function(e) {
    e.preventDefault();
    var filePath = $('#newFileName').val();
    var fileName = filePath.replace(/^.*[\\\/]/, '');
    this.saveFile(filePath);
    this.setState({
      currentFile: filePath,
      mode: 'update',
      enableAllBtns: [1,1,0,1,1]
    });
    $('#newFileName').val("");
    //console.log("[Page.handleSaveNewFile]...", filePath);
  },
  saveFile: function(filePath) {
    //console.log("Page.saveFile...", filePath);
    // Create a database
    var db = new sql.Database();
    
    // Execute some sql 
    var sqlstr = "DROP TABLE IF EXISTS questions;";
    sqlstr += "DROP TABLE IF EXISTS answers;";
    sqlstr += "CREATE TABLE questions (id integer primary key, title varchar(200), subject varchar(300), richSubject varchar(1024), type integer, import_file blob, file_type varchar(50), answer varchar(50));";
    sqlstr += "CREATE TABLE answers (id integer primary key, qid integer, sender_name varchar(100), answer varchar(50), image blob);"
    db.run(sqlstr);
    
    
    // handle questions
    var records = this.state.questions, question, record, imgType, imageArray;
    //console.log("Save questions=", records);
    for(var key in records){
      question = records[key];
      imgType = (question.image)? question.image.substring(11, question.image.indexOf(';')) : null;
      imageArray = (question.image)? new convertDataURIToBinary(question.image) : null;
      //console.log("imageArray=", imageArray);
      record = {':id': question.qid,
                ':title': $.t('voting.newTitle') + question.num,
                ':subject': question.subject,
                ':richSubject': question.richSubject,
                ':type': question.type,
                ':importFile': imageArray,
                ':fileType': imgType,
                ':answer': question.correct
               };
      //console.log(i, question, imgType);
      db.run("INSERT INTO questions VALUES (:id, :title, :subject, :richSubject, :type, :importFile, :fileType, :answer)", record);
      //console.log("record=", record);
    }
 
    // handle answers
    var records = this.state.answers, answer, record, imageArray;
    //console.log("records=", records);
    for(var key in records){
      answer = records[key];
      imageArray = answer.imageBLOB;
      //console.warn(key, "answer=", answer, imageArray);
      if (answer.imageBLOB == null) {
        //imgType = (answer.image)? answer.image.substring(11, answer.image.indexOf(';')) : null;
        imageArray = (answer.image)? new convertDataURIToBinary(answer.image) : null;
        //answer.imageBLOB = imageArray;
        //console.log("imageArray=", imageArray);
      }
      record = {':id': key,
                ':qid': answer.qid,
                ':sender_name': answer.senderName,
                ':answer': answer.answer,
                ':image': ((answer.imageBLOB != null)? answer.imageBLOB : imageArray)
               };
      db.run("INSERT INTO answers VALUES (:id, :qid, :sender_name, :answer, :image)", record);
      //console.log("record=", record);
    }

    // Export the database to an Uint8Array containing the SQLite database file 
    var binaryArray = db.export();
    // convert the result of db.export to a buffer
    var buffer = new Buffer(binaryArray);
    // write a database to the disk
    //fs.writeFileSync(filePath, buffer);
    fs.writeFile(filePath, buffer, function (err) {
      if (err) {
        console.error('Failed!', err);
        $('#dialog-message em').text($.t('voting.writeFileError'));
        $('#dialog-message p').css("white-space", "normal")
        return;
      };
      //console.log("It's saved!");
      var filename = filePath.replace(/^.*[\\\/]/, '');
      $('#dialog-message em').text($.t('voting.saveFileMsg', { postProcess: "sprintf", sprintf: [filename]}));
      $('#dialog-message p').css("white-space", "normal")
    });

    var p = this;
    //console.log("[Page.saveFile]...p.state.closeWin=", p.state.closeWin);
    $("#dialog-message").dialog({
      resizable: false,
      draggable: false,
      modal: true,
      dialogClass: "titlbar-only-close",
      width: 500,
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: $.t('button.ok'),
          click: function() {
            if (p.state.goCloseWin){
              mainWin.show(true);
              win.close(true);  // real closing window
              return;
            }
            $(this).dialog("close");
            if (p.state.goCreateFile){
              p.handleCreateFile();
              p.setState({goCreateFile: false});
              return;
            }
            if (p.state.goOpenFile){
              $('#openFileDialog').trigger('click');
              p.setState({goOpenFile: false});
              return;
            }
          }
        }
      ]
    });

  },
  handleDelete: function(j) {
    //console.log("Page.handleDelete...", j);
    var p = this;
    $('#dialog-message em').text($.t('voting.deleteConfirmMsg'));
    $("#dialog-message").dialog({
      resizable: false,
      draggable: false,
      modal: true,
      dialogClass: "titlbar-only-close",
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: $.t('button.yes'),
          click: function() {
            p.deleteQuestion(j);
            $(this).dialog("close");
          }
        },
        {
          text: $.t("button.no"),
          click: function() {
            $(this).dialog("close");
          }
        }
      ]
    });
  },
  deleteQuestion: function(j) {
    //console.log("Page.deleteQuestion...", j);
    //console.log("currentQuestion=", this.state.currentQuestion.qid, this.state.currentQuestion);
    //console.log("questions=", this.state.questions.length, this.state.questions);
    //console.log("answers=", this.state.answers.length, this.state.answers);
    var questions = this.state.questions;
    var currentQuestion = this.state.currentQuestion;
    var currentIndex = questions.indexOf(this.state.currentQuestion);
    var qid = currentQuestion.qid;
    if(currentIndex > -1) {
      // check if existed answers should be deleted.
      if (qid != undefined && qid > -1) {
        var answers = this.state.answers,
            newAnswers = [];
        answers.map(function(answer, k){
          if (answer.qid != qid) {
            //console.warn('PUSH answer.qid=', answer.qid, k);
            newAnswers.push(answer);
          }
        });
        this.setState({answers: newAnswers});
        //console.warn("[END] answers=", newAnswers.length, newAnswers);
      }
      
      // handle questions
      var newQestions = [];
      questions.splice(currentIndex, 1); // delete currentQuestion
      //console.warn("[Start] questions=", questions.length, questions);
      questions.map(function(question, k){
        //console.warn('PUSH questions.qid=', question.qid, k);
        question.num==k;
        newQestions.push(question);
      });
      questions = newQestions;
      //console.warn("[END] questions=", questions.length, questions);
      
      // Update currentIndex
      currentIndex = (j)? j : Math.max(1, this.state.idx-1);
      //console.warn("currentIndex=", currentIndex);
    }
    
    // add mode
    //console.warn("currentIndex=", currentIndex, "this.state.idx=", this.state.idx);
    //console.warn("questions=", questions.length, questions);
    if (this.state.idx==1 && questions.length==0){
      this.setState({
        questions: questions,
        preQuestions: questions,
        idx: 1,
        mode: 'update'
      });
      this.handleAddQuestion();
      return;
    }
    
    // update mode
    this.setState({
      questions: questions,
      preQuestions: questions,
      idx: currentIndex,
      currentQuestion: questions[currentIndex-1],
      enableAllBtns: [1,1,1,1,1],
      mode: 'update'
    });
  },
  handleCancel: function() {
    //console.log("Page.handleCancel...");
    var questions = jQuery.extend(true, [], this.state.preQuestions);  // reset changes
    this.setState({
      questions: questions,
      currentQuestion: questions[this.state.idx-1]
    });
    this.handleChangeDisplay(2);
  },
  handleChangeDisplay: function(i) {
    //console.log("Page.handleChangeDisplay...");
    this.setState({display: i});
  },
  handleOnAir: function(){
    //console.log("Page.handleOnAir...");
    var questions = this.state.questions,
        currentQuestion = this.state.currentQuestion,
        answers = this.state.answers,
        currentAnswers = [],
        answeredUsers = {};
    currentQuestion.onAir = true;
    currentQuestion.correctOpen = false;
    questions[this.state.idx-1] = currentQuestion;
    this.setState({
      questions: questions,
      preQuestions: questions,
      currentQuestion: currentQuestion,
      onAirIndex: this.state.idx,
      enableAllBtns: [0,0,0,0,0]
    });
    // TODO: send question to members
    var sendingJson = {
          //correct: currentQuestion.correct,
          correct: '',
          image: currentQuestion.image,
          num: currentQuestion.num,
          richSubject: currentQuestion.richSubject,
          subject: currentQuestion.subject,
          title: $.t('voting.newTitle') + currentQuestion.num,
          type: currentQuestion.type
        };
    mainWindow.window.votingEditorHandler('startVoting', sendingJson);
    //console.log("[Page.handleOnAir] sendingJson=", sendingJson);
    
    // TODO: handle answers
    // get user list
    var users = mainWindow.window.votingEditorHandler('getUserList');
    //console.log("Page.handleOnAir... users=", users);
    
    labels = {}; // get all names
    users.map(function(user, i){
      labels[convertNameToCompare(user.label)] = user.label;
    });
    //console.log("labels=", labels);
    
    answers.map(function(answer, j){
      var compare = convertNameToCompare(answer.senderName);
      if (labels.hasOwnProperty(compare)){
        //convert name to new name
        answer.senderName = labels[compare];
        //console.log("answer=", answer.qid, answer.senderName);
      }
      if (currentQuestion.qid == answer.qid){
        // get answered list
        currentAnswers.push(answer);
        answeredUsers[answer.senderName] = answer;
      }
    });
    //console.log("Page.handleOnAir...", currentAnswers, answeredUsers);
    
    var answeredNames = {};
    currentAnswers.map(function(answer, j){
      answeredNames[convertNameToCompare(answer.senderName)] = true; // get all answered names
    });
    //console.log("answeredNames=", answeredNames);
    
    users.map(function(user, i){
      //console.log("user=", answeredUsers);
      var compare = convertNameToCompare(user.label);
      //console.log("compare=", compare);
      if (user.online && !user.isMyself && !answeredNames[compare]) {
        //console.log("user (New)=", user);
        var answer = {id: null,
                      qid: currentQuestion.qid,
                      senderName: user.label,
                      answer: null,
                      imageBLOB: null,
                      image: null
                     };
        currentAnswers.push(answer);
        answers.push(answer);
      }
    });

    //console.log("[Page.handleOnAir] currentAnswers=", currentAnswers);
  },
  refreshAnswer: function() {
    // {answer: 5, user_name: "novocampus.delta", num: 1}
    var currentQuestion = this.state.currentQuestion,
        answers = this.state.answers;
    
    //console.debug("[Page.refreshAnswer] userAnswer=", userAnswer);

    // get answered list
    answers.map(function(answer, j){
      if (userAnswer.num == currentQuestion.num && 
          currentQuestion.qid == answer.qid && 
          answer.senderName == userAnswer.user_name){
        //currentAnswers.push(answer);
        //answeredUsers[answer.senderName] = answer;
        if (currentQuestion.type==6) {
          answer.answer = null;
          answer.image = "data:image/jpg;base64," + userAnswer.answer;
          answer.imageBLOB = (answer.image)? new convertDataURIToBinary(answer.image) : null;
        } else {
          answer.answer = userAnswer.answer;
        }
        //console.log("type=", currentQuestion.type, "answer=", answer);
      }
    });

    //console.log("answers=", answers);
    this.setState({answers: answers});
  },
  handleOnStop: function(){
    //console.log("Page.handleOnStop...");
    var questions = this.state.questions,
        currentQuestion = this.state.currentQuestion;
    currentQuestion.onAir = false;
    currentQuestion.correctOpen = false;
    questions[this.state.idx-1] = currentQuestion;
    this.setState({
      questions: questions,
      preQuestions: questions,
      currentQuestion: currentQuestion,
      onAirIndex: -1,
      enableAllBtns: [1,1,1,1,1]
    });
    mainWindow.window.votingEditorHandler('stopVoting');
  },
  handleClearHistory: function() {
    //console.log("Page.handleClearHistory...");
    this.setState({
      answers: [],
      enableAllBtns: [1,1,1,1,1]
    });
  },
  handleExportPDF: function() {
    //console.log("Page.handleExportPDF...");
    // TODO: export PDF
    $('#pdfFileDialog').trigger('click');
  },
  handlePDFFile: function(e) {
    e.preventDefault();
    //console.log("[Page.handlePDFFile]");
/*
    var filePath = $('#pdfFileName').val();
    var fileName = filePath.replace(/^.*[\\\/]/, '');
    
    var text = 'ANY_TEXT_YOU_WANT_TO_WRITE_IN_PDF_DOC';

    var summaryData = {
          questions: this.state.questions,
          answers: this.state.answers
        };
    var summaryObj = (<Summary data={summaryData} />);
    console.log("PDF=", PDF);

    //var kit = new PDF.new(html, :page_size => 'Letter');
    
    //creating a new PDF object
    var doc = new PDF();
    
    //creating a write stream 
    doc.pipe(fs.createWriteStream(filePath));
                
    //to write the content on the file system
    //doc.text(text, 100, 100);   //adding the text to be written, 
    doc.text(React.renderToStaticMarkup(summaryObj), 100, 100);   //adding the text to be written, 
    //doc.text(summaryObj.toString());   //adding the text to be written, 

    // Add another page
    doc.addPage({
          margins: {top: 50, bottom: 50, left: 72, right: 72}
        })
       .fontSize(25)
       .text('Here is some vector graphics...', 100, 100);

    // Draw a triangle
    doc.save()
       .moveTo(100, 150)
       .lineTo(100, 250)
       .lineTo(200, 250)
       .fill("#FF3300");

    // Apply some transforms and render an SVG path with the 'even-odd' fill rule
    doc.scale(0.6)
       .translate(470, -380)
       .path('M 250,75 L 323,301 131,161 369,161 177,301 z')
       .fill('red', 'even-odd')
       .restore();

    // Add some text with annotations
    doc.addPage()
       .fillColor("blue")
       .text('Here is a link!', 100, 100)
       .underline(100, 100, 160, 27, {color: "#0000FF"})
       .link(100, 100, 160, 27, 'http://google.com/');

    doc.end(); //we end the document writing.
    //console.log("doc=", doc);

    console.log("Page.handlePDFFile...", filePath, fileName);    
    // TODO: save to .pdf file    
    //this.savePDFFile(filePath, doc);
    $('#pdfFileName').val("");
*/
    //console.log("Page.handlePDFFile...", this.state.currentFile);
  },
  savePDFFile: function(filePath, doc) {
    //console.log("Page.savePDFFile...", filePath, outputPDF);
    $('#dialog-message em').text('');
    $('#dialog-message p').css("white-space", "normal")

    // write csv to the disk
    fs.writeFile(filePath, doc, function (err) {
      if (err) {
        console.error('Failed!', err);
        $('#dialog-message em').text($.t('voting.writeFileError'));
        return;
      };
      //console.log("It's saved!");
      var filename = filePath.replace(/^.*[\\\/]/, '');
      $('#dialog-message em').text($.t('voting.exportPdfFileMsg', { postProcess: "sprintf", sprintf: [filename]}));
    });

    $("#dialog-message").dialog({
      resizable: false,
      draggable: false,
      modal: true,
      dialogClass: "titlbar-only-close",
      width: 500,
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: $.t('button.ok'),
          click: function() {
            $(this).dialog("close");
          }
        }
      ]
    });
    return true;
  },
  handleExportCSV: function() {
    //console.log("Page.handleExportExcel...");
    // no file and go file dialog
    $('#csvFileDialog').trigger('click');
  },
  handleCSVFile: function(e) {
    e.preventDefault();
    //console.log("[Page.handleCSVFile]");
    
    // TODO: export Excel
    var questions = this.state.questions,
        answers = this.state.answers,
        qq = {},
        row1 = ["Question"],
        row2 = ["Answer"],
        output1 = [], output2 = [],
        trasnferAnswer = {
          null: '',
          '': '',
          0: '',
          1: 'True',
          2: 'False',
          3: 'Thumb-up',
          4: 'Thumb-down',
          5: 'A',
          6: 'B',
          7: 'C',
          8: 'D',
          9: 'E'
        };

    questions.map(function(question, i){
      //console.log("question=", question.num, question.qid, question);
      row1.push(question.num);
      //row2.push(question.correct);
      row2.push(trasnferAnswer[question.correct]);
      qq[question.qid] = question;
    });
    output1.push(row1, row2);
    output2.push(row1, row2);

    //console.warn("row1=", row1);
    //console.warn("row2=", row2);
    //console.warn("qq=", qq);
    //console.warn("output1=", output1);
    //console.warn("output2=", output2);

    var members = {};
    answers.map(function(answer, i){
      //console.log("answer=", answer.qid, answer.answer, answer);
      var cols1 = [], cols2 = [],
          num = qq[answer.qid].num-1,
          correct = row2[num+1];
      
      if (members.hasOwnProperty(answer.senderName)) {
        cols1 = members[answer.senderName][0];
        cols2 = members[answer.senderName][1];
      } else {
        var j = 0;
        while (++j <= questions.length){
          cols1.push(''); 
          cols2.push('');
        }
      }

      //console.log(num, correct, cols1, cols2);
      cols1[num] = trasnferAnswer[answer.answer];
      cols2[num] = (qq[answer.qid].type!=1 && qq[answer.qid].type!=6 && correct!="" && cols1[num]!="")?
                      ((cols1[num] == correct)? "O" : "X") : "";
      //if (cols1[num] != "" && cols1[num] == correct) console.log(num, correct, 'the same??', "cols1=", cols1[num], "cols2=", cols2[num]);
      members[answer.senderName] = [cols1, cols2];
    });

    //console.warn("members=", members);

    $.map(members, function(value, key) {
      //return <span>{$.t(summaryLibary[key].type)}</span>;
      var row = value[0];
      row.unshift(key);
      output1.push(row);
      row = value[1];
      row.unshift(key);
      output2.push(row);
      //console.warn("member=", key, value, row);
    });
    
    //console.warn("output1=", output1);
    //console.warn("output2=", output2);
    
    var outputXLS = '';
    output1.map(function(opt, i){
      //console.log("opt=", i, opt, opt.join('","'));
      outputXLS += '\"' + opt.join('\",\"') + '\"\n';
    });
    
    outputXLS += "\n\n\n";

    output2.map(function(opt, i){
      //console.log("opt2=", i, opt, opt.join('","'));
      outputXLS += '\"' + opt.join('\",\"') + '\"\n';
    });

    //console.warn("outputXLS=", outputXLS);
    
    var filePath = $('#csvFileName').val();
    var fileName = filePath.replace(/^.*[\\\/]/, '');
    //console.log("Page.handleCSVFile...", filePath);
    
    // TODO: save to .csv file
    this.saveCSVFile(filePath, outputXLS);

    $('#csvFileName').val("");
    //console.log("Page.handleCSVFile...", this.state.currentFile);
  },
  saveCSVFile: function(filePath, outputXLS) {
    //console.log("Page.saveStringFile...", filePath, outputXLS);
    $('#dialog-message em').text('');
    $('#dialog-message p').css("white-space", "normal")

    // write csv to the disk
    fs.writeFile(filePath, outputXLS, {encoding: 'utf8'}, function (err) {
      if (err) {
        console.error('Failed!', err);
        $('#dialog-message em').text($.t('voting.writeFileError'));
        return;
      };
      console.log("It's saved!");
      var filename = filePath.replace(/^.*[\\\/]/, '');
      $('#dialog-message em').text($.t('voting.exportCsvFileMsg', { postProcess: "sprintf", sprintf: [filename]}));
    });
    
    $("#dialog-message").dialog({
      resizable: false,
      draggable: false,
      modal: true,
      dialogClass: "titlbar-only-close",
      width: 500,
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: $.t('button.ok'),
          click: function() {
            $(this).dialog("close");
          }
        }
      ]
    });
    return true;
  },
  refreshStartButton: function() {
    console.log("Page.refreshStartButton...", getModerator());
    this.setState({moderator: getModerator()});
  },
  handleOnMax: function() {
    //console.log("[Page.handleOnMax]...");
    var h2 = $(window).height() - $('.header').outerHeight() - 25;
    $('#main-content').css('height', h2 + 'px');
    this.setState({maxWindow: true});
    $(".icon-fullscreen").addClass('hide');
    $(".icon-restore").removeClass('hide');
  },
  handleOnRestore: function() {
    //console.log("[Page.handleOnRestore]...");
    $('#main-content').css('height','570px');
    this.setState({maxWindow: false});
    $(".icon-fullscreen").removeClass('hide');
    $(".icon-restore").addClass('hide');
  },
  componentDidMount: function() {
    //console.log("[Page.componentDidMount]...");
    //window.addEventListener("resize", this.updateDimensions);
    win.on('maximize', this.handleOnMax);
    win.on('unmaximize', this.handleOnRestore);
    //win.once('close', this.handleOnCloseWin);
    win.on('close', this.handleOnCloseWin);
    //console.log("[Page.componentDidMount] win=", win);
  },
  componentWillUnmount: function() {
    //console.log("[Page.componentWillUnmount]...");
    //window.removeEventListener("resize", this.updateDimensions);
    win.removeListener('maximize', this.handleOnMax);
    win.removeListener('unmaximize', this.handleOnRestore);
    win.removeListener('close', this.handleOnCloseWin);
  },
  handleOnCloseWin: function() {
    //win.close(false);  // stop closing window
    //console.log("[Page.handleOnCloseWin]...", this.state.mode, this.state);
    if (this.state.onAirIndex > -1){
      //stopVoting();
      $('#dialog-message em').text($.t('voting.quitOnStartVotingMsg'));
      $('#dialog-message p').css("white-space", "normal")
      $("#dialog-message").dialog({
        resizable: false,
        draggable: false,
        modal: true,
        dialogClass: "titlbar-only-close",
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        buttons: [
          {
            text: $.t('button.ok'),
            click: function() {
              $(this).dialog("close");
            }
          },
        ]
      });
      return;
    }

    if (!this.checkFileChange()) {
      mainWin.show(true);
      win.close(true);  // real closing window
      return;
    }

    var p = this;
    $('#dialog-message em').text($.t('voting.quitConfirmMsg'));
    $('#dialog-message p').css("white-space", "normal")
    $("#dialog-message").dialog({
      resizable: false,
      draggable: false,
      modal: true,
      minWidth: 400,
      dialogClass: "titlbar-only-close",
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: $.t('button.yes'),
          click: function() {
            $(this).dialog("close");
            p.setState({goCloseWin: true});
            p.handleSaveFile();
          }
        },
        {
          text: $.t('button.no'),
          click: function() {
            $(this).dialog("close");
            mainWin.show(true);
            win.close(true);  // real closing window
          }
        },
        {
          text: $.t('button.cancel'),
          click: function() {
            $(this).dialog("close");
          }
        },
      ]
    });
  },
  render: function() {
    var questions = this.state.questions;
    var answers = this.state.answers;
    var toolbarData = {
          enableAllBtns: this.state.enableAllBtns,
          onCreate: this.handleNewFile,
          onOpen: this.handleOpenFile,
          onSave: this.handleSaveFile,
          onClearHistory: this.handleClearHistory,
          onExportPDF: this.handleExportPDF,
          onExportCSV: this.handleExportCSV,
          onAirIndex: this.state.onAirIndex
        };
    var sideData = {
          questions: this.state.questions,
          idx: this.state.idx,
          mode: this.state.mode,
          onAirIndex: this.state.onAirIndex,
          onSummary: this.handleClickSummary,
          onAdd: this.handleAddQuestion,
          onSwitch: this.switchQestion,
          onMax: this.state.maxWindow
        };
    var summaryData = {
          questions: this.state.questions,
          answers: this.state.answers,
          onMax: this.state.maxWindow
        };
    var sectionData = {
          question: this.state.currentQuestion,
          answers: this.state.answers,
          mode: this.state.mode,
          display: this.state.display,
          moderator: this.state.moderator,
          onAirIndex: this.state.onAirIndex,
          onUpdate: this.handleUpdateQuestion,
          onDelete: this.handleDelete,
          onCancel: this.handleCancel,
          onChangeDisplay: this.handleChangeDisplay,
          onAir: this.handleOnAir,
          onStop: this.handleOnStop,
          onMax: this.state.maxWindow
        };
    var showSection = (this.state.currentSummary)? React.createElement(Summary, {data: summaryData}) : React.createElement(Section, {data: sectionData}) ;
    var output = (questions.length > 0)? (React.createElement("div", {id: "main-content"}, React.createElement(Side, {data: sideData}), showSection)) : (React.createElement("div", {id: "main-content"}));
    
    return (React.createElement("div", null, 
        React.createElement("div", {className: "header"}, 
          React.createElement(Taskbar, {onClose: this.handleOnCloseWin}), 
          React.createElement(Toolbar, {data: toolbarData, currentFile: this.state.currentFile})
        ), 
        output, 
        React.createElement("div", null, 
          React.createElement("input", {type: "hidden", id: "newFileName", onClick: this.handleSaveNewFile}), 
          React.createElement("input", {type: "hidden", id: "openFileName", onClick: this.handleOpenExistedFile}), 
          React.createElement("input", {type: "hidden", id: "refreshStart", onClick: this.refreshStartButton}), 
          React.createElement("input", {type: "hidden", id: "votingStartFail", onClick: this.handleOnStop}), 
          React.createElement("input", {type: "hidden", id: "receiveAnswer", onClick: this.refreshAnswer}), 
          React.createElement("input", {type: "hidden", id: "csvFileName", onClick: this.handleCSVFile}), 
          React.createElement("input", {type: "hidden", id: "pdfFileName", onClick: this.handlePDFFile})
        )
      )
    );
  }
});

ReactDOM.render(
  React.createElement(Page, null),
  document.getElementById('page')
);