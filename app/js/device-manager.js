var rvaClient, panel;
var async = require('async');
var gui = require('nw.gui'),
  win = gui.Window.get();

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function() {

  var language = getParameterByName('setLng');
  //var language = window.navigator.language;
  //var language = 'es'; //zh-TW zh-CN en-US en-GB ja fr es ru
  translate(language);

  $(".icon-minimize").off().click(function() {
    win.minimize();
  });
  $('.icon-close').off().click(function() {
    // Close self
    window.close();
  });

  panel = $('#main-content').devicePanel(); // init device panel

  console.log($('#wifi-type-1'), $('#hdmicec'));

  checkResize();
  win.setResizable(false);
  document.body.addEventListener('dragover', function(e) {
    e.preventDefault();
    e.stopPropagation();
  }, false);
  document.body.addEventListener('drop', function(e) {
    e.preventDefault();
    e.stopPropagation();
  }, false);

  var arr = ['fr', 'es', 'ru'];
  if (jQuery.inArray(language, arr) > -1) {
    $('#main-content section .cols > span:first-child').css('width', '100px');
  }

  //printMemory();
});

win.on('closed', function() {
  //console.info('Destory Events...', rvaClient);
  rvaClient.removeListener('SETTING_CHANGE_SUCCESS', settingChangeSuccess);
  rvaClient.removeListener('REBOOT_DEVICE_NEEDED', rebootDeviceNeeded);
  rvaClient.removeListener('SETTINGS_TIMEOUT', settingsTimeout);
  rvaClient.removeListener('SETTING_FIRMWARE_IS_UP_TO_DATE', settingFirmwareIsUpToDate);
  rvaClient.removeListener('UPGRADE_IN_PROGRESS', upgradeInProgress);
  rvaClient.removeListener('SETTING_NO_PERMISSION', settingNoPermission);
  rvaClient.removeListener('NEW_PASSWORD_IS_NOT_MEET_REQUIREMENT', newPasswordIsNotMeetRequirement);
  rvaClient.removeListener('SERVER_NOT_FOUND', serverNotFound);
  rvaClient.removeListener('UNKNOWN_REASONS', unknownReasons);
  rvaClient.removeListener('RESET_RVA_NOT_ALLOWED_WITH_ONLINE_USERS', resetRVANotAllowedWithOnlineUsers);
  rvaClient.removeListener('RESET_DEVICE_NOT_ALLOWED_WITH_ONLINE_USERS', resetDeviceNotAllowedWithOnlineUsers);
  rvaClient.removeListener('CHANGE_WIFI_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS', changeWifiSettingsNotAllowedWithOnlineUsers);
  rvaClient.removeListener('CHANGE_DISPLAY_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS', changeDisplaySettingsNotAllowedWithOnlineUsers);
  rvaClient.removeListener('CHANGE_DEV_NAME_NOT_ALLOWED_WITH_ONLINE_USERS', changeDevNameNotAllowedWithOnlineUsers);
  rvaClient.removeListener('CHANGE_LANGUAGE_NOT_ALLOWED_WITH_ONLINE_USERS', changeLanguageNotAllowedWithOnlineUsers);
  rvaClient.removeListener('CHANGE_PASSWORD_NOT_ALLOWED_WITH_ONLINE_USERS', changePasswrodNotAllowedWithOnlineUsers);
  rvaClient.removeListener('UPGRADE_NOT_ALLOWED_WITH_ONLINE_USERS', upgradeNotAllowedWithOnlineUsers);
  rvaClient.removeListener('invalidIp', invalidIp);
});

function printMemory() {
  function printMem() {
    var mem = process.memoryUsage(); // Initial usage
    //Object {rss: 140832768, heapTotal: 45956736, heapUsed: 32527024}
    console.debug("Memory-rss: " + (mem.rss / 1024) + ' KB');
    //console.debug("Memory-heapTotal: " + (mem.heapTotal/1024) + ' KB');
    //console.debug("Memory-heapUsed: " + (mem.heapUsed/1024) + ' KB');
    //console.debug("Memory-heapTotal: " + formatBytes(mem.rss));
    //console.debug("Memory-heapUsed: " + formatBytes(mem.heapUsed));
    mem = null;
  }
  window.setInterval(printMem, 5000);
  //window.clearInterval(printMem);
}

/**
 * init RVA info
 */
function init(ip, rva) {
  console.log('init...', ip, rva);
  var rvaIP = $.trim(ip);
  rvaClient = rva;

  rvaClient.on('SETTING_CHANGE_SUCCESS', settingChangeSuccess);
  rvaClient.on('REBOOT_DEVICE_NEEDED', rebootDeviceNeeded);
  rvaClient.on('SETTINGS_TIMEOUT', settingsTimeout);
  rvaClient.on('SETTING_FIRMWARE_IS_UP_TO_DATE', settingFirmwareIsUpToDate);
  rvaClient.on('UPGRADE_IN_PROGRESS', upgradeInProgress);
  rvaClient.on('SETTING_NO_PERMISSION', settingNoPermission);
  rvaClient.on('NEW_PASSWORD_IS_NOT_MEET_REQUIREMENT', newPasswordIsNotMeetRequirement);
  rvaClient.on('SERVER_NOT_FOUND', serverNotFound);
  rvaClient.on('UNKNOWN_REASONS', unknownReasons);
  rvaClient.on('RESET_RVA_NOT_ALLOWED_WITH_ONLINE_USERS', resetRVANotAllowedWithOnlineUsers);
  rvaClient.on('RESET_DEVICE_NOT_ALLOWED_WITH_ONLINE_USERS', resetDeviceNotAllowedWithOnlineUsers);
  rvaClient.on('CHANGE_WIFI_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS', changeWifiSettingsNotAllowedWithOnlineUsers);
  rvaClient.on('CHANGE_DISPLAY_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS', changeDisplaySettingsNotAllowedWithOnlineUsers);
  rvaClient.on('CHANGE_DEV_NAME_NOT_ALLOWED_WITH_ONLINE_USERS', changeDevNameNotAllowedWithOnlineUsers);
  rvaClient.on('CHANGE_LANGUAGE_NOT_ALLOWED_WITH_ONLINE_USERS', changeLanguageNotAllowedWithOnlineUsers);
  rvaClient.on('CHANGE_PASSWORD_NOT_ALLOWED_WITH_ONLINE_USERS', changePasswrodNotAllowedWithOnlineUsers);
  rvaClient.on('UPGRADE_NOT_ALLOWED_WITH_ONLINE_USERS', upgradeNotAllowedWithOnlineUsers);
  rvaClient.on('invalidIp', invalidIp);

  $('#deviceIP').off().click(function(e) {
    e.preventDefault();
    panel.devicePanel("normalIpHandler");
  });
  $('#rvaIPInput').val(rvaIP);
  $('#deviceIP > span').text((rvaIP != '') ? rvaIP : i18n.t('device.clickToSelect'));

  if (!rvaIP || rvaIP == '' || rvaIP == null) {
    wrongIpHandler();
    rvaIP = null;
    return;
  }

  if (!validateIPaddress(rvaIP)) {
    console.error("invalidIPaddress...", rvaIP);
    invalidIp();
    rvaIP = null;
    return;
  }

  console.debug("start getRVASettings...");
  getRVASettings(function(options) {
    console.debug("getRVASettings...", rvaIP, rvaClient, options);
    panel.devicePanel("refresh", rvaIP, rvaClient, options);
  });

}

function validateIPaddress(ipaddress) {
  if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {
    return (true)
  }
  console.error("Invalid IP address: ", ipaddress);
  return (false)
}

var invalidIp = function() {
  console.log('invalidIp');
  wrongIpHandler();
}

var settingChangeSuccess = function() {
  console.log('SETTING_CHANGE_SUCCESS');
  closeWaiting(); // close waiting
  panel.devicePanel("updateChange");
  panel.devicePanel("dialogHandler", $.t('device.successfulCmd'));
};

var rebootDeviceNeeded = function() {
  console.log('REBOOT_DEVICE_NEEDED');
  closeWaiting(); // close waiting
  rebootReminder();
}

var settingsTimeout = function() {
  console.log('SETTINGS_TIMEOUT');
  closeWaiting();
  wrongIpHandler();
}

var settingFirmwareIsUpToDate = function() {
  console.log('SETTING_FIRMWARE_IS_UP_TO_DATE');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.upToDate'));
}

var settingNoPermission = function() {
  console.log('SETTING_NO_PERMISSION');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.noPermission'));
}

var resetRVANotAllowedWithOnlineUsers = function() {
  console.log('RESET_RVA_NOT_ALLOWED_WITH_ONLINE_USERS');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.errorWithOnLineUser'));
}

var resetDeviceNotAllowedWithOnlineUsers = function() {
  console.log('RESET_DEVICE_NOT_ALLOWED_WITH_ONLINE_USERS');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.errorWithOnLineUser'));
}

var changePasswrodNotAllowedWithOnlineUsers = function() {
  console.log('CHANGE_PASSWORD_NOT_ALLOWED_WITH_ONLINE_USERS');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.errorWithOnLineUser'));
}

var newPasswordIsNotMeetRequirement = function() {
  console.log('NEW_PASSWORD_IS_NOT_MEET_REQUIREMENT');
  closeWaiting();
  // password length 3<= length <= 12
  panel.devicePanel("dialogHandler", $.t('device.newPasswordNotMeetRequirement') + ' ' + $.t('device.passwordLengthError'));
}

var changeWifiSettingsNotAllowedWithOnlineUsers = function() {
  console.log('CHANGE_WIFI_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.errorWithOnLineUser'));
}

var changeDevNameNotAllowedWithOnlineUsers = function() {
  console.log('CHANGE_DEV_NAME_NOT_ALLOWED_WITH_ONLINE_USERS');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.errorWithOnLineUser'));
}

var serverNotFound = function() {
  console.log('SERVER_NOT_FOUND');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.timeOut'));
}

var unknownReasons = function() {
  console.log('UNKNOWN_REASONS');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.unknownReason'));
}

var upgradeNotAllowedWithOnlineUsers = function() {
  console.log('UPGRADE_NOT_ALLOWED_WITH_ONLINE_USERS');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.errorWithOnLineUser'));
}

var changeDisplaySettingsNotAllowedWithOnlineUsers = function() {
  console.log('CHANGE_DISPLAY_SETTINGS_NOT_ALLOWED_WITH_ONLINE_USERS');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.errorWithOnLineUser'));
}

var changeLanguageNotAllowedWithOnlineUsers = function() {
  console.log('CHANGE_LANGUAGE_NOT_ALLOWED_WITH_ONLINE_USERS');
  closeWaiting();
  panel.devicePanel("dialogHandler", $.t('device.errorWithOnLineUser'));
}

var upgradeInProgress = function() {
  console.log('UPGRADE_IN_PROGRESS');
  closeWaiting();
  upgradeReboot();
}

/**
 * getRVASettings
 */
function getRVASettings(cb) {
  waiting();
  var rvaIP = $('#rvaIPInput').val();
  var options = {
    rvaVersion: 1.6,
    wifiType: 0,
    ssid: '',
    display: 1,
    room: 'Meeing Room Name',
    language: 0,
    enablePassword: false,
    rvaPassword: '',
    resolution: [], // added after RVA 2.1 (included)
    // below is after RVA 2.2.0 (included)
    channel: 6,
    channelList: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], // [1,2,3,4,...,11,12,13] US: 11, the rest: 13
    routing: false, // false: Disable true: Enable
    hdmiCec: true, // false: Disable true: Enable
  };
  async.series([
    function (done) {
      rvaClient.getRvaVersion(rvaIP, function(result){
        console.log('rvaClient.getRvaVersion...');
        options.timeout = result.timeout;
        if (options.timeout == true) {
          console.log('rvaClient.getRvaVersion timeout...');
          wrongIpHandler();
          cb(options);
        }
        else {
          console.log('RvaVersion: ', result);
          options.rvaVersion = result;
          done();
        }
      })
    },
    function(done) {
      rvaClient.getAllData(options.rvaVersion, rvaIP, function(result) {
        console.log('rvaClient.getAllData...');
        console.log(result);
        options.timeout = result.timeout;
        options.channelList = result.channelList;
        options.enablePassword = result.enablePassword;
        options.rvaPassword = result.rvaPassword;
        options.display = result.display;
        options.language = result.language;
        options.rvaEdition = result.rvaEdition;
        options.hdmiCec = result.hdmiCec;
        options.rvaVersion = result.rvaVersion;
        options.bspVersion = result.bspVersion;
        options.nvcModel = result.nvcModel;
        options.room = result.room;

        if (parseFloat(options.rvaVersion) <= 2.0) {
          options.resolution = [{
            key: "0",
            value: $.t('device.autoConfig')
          }, {
            key: "1",
            value: "XGA (1024x768)"
          }, {
            key: "2",
            value: "WXGA (1280x800)"
          }, {
            key: "3",
            value: "720p (1280x720)"
          }, {
            key: "4",
            value: "1080p (1920x1080)"
          }];
        }

        if (result.timeout === true) {
          console.log('getAllData timeout...');
          wrongIpHandler();
          cb(options);
        } else {
          options.wifiType = result.wifiType;
          options.ssid = result.ssid;
          done();
        }
      });
    },
    function(done) {
      console.log('rvaClient.getNetworkInterfaceInfo...');
      rvaClient.getNetworkInterfaceInfo(rvaIP, function(networkSettings) {
        options.timeout = networkSettings.timeout;
        if (options.timeout == true) {
          console.log('getNetworkInterfaceInfo timeout...');
          wrongIpHandler();
          cb(options);
        } else {
          options.wifiType = networkSettings.wifiType;
          options.ssid = networkSettings.ssid;
          done();
        }
      });
    },
    function(done) {
      console.log('rvaClient.getNetworkInterface1Info...');
      if (parseFloat(options.rvaVersion) > 2.0) {
        rvaClient.getNetworkInterface1Info(rvaIP, function(hotspotSettings) {
          options.timeout = hotspotSettings.timeout;
          if (options.timeout == true) {
            console.log('getNetworkInterface1Info timeout...');
            wrongIpHandler();
            cb(options);
          } else {
            options.channel = hotspotSettings.channel;
            options.routing = hotspotSettings.enabled;
            console.log('hotspotSettings...');
            console.log(options);
            done();
          }
        });
      } else {
        options.channel = null;
        options.routing = null;
          done();
      }
    },
    function(done) {
      console.log('rvaClient.getSupportedResolutions...');
      rvaClient.getSupportedResolutions(rvaIP, function(displaySetting) {
        console.log('displaySetting', displaySetting);
        options.timeout = displaySetting.timeout;
        if (options.timeout === true) {
          console.log('displaySetting timeout');
          wrongIpHandler();
          options.resolution = [{
              key: "0",
              value: $.t('device.autoConfig')
            }, {
              key: "1",
              value: "XGA (1024x768)"
            }, {
              key: "2",
              value: "WXGA (1280x800)"
            }, {
              key: "3",
              value: "720p (1280x720)"
            }, {
              key: "4",
              value: "1080p (1920x1080)"
            },
            //{key: "5", value: "WUXGA (1920x1200)"},
            //{key: "6", value: "4K (3840x2160)"}
          ];
          cb(options);
        }

        var resolutionArray = displaySetting.value;
        console.log('Resolutions: ', resolutionArray);
        var arrmap = {
          "0": $.t('device.autoConfig'),
          "1": "XGA (1024x768)",
          "2": "WXGA (1280x800)",
          "3": "720p (1280x720)",
          "4": "1080p (1920x1080)",
          "5": "WUXGA (1920x1200)",
          "6": "4K (3840x2160)",
        };
        var item = {};
        var result = [];
        for (var k = 0; k < resolutionArray.length; k++) {
          //console.log('resolutionArray[k]: ', resolutionArray[k]);
          result.push({
            key: resolutionArray[k],
            value: arrmap[resolutionArray[k]]
          });
        }
        options.resolution = result;
        //console.log('result: ', result);
        cb(options);
        done();
      });
    }
  ]);
}

/**
 * waiting
 */
var waitingDialog = null;

function waiting() {
  console.log("Waiting...");
  $("#dialog-waiting").dialog("open");
  waitingDialog = setTimeout(function() {
    console.warn("Run waiting timeout...");
    wrongIpHandler();
  }, 10000);
}

/**
 * close waiting dialog
 */
function closeWaiting() {
  console.log("closeWaiting...");
  if (waitingDialog != null) {
    //console.log("clearTimeout...");
    clearTimeout(waitingDialog);
  }
  waitingDialog = null;
  $("#dialog-waiting").dialog("close");
}

/**
 * Wrong IP Handler
 */
function wrongIpHandler() {
  panel.devicePanel("wrongIpHandler");
}

/**
 * reboot RVA
 */
function rebootReminder() {
  console.log("rebootReminder...");
  panel.devicePanel("rebootReminder");
}

function upgradeReboot() {
  console.log("upgradeReboot...");
  $('#dialog-reboot > p').text($.t('device.upgradeRVA'));
  panel.devicePanel("rebootReminder");
}


/**
 * translation
 */
function translate(language) {
  var opts = {
    lng: language,
    fallbackLng: 'en-US',
    debug: true,
    resGetPath: "../locales/__ns__-__lng__.json"
  };

  i18n.init(opts, function(t) {
    $(document).i18n();
  });
}

function checkResize() {
  var win = gui.Window.get();
  var w = $('#device-manager').width(),
    h = $('#device-manager').height();

  if (gui.App.manifest.window.frame) {
    w = w + 20;
    h = h + 80;
  } else {
    w = w + 4;
    h = h + 4;
  }
  win.resizeTo(w, h);

  var x = Math.round((screen.availWidth - w) / 2),
    y = Math.round((screen.availHeight - h) / 2);
  win.moveTo(x, y);
  win.show(true);
  //console.log('checkResize...', screen.availWidth, screen.availHeight, window.outerWidth, window.outerHeight, x, y);
}
