var rvaClient;

$.widget("custom.devicePanel", {
  // Default options
  options: {
    rvaIP: '',
    rvaOptions: {
      rvaVersion: 2.0,
      wifiType: 0,  // 0/1/2
      ssid: '',
      display: 1,
      room: '',
      language: 0,
      enablePassword: false,
      rvaPassword: '',
    },
    changed: {}
  },
  _create: function _create() {
    $("#dialog-waiting").dialog({
      autoOpen: false,
      closeOnEscape: false,
      dialogClass: "no-close",
      resizable: false,
      modal: true
    });
  },
  _init: function init() {
    this._disableSidebar();
    this._disableTabs();
    this.options.changed = {};
  },
  refresh: function refresh(ip, rva, data) {
    //console.log("refresh...");

    rvaClient = rva;
    this.options.rvaIP = ip;
    this.options.rvaOptions = data;
    this.options.rvaOptions.rvaVersion = parseFloat(data.rvaVersion)
    this.options.changed = {};

    // left sidebar
    this._enableSidebar();

    // all tabs & settings
    this._enableTabs();

    closeWaiting();

  },
  updateChange: function updateChange(){
    //console.info('updateChange... this.options.changed=', this.options.changed);
    var changed = this.options.changed;

    if (changed.room) {
      this.options.rvaOptions.room = changed.room;
      $('#room-apply').attr('disabled', true);
      changed = null;
      this.options.changed = {};
      return;
    }
    if (changed.enablePassword != undefined) {
      this.options.rvaOptions.enablePassword = changed.enablePassword;
      this.options.rvaOptions.rvaPassword = changed.rvaPassword;
      $('#protection-form button').attr("disabled",true);
      changed = null;
      this.options.changed = {};
      return;
    }
    changed = null;
  },
  _enableSidebar: function _enableSidebar(){
    $('#main-content > nav').show();
    (this.options.rvaOptions.rvaVersion == 1.6)? $('.upgrade-function').addClass('enabled') : $('.upgrade-function').removeClass('enabled');

    // enable click sidebar
    $('nav > div.enabled').click(function() {
      $('nav > div').removeClass('active');
      $(this).addClass('active');
      $('section > div').hide().eq($(this).index()).show();
    });
    // default: click first tab of sidebar
    $('nav > div').eq(0).trigger('click');
  },
  _disableSidebar: function _disableSidebar(){
    $('#main-content > nav').hide();
    // disable click sidebar
    $('nav > div').removeClass('active').off();
    $('section > div').hide();
  },
  _enableTabs: function _enableTabs(){
    $('#main-content > section').show();
    // enable all tabs & settings
    this._enableTab1();
    this._enableTab2();
    this._enableTab3();
    this._enableTab4();
    //$($("nav > div")[1]).trigger("click");
  },
  _disableTabs: function _disableTabs(){
    $('#main-content > section').hide();
    // disable all tabs & settings
    this._disableTab1();
    this._disableTab2();
    this._disableTab3();
    this._disableTab4();
  },
  _enableTab1: function _enableTab1(){
    var instance = this;
    var rvaIP = instance.options.rvaIP;

    $('#quick-reset').click(function(e) {
      e.preventDefault();
      $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        buttons: [
          {
            text: i18n.t('button.no'),
            click: function() {
              $(this).dialog("close");
            }
          },
          {
            text: i18n.t('button.yes'),
            click: function() {
              console.log('Quick Reset - restart software', rvaIP);
              $(this).dialog("close");
              instance.needToValidatePw(function() {
                rvaClient.send_CMD_RESET_RVA(rvaIP);
                $('#dialog-reboot > p').text($.t('device.rebootSoftwareMsg'));
              });
            }
          },
        ]
      });
    });
    $('#full-reset').click(function(e) {
      e.preventDefault();
      $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        buttons: [
          {
            text: i18n.t('button.no'),
            click: function() {
              $(this).dialog("close");
            }
          },
          {
            text: i18n.t('button.yes'),
            click: function() {
              console.log('Full Reset - restart device', rvaIP);
              $(this).dialog("close");
              instance.needToValidatePw(function() {
                rvaClient.send_CMD_RESET_DEVICE(rvaIP);
                $('#dialog-reboot > p').text($.t('device.rebootDeviceMsg'));
              });
            }
          },
        ]
      });
    });
  },
  _disableTab1: function _disableTab1(){
    $('#quick-reset').off();
    $('#full-reset').off();
  },
  _enableTab2: function _enableTab2(){
    // Tab 2: RVA infomation
    var instance = this;
    // get RVA options
    var options = instance.options.rvaOptions;
    //console.debug('_enableTab2...instance.options=', instance.options);
    var rvaIP = instance.options.rvaIP;
    console.debug('_enableTab2', options.rvaVersion, rvaIP, options);

    // WiFi hotspot settings
    var enableWifiHotspotSettings = function () {
      $('.wifi.hotspot').show();
      (options.rvaVersion >= 2.2)? $('.v220').show() : $('.v220').hide();
      $('.wifi.client').hide();
      $('.wifi.off').hide();
    }

    // WiFi client settings
    var enableWifiClientSettings = function () {
      $('.wifi.hotspot').hide();
      $('.wifi.client').show();
      $('.wifi.off').hide();
    }

    // WiFi Off
    var enableWifiOffSettings = function () {
      $('.wifi.hotspot').hide();
      $('.wifi.client').hide();
      $('.wifi.off').show();
    }

    // WiFi Type
    $('input[name="wifiType"][value="' + options.wifiType + '"]').prop("checked", true);
    $('input[name="wifiType"]').change(enableWifiApply);
    $('#wifi-type-0').change(enableWifiClientSettings);   // WiFi client
    $('#wifi-type-1').change(enableWifiHotspotSettings);  // WiFi hotspot
    if (options.rvaVersion < 2) {
      $('#wifi-disable').hide();  // support after RVA 2.0
    }
    else {  // after RVA 2.0
      $('#wifi-disable').show();
      $('#wifi-type-2').change(enableWifiOffSettings);
    }

    if (options.rvaVersion >= 2.2) {
      $('.v220').show();

      // WiFi hotspot setting
      var channelOption = $('select[name="channel"]').empty();
      options.channelList.forEach(function(item) {
        channelOption.append($('<option>').val(item).text(item));
      });
      channelOption.val(options.channel);
      $('input[name="routing"][value="true"]').prop("checked", options.routing);
      $('#hotspot-apply').off().click(function (e) {
        e.preventDefault();
        var wifiType = $('input[name="wifiType"]:checked').val(),
            channel = $('select[name="channel"]').val(),
            routing = ($('input[name="routing"]:checked').val())? true : false;
        //console.log('Apply network setting...', wifiType, channel, routing);
        instance.needToValidatePw(function() {
          $('#dialog-reboot > p').text($.t('device.rebootMsgChangeSetting'));
          console.debug('channel, routing, rvaIP ' , channel, routing, rvaIP);
          // TODO: call Eddie, set hotspot mode and settings
          rvaClient.sendHotspotSettings(channel, routing, rvaIP); // will reboot
        });
      });

      // HDMI-CEC
      $('input[name="hdmicec"]').off().change(function () {
        //console.log("Click...", $(this));
        $('#hdmicec-apply').removeAttr('disabled');
      });
      $('input[name="hdmicec"][value="'+ options.hdmiCec +'"]').prop("checked", true);
      $('#hdmicec-apply').off().click(function (e) {
        e.preventDefault();
        var hdmicec = $('input[name="hdmicec"]:checked').val();
        console.log('Apply HDMI-CEC setting...', hdmicec);
        instance.needToValidatePw(function() {
          rvaClient.setHdmiCec(hdmicec, rvaIP); // no reboot
          /*
          //ask Rex it will reboot or not
          instance.options.changed = {
            'hdmiCec': hdmicec
          };
          */
        });
      });

    }
    else {  // below to RVA 2.2, hide sth. and show sth.
      $('.v220').hide();
      $('#hotspot-apply').off().click(function (e) {
        e.preventDefault();
        console.log('Apply network setting...', 1);
        instance.needToValidatePw(function() {
          $('#dialog-reboot > p').text($.t('device.rebootMsgChangeSetting'));
          rvaClient.sendWifiSettings("", 1, "", rvaIP); // will reboot
        });
      });
    }

    // WiFi client setting
    var enableWifiApply = function () {
      $('#wifi-client-apply').removeAttr('disabled');
    }
    $('input[name="ssid"]').val(options.ssid).off().keyup(enableWifiApply);
    $('input[name="key"]').prop('disabled',!$('#wifi-secure').prop('checked')).off().keyup(enableWifiApply);
    $('input[name="wifiSecure"][value="' + options.wifiSecure + '"]').prop("checked", true);
    $('#wifi-secure').off().change(function(){
      var checked = $(this).prop('checked');
      $('input[name="key"]').prop('disabled',!checked);
      (checked)? $('input[name="key"]').focus() : null;
      enableWifiApply();
      checked = null;
    });
    $('#wifi-client-apply').off().click(function (e) {
      e.preventDefault();
      var wifiType = $('input[name="wifiType"]:checked').val();
      var ssid = ($('input[name="ssid"]').prop('disabled'))? '' : $('input[name="ssid"]').val();
      var wifiSecure = ($('input[name="ssid"]').prop('disabled'))? false : $('input[name="wifiSecure"]:checked').val();
      var key = (wifiSecure)? $('input[name="key"]').val() : '';
      console.log('Apply network setting...', wifiType, ssid, key, wifiSecure, rvaIP);
      instance.needToValidatePw(function() {
        $('#dialog-reboot > p').text($.t('device.rebootMsgChangeSetting'));
        rvaClient.sendWifiSettings(ssid, 0, key, rvaIP); // will reboot
      });
    });

    // WiFi OFF setting
    $('#wifi-off-apply').off().click(function (e) {
      e.preventDefault();
      console.log('Apply network setting...', 2);
      instance.needToValidatePw(function() {
        $('#dialog-reboot > p').text($.t('device.rebootMsgChangeSetting'));
        rvaClient.sendWifiSettings("", 2, "", rvaIP); // will reboot
      });
    });


    // set current setting
    (options.wifiType == 2) ?
      enableWifiOffSettings() :
      ((options.wifiType == 1) ?
        enableWifiHotspotSettings() :
        enableWifiClientSettings());

    // Meeting Room Name
    $('input[name="room"]').val(options.room).off().keyup(function () {
      $('#room-apply').removeAttr('disabled');
    });
    $('#room-apply').off().click(function (e) {
      e.preventDefault();
      //console.log('Apply room setting...');
      instance.needToValidatePw(function() {
        var newRoomName = $('input[name="room"]').val();
        rvaClient.sendDeviceName(newRoomName, rvaIP);
        instance.options.changed = {
          'room': newRoomName
        };
      });
    });


    // Display
    if (options.resolution.length > 0) {
      var element = $('select[name="display"]');
      element.empty();
      $.each(options.resolution, function( index, obj ) {
        element.append($('<option>').val(obj.key).text(obj.value));
      });
      element.val(options.display).off().change(function () {
        $('#display-apply').removeAttr('disabled');
      });
      $('#display-apply').off().click(function (e) {
        e.preventDefault();
        console.log('Apply display setting...');
        instance.needToValidatePw(function() {
          //console.log('Apply display=', $('select[name="display"]').val());
          $('#dialog-reboot > p').text($.t('device.rebootMsgChangeSetting'));
          rvaClient.sendDisplaySetting($('select[name="display"]').val(), rvaIP); // will reboot
        });
      });
      $('.resolution-block').show();
    }
    else {
      $('.resolution-block').hide();
    }

    // Language
    $('select[name="language"]').val(options.language).off().change(function () {
      $('#language-apply').removeAttr('disabled');
    });
    $('#language-apply').off().click(function (e) {
      e.preventDefault();
      console.log('Apply language setting...');
      instance.needToValidatePw(function() {
        $('#dialog-reboot > p').text($.t('device.rebootMsgChangeSetting'));
        rvaClient.sendLanguage($('select[name="language"]').val(), rvaIP); // will reboot
      });
    });
    $('.language-block').show();

  },
  _disableTab2: function _disableTab2(){
    // Tab 2: RVA infomation
    $('.resolution-block').hide();
    $('.language-block').hide();
  },
  _enableTab3: function _enableTab3(){
    // Tab 3: RVA Protection
    var instance = this;
    var rvaIP = instance.options.rvaIP;
    var enablePassword = instance.options.rvaOptions.enablePassword;

    // default status
    $('input[name="protectionEnabled"][value="' + enablePassword + '"]').prop('checked',true);
    if (enablePassword) {
      $('.pw-inputs').show();
    }
    else {
      $('.pw-inputs').hide();
    }
    $('#protection-form button').attr("disabled",true);

    var Key = $('input[name="protectionKey"]')[0],
        KeyConfirm = $('input[name="protectionKeyConfirm"]')[0];

    $('#protection-enabled').off().click(function() {
      $(Key).attr('data-i18n', '[placeholder]device.protectionKey').i18n();
      $(KeyConfirm).attr('data-i18n', '[placeholder]device.protectionKeyConfirm').i18n();
      $('#protection-form button').attr("disabled",false);
      $('.pw-inputs').show();
    });

    $('#protection-disabled').off().click(function() {
      $(Key).removeAttr('placeholder').val("");
      $(KeyConfirm).removeAttr('placeholder').val("");
      $('#protection-form button').attr("disabled",false);
      $('.pw-inputs').hide();
    });

    $(Key).blur(function() {
      $('#protectionKeyMsg').text('');
      $(this).removeClass('error');
      $('#protection-form button').attr("disabled",false);
    });

    $(KeyConfirm).blur(function() {
      $('#protectionKeyConfirmMsg').text('');
      $(this).removeClass('error');
      $('#protection-form button').attr("disabled",false);
    });

    $('#protection-cancel').off().click(function(e) {
      e.preventDefault();
      $(Key).val("");
      $(KeyConfirm).val("");
      $('input[name="protectionEnabled"][value="' + enablePassword + '"]').prop('checked',true);
      if (enablePassword) {
        $('.pw-inputs').show();
      }
      else {
        $('.pw-inputs').hide();
      }
      $('#protection-form button').attr("disabled",true);
      return false;
    });

    $('#protection-form').off().submit(function(e) {
      e.preventDefault();
      var enable = $('input[name=protectionEnabled]:checked').val();
      //console.log('Submit protection...  enable =', enable);

      //Submit protection
      var newpw1 = $(Key).val(),
          newpw2 = $(KeyConfirm).val();

      if(enable=="true"){

        // check Key & KeyConfirm
        if (newpw1==""){
          $('#protectionKeyMsg').text($.t('login.required'));
          $(Key).addClass('error').focus();
          return false;
        }
        if (newpw2==""){
          $('#protectionKeyConfirmMsg').text($.t('login.required'));
          $(KeyConfirm).addClass('error').focus();
          return false;
        }
        if (newpw1!=newpw2){
          $('#protectionKeyMsg').text($.t('device.notEqual'));
          $(Key).addClass('error').focus();
          return false;
        }

        instance.needToValidatePw(function() {
          //Enable password
          rvaClient.setSettingsPassword(newpw1, rvaIP);
          enablePassword = true;
          instance.options.changed = {
            enablePassword: true,
            rvaPassword: newpw1
          };
        });

      }
      else {
        instance.needToValidatePw(function() {
          waiting();
          //Disable password
          rvaClient.disableSettingsPassword(rvaIP);
          enablePassword = false;
          instance.options.changed = {
            enablePassword: false,
            rvaPassword: ''
          };
        });
      }

      return false;
    });
  },
  _disableTab3: function _disableTab3(){
    // Tab 3: RVA Protection
    $('#protection-form button').attr("disabled",true);
  },
  _enableTab4: function _enableTab4(){
    var instance = this;
    var rvaIP = instance.options.rvaIP;
    // Tab 4: RVA Upgrade
    $('#upgrade').click(function(e) {
      e.preventDefault();
      console.log('Click to upgrade device...');
      instance.needToValidatePw(function() {
        rvaClient.rvaUpgrade(rvaIP); // will reboot
      });
    });
  },
  _disableTab4: function _disableTab4(){
    // Tab 4: RVA Upgrade
    $('#upgrade').off();
  },
  normalIpHandler: function normalIpHandler(){
    console.log('instance.normalIpHandler...');

    closeWaiting();

    var instance = this;
    $("#dialog-ip").dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      open: function( event, ui ) {
        $('#rvaIPInput').val(instance.options.rvaIP).removeClass('error');
        $(this).find('.errMsg').empty().end();
      },
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: i18n.t('button.cancel'),
          click: function() {
            $(this).dialog("close");
          }
        },
        {
          text: i18n.t('button.connect'),
          click: function() {
            //console.log('Click connect...');
            var rvaIP = $('#rvaIPInput').val();
            $('#deviceIP > span').text(rvaIP);
            instance.options.rvaIP = rvaIP;
            if (rvaIP=="" || !validateIPaddress(rvaIP)){
              $(this).dialog("close");
              instance.wrongIpHandler();
              return;
            }
            $(this).dialog("close");
            //console.debug('Ask RVA getRVASettings...', rvaIP, instance);
            getRVASettings(function (options) {
              instance.refresh(rvaIP, rvaClient, options);
            });
          }
        },
      ]
    });
  },
  wrongIpHandler: function wrongIpHandler(){
    console.log('instance.wrongIpHandler...', this.options.rvaIP);

    closeWaiting();

    var instance = this;
    $("#dialog-ip").dialog({
      autoOpen: true,
      closeOnEscape: false,
      resizable: false,
      modal: true,
      open: function( event, ui ) {
        $('#rvaIPInput').val(instance.options.rvaIP).addClass('error');
        $(this).find('.errMsg').empty().text(i18n.t('device.wrongIPMessage')).end();
      },
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: i18n.t('button.close'),
          click: function() {
            window.close();
          }
        },
        {
          text: i18n.t('button.ok'),
          click: function() {
            //console.log('Click ok...');
            var rvaIP = $('#rvaIPInput').val();
            $('#deviceIP > span').text(rvaIP);
            instance.options.rvaIP = rvaIP;
            if (rvaIP=="" || !validateIPaddress(rvaIP)){
              $('#rvaIPInput').focus();
              return false;
            }
            $(this).dialog("close");
            //console.debug('Ask RVA getRVASettings...', rvaIP, instance);
            getRVASettings(function (options) {
              instance.refresh(rvaIP, rvaClient, options);
            });
          }
        },
      ]
    });

  },
  rebootReminder: function rebootReminder(){
    closeWaiting();
    $("#dialog-reboot").dialog({
      autoOpen: true,
      closeOnEscape: false,
      resizable: false,
      modal: true,
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: $.t('button.close'),
          click: function() {
            window.close();
          }
        }
      ]
    });
  },
  needToValidatePw: function needToValidatePw(action){
    var instance = this;

    if (instance.options.rvaOptions.enablePassword) {
      $("#dialog-ask-pw").dialog({
        autoOpen: true,
        resizable: false,
        modal: true,
        close: function(event, ui) {
          $(this).dialog('destroy');
        },
        buttons: [
          {
            text: i18n.t('button.no'),
            click: function() {
              $('#rvaPw').val('').removeClass('error');
              $(".errMsg",this).empty();
              $(this).dialog("close");
            }
          },
          {
            text: i18n.t('button.yes'),
            click: function() {
              console.log('Apply password validation...');
              if ($('#rvaPw').val() == instance.options.rvaOptions.rvaPassword){
                $('#rvaPw').val('').removeClass('error');
                $(".errMsg",this).empty();
                action();
                $(this).dialog("close");
                waiting();
              }
              else {
                $(this).dialog("close");
                $('#rvaPw').addClass('error');
                $(".errMsg",this).text(i18n.t('device.invalidPassword'));
                instance.needToValidatePw(action);
              }
            }
          },
        ]
      });
    }
    else {
      action();
      waiting();
    }
  },
  dialogHandler: function dialogHandler(msg){
    $("#dialog-information > p").text(msg);
    $("#dialog-information").dialog({
      autoOpen: true,
      //closeOnEscape: false,
      resizable: false,
      modal: true,
      close: function(event, ui) {
        $(this).dialog('destroy');
      },
      buttons: [
        {
          text: "OK",
          click: function() {
            $(this).dialog("close");
          }
        }
      ]
    });
  },
  _destroy: function() {
    //this.remove();
    this.options.rvaIP = null;
    this._disableSidebar();
    this._disableTabs();
  },
});
